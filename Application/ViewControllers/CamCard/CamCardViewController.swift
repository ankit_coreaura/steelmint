//
//  CamCardViewController.swift
//  Application
//
//  Created by Ankit Nandal on 11/03/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//

import UIKit
import MobileCoreServices
import ISBusinessCardSDK
import ContactsUI
import AddressBook


class CamCardViewController: UIViewController ,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    var indicator:UIActivityIndicatorView?
    var userDict:String?

    override func viewDidLoad() {
        super.viewDidLoad()
       
        NSNotificationCenter.defaultCenter().addObserverForName("kCamCardNotification", object: nil, queue:  nil) { (notification) -> Void in
           print("notification object is \(notification.object)")
            let dict = notification.object as? NSDictionary

            
            self.stopActivityIndicator()
            
            let alert = UIAlertController(title: "", message: "Save Details of Card", preferredStyle: .ActionSheet)
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
            let firstAction = UIAlertAction(title: "Save Contact", style: .Default) { (alert: UIAlertAction!) -> Void in
                
                if #available(iOS 9.0, *) {
                    let store = CNContactStore()
                    switch CNContactStore.authorizationStatusForEntityType(.Contacts){
                    case .Authorized:
                    self.createContact(dict,store: store)
                    case .NotDetermined:
                        store.requestAccessForEntityType(.Contacts){succeeded, err in
                            guard err == nil && succeeded else{
                                return
                            }
                        }
                     self.createContact(dict,store: store)
                    default:
                        print("Not handled")
                    }
                } else {
                    let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
                    ABAddressBookRequestAccessWithCompletion(addressBookRef) {
                        (granted: Bool, error: CFError!) in
                        dispatch_async(dispatch_get_main_queue()) {
                            if granted == false {
                                print("Just denied")
                            } else {
                                print("Just authorized")
                                let authorizationStatus = ABAddressBookGetAuthorizationStatus()
                                
                                switch authorizationStatus {
                                case .Denied, .Restricted:
                                    //1
                                    print("Denied")
                                case .Authorized:
                                    //2
                                    print("Authorized")
                                    
                                    self.createContact8(dict,addressBookRef: addressBookRef)
                                    
                                    
                                case .NotDetermined:
                                    self.createContact8(dict,addressBookRef: addressBookRef)
                                    //3
                                    print("Not Determined")
                                }
                            }
                        }
                    }
                  
                    // Fallback on earlier versions
                }
                
                
            }
            
                alert.addAction(cancelAction)
                alert.addAction(firstAction)
                self.presentViewController(alert, animated: true, completion: nil)
            
        }
    }
    @available(iOS 9.0, *)
    func createContact(dict:NSDictionary?,store:CNContactStore){
        let contactData = CNMutableContact()

        guard let dictThere = dict else{return}
        let tempPH = (dictThere["ph"] as? NSArray)?.objectAtIndex(0)
        if let temp = tempPH{
    
    let phoneString = "\(temp)"
    
    let phoneNo = phoneString.componentsSeparatedByString("info:")[1]
            let homePhone = CNLabeledValue(label: CNLabelHome,value: CNPhoneNumber(stringValue: phoneNo ?? "NA"))
            contactData.phoneNumbers = [homePhone]
        }
        
        let tempStr = (dictThere["em"] as? NSArray)?.objectAtIndex(0)
        if let temp = tempStr{
            let emailIdString = "\(temp)"
            let emailID = emailIdString.componentsSeparatedByString("info:")[1]
            let workEmail = CNLabeledValue(label: CNLabelWork,value: emailID ?? "NA")
            
            contactData.emailAddresses = [workEmail]
   
        }
        
        contactData.givenName = dict?["fn"] as? String ?? ""
        contactData.familyName = dict?["ln"] as? String ?? ""
        

        // Save details:
        
        let request = CNSaveRequest()
        request.addContact(contactData, toContainerWithIdentifier: nil)
        
        do{
            try store.executeSaveRequest(request)
            
            showAlertController("SteelMint", message: "Contact Details sucessfully added", reference: self)
            
            print("Successfully added the contact")
        } catch let err{
            showAlertController("!!", message: "Contact Details not added sucessfully ", reference: self)
            print("Failed to save the contact. \(err)")
        }
        
    }
    
    func createContact8(dict:NSDictionary?,addressBookRef:ABAddressBook){
        
        guard let dictThere = dict else{return}
        let petRecord: ABRecordRef = ABPersonCreate().takeRetainedValue()

        let tempPH = (dictThere["ph"] as? NSArray)?.objectAtIndex(0)
        if let temp = tempPH{
            
            let phoneString = "\(temp)"
            
            let phoneNo = phoneString.componentsSeparatedByString("info:")[1]
            let phoneNumbers: ABMutableMultiValue = ABMultiValueCreateMutable(ABPropertyType(kABMultiStringPropertyType)).takeRetainedValue()
            ABMultiValueAddValueAndLabel(phoneNumbers, phoneNo, kABPersonPhoneMainLabel, nil)
            ABRecordSetValue(petRecord, kABPersonPhoneProperty, phoneNumbers, nil)
        }
        
        let tempStr = (dictThere["em"] as? NSArray)?.objectAtIndex(0)
        if let temp = tempStr{
            let emailIdString = "\(temp)"
            let emailID = emailIdString.componentsSeparatedByString("info:")[1]

        }
        ABRecordSetValue(petRecord, kABPersonFirstNameProperty, dictThere["fn"] as? String ?? "", nil)

        ABRecordSetValue(petRecord, kABPersonLastNameProperty, dictThere["ln"] as? String ?? "", nil)
        
        ABAddressBookAddRecord(addressBookRef, petRecord, nil)
        saveAddressBookChanges(addressBookRef)
        
        
    }

    
    func saveAddressBookChanges(addressBookRef:ABAddressBook) {
        if ABAddressBookHasUnsavedChanges(addressBookRef){
            var err: Unmanaged<CFErrorRef>? = nil
            let savedToAddressBook = ABAddressBookSave(addressBookRef, &err)
            if savedToAddressBook {
                print("Successully saved changes.")
            } else {
                print("Couldn't save changes.")
            }
        } else {
            print("No changes occurred.")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func pop(){
    self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func choosePhoto(){
      let ip = UIImagePickerController()
        ip.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        ip.mediaTypes = [kUTTypeImage as String]
        ip.allowsEditing = false
        ip.delegate = self
        presentViewController(ip, animated: true, completion: nil)
    
    }
    @IBAction func takePhoto(){
        let ip = UIImagePickerController()
        ip.sourceType = UIImagePickerControllerSourceType.Camera
        ip.mediaTypes = [kUTTypeImage as String]
        ip.allowsEditing = false
        ip.delegate = self
        presentViewController(ip, animated: true, completion: nil)

    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
//    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
//                   reconizeCardWithCardImage(image)
//        
//        dismissViewControllerAnimated(true, completion: nil)
//    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            showNativeActivityIndicator()
            let scale = 800 / pickedImage.size.width
            let newHeight = pickedImage.size.height * scale
            UIGraphicsBeginImageContext(CGSizeMake(800, newHeight))
            pickedImage.drawInRect(CGRectMake(0, 0, 800, newHeight))
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            //dispatch_after(1, dispatch_get_main_queue(), { () -> Void in
                MySupportClass.reconizeCardWithCardImage(newImage,self)
                //self.dismissViewControllerAnimated(true, completion: nil)

            //})
  
                    }
                    dismissViewControllerAnimated(true, completion: nil)
    }

    
    
    
    func showNativeActivityIndicator(){
        if indicator == nil{
            indicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
            indicator?.frame = CGRectMake(GlobalConstants.SCREEN_WIDTH/2, GlobalConstants.SCREEN_HEIGHT/2, 10, 10)
            self.view.addSubview(indicator!)
        }
        indicator?.startAnimating()
    }
    
    func stopActivityIndicator() {
        indicator?.stopAnimating()
    }

   
}
