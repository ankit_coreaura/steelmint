//
//  ISPersonCard.h
//  ISBusinessCardSDK
//
//  Created by Nadia Ye on 15/5/25.
//  Copyright (c) 2015年 IntSig. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  contact info : mobile, cell, fax, email and url info will be this type item
 */
@interface ISContactItem : NSObject

@property (nonatomic, strong) NSString *itemInfo;
@property (nonatomic, strong) NSString *itemLabel;

- (instancetype) initWithItemLabel:(NSString *)itemLabel itemInfo:(NSString *)itemInfo;

@end

/**
 *  Contact IM info
 *  ItemLabel can be AIM, QQ, GTALK, YAHOO, MSN, ICQ, JABBER, SKYPE and so on.
 */

@interface ISContactIM : NSObject

@property (nonatomic, copy) NSString *itemInfo;
@property (nonatomic, copy) NSString *itemLabel;

- (instancetype) initWithItemLabel:(NSString *)itemLabel itemInfo:(NSString *)itemInfo;

@end

/**
 *  Contact SNS info
 *  ItemLabel can be FACEBOOK, GOOGLE+, TWITTER, LINKEDIN, WEIBO, WECHAT, BADOO and so on.
 */

@interface ISContactSNS : NSObject

@property (nonatomic, copy) NSString *snsType;
@property (nonatomic, copy) NSString *snsAccount;
@property (nonatomic, copy) NSString *snsURL;

- (instancetype) initWithType:(NSString *)snsType account:(NSString *)snsAccount;

@end

@interface ISContactCompanyInfo : NSObject

@property (nonatomic, copy) NSString *company;
@property (nonatomic, copy) NSString *jobtitle;
@property (nonatomic, copy) NSString *department;

- (instancetype) initWithCompany:(NSString *)company jobtitle:(NSString *)jobtitle department:(NSString *)department;

@end

@interface ISContactAddress : NSObject

@property (nonatomic, copy) NSString *label;
@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *province;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *street; // if the contactInfo is from recognization, then address info will all be in street
@property (nonatomic, copy) NSString *street2;
@property (nonatomic, copy) NSString *postcode;

- (instancetype) initWithLabel:(NSString *)label country:(NSString *)country province:(NSString *)province city:(NSString *)city street:(NSString *)street street2:(NSString *)street2 postcode:(NSString *)postcode;

@end

@interface ISContactInfo : NSObject

@property (nonatomic, copy) NSString *firstName;
@property (nonatomic, copy) NSString *middleName;
@property (nonatomic, copy) NSString *lastName;
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, strong) NSArray *phones;
@property (nonatomic, strong) NSArray *emails;
@property (nonatomic, strong) NSArray *urls;
@property (nonatomic, strong) NSArray *companys;
@property (nonatomic, strong) NSArray *addresses;
@property (nonatomic, strong) NSArray *sns;
@property (nonatomic, strong) NSArray *im;
@property (nonatomic, assign) NSInteger rotateAngle;
@property (nonatomic, copy) NSString *companyRegisterCode;  // for zh_hant

@property (nonatomic, strong) NSData *avatar;
@property (nonatomic, strong) NSData *imageFeature;


- (void) addAddressItem:(ISContactAddress *)address;
- (void) addCompanyItem:(ISContactCompanyInfo *)companyInfo;
- (void) addIMItem:(ISContactIM *)companyInfo;
- (void) addSNSItem:(ISContactSNS *)snsInfo;
- (void) addphoneItem:(ISContactItem *)itemInfo;
- (void) addEamilItem:(ISContactItem *)itemInfo;
- (void) addUrlItem:(ISContactItem *)itemInfo;



@end
