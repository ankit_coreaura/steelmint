//
//  ISRecogenizeCard.h
//  ISBusinessCardSDK
//
//  Created by Nadia Ye on 15/5/25.
//  Copyright (c) 2015年 IntSig. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ISDefine.h"
#import "ISSDKAuthorizeStatus.h"

@interface ISRecogenizeCard : NSObject

/*
 You should call this method to construct resources before call other APIs in the SDK,calling this API will cause an online authorization for the first time this SDK is used.Once authorized successfully, this API will be returned immediately within expire time and try to update authorization status in background
 */
- (ISSDKAuthorizeStatus)constructResourcesWithAppKey:(NSString *)appKey subAppkey:(NSString *)subAppKey;

/**
 *  Recognize card with recognize result content , if recognize failed, then return error
 *
 *  @param cardImage           the image that need to be recognized
 *  @param recognizeLanguages  recognized languages that need to be preset, e.g. @[LANGUAGE_EN, LANGUAGE_CHS, LANGUAGE_DE]
 *  @param needProcess         if need to process image, please set to YES
 *  @param recognizeHandle     return recognized result content and error if recognize failed
 *  @param processResultHandle return processed image and error if process failed
 */

- (void) recognizeCard:(UIImage *)cardImage
     recognizeLanguage:(NSArray *)recognizeLanguages
      needImageProcess:(BOOL)needProcess
recognizeResultHandler:(ISRecognizeResultHanler)recognizeResultHandler
imageProcessResultHandler:(ISProcessCardResultHandler)processResultHandler;

@end
