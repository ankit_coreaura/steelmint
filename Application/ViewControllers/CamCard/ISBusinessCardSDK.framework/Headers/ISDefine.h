//
//  ISDefine.h
//  ISRecognizeCardSDK
//
//  Created by Nadia Ye on 15/6/4.
//  Copyright (c) 2015年 Nadia Ye. All rights reserved.
//

#ifndef ISRecognizeCardSDK_ISDefine_h
#define ISRecognizeCardSDK_ISDefine_h


@class ISContactInfo;
typedef void (^ISRecognizeResultHanler) (ISContactInfo * recognizeResultInfo, NSError *error); //vcf feature is for card match
typedef void (^ISProcessCardResultHandler) (UIImage * cropResultImage, NSError *error);


// language for recognize
#define LANGUAGE_EN        @"en"
#define LANGUAGE_CHS       @"zh-Hans"
#define LANGUAGE_CHT       @"zh-Hant"
#define LANGUAGE_JA        @"ja"
#define LANGUAGE_KO        @"ko"
#define LANGUAGE_FR        @"fr"
#define LANGUAGE_ES        @"es"
#define LANGUAGE_PT        @"pt"
#define LANGUAGE_DE        @"de"
#define LANGUAGE_IT        @"it"
#define LANGUAGE_NL        @"nl"
#define LANGUAGE_RU        @"ru"
#define LANGUAGE_GR        @"gr"
#define LANGUAGE_TR        @"tr"
#define LANGUAGE_SV        @"sv"
#define LANGUAGE_FI        @"fi"
#define LANGUAGE_DA        @"da"
#define LANGUAGE_NB        @"nb"
#define LANGUAGE_HU        @"hu"
#define LANGUAGE_RU        @"ru"


// error code

typedef NS_ENUM(NSInteger, ISBusinessCardErrorCode)
{
    ISBusinessCardInvalid = 1001,   // bundle id is not compared or time has expired
    ISBusinessCardInitialError = 1002,
    ISBusinessCardEmptyCardImage = 1003,
    ISBusinessCardRecognizeCardFailed = 1004,
    ISBusinessCardProcessCardFailed = 1005,
};

#endif
