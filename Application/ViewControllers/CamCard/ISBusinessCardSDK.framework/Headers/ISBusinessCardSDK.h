//
//  ISBusinessCardSDK.h
//  ISBusinessCardSDK
//
//  Created by Felix on 15/5/20.
//  Copyright (c) 2015年 IntSig. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <ISBusinessCardSDK/ISRecogenizeCard.h>
#import <ISBusinessCardSDK/ISDefine.h>
#import <ISBusinessCardSDK/ISContactInfo.h>
#import <ISBusinessCardSDK/ISSDKAuthorizeStatus.h>


