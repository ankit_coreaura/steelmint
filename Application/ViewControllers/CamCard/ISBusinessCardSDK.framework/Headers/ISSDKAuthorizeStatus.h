//
//  ISSDKAuthorizeStatus.h
//  ISSDKAuthorizeFramework
//
//  Created by Felix on 15/5/15.
//  Copyright (c) 2015年 IntSig. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, ISSDKAuthorizeStatus)
{
    ISSDKAuthorizeStatusUnauthorized = -1,
    ISSDKAuthorizeStatusSuccess,
    ISSDKAuthorizeStatusUnreachable,
    ISSDKAuthorizeStatusDeviceIDError,
    ISSDKAuthorizeStatusAppIDError,
    ISSDKAuthorizeStatusAppKeyError,
    ISSDKAuthorizeStatusAuthExpiredError,
    ISSDKAuthorizeStatusDeviceCappedError,
    ISSDKAuthorizeStatusDetectCappedError,
    ISSDKAuthorizeStatusSubAppKeyError,
    ISSDKAuthorizeStatusUnsupportedAuthError,
    ISSDKAuthorizeStatusAuthorizeInfoError,
};
