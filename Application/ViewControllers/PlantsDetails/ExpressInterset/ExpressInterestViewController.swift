//
//  ExpressInterestViewController.swift
//  Application
//
//  Created by Ankit Nandal on 13/11/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit

class ExpressInterestViewController: UIViewController {
    let webservicesInstance = WebServices()
    @IBOutlet weak var commentsTextView: UITextView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var companyNameTextField: UITextField!
    @IBOutlet weak var mobleTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    let filterArrayOfCountries = NSMutableArray()

    @IBOutlet weak var addressTableview:UITableView!
    @IBOutlet weak var addressTextField: UITextField!
    
    var leadId = ""
    var dictOfLocation = ["country":"","state":"","city":""]
    var completecountryList = NSDictionary()

    override func viewDidLoad() {
        super.viewDidLoad()
       initialSetup()

    }
   
    func initialSetup(){
        
        let s = try! String(contentsOfFile: NSBundle.mainBundle()
            .pathForResource("countryList", ofType: "txt")!,
            encoding: NSUTF8StringEncoding)
        let x = parsedJsonFrom(s.dataUsingEncoding(NSUTF8StringEncoding))
        if x != nil{
                if let dict = x?.valueForKey("data#") as? NSDictionary{
            if self.completecountryList.count == 0{
            self.completecountryList = dict
            }
                }
        }
        
        addressTextField.layer.borderColor = UIColor.lightGrayColor().CGColor
        commentsTextView.layer.borderColor = UIColor.lightGrayColor().CGColor
        commentsTextView.layer.borderWidth = 1.0
        addressTextField.layer.borderWidth = 1.0

        nameTextField.layer.borderColor = UIColor.lightGrayColor().CGColor
        companyNameTextField.layer.borderColor = UIColor.lightGrayColor().CGColor
        mobleTextField.layer.borderColor = UIColor.lightGrayColor().CGColor
        emailTextField.layer.borderColor = UIColor.lightGrayColor().CGColor
        nameTextField.layer.borderWidth = 1.0
        companyNameTextField.layer.borderWidth = 1.0
        mobleTextField.layer.borderWidth = 1.0
        emailTextField.layer.borderWidth = 1.0
        
        addressTableview.hidden = true
        addressTableview.layer.borderColor = UIColor.lightGrayColor().CGColor
        addressTableview.layer.borderWidth = 1.0
        addressTableview.backgroundColor = UIColor(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        
        let userInfoDict = CacheManager.sharedInstance.loadObject("userinfo") as?NSMutableDictionary
            if userInfoDict?.count>0{
                nameTextField.text = userInfoDict?.valueForKey("data#")?.valueForKey("name") as?String
                emailTextField.text = userInfoDict?.valueForKey("data#")?.valueForKey("username") as?String
                mobleTextField.text = userInfoDict?.valueForKey("data#")?.valueForKey("phoneno") as?String
                companyNameTextField.text = userInfoDict?.valueForKey("data#")?.valueForKey("company") as?String
            }
            
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func backBtnEvent(sender: UIButton) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func submitBtnEvent(sender: UIButton) {
        var isEveryThingValidated = true
        if nameTextField.text!.isEmpty || emailTextField.text!.isEmpty || mobleTextField.text!.isEmpty || commentsTextView.text!.isEmpty || dictOfLocation["country"] == ""
        {
            showAlertController("SteelMint", message: "Please Complete all the details", reference: self)
        }else{
            let dict = NSMutableDictionary()
            
            if emailTextField.text!.isEmail{
                dict.setObject(emailTextField.text!, forKey: "email")
                
            }else{
                showAlertController("SteelMint", message: "Please provide valid email id", reference: self)
                isEveryThingValidated = false
                return
            }
            
            if mobleTextField.text!.isPhoneNumber{
                dict.setObject(mobleTextField.text!, forKey: "mobile")
                
            }else{
                showAlertController("SteelMint", message: "Please provide valid Phone Number", reference: self)
                isEveryThingValidated = false
                return
            }
            
            if isEveryThingValidated{
                dict.setObject(leadId, forKey: "lead_id")
                dict.setObject(commentsTextView.text!, forKey: "comments")
                dict.setObject(nameTextField.text!, forKey: "name")
                dict.setObject(dictOfLocation["country"]!, forKey: "country_id")
                dict.setObject(dictOfLocation["state"]!, forKey: "state_id")
                dict.setObject(dictOfLocation["city"]!, forKey: "city_id")
                
                webservicesInstance.progresssIndicatorText = "Saving Interset"
                webservicesInstance.expressInterset(dict) { (responseData) -> () in
                    if responseData != nil{
                        if let success = responseData?["status#"] as? String{
                            if success == "1"{
                                showAlertDifferentType("Your interest regarding this offer has been successfully submitted. We will contact you soon.")

                                self.navigationController?.popViewControllerAnimated(true)
                            }else{
                             showAlertDifferentType("Your interest regarding this offer has not successfully submitted. Please try again.")
                            }
                        
                        }else{
                           showAlertDifferentType("Your interest regarding this offer has not successfully submitted. Please try again.") 
                        }
                    }else{
                        showAlertDifferentType("Your interest regarding this offer has not successfully submitted. Please try again.")
                    }
                }
            }
        }
        
    }
    
    //MARK: Country State Management
    
    @IBAction func textFieldValueChanged(sender: UITextField) {
        print("outside")
        filterArrayOfCountries.removeAllObjects()
        
        if sender.text?.characters.count > 2{
            if addressTableview.hidden == true{
                let yAxis = ((sender.superview?.superview as! UIScrollView).contentOffset.y + 100)
                addressTableview.hidden = false
                (sender.superview?.superview as! UIScrollView).setContentOffset(CGPointMake(0, yAxis), animated: true)
                
            }
            dictOfLocation["city"] = ""
            dictOfLocation["state"] = ""
            dictOfLocation["country"] = ""
            
            
            print("inside")
            for values in completecountryList.allValues{
                let dictOfLocation = NSMutableDictionary()
                
                //stringCheck = false
                //country
                if (values["name"] as! String).lowercaseString.containsString(sender.text!.lowercaseString){
                    // stringCheck = true
                    dictOfLocation.setObject(values["name"] as! String, forKey: "cntName")
                    dictOfLocation.setObject(values["id"] as! String, forKey: "cntId")
                }
                //state
                if values["state"] != nil {
                    for valuesState in (values["state"] as! NSDictionary).allValues{
                        if (valuesState["name"] as! String).lowercaseString.containsString(sender.text!.lowercaseString){
                            // stateCheck = true
                            dictOfLocation.setObject(values["name"] as! String, forKey: "cntName")
                            dictOfLocation.setObject(values["id"] as! String, forKey: "cntId")
                            
                            dictOfLocation.setObject(valuesState["name"] as! String, forKey: "stateName")
                            dictOfLocation.setObject(valuesState["id"] as! String, forKey: "stateId")
                        }
                        
                        
                        
                        //city
                        if valuesState["city"] != nil  {
                            for valuesCity in valuesState["city"] as! NSArray{
                                if (valuesCity["name"] as! String).lowercaseString.containsString(sender.text!.lowercaseString){
                                    //  cityCheck = true
                                    
                                    
                                    dictOfLocation.setObject(values["name"] as! String, forKey: "cntName")
                                    dictOfLocation.setObject(values["id"] as! String, forKey: "cntId")
                                    
                                    dictOfLocation.setObject(valuesState["name"] as! String, forKey: "stateName")
                                    dictOfLocation.setObject(valuesState["id"] as! String, forKey: "stateId")
                                    
                                    dictOfLocation.setObject(valuesCity["name"] as! String, forKey: "cityName")
                                    dictOfLocation.setObject(valuesCity["id"] as! String, forKey: "cityId")
                                }
                            }
                            
                        }
                    }
                    
                    
                    
                    
                    
                }
                
                
                
                // print(filterArrayOfCountries)
                if dictOfLocation.count > 0{
                    filterArrayOfCountries.addObject(dictOfLocation)
                    
                }
            }
            
            //
        }else{
            dictOfLocation["city"] = ""
            dictOfLocation["state"] = ""
            dictOfLocation["country"] = ""
            addressTableview.hidden = true
        }
        addressTableview.reloadData()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("addressCell", forIndexPath: indexPath)
        let city = cell.viewWithTag(76) as! UILabel
        let state = cell.viewWithTag(77) as! UILabel
        let country = cell.viewWithTag(78) as! UILabel
        
        city.text = nil
        state.text = nil
        country.text = nil
        
        if let dict = filterArrayOfCountries[indexPath.row] as? NSDictionary{
            if addressTextField.text?.characters.count > 2{
                //city
                if dict["cityName"]  == nil{
                    
                    if dict["stateName"]  == nil{
                        city.text = (dict["cntName"] as? String)?.capitalizedString
                    }else{
                        city.text = (dict["stateName"] as? String)?.capitalizedString
                    }
                    
                }else{
                    city.text = (dict["cityName"] as? String)?.capitalizedString
                }
                
                
                //state
                if city.text == (dict["stateName"] as? String)?.capitalizedString{
                    state.text = (dict["cntName"] as? String)?.capitalizedString
                }
                else if city.text == (dict["cntName"] as? String)?.capitalizedString{
                    
                }
                else{
                    if dict["stateName"] == nil {
                        state.text = (dict["cntName"] as? String)?.capitalizedString
                        
                        
                    }else{
                        state.text = (dict["stateName"] as? String)?.capitalizedString
                        
                    }
                }
                
                
                //country
                if state.text == dict["cntName"] as? String || city.text == dict["cntName"] as? String{
                    
                }else{
                    country.text = (dict["cntName"] as? String)?.capitalizedString
                }
            }
        }
        
        
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print(filterArrayOfCountries[indexPath.row])
        let dict = filterArrayOfCountries[indexPath.row]
        addressTableview.hidden = true
        var address = ""
        if dict["cityName"] as? String != nil{
            dictOfLocation["city"] = (dict["cityId"] as! String)
            address = dict["cityName"] as! String + ", "
        }
        if dict["stateName"]as? String != nil{
            dictOfLocation["state"] = (dict["stateId"] as! String)
            
            address += dict["stateName"] as! String + ", "
        }
        if dict["cntName"]as? String != nil{
            dictOfLocation["country"] = (dict["cntId"] as! String)
            address += dict["cntName"] as! String
        }
        
        addressTextField.text = address.capitalizedString
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filterArrayOfCountries.count > 10 {
            return 7
        }
        return filterArrayOfCountries.count
    }

  
    
}
