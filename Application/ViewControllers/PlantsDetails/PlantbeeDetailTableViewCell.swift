//
//  PlantbeeDetailTableViewCell.swift
//  Application
//
//  Created by Ankit Nandal on 14/01/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//

import UIKit
import MapKit
class PlantbeeDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var mapkitView: MKMapView!
    @IBOutlet weak var mapLocation: UILabel!
    @IBOutlet weak var imageVIew: UIImageView!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var capacityOfMAchine: UILabel!
    @IBOutlet weak var aboutCompany: UILabel!

    @IBOutlet weak var addressOfCompany: UILabel!
    @IBOutlet weak var businessType: UILabel!
    @IBOutlet weak var yearOFMachine: UILabel!
    @IBOutlet weak var makeOfMAchine: UILabel!
    @IBOutlet weak var numberOfMachines: UILabel!
    @IBOutlet weak var machineDetails: UILabel!
    @IBOutlet weak var offer: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var company: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var nameOFCompany: UILabel!
}
