//
//  SubscribeViewController.swift
//  SteelMint
//
//  Created by Ankit Nandal on 03/11/15.
//  Copyright © 2015 Saurabh Singh. All rights reserved.
//

import UIKit
import StoreKit
import SCLAlertView

class SubscribeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var indicator:UIActivityIndicatorView?
    var tempVIew:UIView?
    @IBOutlet weak var navigationLeftButton:UIButton!
    let webservicesInstance = WebServices()
    private var orderId:String?
    @IBOutlet weak var tableView: UITableView!
    var viaOption = ""
    var productsArray = [AnyObject]()
    override func viewDidLoad() {
        super.viewDidLoad()
        if viaOption == "PLANTBEE"{
            navigationLeftButton.setImage(UIImage(named: "back_button"), forState: .Normal)
        }
        setupForTableView()
        setupForInAppPurchase()
    }
    
    @IBAction func onClickOfLeftBarButton(sender:UIButton){
        if viaOption == "PLANTBEE"{
            navigationController?.popToRootViewControllerAnimated(true)
            return
        }
        openLeft()
    }
    
    func setupForTableView(){
        tableView.layer.borderColor = GlobalConstants.APP_THEME_BLUE_COLOR.CGColor
        tableView.layer.borderWidth = 1
        tableView.estimatedRowHeight = 150
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func setupForInAppPurchase(){
        
    showNativeActivityIndicator()
//        //UnAvailable Products
//        NSNotificationCenter.defaultCenter().addObserverForName("kProductsUnavailable", object: nil, queue: NSOperationQueue()) { (notification) -> Void in
//            print("product unavail")
//             MKStoreKit.sharedKit().startProductRequest()
//            }
        
        //Available Products
       
        NSNotificationCenter.defaultCenter().addObserverForName(kMKStoreKitProductsAvailableNotification, object: nil, queue: NSOperationQueue()) { (notification) -> Void in
            print("product avail")
            self.productsArray = MKStoreKit.sharedKit().availableProducts
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.stopActivityIndicator()
                self.tableView.reloadData()
            })
        }
        //Purchased Subscribed
        NSNotificationCenter.defaultCenter().addObserverForName(kMKStoreKitProductPurchasedNotification, object: nil, queue: NSOperationQueue()) { (notification) -> Void in
           //
            
            guard let order = self.orderId else{return}
            print("API HIT API HIT")
            let information = NSDictionary(objects: [sessionId(),order,"true","success"], forKeys: ["token","order_id","payment_detail","status"])
            self.webservicesInstance.progresssIndicatorText = nil
            self.webservicesInstance.progresssIndicatorText = "Please Wait.."

            self.webservicesInstance.inAppAfterPayment(information, completionBlock: { (responseData) -> () in
                if let status = responseData?["message#"] as? String{
                    if let statusKey = responseData?["status#"] as? String{
                        if statusKey == "1310"{
                            showAlertController("Steelmint ", message: "Verification Failed. Please take screenshot of it and immediately contact customer support. \n Order Id : \(self.orderId!)", reference: self)
                        }else if statusKey == "1"{
                            DatabaseManager.sharedInstance.resetCoreDataEntities()
                            
                            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
                            dispatch_after(delayTime, dispatch_get_main_queue()) {
                                        self.slideMenuController()?.changeMainViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("TabController"), close: true)
                            }

                            
                            
                        }
                    }
                    showAlertController("Subscription", message: status, reference: self)
                }
            })
            

            
            
            //
           // MKStoreKit.sharedKit().refreshAppStoreReceipt()
           
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.tableView.reloadData()
            })
        }
        //Restored Purchases
        NSNotificationCenter.defaultCenter().addObserverForName(kMKStoreKitRestoredPurchasesNotification, object: nil, queue: NSOperationQueue()) { (notification) -> Void in
            print("Purchases Restored");
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.tableView.reloadData()
            })
        }
        //Failed Purchases
        NSNotificationCenter.defaultCenter().addObserverForName(kMKStoreKitProductPurchaseFailedNotification, object: nil, queue: NSOperationQueue()) { (notification) -> Void in
            guard let order = self.orderId else{return}
            
            let information = NSDictionary(objects: [sessionId(),order,"false","failure"], forKeys: ["token","order_id","payment_detail","status"])
            self.webservicesInstance.progresssIndicatorText = "Please wait...."
            self.webservicesInstance.inAppAfterPayment(information, completionBlock: { (responseData) -> () in
                if let status = responseData?["message#"] as? String{
                    showAlertController("Subscription", message: status, reference: self)
                }
            })
            print("Purchases Failed");
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.tableView.reloadData()
            })
        }
        
        
        // VERIFICATION:
        
        NSNotificationCenter.defaultCenter().addObserverForName("kReceipt", object: nil, queue: nil, usingBlock: {(notification) -> Void in
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "kReceipt", object: nil)
         //   print("Purchased/Subscribed to product with id: %@", notification.object);
            let receipt = notification.object
            guard let order = self.orderId else{return}
            print("API HIT API HIT")
            let information = NSDictionary(objects: [sessionId(),order,receipt ?? "false","success"], forKeys: ["token","order_id","payment_detail","status"])
            self.webservicesInstance.progresssIndicatorText = nil
            self.webservicesInstance.inAppAfterPayment(information, completionBlock: { (responseData) -> () in
                if let status = responseData?["message#"] as? String{
                    if let statusKey = responseData?["status#"] as? String{
                        if statusKey == "1310"{
                            showAlertController("Steelmint ", message: "Verification Failed. Please take screenshot of it and immediately contact customer support. \n Order Id : \(self.orderId!)", reference: self)
                        }
                    }
                    showAlertController("Subscription", message: status, reference: self)
                }
            })
            
            
            
        })

        
        MKStoreKit.sharedKit().startProductRequest()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell=tableView.dequeueReusableCellWithIdentifier("c1", forIndexPath: indexPath)
//        cell.layer.borderColor = GlobalConstants.APP_THEME_BLUE_COLOR.CGColor
//        cell.layer.borderWidth = 2.0
        let planTitle=(cell.viewWithTag(101) as! UILabel)
        let planPrice=(cell.viewWithTag(102) as! UILabel)
        let planDetail=(cell.viewWithTag(103) as! UILabel)
        planDetail.numberOfLines = 0
        
        let product = productsArray[indexPath.row] as! SKProduct
        planTitle.text = product.localizedTitle
        planDetail.text = product.localizedDescription
        
        let numberFormatter = NSNumberFormatter()
        numberFormatter.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        numberFormatter.formatterBehavior = NSNumberFormatterBehavior.Behavior10_4
        numberFormatter.locale = product.priceLocale
        planPrice.text = numberFormatter.stringFromNumber(product.price)
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productsArray.count
    }
    
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        let product = productsArray[indexPath.row] as! SKProduct
//        let size = getSizeBasedOnText(GlobalConstants.SCREEN_WIDTH-56, heightOfContainer: 10, string: product.localizedDescription, fontSize: 20)
//        if size.height < 59 {
//            return 182
//        }
//        else{
//            return size.height+100
//        }
//    }
    
     @IBAction func onClickOfBuyButton(sender:UIButton){
        
        if sessionId().characters.count < 2{
            
                let alertView = SCLAlertView()
                alertView.showTitle("SteelMint", subTitle: "You need to login first to perform this action", style: SCLAlertViewStyle.Info, closeButtonTitle: "Cancel", duration: 20, colorStyle: 0xE4E4E4, colorTextButton: 0xFFFFFF)
                let button =  alertView.addButton("Login") { () -> Void in
                    (UIApplication.sharedApplication().delegate as! AppDelegate).subscriptionFlag = true
                    self.slideMenuController()?.changeMainViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("LoginViewControllerNavigation"), close: true)
                }
                button.backgroundColor =  GlobalConstants.APP_THEME_DARK_BLUE_COLOR
            
        }
        
        
        else{
        
        let x = sender.convertPoint(CGPointZero, toView: tableView)
        let indexpath = tableView.indexPathForRowAtPoint(x)
        guard let indexPath = indexpath else{showAlertController("In App Purchase", message: "Currently unable to make purchase", reference: self);return}
        let product = productsArray[indexPath.row] as! SKProduct
            print(product)
        let numberFormatter = NSNumberFormatter()
        numberFormatter.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        numberFormatter.formatterBehavior = NSNumberFormatterBehavior.Behavior10_4
        numberFormatter.locale = product.priceLocale
        webservicesInstance.progresssIndicatorText = "Initiating your purchase.. Please Wait"
        let dict = NSDictionary(objects: [sessionId(),product.productIdentifier,product.price,numberFormatter.currencyCode,"true"], forKeys: ["token","plancode","price","currency","iphone"])
            print(dict)
        webservicesInstance.inAppBeforePayment(dict) { (responseData) -> () in
            print(responseData)
            guard let response = responseData else{return}
            self.orderId = response["data#"]??["order_id"] as? String
            
            if let _ = self.orderId{
               MKStoreKit.sharedKit().initiatePaymentRequestForProductWithIdentifier(product.productIdentifier)
            }else{
                showAlertController("", message: "Temporary Unable to Subscribe. Please contact customer care", reference: self)
            }
           
 
        }
          
        }
    
    }
    
    
    //MARK: ACTIVITY INDICATOR
    func showNativeActivityIndicator(){
        self.tableView.allowsSelection = false
        if indicator == nil{
            // Setup ActivityIdicator
            indicator = UIActivityIndicatorView  (activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            indicator?.frame = CGRectMake(GlobalConstants.SCREEN_WIDTH/2-10, GlobalConstants.SCREEN_HEIGHT/2-10, 10.0, 10.0)
           tempVIew = UIView(frame: tableView.frame)
            view.addSubview(tempVIew!)
            view.addSubview(indicator!)
        }
        indicator?.startAnimating()
    }
    
    func stopActivityIndicator() {
        self.tableView.allowsSelection = true
        
        indicator?.stopAnimating()
        tempVIew?.removeFromSuperview()
    }
    
    
}
