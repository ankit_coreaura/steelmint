//
//  NewsDetailViewController.swift
//  SteelMint
//
//  Created by Ankit Nandal on 02/11/15.
//  Copyright © 2015 Saurabh Singh. All rights reserved.
//

import UIKit

class NewsDetailViewController: UIViewController,UITableViewDataSource,UITableViewDelegate ,UIWebViewDelegate{
    //
    @IBOutlet weak var tableView:UITableView!
    var newsDetailData:NSMutableDictionary?
    var newsObject:News?
    var heightOfWebView = 100
    var delegateCalled:Bool?
    var wv = UIWebView()
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if newsObject?.isBookmarked == true{
            (view.viewWithTag(880) as? UIButton)?.setImage(UIImage(named: "tag_bookmarked.png") , forState: .Normal)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
         self.automaticallyAdjustsScrollViewInsets = false

        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }
    
    func initialSetup()
    {
        guard let newsDetailDict=newsDetailData else {return}
        let dict = NSMutableDictionary()
        if let _=newsDetailDict.valueForKey("news_id"){
            dict.setObject(newsDetailDict.valueForKey("news_id")!, forKey: "ID")
            
        }
        else{
            dict.setObject(newsDetailDict.valueForKey("ID")!, forKey: "ID")
        }
    newsObject = DatabaseManager.sharedInstance.getNews(dict)
        
		
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onClickOfLeftBarButton(sender:UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func onClickOfShareButton(sender:UIButton){
        var title : NSString?
        var description : NSString?
        var url : NSString?
        title = newsDetailData?.valueForKey("post_title") as? NSString
        description = newsDetailData?.valueForKey("post_content") as? NSString
        url = newsDetailData?.valueForKey("href") as? NSString
        AppCommonFunctions.sharedInstance.share(title, description: description, url: url, fromViewController: self)
    }
    
    @IBAction func onClickOfBookMarkButton(sender:UIButton){
        
        
        if (newsObject == nil){
            guard let newsDetailDict=newsDetailData else {return}
            DatabaseManager.sharedInstance.addNews(newsDetailDict)
            let dict = NSMutableDictionary()
            if let _=newsDetailDict.valueForKey("news_id"){
                dict.setObject(newsDetailDict.valueForKey("news_id")!, forKey: "ID")
                
            }
            else{
                dict.setObject(newsDetailDict.valueForKey("ID")!, forKey: "ID")
            }
            newsObject = DatabaseManager.sharedInstance.getNews(dict)
            newsObject?.viaSearchOrFilter = true
        }
        
        if newsObject?.isBookmarked != true{
            newsObject?.isBookmarked = true
            sender.setImage(UIImage(named: "tag_bookmarked.png") , forState: .Normal)
            bookmarkAnimatedView("Bookmarked")
        }
        else{
            newsObject?.isBookmarked = false
            sender.setImage(UIImage(named: "tagT") , forState: .Normal)
            
        }
        DatabaseManager.sharedInstance.saveContext()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell=UITableViewCell()
        switch indexPath.row
        {
        case 1:
            cell=tableView.dequeueReusableCellWithIdentifier("c1", forIndexPath: indexPath)
            let newsImageView = (cell.viewWithTag(818) as! UIImageView)
            if let newsData = newsDetailData{
               
                if isNotNull(newsData.valueForKey("medium_image") as? String){
                    if let url = NSURL(string:newsData.valueForKey("medium_image") as! String){
                        newsImageView.sd_setImageWithURL(url)
                    }else{
                        newsImageView.image = UIImage(named: "placeholderImage")
                        
                    }
                    
                }else{
                    newsImageView.image = UIImage(named: "placeholderImage")
                }
                
            }
        case 0:
            cell=tableView.dequeueReusableCellWithIdentifier("c2", forIndexPath: indexPath)
            let newsTitle = (cell.viewWithTag(2010) as! UILabel)
            let  newsPostedDate = (cell.viewWithTag(2011) as! UILabel)
            // setting Data
            if let newsData = newsDetailData{
                //newsData[]
                
                newsPostedDate.text = newsData.valueForKey("post_modified") as? String
                newsTitle.text = newsData.valueForKey("post_title") as? String
            }
        case 2:
            cell=tableView.dequeueReusableCellWithIdentifier("c3", forIndexPath: indexPath)
           // let   newsDetail = (cell.viewWithTag(2012) as! UILabel)
            let   publishedBy = (cell.viewWithTag(2013) as! UILabel)
            let   firstPublishedOn = (cell.viewWithTag(2014) as! UILabel)
            let   webView = (cell.viewWithTag(3) as! UIWebView)

            
            // setting Data
            if let newsData = newsDetailData{
                webView.loadHTMLString(newsData.valueForKey("post_content") as? String ?? "", baseURL: nil)
					print("News detail: post content \(newsData.valueForKey("post_content") as? String)")
                webView.backgroundColor = UIColor.whiteColor()
                webView.delegate = self
                
              
                webView.scrollView.scrollEnabled = true
                webView.scrollView.showsHorizontalScrollIndicator=true
                webView.scrollView.bounces = false
                webView.scrollView.delegate=self
                             //
               
                publishedBy.text = newsData.valueForKey("author_name") as? String
                firstPublishedOn.text = getDateFromTimestamp2(newsData.valueForKey("post_date") as? NSNumber, dateFormat: "MMM dd, yyyy HH:mm")
                if firstPublishedOn.text == "" {
                    firstPublishedOn.text = newsData.valueForKey("post_date") as? String
                }
                
            }
        default:
            break
            
        }
        return cell
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.row
        {
        case 0:
            
            return UITableViewAutomaticDimension
        case 1:
            return 200
            
        case 2:
            return CGFloat(heightOfWebView+55)
       // return UITableViewAutomaticDimension

        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        if indexPath.row == 1{
            let cell = tableView.cellForRowAtIndexPath(indexPath)
            let newsImageView = (cell!.viewWithTag(818) as! UIImageView)
            if let newsData = newsDetailData{
                if isNotNull(newsData.valueForKey("medium_image")!){
                    AppCommonFunctions.sharedInstance.showImages(self, images: [NSURL(string:newsData.valueForKey("medium_image") as! String)!], initialIndex: 0, zoomFromView: newsImageView)
                    
                }
            }
        }
    }
    func bookmarkAnimatedView(str:String?){
        let bookmarkLabel = UILabel()
                bookmarkLabel.frame = CGRect(x: (GlobalConstants.SCREEN_WIDTH/2)-50, y: (GlobalConstants.SCREEN_HEIGHT)-80, width: 110, height: 20)

        bookmarkLabel.text = str
        bookmarkLabel.textColor = UIColor.whiteColor()
        bookmarkLabel.backgroundColor = GlobalConstants.APP_THEME_BLUE_COLOR
        bookmarkLabel.layer.cornerRadius = 5.0
        bookmarkLabel.clipsToBounds = true
        bookmarkLabel.textAlignment = .Center
        self.view.addSubview(bookmarkLabel)
        bookmarkLabel.layer.opacity = 0
        UIView.animateWithDuration(0.5) { () -> Void in
            bookmarkLabel.layer.opacity = 1.0
        }
        
        dispatch_after(1, dispatch_get_main_queue()) { () -> Void in
            UIView.animateWithDuration(1.2) { () -> Void in
                bookmarkLabel.layer.opacity = 0
            }
        }
        
    }
    
    //MARK: WEB VIEW DELEGATES

    func webViewDidFinishLoad(webView: UIWebView) {
        webView.frame = CGRectMake(10, 10, GlobalConstants.SCREEN_WIDTH, webView.scrollView.contentSize.height)
        webView.scrollView.contentSize = CGSizeMake( webView.scrollView.contentSize.width, webView.scrollView.contentSize.height);
        if delegateCalled == nil {
//            webView.frame = CGRectMake(10, 10, GlobalConstants.SCREEN_WIDTH - 20, webView.scrollView.contentSize.height)
//            webView.scrollView.contentSize = CGSizeMake( webView.scrollView.contentSize.width - 100, webView.scrollView.contentSize.height);
            heightOfWebView = Int(webView.scrollView.contentSize.height)
            tableView.layoutIfNeeded()
            tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: 2, inSection: 0)], withRowAnimation: .Automatic)
            delegateCalled = true
        }
        
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        if(scrollView.superview!.isKindOfClass(UIWebView))
        {
          
                            if scrollView.contentOffset.y > 0  {
                               scrollView.scrollEnabled=false
                                scrollView.contentOffset.y=0
                            }
            else
                            {
                                scrollView.scrollEnabled=true
  
            }
          

        }
        else
        {
            
        }
        }
        
    
    
}
