//
//  CCPOViewController.m
//  CCIntegrationKit
//
//  Created by test on 5/12/14.
//  Copyright (c) 2014 Avenues. All rights reserved.
//

#import "CCWebViewController.h"
#import "CCResultViewController.h"
#import "CCTool.h"
#import "Steelmint-Swift.h"

@interface CCWebViewController ()

@end

@implementation CCWebViewController

@synthesize rsaKeyUrl;@synthesize accessCode;@synthesize merchantId;@synthesize orderId;
@synthesize amount;@synthesize currency;@synthesize redirectUrl;@synthesize cancelUrl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   AppDelegate *del =  (AppDelegate *)[[UIApplication sharedApplication] delegate];

    self.viewWeb.delegate = self;
    
    redirectUrl = [del redirectUrlCCAvenue];
    cancelUrl = [del redirectUrlCCAvenue];
    NSLog(@"cancel and redirect url ===>>>%@",[del redirectUrlCCAvenue]);
   // Getting RSA Key
    NSString *rsaKeyDataStr = [NSString stringWithFormat:@"access_code=%@&order_id=%@",accessCode,orderId];
    NSData *requestData = [NSData dataWithBytes: [rsaKeyDataStr UTF8String] length: [rsaKeyDataStr length]];
    NSMutableURLRequest *rsaRequest = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: rsaKeyUrl]];
    [rsaRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [rsaRequest setHTTPMethod: @"POST"];
    [rsaRequest setHTTPBody: requestData];
    NSData *rsaKeyData = [NSURLConnection sendSynchronousRequest: rsaRequest returningResponse: nil error: nil];
    NSString *rsaKey = [[NSString alloc] initWithData:rsaKeyData encoding:NSASCIIStringEncoding];
    //_RSAKEY;
    rsaKey = [rsaKey stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    rsaKey = [NSString stringWithFormat:@"-----BEGIN PUBLIC KEY-----\n%@\n-----END PUBLIC KEY-----\n",rsaKey];
    NSLog(@"key ======== %@",rsaKey);
    
    //Encrypting Card Details
    NSString *myRequestString = [NSString stringWithFormat:@"amount=%@&currency=%@",amount,currency];
    CCTool *ccTool = [[CCTool alloc] init];
    NSString *encVal = [ccTool encryptRSA:myRequestString key:rsaKey];
    encVal = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                        (CFStringRef)encVal,
                                                                        NULL,
                                                                        (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                        kCFStringEncodingUTF8 ));
    
    //Preparing for a webview call
    NSString *urlAsString = [NSString stringWithFormat:@"https://secure.ccavenue.com/transaction/initTrans"];
    NSString *encryptedStr = [NSString stringWithFormat:@"merchant_id=%@&order_id=%@&redirect_url=%@&cancel_url=%@&enc_val=%@&access_code=%@",merchantId,orderId,redirectUrl,cancelUrl,encVal,accessCode];
    NSLog(@"encrypted str = ----%@",encryptedStr);
    
    NSData *myRequestData = [NSData dataWithBytes: [encryptedStr UTF8String] length: [encryptedStr length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlAsString]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setValue:urlAsString forHTTPHeaderField:@"Referer"];
    [request setHTTPMethod: @"POST"];
    [request setHTTPBody: myRequestData];
    NSLog(@"request str = ----%@",request);
    NSLog(@"request str = ----%@",request.HTTPBody);


    [_viewWeb loadRequest:request];
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
    NSLog(@"firest");
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    NSLog(@"Second");

    NSString *string = webView.request.URL.absoluteString;
    NSLog(@"url string ======>   %@",string);
    //v3/action/events/payment
    ///ccavResponseHandler.jsp
    if ([string rangeOfString:@"/v3/action/events/payment"].location != NSNotFound) {
        
        NSString *html = [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.outerHTML"];
        NSLog(@"HTML == %@",html);
        
        NSScanner *myScanner;
        NSString *text = nil;
        myScanner = [NSScanner scannerWithString:html];
        
        while ([myScanner isAtEnd] == NO) {
            
            [myScanner scanUpToString:@"<" intoString:NULL] ;
            
            [myScanner scanUpToString:@">" intoString:&text] ;
            
            html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
        }
        //
        html = [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        NSLog(@"HTML == %@",html);
        
        NSData *data = [html dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"data == %@",json);

        
        
        if ([[json objectForKey:@"status"] isEqualToString:@"1"]) {
            [[[UIAlertView alloc]initWithTitle:@"SteelMint" message:@"Your transaction was successfull !!!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles :nil] show];
            
        }else{
            [[[UIAlertView alloc]initWithTitle:@"SteelMint" message:@"Your transaction was not successfull. Please try again or contact customer support" delegate:self cancelButtonTitle:@"OK" otherButtonTitles :nil] show];

        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
-(IBAction)cancelBtn:(id)sender{
    [self dismissViewControllerAnimated:true completion:nil];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
      [self dismissViewControllerAnimated:true completion:nil];
    }
}


@end
