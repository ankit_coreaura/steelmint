//
//  CCVaultOption.h
//  CCIntegrationKit
//
//  Created by test on 11/13/14.
//  Copyright (c) 2014 Avenues. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCVaultOption : NSObject
    @property (nonatomic, retain) NSString *payCardType;
    @property (nonatomic, retain) NSString *payCardName;
    @property (nonatomic, retain) NSString *payCardNo;
    @property (nonatomic, retain) NSString *payOption;
@end
