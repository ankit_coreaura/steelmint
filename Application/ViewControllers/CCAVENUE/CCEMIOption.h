//
//  CCEMIOption.h
//  CCIntegrationKit
//
//  Created by test on 11/19/14.
//  Copyright (c) 2014 Avenues. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCEMIOption : NSObject
    @property (nonatomic, retain) NSString *emiPlanId;
    @property (nonatomic, retain) NSString *emiTenureId;
@end
