//
//  CCPaymentOption.h
//  CCIntegrationKit
//
//  Created by test on 5/14/14.
//  Copyright (c) 2014 Avenues. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCPaymentOption : NSObject

@property (nonatomic, retain) NSString *payOptId;
@property (nonatomic, retain) NSString *name;

@end
