//
//  CCEMIPlan.h
//  CCIntegrationKit
//
//  Created by test on 11/21/14.
//  Copyright (c) 2014 Avenues. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCEMIPlan : UIViewController
    @property (nonatomic, retain) NSString *emiPlanId;
    @property (nonatomic, retain) NSString *emiBankName;
    @property (nonatomic, retain) NSString *allowedCards;
@end
