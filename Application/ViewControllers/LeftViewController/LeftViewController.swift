//
//  LeftViewController.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 12/3/14.
//

import UIKit
import SCLAlertView
import NSURL_Gravatar
import MessageUI
import Parse
import Bolts
import ZDCChat

enum LeftMenu: Int {
    case HOME=0
    case LOGIN=2
    case SUBSCRIPTION=3
    case NOTIFICATION=4
    case BOOKMARKS=5
    case RATEUS=13
    case ABOUTUS=8
    case FEEDBACK=7
    case CONTACTUS=15
    case CHATPARSE=14

    case TUTORIALS=10
    case FAQS=9
    case INVITE_FRIENDS = 11
    
}

protocol LeftMenuProtocol : class {
    func changeViewController(menu: LeftMenu)
}

class LeftViewController : UIViewController, LeftMenuProtocol,UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate {
    //CONNECTING OUTLETS
    
    @IBOutlet weak var editProfileBtnOutlet: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profileImageViewOutlet: UIImageView!
    @IBOutlet weak var profileNameLabelOutlet: UILabel!
    
    //@END
    
    var menus = ["HOME","My Account","LOGIN","SUBSCRIPTION", "NOTIFICATIONS","BOOKMARKS","About Us","FEEDBACK","ABOUT US","FAQ's","TUTORIALS","INVITE FRIENDS","CONTACT","RATE US","CHATS"]
    var menuImagesArray = ["homez","","login.png","Subscription.png","notifications", "tag","","feedback","aboutus","faqz","tutorialsz","invite_friendsz","contact","rateus","buyChat"]
    var contactControllerSelection:String?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        setupUserInfo()
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture=UITapGestureRecognizer()
        tapGesture.numberOfTapsRequired=1
        tapGesture.addTarget(self, action: Selector("profileEditBtnEvent:"))
        profileImageViewOutlet.userInteractionEnabled=true
        profileImageViewOutlet.addGestureRecognizer(tapGesture)
        profileImageViewOutlet.clipsToBounds=true
        profileImageViewOutlet.layer.cornerRadius=profileImageViewOutlet.frame.size.width/2
        profileImageViewOutlet.layer.masksToBounds=true
        profileImageViewOutlet.layer.borderColor=UIColor.whiteColor().CGColor
        profileImageViewOutlet.layer.borderWidth=2
        self.tableView.registerCellClass(BaseTableViewCell.self)
        
    }
    
    
    func setupUserInfo()
    {
        editProfileBtnOutlet.hidden=false
        
        if !AppCommonFunctions.sharedInstance.isUserLoggedIn(){
            editProfileBtnOutlet.hidden=true
        }
        let userInfoDict = CacheManager.sharedInstance.loadObject("userinfo") as?NSMutableDictionary
        if userInfoDict?.count>0{
            profileNameLabelOutlet.text = userInfoDict?.valueForKey("data#")?.valueForKey("name") as?String
        }
        else
        {
            profileNameLabelOutlet.text = "Guest"
            profileImageViewOutlet.sd_setImageWithURL(NSURL(gravatarEmail: userInfoDict?.valueForKey("data#")?.valueForKey("name") as?String, size: 512), placeholderImage: UIImage(named: "profile_menu"))
        }
        tableView .reloadData()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if AppCommonFunctions.sharedInstance.isUserLoggedIn(){
            return menus.count
        }else{
            return menus.count - 1
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        switch indexPath.row{
        case 0,2,3,4,5,7,8,9,10,11,12,13,14:
            let cell = tableView.dequeueReusableCellWithIdentifier("cell2", forIndexPath: indexPath) as! contactUsTableViewCell
            if indexPath.row==2 {
                if AppCommonFunctions.sharedInstance.isUserLoggedIn(){
                    cell.textLabelOutlet?.text = "LOGOUT"
                    cell.cellImageViewOutlet?.image=UIImage(named:"logout.png")
                }
                else{
                    cell.textLabelOutlet?.text = menus[indexPath.row]
                    cell.cellImageViewOutlet?.image=UIImage(named: menuImagesArray[indexPath.row])
                    
                }
                
            }
            else{
                cell.cellImageViewOutlet?.image=UIImage(named: menuImagesArray[indexPath.row])
                cell.textLabelOutlet?.text = menus[indexPath.row]
            }
            cell.textLabelOutlet?.font = UIFont(name: "Helvetica-Light", size: 13)
            cell.textLabelOutlet?.textColor = UIColor.blackColor()
            cell.cellImageViewOutlet?.clipsToBounds=true
            cell.cellImageViewOutlet?.contentMode = .ScaleAspectFit
            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier("contactUsCustomCell", forIndexPath: indexPath) as! contactUsTableViewCell
            cell.sectionLabel.text = menus[indexPath.row]
            cell.imageView?.clipsToBounds=true
            cell.imageView?.contentMode = .ScaleAspectFit
            
            return cell
        default:
            let cell = tableView.dequeueReusableCellWithIdentifier("contactUsCustomCell", forIndexPath: indexPath) as! contactUsTableViewCell
            cell.sectionLabel.text = menus[indexPath.row]
            cell.imageView?.clipsToBounds=true
            cell.imageView?.contentMode = .ScaleAspectFit
            
            return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let selectedCell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
        selectedCell.contentView.backgroundColor = UIColor(red: 34.0/255.0, green: 137.0/255.0, blue: 222.0/255.0, alpha: 1.0)
        selectedCell.textLabel?.textColor=UIColor.whiteColor()
        if indexPath.row==12{
            showActionSheet()
        }
        else{
            if indexPath.row==2{
                if AppCommonFunctions.sharedInstance.isUserLoggedIn(){
                    logout()
                }
                else{
                    if let menu = LeftMenu(rawValue: indexPath.item) {
                        self.changeViewController(menu)
                    }
                }
                
            }
            else{
                if let menu = LeftMenu(rawValue: indexPath.item) {
                    self.changeViewController(menu)
                }}
        }
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        
        if let cellToDeSelect:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath){
            cellToDeSelect.contentView.backgroundColor = UIColor.whiteColor()
            cellToDeSelect.textLabel?.textColor=UIColor(red: 15/255, green: 72/255, blue: 117/255, alpha: 1.0)
        }
        
        
        
        
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        switch indexPath.row{
        case 1,6:
            return 28
        default:
            return 40
        }
    }
    
    func changeViewController(menu: LeftMenu) {
        
        var viewControllerIdentifier : String?
        switch menu {
        case .SUBSCRIPTION:
            viewControllerIdentifier = "SubscribeViewController"
        case .TUTORIALS:
            viewControllerIdentifier = "TutorialsViewController"
        case .NOTIFICATION:
            viewControllerIdentifier = "SettingsViewController"
        case .RATEUS:
            RateMyApp.sharedInstance.showRatingAlert()
        case .LOGIN:
            viewControllerIdentifier = "LoginViewControllerNavigation"
        case .BOOKMARKS:
            viewControllerIdentifier = "BookmarkViewController"
        case .HOME:
            viewControllerIdentifier = "TabController"
        case .FEEDBACK:
            viewControllerIdentifier = "FeedbackViewController"
        case .ABOUTUS:
            viewControllerIdentifier = "AboutUsViewController"
        case .CONTACTUS:
            viewControllerIdentifier = "ContactUsViewController"
        case .FAQS:
            viewControllerIdentifier = "FAQsViewController"
        case .INVITE_FRIENDS:
            
            inviteFriends()
        case .CHATPARSE:
            viewControllerIdentifier = "ChatListingsViewController"
  
            
        }
        if let id = viewControllerIdentifier{
            if id == "TutorialsViewController" {
                let tutorialVC  = UIStoryboard(name: "Secondary", bundle:nil).instantiateViewControllerWithIdentifier(id) as! TutorialsViewController
                tutorialVC.viaOption = "leftMenu"
                self.slideMenuController()?.changeMainViewController(tutorialVC, close: true)
            }
            else if id == "FAQsViewController"{
                let faqVC  = UIStoryboard(name: "Secondary", bundle:nil).instantiateViewControllerWithIdentifier(id) as! FAQsViewController
                self.slideMenuController()?.changeMainViewController(faqVC, close: true)
            }
                else if id == "ChatListingsViewController"{
                self.slideMenuController()?.changeMainViewController(UIStoryboard(name: "Secondary", bundle: nil).instantiateViewControllerWithIdentifier(id), close: true)
                
            }
            else{
//                id == "SubscribeViewController" ||
                if id == "SettingsViewController"{
                    
                    
                   // if AppCommonFunctions.sharedInstance.isUserLoggedIn(){
                        self.slideMenuController()?.changeMainViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier(id), close: true)
//                    }else{
//                        let alertView = SCLAlertView()
//                        alertView.showTitle("SteelMint", subTitle: "You need to login first to perform this action", style: SCLAlertViewStyle.Info, closeButtonTitle: "Cancel", duration: 20, colorStyle: 0xE4E4E4, colorTextButton: 0xFFFFFF)
//                        let button =  alertView.addButton("Login") { () -> Void in
//                            self.slideMenuController()?.changeMainViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("LoginViewControllerNavigation"), close: true)
//                        }
//                        button.backgroundColor =  GlobalConstants.APP_THEME_DARK_BLUE_COLOR
//                    }
          
                }
                else{
                    self.slideMenuController()?.changeMainViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier(id), close: true)
                }
            }
        }
        
    }
    
    @IBAction func profileEditBtnEvent(sender: UIButton) {
        if AppCommonFunctions.sharedInstance.isUserLoggedIn(){
            let navigationController = UINavigationController(rootViewController: UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ProfileViewController"))
            navigationController.navigationBarHidden = true
            self.slideMenuController()?.changeMainViewController(navigationController, close: true)
        }else{
            let alertView = SCLAlertView()
            alertView.showTitle("SteelMint", subTitle: "You need to login first to perform this action", style: SCLAlertViewStyle.Info, closeButtonTitle: "Cancel", duration: 20, colorStyle: 0xE4E4E4, colorTextButton: 0xFFFFFF)
            let button =  alertView.addButton("Login") { () -> Void in
                self.slideMenuController()?.changeMainViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("LoginViewControllerNavigation"), close: true)
            }
            button.backgroundColor =  GlobalConstants.APP_THEME_DARK_BLUE_COLOR
        }
    }
    
    func showActionSheet(){
        let alert = UIAlertController(title: "Contact Us", message: "Plase select an option to serve you", preferredStyle: .ActionSheet)
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        let firstAction = UIAlertAction(title: "Email", style: .Default) { (alert: UIAlertAction!) -> Void in
            self.sendEmail()
        }
        let secondAction = UIAlertAction(title: "Call", style: .Default) { (alert: UIAlertAction!) -> Void in
            let alertCall = UIAlertController(title: " ", message: "Do you want to call. Carrier charges may apply", preferredStyle: .Alert) // 1
            let cancelAxn = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
            let callAction = UIAlertAction(title: "Call", style: .Default, handler: { (UIAlertAction) -> Void in
                let phoneNumber = "0091-9575100530"
                if let phoneCallURL = NSURL(string: "tel://\(phoneNumber)") {
                    let application = UIApplication.sharedApplication()
                    if (application.canOpenURL(phoneCallURL)) {
                        application.openURL(phoneCallURL);
                    }
                    
                }
            })
            alertCall.addAction(callAction)
            alertCall.addAction(cancelAxn)
            self.presentViewController(alertCall, animated: true, completion: nil)
        }
        let thirdAction = UIAlertAction(title: "Live Chat", style: .Default) { (alert: UIAlertAction!) -> Void in
            ZDCChat.updateVisitor({ (visitor:ZDCVisitorInfo!) -> Void in
					//Check loggedin user user
					if let userInfo = CacheManager.sharedInstance .loadObject("userinfo") as! NSDictionary?{
						let username = ((userInfo.objectForKey("data#") as? NSDictionary)?.objectForKey("username") as? String)
						
					   visitor.email = username
						visitor.name = ((userInfo.objectForKey("data#") as? NSDictionary)?.objectForKey("name") as? String)!
						visitor.phone = ((userInfo.objectForKey("data#") as? NSDictionary)?.objectForKey("phoneno") as? String)!
					
					}else{

						visitor.name = "Guest";
						visitor.email = "Guest@example.com";
						visitor.phone = "0123456789";}
            })
            
            self.navigationController!.navigationBar.tintColor=UIColor.whiteColor()
            ZDCChat.startChatIn(self.navigationController, withConfig: nil)
        }
        alert.addAction(cancelAction)
        alert.addAction(firstAction)
        alert.addAction(secondAction)
        alert.addAction(thirdAction)        
        presentViewController(alert, animated: true, completion:nil)
    }
    
    
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["info@steelmint.com"])
            mail.setMessageBody("", isHTML: true)
            mail.navigationBar.tintColor=UIColor.whiteColor()
            
            presentViewController(mail, animated: true, completion: nil)
        } else {
            showAlertController("Steelmint", message: "Email not supported", reference: self)        }
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    func logout(){
        AppCommonFunctions.sharedInstance.logOutUser(self)
        
    }
    func inviteFriends(){
        AppCommonFunctions.sharedInstance.share("Hello There!!!!!              ,", description: "SteelMint is a research and data provider. It provide you Steel prices, Steel news, Steel reports, Iron ore prices (export) and updates on Steel industry, information on logistics such as vessel freight, vessel line-up of coal/iron ore, cargo position at different ports etc.", url: "https://itunes.apple.com/in/app/steelmint/id885759135?mt=8", fromViewController: self)
    }
}




