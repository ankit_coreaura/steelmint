//
//  contactUsTableViewCell.swift
//  Application
//
//  Created by Ankit Nandal on 28/11/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit

class contactUsTableViewCell: UITableViewCell {

    @IBOutlet weak var sectionLabel: UILabel!
    
    @IBOutlet weak var cellImageViewOutlet: UIImageView!
    
    @IBOutlet weak var textLabelOutlet: UILabel!
}
