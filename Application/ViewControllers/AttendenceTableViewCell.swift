//
//  AttendenceTableViewCell.swift
//  Application
//
//  Created by Ankit Nandal on 24/02/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//

import UIKit

class AttendenceTableViewCell: UITableViewCell {

    @IBOutlet weak var acessoryImageView: UIImageView!
    @IBOutlet weak var nameTextFiled: BottomBoderTextField!
   
    @IBOutlet weak var cityBtnOutlet: UIButton!
    @IBOutlet weak var stateBtnEvents: UIButton!
    @IBOutlet weak var countryBtnOutlet: UIButton!
    @IBOutlet weak var companyNameTextField: BottomBoderTextField!
    @IBOutlet weak var mobileTextFiled: BottomBoderTextField!
    @IBOutlet weak var emailTextField: BottomBoderTextField!
}
