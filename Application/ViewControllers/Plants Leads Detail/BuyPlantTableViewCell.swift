//
//  BuyPlantTableViewCell.swift
//  Application
//
//  Created by Ankit Nandal on 13/01/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//

import UIKit

class BuyPlantTableViewCell: UITableViewCell {

    @IBOutlet weak var offerDetail: UILabel!
  
    @IBOutlet weak var particulars: UILabel!

    @IBOutlet weak var remarks: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var year: UILabel!
    @IBOutlet weak var make: UILabel!
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var capacity: UILabel!
}
