//
//  NewsViewController.swift
//  SteelMint
//
//  Created by Ankit Nandal on 08/09/15.
////

import UIKit
import CarbonKit
import DCPathButton
import CoreData
import Parse

class NewsViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate,searchDelegate{
    var viaPushId:String?
    @IBOutlet weak var navigationView:UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    var lastButtonPressed :Int?=2
    var lastCategoryButtonPressed = ""
    var newsDataArrayTemp: [AnyObject]?
    var searchedArray :[AnyObject]?
    let webservicesInstance=WebServices()
    var arrayOfCategories = NSMutableArray()
    var filterVC : FilterViewController?
    var previous = 0
    var searchActive = false
    var indicator = UIActivityIndicatorView()
    var tempVIew = UIView()
    var filterActive:Bool?
    var filterDictionary:NSDictionary?
    var tempFilterVIew:UIView?
    var noResults:Bool?
    var tempArray: [AnyObject]?
    var apiSucess:Bool?
    var shouldAnimate : Bool?
    @IBOutlet weak var tableView: UITableView!
    var refreshControlDefault = UIRefreshControl()
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)

    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        viaPushId = nil
    }
    override func viewDidLoad() {
       // UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        super.viewDidLoad()
        setupPullToRefresh()
        startUpInitialisations()
        setupScrollview()

        tableView.estimatedRowHeight = 85
        tableView.rowHeight = UITableViewAutomaticDimension
        
        
        
    }
    
   
    func pushNotificationManagement(){
        if let _ = viaPushId{
            UIApplication.sharedApplication().beginIgnoringInteractionEvents()

            getCategoryWiseNewsWithoutFilter(lastCategoryButtonPressed)
        }
    }
   
    func startUpInitialisations(){
       // OFFLINE NEWS
        getContentsToShow()
        
        //GETTING NEWS CATEGORIES:
        gettingNewsCategories()
       
        //GETTING NEWS filters:
        filterVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("FilterViewController") as? FilterViewController
        
        self.filterVC!.completionBlock = {
            (appliedFilters) -> ()in

            if appliedFilters.count>0{
                if appliedFilters["NO_INT"] != nil{
                    self.filterActive = false
                    showAlertController("Steelmint", message: "You cannot use filters in offline mode. Please connect to internet", reference: self)
                    return
                }
                if isInternetConnectivityAvailable(false){
                    if self.tempArray?.count == 0  || self.tempArray == nil{
                    self.tempArray = self.newsDataArrayTemp
                    }
                    self.newsDataArrayTemp?.removeAll()
                    self.tableView.reloadData()
                    self.filterActive = true
                    self.filterDictionary = appliedFilters
                    self.setupFilterView()
                    self.getCategoryWiseNews(self.lastCategoryButtonPressed, filter:appliedFilters)
            }
                else{
                    self.filterActive = false
                    showAlertController("Steelmint", message: "You cannot use filters in offline mode. Please connect to internet", reference: self)
                }
                //print(appliedFilters)
            }
            else{
                
               // if self.tempArray?.count > 0{
                self.newsDataArrayTemp = self.tempArray
                self.tempArray?.removeAll()
                //}
                self.filterActive = false
                self.scrollView.userInteractionEnabled = true
                self.tempFilterVIew?.removeFromSuperview()
                self.tempFilterVIew = nil

                self.tableView.reloadData()
                self.getContentsToShow()

                self.getCategoryWiseNews(self.lastCategoryButtonPressed, filter:appliedFilters)
            }
        }
        
        //OFFLINE HANDLER:
        if CacheManager.sharedInstance.loadObject("newsFilters") != nil{
            filterVC!.data = CacheManager.sharedInstance.loadObject("newsFilters") as! NSArray
        }
        
        //GETTING NEWS filters from api:

        let information = NSMutableDictionary()
        WebServices().getNewsFilters(information, completionBlock: {
            (responseData) -> ()in
            if responseData != nil{
                if self.filterVC!.data.count == 0{
                self.filterVC!.data = responseData?.objectForKey("data#") as! NSArray
                }
            CacheManager.sharedInstance.saveObject(responseData?.valueForKey("data#"), identifier: "newsFilters")
            }
        })
    }
    func clearFilters(){
        //if tempArray?.count > 0{
            self.newsDataArrayTemp = tempArray
            tempArray?.removeAll()
       // }
        self.filterActive = false
        self.scrollView.userInteractionEnabled = true
        self.tempFilterVIew?.removeFromSuperview()
        self.tempFilterVIew = nil
        
        self.tableView.reloadData()
        filterVC!.cancelBtnEvent(UIButton())
        getContentsToShow()
        self.getCategoryWiseNews(self.lastCategoryButtonPressed, filter:NSDictionary())
    }
    func getContentsToShow()
    {
        shouldAnimate = false
        previous = 0
        // Managing Last News ID For api request
        if lastCategoryButtonPressed=="Latest News" || lastCategoryButtonPressed=="" {
        
            if let offlineData = DatabaseManager.sharedInstance.getAllNewsId("") {
                for objects in offlineData{
                    if let id = objects["news_id"] as? String{
                        let val = NSNumberFormatter().numberFromString(id)!.integerValue
                        if val > previous{
                            previous = val
                        }
                    }
                }
            }
        
        }else{
            if let offlineData = DatabaseManager.sharedInstance.getAllNewsId(lastCategoryButtonPressed) {
                if offlineData.count > 30 {
                    
                    for objects in offlineData{
                        if let id = objects["news_id"] as? String{
                            let val = NSNumberFormatter().numberFromString(id)!.integerValue
                            if val > previous{
                                previous = val
                            }
                        }
                    }

                    
                }else{
                 previous = 0
                }
                }
                }
       print("id is fetched")
     guard let offlineData = DatabaseManager.sharedInstance.getAllNews() else{return}
       
        if lastCategoryButtonPressed=="Latest News" || lastCategoryButtonPressed=="" {
            let predicate = NSPredicate(format: "viaSearchOrFilter = %@ ", argumentArray:[false])
            
            let filteredArray = offlineData.filter { predicate.evaluateWithObject($0) };
            newsDataArrayTemp = filteredArray
        }
        else{
            let predicate = NSPredicate(format: "main_category  == [C] %@  || category = [C] %@ && viaSearchOrFilter = %@", argumentArray:[lastCategoryButtonPressed,lastCategoryButtonPressed,false])
            
            let filteredArray = offlineData.filter { predicate.evaluateWithObject($0) };
           // print(" filtered data is: \(filteredArray)")
            newsDataArrayTemp = filteredArray
          
            
        }

        
        if let via = self.viaPushId {
            for index in  self.newsDataArrayTemp! as [AnyObject]{
                
                if via == index["news_id"] as? String{
                    
                    
                    if let passdict = index as? NSDictionary {
                        
                        
                        let obj = self.storyboard?.instantiateViewControllerWithIdentifier("NewsDetailViewController") as! NewsDetailViewController
                        obj.newsDetailData = passdict as? NSMutableDictionary
                        self.viaPushId = nil

                        self.navigationController?.pushViewController(obj, animated: true)

                        
                    }
                    
                    
                    
                }
                
            }
        }
        
        if newsDataArrayTemp?.count == 0{
            if indicator.isAnimating() == false{
                shouldAnimate = true
            }
        }
        reloadTableView()

        
    }
    
    @IBAction func onClickOfLeftBarButton(sender:UIButton){
        openLeft()
    }
    
    
    @IBAction func onClickOfRightBarButton(sender:UIButton){
        filterVC?.showAnimated()
    }
    
    func setupPullToRefresh(){
        self.refreshControlDefault.addTarget(self, action: "updateContents", forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControlDefault)
        
        // Setup ActivityIdicator
        indicator = UIActivityIndicatorView  (activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        indicator.frame = CGRectMake(GlobalConstants.SCREEN_WIDTH/2-10, GlobalConstants.SCREEN_HEIGHT/2-10, 10.0, 10.0)
        tempVIew = UIView(frame: tableView.frame)
        tempVIew.backgroundColor = UIColor.whiteColor()
    }
    
    func updateContents(){
        //GETTING LATEST NEWS BY DEFAULT
        if filterActive == true{
            NSObject.cancelPreviousPerformRequestsWithTarget(self)
            performSelector("callFilterDataApi", withObject: nil, afterDelay: 1)
        }else{
        NSObject.cancelPreviousPerformRequestsWithTarget(self)
            
            if apiSucess == true{
                performSelector("getCategoryWiseNewsWithoutFilter:", withObject: lastCategoryButtonPressed, afterDelay: 1)
            }else{
                gettingNewsCategories()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func setupScrollview(){
        if CacheManager.sharedInstance.loadObject("newsCategories") != nil{
            let arr = CacheManager.sharedInstance.loadObject("newsCategories") as! NSMutableArray
            arr.insertObject("Latest News", atIndex: 0)
            apiSucess = true
            
            // Hitting News Api
            getCategoryWiseNewsWithoutFilter(lastCategoryButtonPressed)
            
            
            AppCommonFunctions.sharedInstance.buttonsetup(arr,fontSize: 16, scrollView: scrollView, refrence: self)
            
            
        }
        else{
//            let arr = ["Latest News","Raw Material","Semi Finished","Uncategorised"]
//            AppCommonFunctions.sharedInstance.buttonsetup(arr,fontSize: 16, scrollView: scrollView, refrence: self)
        }
    }
    // MARK:  TABLE VIEW DELEGTES
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = nil
        if searchActive {

            if let items = searchedArray?.count {
                if items == 0{
                    noDataToShow()
                }
                return items
            }else{
                noDataToShow()
            }
        }else{
        if let items = newsDataArrayTemp?.count {
            if items == 0{
                noResults = true
                noDataToShow()
    
            }
            return items
            }}
        noDataToShow()
        return 0
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell1", forIndexPath:indexPath)
        let newsImageView=(cell.viewWithTag(2001) as! UIImageView)
        let newsLabel=(cell.viewWithTag(2002) as! UILabel)
        let newsDateLabel=(cell.viewWithTag(2003) as! UILabel)
        if searchActive {
            if let newsData = searchedArray?[indexPath.row] as? NSDictionary {
               
                
                if isNotNull(newsData.valueForKey("image") as? String){
                    if let url = NSURL(string:newsData.valueForKey("image") as! String){
                        newsImageView.sd_setImageWithURL(url)
                    }else{
                        newsImageView.image = UIImage(named: "placeholderImage")
                        
                    }
                    
                }else{
                    newsImageView.image = UIImage(named: "placeholderImage")
                    
                }

                
                newsDateLabel.text = newsData.valueForKey("post_date") as? String
                newsLabel.text = newsData.valueForKey("post_title") as? String
            
            }}else{
        if let newsData = newsDataArrayTemp?[indexPath.row] as? NSDictionary {
            if isNotNull(newsData.valueForKey("image") as? String){
                if let url = NSURL(string:newsData.valueForKey("image") as! String){
                    newsImageView.sd_setImageWithURL(url)
                }else{
                    newsImageView.image = UIImage(named: "placeholderImage")
                    
                }
                
            }else{
                newsImageView.image = UIImage(named: "placeholderImage")
                
            }
            newsLabel.text = newsData.valueForKey("post_title") as? String
            newsDateLabel.text = getDateFromTimestamp2(newsData.valueForKey("post_date") as? NSNumber, dateFormat: "MMM dd, yyyy HH:mm")
            if newsDateLabel.text?.characters.count == 0 {
            newsDateLabel.text = newsData.valueForKey("post_date") as? String
            }
            }}
        return cell
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let path = self.tableView.indexPathForSelectedRow

        if searchActive{
            let newsDetail = segue.destinationViewController as? NewsDetailViewController
            let exists = path!.row < searchedArray?.count ? true : false
            if exists{
            if let passdict = searchedArray{
                newsDetail?.newsDetailData = passdict[path!.row] as? NSMutableDictionary

                }}
        }else{
        let newsDetail = segue.destinationViewController as? NewsDetailViewController
            newsDetail!.hidesBottomBarWhenPushed = true

        if let passdict = newsDataArrayTemp{
            
            newsDetail?.newsDetailData = passdict[path!.row] as? NSMutableDictionary
        }
        }
    }
   
    // MARK:  CATEGORIES CLICKED
    
    
    func butonIndexed(sender:UIButton){
        refreshControlDefault.endRefreshing()
        var offsetX = sender.frame.origin.x+sender.frame.size.width/2 -  GlobalConstants.SCREEN_WIDTH/2
        if offsetX > scrollView.contentSize.width -  GlobalConstants.SCREEN_WIDTH {
            offsetX = scrollView.contentSize.width -  GlobalConstants.SCREEN_WIDTH
        }
        if offsetX < 0 {
            offsetX = 0
        }
        scrollView.setContentOffset(CGPointMake(offsetX, 0), animated: true)
        if let buttonPressed = lastButtonPressed{
            (scrollView.viewWithTag(buttonPressed) as? UIButton)?.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
            (scrollView.viewWithTag(buttonPressed) as? UIButton)?.titleLabel?.font=UIFont.systemFontOfSize(13)
            (scrollView.viewWithTag(buttonPressed+15) as? UIImageView)?.removeFromSuperview()
        }
        lastButtonPressed = sender.tag
        sender.titleLabel?.font=UIFont.boldSystemFontOfSize(14)
        sender.setTitleColor(UIColor(red: 30.0/255.0, green: 136.0/255.0, blue: 229.0/255.0, alpha: 1), forState: .Normal)
        
        print(sender.currentTitle)
        lastCategoryButtonPressed=sender.currentTitle!
            getContentsToShow()
            NSObject.cancelPreviousPerformRequestsWithTarget(self)
            performSelector("getCategoryWiseNewsWithoutFilter:", withObject: sender.currentTitle!, afterDelay: 1)
    
    }
    func setupFilterView(){
        
        if tempFilterVIew == nil{
            self.scrollView.userInteractionEnabled = false
            tempFilterVIew = UIView(frame: CGRectMake(0, 64, GlobalConstants.SCREEN_WIDTH,35))
            tempFilterVIew!.backgroundColor = UIColor.whiteColor()
            let label = UILabel(frame: CGRectMake(10, tempFilterVIew!.bounds.minY, 180, 30))
            let clearAllBtn = UIButton(frame: CGRectMake(GlobalConstants.SCREEN_WIDTH-35, tempFilterVIew!.bounds.minY+3, 30, 30))
            clearAllBtn.setImage(UIImage.image(UIImage(named: "cancelImage")!, withColor: UIColor.whiteColor()), forState: .Normal)
            clearAllBtn.addTarget(self, action: "clearFilters", forControlEvents: UIControlEvents.TouchUpInside)
            clearAllBtn.backgroundColor = GlobalConstants.APP_THEME_BLUE_COLOR
            clearAllBtn.layer.cornerRadius = 15.0
           clearAllBtn.layer.borderWidth = 2.0
           clearAllBtn.layer.borderColor = UIColor.whiteColor().CGColor
            clearAllBtn.tintColor = UIColor.whiteColor()
            tempFilterVIew!.addSubview(clearAllBtn)
            tempFilterVIew!.addSubview(label)
            label.text = "Filtered Results:"
            label.font = UIFont.systemFontOfSize(13)
            label.textColor = UIColor.darkGrayColor()
            tempFilterVIew!.layer.borderColor = UIColor.grayColor().CGColor
            tempFilterVIew!.layer.borderWidth = 1.0

            self.view.addSubview(tempFilterVIew!)
        }
    }
    func callFilterDataApi(){
        if filterDictionary?.count>0{
        self.getCategoryWiseNews(self.lastCategoryButtonPressed, filter:filterDictionary!)
        }
    }
    func getCategoryWiseNews(var categoryName:String,filter:NSDictionary){
        self.noResults = false

        print("NEWS API HIT /n***********************/n***************")
        
        let information = NSMutableDictionary()
        
        information.setObject(filter, forKey: "filter")
        if filterActive == true{
            showNativeActivityIndicator()
        }else{
            if categoryName == "Latest News"{categoryName = ""}
            information.setObject(categoryName, forKey: "category")
            
//        if filter.count == 0 {
//            information.setObject("new:\(previous)", forKey: "postfrom")
//        }
    if indicator.isAnimating() == false && DatabaseManager.sharedInstance.getAllNews()?.count == nil {
            showNativeActivityIndicator()
        }
        }
        if isInternetConnectivityAvailable(true)==false {
            self.refreshControlDefault.endRefreshing()
            if indicator.isAnimating(){
                self.stopActivityIndicator()
                
            }
            return;
        }
        webservicesInstance.getNewsCategoryWise(information, completionBlock: {
            (responseData) -> ()in
            self.refreshControlDefault.endRefreshing()
            if self.indicator.isAnimating(){
                self.stopActivityIndicator()
                
            }

            if responseData != nil{
                
                // FILTER HANDLER
                if filter.count>0{
                    guard let newsData = responseData?.valueForKey("data#") as?NSMutableArray else{ self.noResults = true;return}
                    if newsData.count == 0{
                        self.noResults = true
                    }
                    self.newsDataArrayTemp?.removeAll()
                    self.newsDataArrayTemp = newsData as [AnyObject]
                    self.reloadTableView()
                }
                    
                    // WITHOUT FILTER HANDLER
                else{
                    guard let newsData = responseData?.valueForKey("data#") as?NSMutableArray else{return}
                    
                    for object in newsData
                    {
                        DatabaseManager.sharedInstance.addNews(object as! NSDictionary)
                    }
                    
                    self.getContentsToShow()
                }
            }
            
        })
    }
    
    func getCategoryWiseNewsWithoutFilter(var categoryName:String){
        self.noResults = false
        print("NEWS API HIT WITHOUT FILTER /n *********************** /n***************")
        let information = NSMutableDictionary()
        if categoryName == "Latest News"{categoryName = ""}
        information.setObject(categoryName, forKey: "category")
          //  information.setObject("new:\(previous)", forKey: "postfrom")
        if indicator.isAnimating() == false && DatabaseManager.sharedInstance.getAllNews()?.count == nil || shouldAnimate == true && indicator.isAnimating() == false {
            showNativeActivityIndicator()
        }
        if isInternetConnectivityAvailable(true)==false {
            self.refreshControlDefault.endRefreshing()
            if indicator.isAnimating(){
                self.stopActivityIndicator()
                
            }
            UIApplication.sharedApplication().endIgnoringInteractionEvents()

            return;
        }
        webservicesInstance.getNewsCategoryWise(information, completionBlock: {
            (responseData) -> ()in
            UIApplication.sharedApplication().endIgnoringInteractionEvents()

            self.refreshControlDefault.endRefreshing()
           
            
            if responseData != nil{
               
          
                guard let newsData = responseData?.valueForKey("data#") as?NSMutableArray else{self.noResults = true;return}
                
//                if DatabaseManager.sharedInstance.getAllNews()?.count>0{
//                    print("/n ##### *** /n #####   background mode is on")
//                   DatabaseManager.sharedInstance.addNewsINBackgroundThread(newsData)
//                }else{
                print("/n **** background mode is off")
                print(DatabaseManager.sharedInstance.getAllNewsId(self.lastCategoryButtonPressed))

                DatabaseManager.sharedInstance.deleteNewsforCategory(self.lastCategoryButtonPressed)
                print(DatabaseManager.sharedInstance.getAllNewsId(self.lastCategoryButtonPressed))

                    for object in newsData
                    {
                        DatabaseManager.sharedInstance.addNews(object as! NSDictionary)
                    }
                print(DatabaseManager.sharedInstance.getAllNewsId(self.lastCategoryButtonPressed))

               // }
                    self.getContentsToShow()
                if self.indicator.isAnimating(){
                    self.stopActivityIndicator()
                }
            }else{
            if self.indicator.isAnimating(){
                self.stopActivityIndicator()
            }
            }
        })
    }
    func reloadTableView()
    {
        self.tableView.reloadData()
    }
    
    func gettingNewsCategories(){
        if isInternetConnectivityAvailable(false) == false{
            self.refreshControlDefault.endRefreshing()
            return
        }
        
         if CacheManager.sharedInstance.loadObject("newsCategories") == nil{
            showNativeActivityIndicator()
        }
        let information = NSMutableDictionary()
        webservicesInstance.getNewsCategories(information, completionBlock: {
            (responseData) -> ()in
          //  self.stopActivityIndicator()

            self.refreshControlDefault.endRefreshing()
            guard let newsCategories = responseData?.valueForKey("data#") as? NSArray else {return}
            self.arrayOfCategories.removeAllObjects()
            for catName in newsCategories{
                self.arrayOfCategories.addObject(catName.valueForKey("cat_name")!)
            }
            if CacheManager.sharedInstance.loadObject("newsCategories") == nil{
                CacheManager.sharedInstance.saveObject(self.arrayOfCategories, identifier: "newsCategories")
                self.scrollView.subviews.forEach({ $0.removeFromSuperview() })
                self.setupScrollview()
            }
            CacheManager.sharedInstance.saveObject(self.arrayOfCategories, identifier: "newsCategories")
        })
    }
    
    // MARK: Search Management
  
    @IBAction func searchBtnEvent(sender:UIButton){
        self.navigationView.userInteractionEnabled = false
        let searchView: SearchView = SearchView()
        searchView.frame = CGRectMake(0, -99, UIScreen.mainScreen().bounds.width, 99)
       
        UIView.animateWithDuration(0.3) { () -> Void in
            searchView.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 99)

        }
        searchView.delegate = self
        self.view.addSubview(searchView)

    }
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissMode.OnDrag

    }
    // MARK: Search View Delegates
    
    func textForSearch(text: String) {
        self.tableView.allowsSelection = false

        NSObject.cancelPreviousPerformRequestsWithTarget(self)
        performSelector("searchApiHit:", withObject: text, afterDelay: 1)
    }
    func cancelCallBack(flag: Bool) {
        self.navigationView.userInteractionEnabled = true
        searchActive = false
        tableView.addSubview(refreshControlDefault)
        tableView.reloadData()
    }
    func searchDidBegin(serachBar: UISearchBar) {
        if refreshControlDefault.refreshing{
        refreshControlDefault.endRefreshing()
        }
        self.refreshControlDefault.removeFromSuperview()
        searchedArray?.removeAll()
        searchActive = true
        tableView.reloadData()
    }
    func textForSearchButtonClicked(text: String) {
        self.tableView.allowsSelection = false

        NSObject.cancelPreviousPerformRequestsWithTarget(self)
        performSelector("searchApiHit:", withObject: text, afterDelay: 1)
    }
    
    func searchApiHit(searchText:String){
        self.tableView.allowsSelection = true

        if noResults == true{
            self.tableView.backgroundView = nil
        }
        self.noResults = false
        if !indicator.isAnimating(){
          showNativeActivityIndicator()
        }
        if !isInternetConnectivityAvailable(true){
            
            if indicator.isAnimating(){
                self.stopActivityIndicator()
            }
            return
        }
        let searchDict = NSDictionary(object: searchText, forKey: "filter")
        webservicesInstance.getNewsSearchWise(searchDict) { (responseData) -> () in
            self.searchedArray?.removeAll()

            if self.indicator.isAnimating(){
                self.stopActivityIndicator()
                
            }

            if responseData != nil{
                guard let response = responseData?["data#"] as? [NSDictionary] else{            self.noResults = true;return}
                if response.count == 0{
                  self.noResults = true
                    
                }else{
                    self.searchedArray = response}
                self.tableView.reloadData()
            }
        }
    }
    //MARK: ACTIVITY INDICATOR
    func showNativeActivityIndicator(){
        self.tableView.allowsSelection = false
        scrollView.layer.opacity = 0.3
        scrollView.userInteractionEnabled = false
        view.addSubview(tempVIew)
        view.addSubview(indicator)
        indicator.startAnimating()
    }
    
    func stopActivityIndicator() {
        scrollView.layer.opacity = 1.0
        scrollView.userInteractionEnabled = true
        self.tableView.allowsSelection = true
        indicator.stopAnimating()
        tempVIew.removeFromSuperview()
    }
    // MARk: No data Management
    func noDataToShow(){
        if noResults == true{
//            let noDataLabel = UILabel (frame: CGRectMake(0, 0,tableView.bounds.size.width,tableView.bounds.size.height))
//            noDataLabel.text             = "No News"
//            noDataLabel.textColor        = UIColor.lightGrayColor()
//            noDataLabel.textAlignment    = .Center
//            tableView.backgroundView = noDataLabel;
        }
    }
    
   }

