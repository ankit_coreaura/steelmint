//
//  DelegateViewController.swift
//  screens
//
//  Created by Authorised Personnel on 26/10/15.
//  Copyright © 2015 Authorised Personnel. All rights reserved.
//

import UIKit

class DelegateViewController: UIViewController,UITableViewDelegate,UITableViewDataSource  {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

     func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("c1", forIndexPath: indexPath)
        
        // Configure the cell...
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }

    @IBAction func backBtnEvent(sender:UIButton){
        navigationController?.popViewControllerAnimated(true)
    }
}
