//
//  SubscribeViewController.swift
//  SteelMint
//
//  Created by Ankit Nandal on 03/11/15.

import UIKit

class AboutUsViewController: UIViewController {

    @IBOutlet weak var valuesTextView: UITextView!
    @IBOutlet weak var missionTextView: UITextView!
    @IBOutlet weak var AnalyticsTextView: UITextView!
    
    @IBOutlet weak var visionTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let response = CacheManager.sharedInstance.loadObject("aboutUs"){
            //self.AnalyticsTextView.text = response as! String
        }
        serviceHit()
        AnalyticsTextView.textContainerInset = UIEdgeInsetsZero

    }
    func serviceHit(){
        
        let webServicesObject = WebServices()
       // webServicesObject.progresssIndicatorText = " "
        webServicesObject.aboutUs(["type":"aboutus"], completionBlock:
            {
                (responseData) -> () in
                if let response = responseData?.valueForKey("data#"){
                    print(response)
                    CacheManager.sharedInstance.saveObject(response, identifier: "aboutUs")
                    //self.AnalyticsTextView.text = response as! String
                }
                
                print(responseData)
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
        @IBAction func onClickOfLeftBarButton(sender:UIButton){
                openLeft()
        }
}
