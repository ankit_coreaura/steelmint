//
//  AttendenceViewController.swift
//  Application
//
//  Created by Chandan Singh on 12/20/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


class AttendenceViewController: UIViewController, PayPalPaymentDelegate , AddressDelegate,UITextFieldDelegate{
    var numberOfTickets = 0
    var price = 0
    var eventID: String?
    var type = ""
    var resultText = "" // empty
    @IBOutlet weak var payButton:UIButton!
    @IBOutlet weak var successView: UIView!
    var dictOfLocation = ["country":"","state":"","city":""]
    var completecountryList = NSDictionary()
    var webserViceInstance = WebServices()
    var order_id:String?
    
    var payPalConfig = PayPalConfiguration() // default
    
    var environment:String = PayPalEnvironmentProduction {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnectWithEnvironment(newEnvironment)
            }
        }
    }
    #if HAS_CARDIO
    // You should use the PayPal-iOS-SDK+card-Sample-App target to enable this setting.
    // For your apps, you will need to link to the libCardIO and dependent libraries. Please read the README.md
    // for more details.
    var acceptCreditCards: Bool = true {
    didSet {
    payPalConfig.acceptCreditCards = acceptCreditCards
    }
    }
    var temp:String = ""{
    willSet{
    }
    
    }
    #else
    var acceptCreditCards: Bool = true {
        didSet {
            payPalConfig.acceptCreditCards = acceptCreditCards
        }
    }
    #endif
    var lastButtonPressed :Int?=2
    var lastCategoryButtonPressed=""
    var selectedIndexPath: NSIndexPath? = nil
    
    
    @IBOutlet weak var tableView: UITableView!
    override func viewWillDisappear(animated: Bool) {
        IQKeyboardManager.sharedManager().enableAutoToolbar = true

    }
    override func viewDidLoad() {
        super.viewDidLoad()

        setupPayment()
        
        payButton.setTitle("Proceed Payment =  \(price)", forState: UIControlState.Normal)
        setupInit()
    }
    
    func setupInit(){
        let s = try! String(contentsOfFile: NSBundle.mainBundle()
            .pathForResource("countryList", ofType: "txt")!,
            encoding: NSUTF8StringEncoding)
        let x = parsedJsonFrom2(s.dataUsingEncoding(NSUTF8StringEncoding))
        if x != nil{
            if let dict = x?.valueForKey("data#") as? NSDictionary{
                if self.completecountryList.count == 0{
                    self.completecountryList = dict
                }
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        IQKeyboardManager.sharedManager().enableAutoToolbar = false

        PayPalMobile.preconnectWithEnvironment(environment)
    }
    func setupPayment(){
        successView.hidden = true
        
        // Set up payPalConfig
        payPalConfig.acceptCreditCards = acceptCreditCards;
        payPalConfig.merchantName = "SteelMint"
        payPalConfig.merchantPrivacyPolicyURL = NSURL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = NSURL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        
        // Setting the languageOrLocale property is optional.
        //
        // If you do not set languageOrLocale, then the PayPalPaymentViewController will present
        // its user interface according to the device's current language setting.
        //
        // Setting languageOrLocale to a particular language (e.g., @"es" for Spanish) or
        // locale (e.g., @"es_MX" for Mexican Spanish) forces the PayPalPaymentViewController
        // to use that language/locale.
        //
        // For full details, including a list of available languages and locales, see PayPalPaymentViewController.h.
        
        payPalConfig.languageOrLocale = NSLocale.preferredLanguages()[0]
        
        // Setting the payPalShippingAddressOption property is optional.
        //
        // See PayPalConfiguration.h for details.
        
        payPalConfig.payPalShippingAddressOption = .None;
        
        print("PayPal iOS SDK Version: \(PayPalMobile.libraryVersion())")
        
    }
    
    @IBAction func onClickOfLeftBarButton(sender:UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return numberOfTickets
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell2", forIndexPath:indexPath) as! AttendenceTableViewCell
        let ip = indexPath
        if selectedIndexPath != nil {
            if ip == selectedIndexPath!
            {cell.acessoryImageView = UIImageView.init(image: UIImage(named:"arrowDownEvents"))
            } else
            {
                cell.acessoryImageView = UIImageView.init(image: UIImage(named:"arrowSideEvents"))            }
        } else
        {
            cell.acessoryImageView = UIImageView.init(image: UIImage(named:"arrowSideEvents"))
        }
        
       
        return cell
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        let ip = indexPath
        if selectedIndexPath != nil {
            if ip == selectedIndexPath!
            {
                return 335
                
            } else
            {
                return 40
            }
        } else
        {
            return 40
            
        }
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        switch selectedIndexPath {
        case nil:
            selectedIndexPath = indexPath
        default:
            if selectedIndexPath! == indexPath
            {
                selectedIndexPath = nil
            } else
            {
                selectedIndexPath = indexPath
            }
        }
        tableView.reloadData()
    }
    
    
    
    // MARK:  CATEGORIES CLICKED
    
    func reloadTableView()
    {
        self.tableView.reloadData()
    }
    
    
    //MARK: Payment Management
    
    
    @IBAction func paymentBtnEvent(sender: AnyObject) {
        
        let arrayOfValues = NSMutableArray()
        let cells = tableView.visibleCells as! [AttendenceTableViewCell]
        var i = 0
        for cell in cells{
            i++
            let dict = NSMutableDictionary()
            if cell.nameTextFiled.text != ""{
                dict.setObject(cell.nameTextFiled.text!, forKey: "name")
            }else{showAlertController("Attendee", message: "Please fill Name of Attendee \(i)", reference: self)
                
                return}
            
            if  cell.mobileTextFiled.text != "" {
                
                if cell.mobileTextFiled.text!.isPhoneNumber{
                    dict.setObject(cell.mobileTextFiled.text!, forKey: "phoneno")
                    
                }else{showAlertController("Attendee", message: "Please fill  Valid Mobile Number of Attendee \(i)", reference: self);return}
                
            }else{showAlertController("Attendee", message: "Please fill Mobile Number of Attendee \(i)", reference: self);return}
            
            
            
            
            if cell.emailTextField.text != ""{
                
                if cell.emailTextField.text!.isEmail{
                    dict.setObject(cell.emailTextField.text!, forKey: "email")
                    
                }else{showAlertController("Attendee", message: "Please fill  Valid Email id of Attendee \(i)", reference: self);return}
                
                
            }else{showAlertController("Attendee", message: "Please fill Email id of Attendee \(i)", reference: self);return}
            
            if cell.companyNameTextField.text != "" {
                dict.setObject(cell.companyNameTextField.text!, forKey: "company")
            }else{showAlertController("Attendee", message: "Please fill Company Name of Attendee \(i)", reference: self);return}
            
            if let _ = cell.countryBtnOutlet.currentTitle{
                dict.setObject(dictOfLocation["country"]!, forKey: "country")
            }else{showAlertController("Attendee", message: "Please Select Country Name of Attendee \(i)", reference: self);return}
            
            if cell.stateBtnEvents.currentTitle != ""{
                dict.setObject(dictOfLocation["state"]!, forKey: "state")
            }
            
            if cell.cityBtnOutlet.currentTitle != ""{
                dict.setObject(dictOfLocation["city"]!, forKey: "city")
            }
            
            dict.setObject("", forKey: "other_city")
            arrayOfValues.addObject(dict)
        }
        let info = NSDictionary(objects: [arrayOfValues,type,sessionId()], forKeys: ["delegates","type","token"])
        var data = NSData()
        do {
            data = try NSJSONSerialization.dataWithJSONObject(info, options:  NSJSONWritingOptions(rawValue: 0))
        }catch let error as NSError{
            print(error.description)
        }
        webserViceInstance.progresssIndicatorText = "Please wait...."
        webserViceInstance.fillDelegateForm(info, id: eventID!,formData:data) { (responseData) -> () in
            if responseData != nil{
                if let statuskey = responseData?["status#"] as? String{
                    if statuskey == "1"{
                        (UIApplication.sharedApplication().delegate as! AppDelegate).redirectUrlCCAvenue = responseData?["data#"]??["page"] as? String
                        if  let gateway = responseData?["data#"]??["payment_gateway"] as? String{
                            //                            
                            var str = ""

                            if let priceTotal = responseData?["data#"]??["payment_detail"] as? NSDictionary{
                                for (keys,values) in priceTotal{
                                    str += String(keys)+" : " + String(values) + "\n"
                                }
                            
                            }
                            let tempResponseData = responseData
                            let alert = UIAlertController(title: "SteelMint", message: str, preferredStyle: .ActionSheet)
                            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
                            let firstAction = UIAlertAction(title: "Proceed", style: .Default) { (alert: UIAlertAction!) -> Void in
                                
                                if gateway.lowercaseString == "ccavenue"{
                                  //  var rsa:String?
                                    if let billNo = responseData?["data#"]??["bill_no"] as? String{
                                        let storyBoard = UIStoryboard(name: "Secondary", bundle: nil)
                                        
                                        let ccObj = storyBoard.instantiateViewControllerWithIdentifier("CCInitViewController") as! CCInitViewController
                                        if let priceTotal = tempResponseData?["data#"]??["payment_detail"]??["Total"] as? String{
                                            ccObj.price = priceTotal
                                            ccObj.rsaKey = "wecewcewc"
                                            ccObj.orderID = tempResponseData?["data#"]??["bill_no"] as? String
                                            
                                           ( UIApplication.sharedApplication().delegate as! AppDelegate).redirectUrlCCAvenue = tempResponseData?["data#"]??["page"] as? String
                                            
                                            self.navigationController?.pushViewController(ccObj, animated: true)}else{
                                            showAlertController("SteelMint", message: "Temporary unable to buy tickets. Please try again later", reference: self)
          
                                    
                                    }
                                  
                                    
                                }
                                }
                                else if gateway.lowercaseString == "paypal" {
                                    if let billNo = responseData?["data#"]??["bill_no"] as? String{
                                    self.order_id = billNo
                                        if let total = tempResponseData?["data#"]??["payment_detail"]??["Total"] as? String{
                                            if let prcInt = NSNumberFormatter().numberFromString(total)?.integerValue{
                                                self.price = prcInt
                                                // Remove our last completed payment, just for demo purposes.
                                                self.resultText = ""
                                                
                                                // Note: For purposes of illustration, this example shows a payment that includes
                                                //       both payment details (subtotal, shipping, tax) and multiple items.
                                                //       You would only specify these if appropriate to your situation.
                                                //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
                                                //       and simply set payment.amount to your total charge.
                                                
                                                // Optional: include multiple items
                                                let item1 = PayPalItem(name: "Ticket for Event", withQuantity: UInt(self.numberOfTickets), withPrice: NSDecimalNumber(integer: self.price), withCurrency: "USD", withSku: "Ticket-0037")
                                                //        let item2 = PayPalItem(name: "Free rainbow patch", withQuantity: 1, withPrice: NSDecimalNumber(string: "0.00"), withCurrency: "USD", withSku: "Hip-00066")
                                                //        let item3 = PayPalItem(name: "Long-sleeve plaid shirt (mustache not included)", withQuantity: 1, withPrice: NSDecimalNumber(string: "37.99"), withCurrency: "USD", withSku: "Hip-00291")
                                                
                                                let items = [item1]
                                                let subtotal = PayPalItem.totalPriceForItems(items)
                                                
                                                // Optional: include payment details
                                                //let shipping = NSDecimalNumber(string: "5.99")
                                                //let tax = NSDecimalNumber(string: "2.50")
                                                let shipping = NSDecimalNumber(string: "0.0")
                                                let tax = NSDecimalNumber(string: "0.0")
                                                let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
                                                //let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: nil, withTax: nil)
                                                let total = subtotal.decimalNumberByAdding(shipping).decimalNumberByAdding(tax)
                                                
                                                let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "Event Tickets", intent: .Sale)
                                                
                                                payment.items = items
                                                //set payment details
                                                
                                                payment.paymentDetails = paymentDetails
                                                //payment.paymentDetails = nil
                                                
                                                if (payment.processable) {
                                                    let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: self.payPalConfig, delegate: self)
                                                    self.presentViewController(paymentViewController!, animated: true, completion: nil)
                                                }
                                                else {
                                                    // This particular payment will always be processable. If, for
                                                    // example, the amount was negative or the shortDescription was
                                                    // empty, this payment wouldn't be processable, and you'd want
                                                    // to handle that here.
                                                    print("Payment not processalbe: \(payment)")
                                                }
                                            }
                                            
                                        }
                                        
  
                                        
                                    }
                                    
                                    
                                }else{
                                    showAlertController("!", message: "Unable to proceed to payment Please contact customer care", reference: self)
                                }
                                
                              ////////////////
                            }
                            alert.addAction(firstAction)
                            alert.addAction(cancelAction)
                            self.presentViewController(alert, animated: true, completion:nil)

                        //
                            
                    }
                    }
                    else{showAlertController("SteelMint", message: "Please try again", reference: self)}
                }
                else{
                    showAlertController("SteelMint", message: "Please try again", reference: self)
                }
                
            }else{
                showAlertController("SteelMint", message: "Please try again", reference: self)
            }}
        
    }
    
  
    
    
    
    // PayPalPaymentDelegate
    
    func payPalPaymentDidCancel(paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        resultText = "error caught in delegate"
        successView.hidden = true
        paymentViewController.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    func payPalPaymentViewController(paymentViewController: PayPalPaymentViewController, didCompletePayment completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        paymentViewController.dismissViewControllerAnimated(true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            guard let order = self.order_id else{return}
            guard let payid = completedPayment.confirmation["response"]?["id"] as? String else{return}
            self.resultText = completedPayment.description
            let dict = NSDictionary(objects: [order,"paypal",sessionId(),payid], forKeys: ["order_id","payment_gateway","token","sale_id"])
            self.webserViceInstance.paypalAfterPayment(dict) { (responseData) -> () in
                if responseData != nil{
                    if let status = responseData?["status#"] as? String{
                     if status == "1"{
                        self.showSuccess()

                     }else{
                      showAlertController("!", message:responseData?["message"] as? String , reference: self)
                        }
                    }
                }else{
                    showAlertController("Please take snapshot of it and immediately contact customer care", message: "Order id: \(order) \n PayID = \(payid)", reference: self)
                }
            }
            
        })
    }
    // MARK: Helpers
    
    func showSuccess() {
        
        let alert = UIAlertController(title: "SteelMint", message: "\n \n Your Payment was Sucessful !!!", preferredStyle: .Alert)
        let firstAction = UIAlertAction(title: "OK", style: .Default) { (alert: UIAlertAction!) -> Void in
            self.navigationController?.popViewControllerAnimated(true)
                        ////////////////
        }
        alert.addAction(firstAction)
        presentViewController(alert, animated: true, completion: nil)

    }
    // MARK: COUNTRY SELECTOR
    
    
    @IBAction func CountrySelection(sender: UIButton) {
        let selectAddressInstance = storyboard?.instantiateViewControllerWithIdentifier("AddressViewController") as! AddressViewController
        let dict = NSMutableDictionary()
        // completecountryList.allValues as! [NSDictionary]
        if completecountryList.count > 0{
            for objects  in completecountryList.allValues{
                dict.setObject(objects["id"] as! String, forKey: objects["name"] as! String)
            }
            selectAddressInstance.delegate = self
            selectAddressInstance.tempdict = dict
            selectAddressInstance.via = "c"
            selectAddressInstance.navigationTitleText = "SELECT COUNTRY"
            
            presentViewController(selectAddressInstance, animated: true, completion: nil)
        }
        
    }
    
    
    @IBAction func stateSelection(sender: UIButton) {
        let selectAddressInstance = storyboard?.instantiateViewControllerWithIdentifier("AddressViewController") as! AddressViewController
        if dictOfLocation["country"]?.characters.count > 0 {
            guard let value = completecountryList[dictOfLocation["country"]!] as? NSDictionary else{return}
            guard let dict2 = value["state"]?.allValues as? [NSDictionary] else{return}
            let dictofStates = NSMutableDictionary()
            for objects  in dict2{
                dictofStates.setObject(objects["id"] as! String, forKey: objects["name"] as! String)
            }
            
            selectAddressInstance.delegate = self
            selectAddressInstance.tempdict = dictofStates
            selectAddressInstance.via = "state"
            selectAddressInstance.navigationTitleText = "SELECT STATE"
            
            presentViewController(selectAddressInstance, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func citySelection(sender: UIButton) {
        let selectAddressInstance = storyboard?.instantiateViewControllerWithIdentifier("AddressViewController") as! AddressViewController
        if dictOfLocation["country"]?.characters.count > 0{
            if dictOfLocation["state"]?.characters.count > 0 {
                guard  let stateCode = dictOfLocation["state"] else {return}
                guard let value = (completecountryList[dictOfLocation["country"]!]?["state"] as? NSDictionary)?.objectForKey(stateCode)?["city"] as? NSArray else{return}
                
                let dict = NSMutableDictionary()
                //        guard let cityDict = (value["state"] as! NSDictionary)[dictOfLocation["state"]!] as? NSDictionary  else{return}
                //        guard let cityList = cityDict["city"] as? NSArray else{return}
                for objects  in value{
                    if objects is NSDictionary{
                        dict.setObject(objects["id"] as! String, forKey: objects["name"] as! String)
                    }
                }
                selectAddressInstance.delegate = self
                selectAddressInstance.tempdict = dict
                selectAddressInstance.via = "city"
                selectAddressInstance.navigationTitleText = "SELECT CITY"
                
                presentViewController(selectAddressInstance, animated: true, completion: nil)
                
            }
        }
        
    }
    
    
    func returnAddressWithCode(value: String,name: String,via:String) {
        guard let selectedIndexPath = selectedIndexPath else{return}
        let cell = tableView.cellForRowAtIndexPath(selectedIndexPath) as! AttendenceTableViewCell
        if via == "c"{
            
            cell.countryBtnOutlet.setTitle(name, forState: .Normal)
            dictOfLocation["country"] = value
            
        }else if via == "state"{
            cell.stateBtnEvents.setTitle(name, forState: .Normal)
            
            dictOfLocation["state"] = value
            
        }else{
            cell.cityBtnOutlet.setTitle(name, forState: .Normal)
            dictOfLocation["city"] = value
        }
        print("\(name)---\(value)")
        
        
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}