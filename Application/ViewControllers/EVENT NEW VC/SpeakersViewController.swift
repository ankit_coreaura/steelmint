//
//  SpeakersViewController.swift
//  Application
//
//  Created by Chandan Singh on 12/16/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit

class SpeakersViewController: UIViewController, searchDelegate {

 @IBOutlet weak var tableView: UITableView!
    
	var selectedIndexPath: NSIndexPath?
	var SpeakerData : [Speaker]?
	var SpeakerDataCopy: [Speaker]?
	var SpeakerSearchResult: [Speaker]?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 85.0
        tableView.rowHeight = UITableViewAutomaticDimension
		  SpeakerDataCopy = SpeakerData
		}
	
	override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let noOfRows = SpeakerData?.count {
            return noOfRows
        }
       return 0
		
    }
	
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		
        var cell=UITableViewCell()
        cell=tableView.dequeueReusableCellWithIdentifier("c1", forIndexPath: indexPath)
        let  speakerImage = cell.viewWithTag(4030) as! UIImageView
        let  speakerNameTitle = (cell.viewWithTag(4031) as! UILabel)
        let  speakerDesigTitle = (cell.viewWithTag(4032) as! UILabel)
        let  speakerDescription = (cell.viewWithTag(4034) as! UILabel)
        let  speakerTopic = (cell.viewWithTag(4033) as! UILabel)
        
        if let speaker = SpeakerData?[indexPath.row]{
            speakerNameTitle.text = speaker.name ?? "--"
            speakerTopic.text = speaker.subject ?? "--"
            speakerDesigTitle.text = speaker.position ?? "--"
            // Setting Image of Speaker
            if let speakerImageData = speaker.valueForKey("thumb_img") as? String {
                if isNotNull(speakerImageData)
                {
                    speakerImage.sd_setImageWithURL(NSURL(string: speakerImageData)!, placeholderImage: UIImage(named: "placeholderImage"))
                    
                }else{
                    speakerImage.image = UIImage(named: "placeholderImage")
 
                }
                
            }
            // Setting description of speaker depending on expanded or collapsed list
                let ip = indexPath
                if selectedIndexPath != nil {
                    if ip == selectedIndexPath!
                    {
                        if speaker.desc?.characters.count > 0{
                            speakerDescription.text = speaker.desc
                        }else{
                           speakerDescription.text = "No description found"
                        }
                        
                        cell.accessoryView = UIImageView.init(image: UIImage(named:"arrowInverted.png"))
                        
                    }
                    else{//
                        cell.accessoryView = UIImageView.init(image: UIImage(named:"arrow.png"))
                        speakerDescription.text = ""
                    }
                }
                else{
                    speakerDescription.text = ""
                 cell.accessoryView = UIImageView.init(image: UIImage(named:"arrow.png"))
            }
        
        
        }
              return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       
        switch selectedIndexPath {
        case nil:
            selectedIndexPath = indexPath
        default:
            if selectedIndexPath! == indexPath
            {
                selectedIndexPath = nil
            } else
            {
                selectedIndexPath = indexPath
            }
        }
        tableView.reloadData()
    }
    
    

    @IBAction func onClickOfLeftBarButton(sender:UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
	
	
	
	
	
	/**
	This method is used to search speaker's data
	*/
	@IBAction func onClickOfSearch(sender:UIButton){
		if SpeakerData?.count < 0 {
			return
		}else
		{
			var searchView: SearchView = SearchView()
			searchView.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 64)
			searchView.delegate = self
			self.view.addSubview(searchView)
//			self.presentViewController(searchVC, animated: true, completion: { () -> Void in
//				
//			})
			//Make predicate here for searched text.
			
		}
	}

	func textForSearch(text: String){
		print(text)
      

		let predicate = NSPredicate(format: "name contains[C] %@ || subject contains[C] %@ || position contains[C] %@", argumentArray: [text,text,text])
		SpeakerSearchResult = SpeakerData?.filter({ predicate.evaluateWithObject($0)})
		if SpeakerSearchResult?.count > 0{
			
			SpeakerData = SpeakerSearchResult
			
		}else{
			SpeakerData = nil
		}
		tableView.reloadData()
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(2 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
           self.SpeakerData   = self.SpeakerDataCopy
        }
     // SpeakerData   = temp
	}

	
	
	func cancelCallBack(flag: Bool){
		SpeakerData = SpeakerDataCopy
		tableView.reloadData()
	}

	
    func searchDidBegin(serachBar: UISearchBar) {
        
    }
    func textForSearchButtonClicked(text: String) {
        print(text)
        let predicate = NSPredicate(format: "name contains[C] %@ || subject contains[C] %@ || position contains[C] %@", argumentArray: [text,text,text])
        SpeakerSearchResult = SpeakerData?.filter({ predicate.evaluateWithObject($0)})
        if SpeakerSearchResult?.count > 0{
            
            SpeakerData = SpeakerSearchResult
            
        }else{
            SpeakerData = nil
        }
        tableView.reloadData()
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(2 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            self.SpeakerData   = self.SpeakerDataCopy
        }
    }
}
