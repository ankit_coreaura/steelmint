//
//  ScheduleViewController.swift
//  Application
//
//  Created by Chandan Singh on 12/17/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit
import CarbonKit
import DCPathButton
import CoreData

class ScheduleViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    var lastButtonPressed :Int?=2
    var eventSchedule:Event?
    var days  = NSMutableArray()
    var filterButton:DCPathButton!
    var arrayOfCategories=NSMutableArray()
    var lastDayBtnClicked = ""
    var dateWiseDict = NSMutableDictionary()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 85.0
        tableView.rowHeight = UITableViewAutomaticDimension
        setupScrollview()
    }

    
    @IBAction func onClickOfLeftBarButton(sender:UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
 
    func setupScrollview(){
        if let eventScheduleData = eventSchedule?.schedules?.array {
        
            let tempArray = NSMutableArray()
            var i = 0
            for objects in eventScheduleData{
               let time = getDateFromTimestamp2(objects.valueForKey("timestamp") as? NSNumber, dateFormat: "dd MMMM")
               
                if !days.containsObject(time){
                if i > 0{
                dateWiseDict.setObject(tempArray.mutableCopy(), forKey: days[i - 1] as! String)
                tempArray.removeAllObjects()
                }
                    days.addObject(time)
                    tempArray.addObject(objects)
                i++
                }else{
                    tempArray.addObject(objects)
                }
               
            }
            if days.count > 0{
            if i == days.count{
             dateWiseDict.setObject(tempArray.mutableCopy(), forKey: days[i - 1] as! String)   
            }
            }
           print(days)
            if days.count>0{
                lastDayBtnClicked = days[0]  as! String
            }
            AppCommonFunctions.sharedInstance.buttonsetup(days,fontSize: 16, scrollView: scrollView, refrence: self)
            self.tableView.reloadData()
        }
        
    }
    // MARK:  TABLE VIEW DELEGTES
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if lastDayBtnClicked != ""{
        if let numberOfRows =  dateWiseDict.objectForKey(lastDayBtnClicked)?.count{
            return numberOfRows
        }
        }
     return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
      var cell = UITableViewCell()
        if indexPath.row%2 == 0
       {
           cell = tableView.dequeueReusableCellWithIdentifier("cell1", forIndexPath:indexPath)
       }else
       {
           cell = tableView.dequeueReusableCellWithIdentifier("cell2", forIndexPath:indexPath)
        }
      
        let neweTittle=(cell.viewWithTag(4060) as! UILabel)
        let newsLabel=(cell.viewWithTag(4061) as! UILabel)
        let newsDateLabel=(cell.viewWithTag(4062) as! UILabel)
        
        
        if let eventScheduleData = (dateWiseDict.objectForKey(lastDayBtnClicked) as? NSArray)?[indexPath.row] as? Schedule{
           
                let time = getDateFromTimestamp2(eventScheduleData.timestamp, dateFormat: "hh")
                neweTittle.text = eventScheduleData.title
            newsLabel.text = eventScheduleData.desc
            print(eventScheduleData.desc)
            print(newsLabel.text)

               newsDateLabel.text = time
        

            
        }
        return cell
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }

    
    

    // MARK:  CATEGORIES CLICKED


    func butonIndexed(sender:UIButton){
        
        var offsetX = sender.frame.origin.x+sender.frame.size.width/2 -  GlobalConstants.SCREEN_WIDTH/2
        if offsetX > scrollView.contentSize.width - GlobalConstants.SCREEN_WIDTH {
            offsetX = scrollView.contentSize.width -  GlobalConstants.SCREEN_WIDTH
        }
        if offsetX < 0 {
            offsetX = 0
        }
        scrollView.setContentOffset(CGPointMake(offsetX, 0), animated: true)
        if let buttonPressed = lastButtonPressed{
            (scrollView.viewWithTag(buttonPressed) as? UIButton)?.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
            (scrollView.viewWithTag(buttonPressed) as? UIButton)?.titleLabel?.font=UIFont.systemFontOfSize(13)
            (scrollView.viewWithTag(buttonPressed+15) as? UIImageView)?.removeFromSuperview()
        }
        lastButtonPressed = sender.tag
        sender.titleLabel?.font=UIFont.boldSystemFontOfSize(14)
        sender.setTitleColor(UIColor(red: 30.0/255.0, green: 136.0/255.0, blue: 229.0/255.0, alpha: 1), forState: .Normal)
        print(sender.currentTitle)
        lastDayBtnClicked = sender.currentTitle!
        self.tableView.reloadData()
        
    }
}
