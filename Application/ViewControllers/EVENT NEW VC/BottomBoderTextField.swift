//
//  BottomBoderTextField.swift
//  Application
//
//  Created by Chandan Singh on 12/20/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit
import Foundation

class BottomBoderTextField: UITextField {
    required init(coder aDecoder: (NSCoder!)) {
        super.init(coder: aDecoder)!
        let bottomLine = CALayer()
        bottomLine.frame = CGRectMake(0.0, self.frame.height - 1, self.frame.width, 1.0)
        bottomLine.backgroundColor = UIColor.lightGrayColor().CGColor
        self.borderStyle = UITextBorderStyle.None
        self.layer.addSublayer(bottomLine)
        
    }
}
