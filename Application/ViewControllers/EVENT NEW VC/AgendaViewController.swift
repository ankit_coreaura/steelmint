//
//  AgendaViewController.swift
//  Application
//
//  Created by Chandan Singh on 12/15/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit

class AgendaViewController: UIViewController {
    var eventDetailData:Event?
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(animated: Bool) {
        
        tableView.reloadData()
        
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onClickOfLeftBarButton(sender:UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell=UITableViewCell()
        cell=tableView.dequeueReusableCellWithIdentifier("c1", forIndexPath: indexPath)
        
        guard let eventsAgenda = eventDetailData else {return UITableViewCell()}
        let  eventTitle = cell.viewWithTag(4020) as! UILabel
        let  eventDiscription = (cell.viewWithTag(4021) as! UILabel)
        //SETTING VALUES
        eventTitle.text = eventsAgenda.valueForKey("name") as? String
        if let agenda = eventsAgenda.valueForKey("agenda") as? String{
            eventDiscription.text = getHTMlRenderedString(agenda).string
        }
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1;
    }
    
}
