//
//  BookTicketViewController.swift
//  Application
//
//  Created by Chandan Singh on 12/18/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit
import CarbonKit
import DCPathButton
import CoreData


class BookTicketViewController: UIViewController {
    var delegatesSelected:Bool?
    var tag:Int?
	var eventId: String?
	var type = "early bird"
	var eventPriceDictionary: NSMutableDictionary?
	var eventPriceEarlyBird: NSMutableDictionary?
	var eventPriceOnSpot: NSMutableDictionary?
	var eventPriceStandard: NSMutableDictionary?
    var curentDataSource:[String]? = [String]()
    var currentDictionary : NSMutableDictionary? = NSMutableDictionary()
    var priceType: String?
    
    var ticketAmounts: [AnyObject]?
    var filterButton:DCPathButton!
    let webservicesInstance=WebServices()
    var arrayOfCategories=NSMutableArray()
    var indicator:UIActivityIndicatorView?
    var tempVIew:UIView?

    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
		  getEventPrice()
        tableView.layer.borderColor = GlobalConstants.APP_THEME_BLUE_COLOR.CGColor
    }
	
	func getEventPrice()
	{
        
        if isInternetConnectivityAvailable(true) == false{
            return
        }
        
        indicator = UIActivityIndicatorView  (activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        indicator!.frame = CGRectMake(GlobalConstants.SCREEN_WIDTH/2-10, GlobalConstants.SCREEN_HEIGHT/2-10, 10.0, 10.0)
        tempVIew = UIView(frame: tableView.frame)
        tempVIew!.backgroundColor = UIColor.whiteColor()
        showNativeActivityIndicator()
        
		let webServiceObj = WebServices()
        //eventId!
        //5/delegate_price
		(view.viewWithTag(8080) as! UISegmentedControl).userInteractionEnabled = false
		webServiceObj.getEventPrice(nil, eventId: eventId) { (responseData) -> () in
            (self.view.viewWithTag(8080) as! UISegmentedControl).userInteractionEnabled = true

            if self.indicator?.isAnimating() == true{
                self.stopActivityIndicator()
            }
			
			self.eventPriceDictionary = (responseData as? NSMutableDictionary)?.valueForKey("data#") as?NSMutableDictionary
            self.eventPriceEarlyBird = (self.eventPriceDictionary?.valueForKey("early_bird") as? NSMutableDictionary)?.valueForKey("delegate") as? NSMutableDictionary
            self.eventPriceOnSpot = (self.eventPriceDictionary?.valueForKey("onspot") as? NSMutableDictionary)?.valueForKey("delegate") as? NSMutableDictionary
			self.eventPriceStandard = (self.eventPriceDictionary?.valueForKey("standard") as?NSMutableDictionary)?.valueForKey("delegate") as? NSMutableDictionary
            self.priceType = (self.eventPriceDictionary?.valueForKey("early_bird") as? NSMutableDictionary)?.objectForKey("p_type") as? String
            self.curentDataSource = self.eventPriceEarlyBird?.allKeys as? [String]
            self.currentDictionary = self.eventPriceEarlyBird
            self.reloadTableView()
            print(responseData)
		}
	}
	
	
    func getContentsToShow()
    {
        
        reloadTableView()
        
	 }
	
    @IBAction func onClickOfLeftBarButton(sender:UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
	
    
    // MARK:  TABLE VIEW DELEGTES
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let numberOfRows = curentDataSource?.count{
            return numberOfRows
   
        }
        return 0
	}
	
	 func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		return 0.01
	 }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell1", forIndexPath:indexPath)
        let delegateLbl: UILabel? = (cell.viewWithTag(4080) as! UILabel)
        let ticketAmountLbl: UILabel? = (cell.viewWithTag(4081) as! UILabel)
		
		let key: String = curentDataSource![indexPath.row]
		   
		
		if let ticketPrice = currentDictionary?.valueForKey(key)
		{
            if priceType?.caseInsensitiveCompare("INR") == NSComparisonResult.OrderedSame{
                if ticketPrice is NSNumber {
                ticketAmountLbl?.text = "₹ " + String(ticketPrice as! NSNumber)
            }else if ticketPrice is String{
                ticketAmountLbl?.text = "₹ " + (ticketPrice as! String)
            }
			
            }else{
                if ticketPrice is NSNumber {
                    ticketAmountLbl?.text = "$ " + String(ticketPrice as! NSNumber)
                }else if ticketPrice is String {
                    ticketAmountLbl?.text = "$ " + (ticketPrice as! String)
                    
                }
            }
        }
			
        delegateLbl?.text = key
        
        (cell.viewWithTag(4082) as! UIButton).setImage(UIImage(named: "radioUnselected"), forState: UIControlState.Normal)
        
        
			return cell
		}
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if delegatesSelected == true{return true}else{
            showAlertController("SteelMint", message: "Please select at least 1 delegate", reference: self)
            return false
        }
    }
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard let tag = self.tag else{return}
        
        let key: String = curentDataSource![tag]
        guard let ticketPrice = currentDictionary?.valueForKey(key) as? Int else{return}
        
        let number = (key as NSString).substringToIndex(1)
        let attendeeObj = segue.destinationViewController as? AttendenceViewController
        attendeeObj?.type = type.stringByReplacingOccurrencesOfString(" ", withString: "_")
        attendeeObj?.eventID = eventId
        attendeeObj?.price = ticketPrice
        attendeeObj?.numberOfTickets = Int(number)!
//		if path?.row == ticketAmounts?.count{
//			//Move to contact us page
//		}
    }
	
    
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		tableView.reloadData()
		let cell = tableView.cellForRowAtIndexPath(indexPath)
        if let cell = cell
        {
          (cell.viewWithTag(4082) as! UIButton).setImage(UIImage(named: "radioSelected"), forState: UIControlState.Normal)
            tag = indexPath.row
        }
        
     delegatesSelected = true
	}
	
	@IBAction func onClickRadioButton(sender:UIButton){
//        tableView.reloadData()
//		sender.selected = (sender.selected == false)
//        
//        var priceType = sender.convertPoint(CGPointZero, toView: self.tableView)
//        let index = tableView.indexPathForRowAtPoint(x)
//        tag = index?.row
		}

    // MARK:  CATEGORIES CLICKED
    
    func reloadTableView()
    {
        self.tableView.reloadData()
    }
    
       @IBAction func segmentBtnEvent(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex{
                    case 0:
                          curentDataSource = eventPriceEarlyBird?.allKeys as? [String]
                          currentDictionary = eventPriceEarlyBird
                          type = "early bird"
                        break
                    case 1:
                        curentDataSource =  eventPriceStandard?.allKeys as? [String]
                        currentDictionary = eventPriceStandard
                        type = "standard"

                        break
                    case 2:
                        curentDataSource =  eventPriceOnSpot?.allKeys as? [String]
                        currentDictionary = eventPriceOnSpot
                        type = "onspot"

                        break
                    default: break
                    
                    }
                    self.reloadTableView()
    }
    //MARK: ACTIVITY INDICATOR
    func showNativeActivityIndicator(){
        self.tableView.allowsSelection = false
        tableView.addSubview(tempVIew!)
        view.addSubview(indicator!)
        indicator!.startAnimating()
    }
    
    func stopActivityIndicator() {
        self.tableView.allowsSelection = true
        indicator!.stopAnimating()
        tempVIew!.removeFromSuperview()
    }
    
}
