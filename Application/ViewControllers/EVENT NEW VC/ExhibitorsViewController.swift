//
//  ExhibitorsViewController.swift
//  Application
//
//  Created by Chandan Singh on 12/16/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit

class ExhibitorsViewController: UIViewController , UITableViewDelegate,UITableViewDataSource{

     @IBOutlet weak var tableView: UITableView!
    var sponsersData:Event?
    let sponsersHeaderTitles = NSMutableArray()
      var sponserCategoryClicked = true
    var sectionWiseData = NSMutableDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
       initialSetup()
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickOfLeftBarButton(sender:UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func initialSetup(){
        if let sponserArray = sponsersData?.sponsors?.array{
            for objects in sponserArray   {
                if  let titleOfsponsers =  objects.valueForKey("sponsors_type") as? String{
                    if !sponsersHeaderTitles.containsObject(titleOfsponsers){
                        sponsersHeaderTitles.addObject(titleOfsponsers)
                    }
                }
            }
            print(sponsersHeaderTitles)
        }
        for sectTitles in sponsersHeaderTitles{
        let sectionPredicate = NSPredicate(format: "sponsors_type = [c] %@", argumentArray: [sectTitles])
           let filteredArray = sponsersData?.sponsors?.filter { sectionPredicate.evaluateWithObject($0) };
            if let filteredData = filteredArray {
            sectionWiseData.setObject(filteredData, forKey: sectTitles as! String)
            }
        }
        print(sectionWiseData)
    }
    
    @IBOutlet weak var btnSponsors: UIButton!
    
    @IBAction func btnSponsorsEvnetAction(sender: UIButton)
    {
        sponserCategoryClicked = true
        sender.backgroundColor = UIColor.whiteColor()
        sender.setTitleColor( GlobalConstants.APP_THEME_BLUE_COLOR, forState: .Normal)
        btnExhibitors.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
        btnExhibitors.backgroundColor =  GlobalConstants.APP_THEME_LIGHTGREY_COLOR
        tableView .reloadData()
    }
    
    
    @IBOutlet weak var btnExhibitors: UIButton!
    
    @IBAction func btnExhibitorsEvnetAction(sender: UIButton)
    {
        sponserCategoryClicked = false
        sender.backgroundColor = UIColor.whiteColor()
        sender.setTitleColor( GlobalConstants.APP_THEME_BLUE_COLOR, forState: .Normal)
        btnSponsors.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
        btnSponsors.backgroundColor =  GlobalConstants.APP_THEME_LIGHTGREY_COLOR
        tableView.reloadData()
        
        
        
    }
    
    
    
     func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if sponserCategoryClicked
        {
            return sponsersHeaderTitles.count
        }else
        {
           return 1
        }
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if sponserCategoryClicked
        {
            if  let numberOfRows = sectionWiseData.objectForKey(sponsersHeaderTitles[section] as! String)?.count{
                return numberOfRows
            }
           
        }
        else
        {
            if let numberOfRows = sponsersData?.exhibitors?.count{
                return numberOfRows
            }
        }
    return 0
    }

     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("c1", forIndexPath: indexPath)
         let  nameTitle = (cell.viewWithTag(4041) as! UILabel)
         let  locationTitle = (cell.viewWithTag(4042) as! UILabel)
         let  companyImageView = (cell.viewWithTag(4040) as! UIImageView)
       companyImageView.image = UIImage(named: "placeholderImage")
        if sponserCategoryClicked{
            guard let sponserData = sectionWiseData.objectForKey(sponsersHeaderTitles[indexPath.section] as! String)?.objectAtIndex(indexPath.row) as? Sponsor else{return UITableViewCell()}
            
            nameTitle.text = sponserData.name
            if nameTitle.text == ""{
                nameTitle.text = "Unavailable"
            }
            locationTitle.text = sponserData.region
            if let url = sponserData.sponser_logo {
                if let urL = NSURL(string: url){
                    companyImageView.sd_setImageWithURL(urL)
                    
                }
            }
            
        }
        else{
            guard let exhibitorsData = sponsersData?.exhibitors?[indexPath.row] as? Exhibitor else{return UITableViewCell()}
            
            nameTitle.text = exhibitorsData.name
            locationTitle.text = exhibitorsData.region
            if let url = exhibitorsData.thumb_img {
                if let urL = NSURL(string: url){
                    companyImageView.sd_setImageWithURL(urL)
  
                }
            }
            
        }
        return cell;
  
      
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 65

    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if sponserCategoryClicked
        {
            return 45
        }else
        {
            return 0
        }

    }
    
     func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        let cell=tableView.dequeueReusableCellWithIdentifier("c2")!
            let  eventTitle = (cell.viewWithTag(4050) as! UILabel)
            eventTitle.text = sponsersHeaderTitles[section] as? String
            //add button on header
            let button   = UIButton(type: UIButtonType.System) as UIButton
            button.frame = CGRectMake(0, 0, 430, 45)
            button.tag = section;
            button.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
            cell.addSubview(button)
            return cell
        
    }
    
    func buttonAction(sender:UIButton!)
    {
        print("Button tapped")
    }

}
