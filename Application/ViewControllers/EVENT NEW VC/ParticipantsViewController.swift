//
//  ParticipantsViewController.swift
//  Application
//
//  Created by Chandan Singh on 12/17/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit
import Parse
import Bolts
import MessageUI

class ParticipantsViewController: UIViewController,searchDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate {
    
    @IBOutlet weak var tableviewTopConstarint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    var tempFilters:NSArray?
    var tempFilterVIew:UIView?
    var indicator:UIActivityIndicatorView?
    var tempVIew:UIView?
    var arrSelected  = NSMutableArray()
    var selectedIndexPath: NSIndexPath? = nil
    var participantsData:[Participant]?
	 var participantsDataCopy: [Participant]?
	 var participantsSearchedResult: [Participant]?
	 var filterVC : FilterViewController?
	 var eventId: String?
    var filteredData:Bool?
    var filterDictArray:NSArray?
    override func viewDidLoad() {
        super.viewDidLoad()
		  participantsDataCopy = participantsData
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 85
        initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
	
	
	func initialSetup(){
		//GETTING PARTICIPANTS filters:
		filterVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("FilterViewController") as? FilterViewController
		
		self.filterVC!.completionBlock = {
			(appliedFilters) -> ()in
			
			if appliedFilters.count>0{
				if appliedFilters["NO_INT"] != nil{
					showAlertController("Steelmint", message: "You cannot use filters in offline mode. Please connect to internet", reference: self)
					return
				}
				if isInternetConnectivityAvailable(false){
					//apply button pressed
                 ///v3/action/events/participants/{event_ID}
					
					self.filterVC?.removeAnimated()
					let params = ["filters": appliedFilters]
					self.setupFilterView()
                    self.showNativeActivityIndicator()
					WebServices().getParticipantsFilteredData(params,eventId:  self.eventId! , completionBlock: { (responseData) -> () in
						print(responseData)
                        self.stopActivityIndicator()
						if let receivedData = responseData?.objectForKey("data#"){
                            self.filteredData = true
						self.filterDictArray = receivedData.objectForKey("participant") as? NSArray
							self.tableView.reloadData()
						}
						
					})
					
				}
				else{
					showAlertController("Steelmint", message: "You cannot use filters in offline mode. Please connect to internet", reference: self)
				}
				//print(appliedFilters)
				
				
			}
			else{
                self.filteredData = false
				self.filterVC?.removeAnimated()
				//clear button pressed
				self.participantsData = self.participantsDataCopy
				self.tableView.reloadData()
				
			}
		}
	
		
		//GETTING participation filters from api:
		guard let Id = self.eventId else {return}
		
		let information = NSMutableDictionary()
		WebServices().getParticipantsFilters(information,eventId: Id,completionBlock: {
			(responseData) -> ()in
			if responseData != nil{
				if self.filterVC!.data.count == 0{
                    if let arr = responseData?.objectForKey("data#") as? NSArray{
                        self.tempFilters = arr
                        self.filterVC!.data = arr
   
                    }
				}
				CacheManager.sharedInstance.saveObject(responseData?.valueForKey("data#"), identifier: "participantFilters")
			}
		})
		
	}

    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filteredData == true{
            if let numberOfRows = filterDictArray?.count{
                return numberOfRows
            }
        }
        if let numberOfRows = participantsData?.count{
            return numberOfRows
        }
        return 0
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
	
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell=tableView.dequeueReusableCellWithIdentifier("c1", forIndexPath: indexPath)
        let  participantImageView = (cell.viewWithTag(4070) as! UIImageView)
        let  participantName = (cell.viewWithTag(4071) as! UILabel)
        let  participantDesignation = (cell.viewWithTag(4072) as! UILabel)
        let  eventVenue = (cell.viewWithTag(4073) as! UILabel)
        participantImageView.image = UIImage(named: "placeholderImage")
        if filteredData == true{
            guard let participantsDict = filterDictArray?[indexPath.row] else{return UITableViewCell()}
            if let participantImageData = participantsDict["thumb_img"] as? String {
                if isNotNull(participantImageData){
                    if let url = NSURL(string: participantImageData){
                        participantImageView.sd_setImageWithURL(url)
                        
                    }else{
                        participantImageView.image = UIImage(named: "placeholderImage")
                        
                    }
                }
                else{
                    participantImageView.image = UIImage(named: "placeholderImage")
                    
                }
            }
            participantName.text = participantsDict["name"] as? String
            
            var designationz = ""
            var companyName = ""
            if let company = participantsDict["company"] as? String{ companyName = company}
            if let designation = participantsDict["designation"] as? String{ designationz = designation}
            eventVenue.text = participantsDict["region"] as? String
            if designationz.characters.count > 1 {
                participantDesignation.text = designationz + ", " + companyName
 
            }else{
               participantDesignation.text = companyName
            }
        }else{
        guard let participantsDict = participantsData?[indexPath.row] else{return UITableViewCell()}
        //  SETTING OUTLETS
        if let participantImageData = participantsDict.thumb_img {
            if isNotNull(participantImageData){
            if let url = NSURL(string: participantImageData){
                participantImageView.sd_setImageWithURL(url)

            }else{
                participantImageView.image = UIImage(named: "placeholderImage")

                }
            }
            else{
                participantImageView.image = UIImage(named: "placeholderImage")
                
            }
        }
        participantName.text = participantsDict.name
        
        var designationz = ""
        var companyName = ""
        if let company = participantsDict.company{ companyName = company}
        if let designation = participantsDict.designation{ designationz = designation}
        eventVenue.text = participantsDict.region
            if designationz.characters.count > 1 {
                participantDesignation.text = designationz + ", " + companyName
                
            }else{
                participantDesignation.text = companyName
            }
       
        }
        return cell
    }
    
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        return 85
//    }
    
    @IBAction func onClickOfLeftBarButton(sender:UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func onClickOfChatButton(sender:UIButton){
        sender.userInteractionEnabled = false
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(3 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            sender.userInteractionEnabled = true
        }
        let x = sender.convertPoint(CGPointZero, toView: tableView)
        let indexpath =  tableView.indexPathForRowAtPoint(x)
        guard let indexPath = indexpath else{sender.userInteractionEnabled = true;return }
        
        if sessionId().characters.count == 0{
            
            
            guard let participant = participantsData?[indexPath.row] else {return}

          let alert = UIAlertController(title: "", message: "Only logged in user can do Chat.", preferredStyle: UIAlertControllerStyle.ActionSheet)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
            let emailAction = UIAlertAction(title: "Email", style: .Default) { (alert: UIAlertAction!) -> Void in
                self.sendEmail(participant.email)
            }
            let messageAction = UIAlertAction(title: "Message", style: .Default) { (alert: UIAlertAction!) -> Void in
                if (MFMessageComposeViewController.canSendText()) {
                    let controller = MFMessageComposeViewController()
                    //controller.body = "Message Body"
                    controller.navigationBar.tintColor = UIColor.whiteColor()
                    controller.recipients = [participant.phoneno ?? "0091-9575100530"]
                    controller.messageComposeDelegate = self
                    sender.userInteractionEnabled = true

                    self.presentViewController(controller, animated: true, completion: nil)
                }
            }
            
            alert.addAction(emailAction)
            if let _ = participant.phoneno {
            alert.addAction(messageAction)
    
            }
            alert.addAction(cancelAction)
            sender.userInteractionEnabled = true

            presentViewController(alert, animated: true, completion: nil)
           
        }
        else{
            
            guard let participant = participantsData?[indexPath.row] else {sender.userInteractionEnabled = true;return}
            let participantEmail = participant.email ?? ""
            if participantEmail.isEmail == false{
               showAlertController("SteelMint", message: "Cannot chat with this user right now. Please try again later", reference: self)
                sender.userInteractionEnabled = true

                return
            }
          
            if isNotNull(PFUser.currentUser()){
                let query = PFQuery(className: GlobalConstants.PF_USER_CLASS_NAME)
                query.whereKey(GlobalConstants.PF_USER_EMAIL, equalTo:participantEmail)
                query.findObjectsInBackgroundWithBlock {(objects,error) -> Void in
                    if error == nil && objects?.count > 0 {
                        let otherUser = objects![0] as! PFUser
                        let chatViewController = ChatViewController()
                        let user1 = PFUser.currentUser()
                        let user2 = otherUser
                        let groupId = Messages.startPrivateChat(user1!, user2: user2)
                        chatViewController.groupId = groupId
                        chatViewController.hidesBottomBarWhenPushed = true
                        chatViewController.titleToshow = user2.username
                        chatViewController.user2 =  user2
                        chatViewController.user2EmailId =  participantEmail
                        //  chatViewController.senderImageUrl = participant.img
                        
                        sender.userInteractionEnabled = true

                        self.navigationController?.pushViewController(chatViewController, animated: true)
                    }
                        
                        
                    else{
                        print("error encountered while chatting")
                        AppCommonFunctions.sharedInstance.signupInParseWith(participantEmail, email: participantEmail)
                        showNotification("Initiating your chat with \(participant.email ?? "") . Please wait.....", showOnNavigation: true, showAsError: false)
                        
                        
                        
                        var notiCheck = false
                        NSNotificationCenter.defaultCenter().addObserverForName("kParseUserSignUp", object: nil, queue: nil, usingBlock: {(notification) -> Void in
                            
                            
                            print("notification for parse chat getting")
                            NSNotificationCenter.defaultCenter().removeObserver(self, name: "kParseUserSignUp", object: nil)
                            
                            
                            if notiCheck == false{
                                print("one time chat done")
                                query.findObjectsInBackgroundWithBlock {(objects,error) -> Void in
                                    
                                    if error == nil && objects?.count > 0 {
                                        let otherUser = objects![0] as! PFUser
                                        let chatViewController = ChatViewController()
                                        let user1 = PFUser.currentUser()
                                        let user2 = otherUser
                                        let groupId = Messages.startPrivateChat(user1!, user2: user2)
                                        chatViewController.groupId = groupId
                                        chatViewController.hidesBottomBarWhenPushed = true
                                        chatViewController.titleToshow = user2.username
                                        chatViewController.user2 =  user2
                                        chatViewController.user2EmailId =  participantEmail
                                        //  chatViewController.senderImageUrl = participant.img
                                        
                                        sender.userInteractionEnabled = true

                                        self.navigationController?.pushViewController(chatViewController, animated: true)
                                    }
                                }
                                
                            }
                            
                            
                            
                            notiCheck = true
                            
                            
                            sender.userInteractionEnabled = true
   
                        })
                        
                    }
                }
            }else{
                showNotification("connecting... try again after some time", showOnNavigation: true, showAsError: false)
                AppCommonFunctions.sharedInstance.prepareUserForParseChat()
                
                
            }
          
        }
        
}
	
	
	/**
	This method is used to search Participants data
	*/
	@IBAction func onClickOfSearch(sender:UIButton){
		if participantsData?.count < 0 {
			return
		}else
		{
			let searchView: SearchView = SearchView()
			searchView.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 64)
			searchView.delegate = self
			self.view.addSubview(searchView)
		}
	}
	
	/**
	This method is used to filter the speaker's data
	*/
	@IBAction func onClickFilter(sender:UIButton){
		if participantsData?.count < 0 {
            showAlertController("", message: "No Filters Available", reference: self)
			return
		}else
		{
            if self.filterVC!.data.count == 0{
                if self.tempFilters != nil && self.tempFilters?.count > 0{
                    self.filterVC!.data = self.tempFilters!
                    filterVC?.showAnimated()
                }else{
                    showAlertController("", message: "No Filters Available", reference: self)
                }
            }else{
                filterVC?.showAnimated()
            }

		}
	}
	
	//MARK: Delegate Mathod of SearchView
	//
	func textForSearch(text: String){
       
		let predicate = NSPredicate(format: "name contains[C] %@ || designation contains[C] %@ || company contains[C] %@ || region contains[C] %@", argumentArray: [text,text,text,text])
        
		participantsSearchedResult = participantsData?.filter({ predicate.evaluateWithObject($0)})
		if participantsSearchedResult?.count > 0{
			participantsData = participantsSearchedResult
			
			}else{
			participantsData = nil
		}
		tableView.reloadData()
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(2 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            self.participantsData   = self.participantsDataCopy
        }
    }

	func cancelCallBack(flag: Bool){
		participantsData = participantsDataCopy
		tableView.reloadData()
	}
	
	func searchDidBegin(serachBar:UISearchBar){
		
	}

    func textForSearchButtonClicked(text: String) {

        let predicate = NSPredicate(format: "name contains[C] %@ || designation contains[C] %@ || company contains[C] %@ || region contains[C] %@", argumentArray: [text,text,text,text])
        participantsSearchedResult = participantsData?.filter({ predicate.evaluateWithObject($0)})
        if participantsSearchedResult?.count > 0{
            participantsData = participantsSearchedResult
            
        }else{
            participantsData = nil
        }
        tableView.reloadData()
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(2 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            self.participantsData   = self.participantsDataCopy
        }
    }
	
    func sendEmail(recipient:String?) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()

            mail.mailComposeDelegate = self
            mail.setToRecipients([recipient ?? "info@steelmint.com"])
            mail.setMessageBody("", isHTML: true)
            mail.navigationBar.tintColor=UIColor.whiteColor()
          
            presentViewController(mail, animated: true, completion: nil)
        } else {
            showAlertController("Steelmint", message: "Email not supported", reference: self)        }
    }
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }

func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
    //... handle sms screen actions
    self.dismissViewControllerAnimated(true, completion: nil)
}
    
    
    func setupFilterView(){
        
        if tempFilterVIew == nil{
            tableviewTopConstarint.constant = 35

            tempFilterVIew = UIView(frame: CGRectMake(0, 64, GlobalConstants.SCREEN_WIDTH,35))
            tempFilterVIew!.backgroundColor = UIColor.whiteColor()
            let label = UILabel(frame: CGRectMake(10, tempFilterVIew!.bounds.minY, 180, 30))
            let clearAllBtn = UIButton(frame: CGRectMake(GlobalConstants.SCREEN_WIDTH-35, tempFilterVIew!.bounds.minY+3, 30, 30))
            clearAllBtn.setImage(UIImage.image(UIImage(named: "cancelImage")!, withColor: UIColor.whiteColor()), forState: .Normal)
            clearAllBtn.addTarget(self, action: "clearFilters", forControlEvents: UIControlEvents.TouchUpInside)
            clearAllBtn.backgroundColor = GlobalConstants.APP_THEME_BLUE_COLOR
            clearAllBtn.layer.cornerRadius = 15.0
            clearAllBtn.layer.borderWidth = 2.0
            clearAllBtn.layer.borderColor = UIColor.whiteColor().CGColor
            clearAllBtn.tintColor = UIColor.whiteColor()
            tempFilterVIew!.addSubview(clearAllBtn)
            tempFilterVIew!.addSubview(label)
            label.text = "Filtered Results:"
            label.font = UIFont.systemFontOfSize(13)
            label.textColor = UIColor.darkGrayColor()
            tempFilterVIew!.layer.borderColor = UIColor.grayColor().CGColor
            tempFilterVIew!.layer.borderWidth = 1.0
            self.view.addSubview(tempFilterVIew!)
        }
    }
    func clearFilters(){
        tableviewTopConstarint.constant = 0

        self.filteredData = false
        self.filterVC?.removeAnimated()
        //clear button pressed
        self.tempFilterVIew?.removeFromSuperview()
        self.tempFilterVIew = nil
        
        filterVC!.cancelBtnEvent(UIButton())
        self.participantsData = self.participantsDataCopy
        self.tableView.reloadData()
    }
    //MARK: ACTIVITY INDICATOR
    func showNativeActivityIndicator(){
        self.tableView.allowsSelection = false
        self.tableView.subviews.forEach { (view) -> () in
            view.userInteractionEnabled = false
        }
        if indicator == nil{
            // Setup ActivityIdicator
            indicator = UIActivityIndicatorView  (activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            indicator?.frame = CGRectMake(GlobalConstants.SCREEN_WIDTH/2-10, GlobalConstants.SCREEN_HEIGHT/2-10, 10.0, 10.0)
            tempVIew = UIView(frame: tableView.frame)
            tempVIew?.backgroundColor = UIColor.whiteColor()
            
            view.addSubview(tempVIew!)
            view.addSubview(indicator!)
        }
        indicator?.startAnimating()
    }
    
    func stopActivityIndicator() {
        self.tableView.allowsSelection = true
        self.tableView.subviews.forEach { (view) -> () in
            view.userInteractionEnabled = true
        }
        indicator?.stopAnimating()
        tempVIew?.removeFromSuperview()
    }
}
