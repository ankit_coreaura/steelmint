//
//  EventDetyailsViewController.swift
//  screens
//
//  Created by Authorised Personnel on 26/10/15.
//  Copyright © 2015 Authorised Personnel. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class EventsDetailsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableView:UITableView!
    var viaPushToScreen:String?
    var indicator = UIActivityIndicatorView()
    var tempview = UIView()

    var eventDatailArray = ["","","Agenda","Speakers","Participants","Schedule","Sponsors/Exhibitors","Book Tickets","Scan visitings Cards"]
    var optionsImages = ["","","agenda","speakers","participants","scheduleEvents","sponsers","bookTicket","scanCard"]
    var eventsDataDict:Event?
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        IQKeyboardManager.sharedManager().enableAutoToolbar = true

        self.navigationController?.navigationBarHidden = true
        UIApplication.sharedApplication().statusBarHidden = false

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        tableView.estimatedRowHeight = 41
        tableView.rowHeight = UITableViewAutomaticDimension

        tableView.registerNib(UINib(nibName: "headerCell", bundle: nil), forCellReuseIdentifier: "headerCell")
        // Do any additional setup after loading the view.
    }
    func initialSetup()
    {
      getEvents()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if eventsDataDict?.isBookmarked == true{
            (view.viewWithTag(880) as? UIButton)?.setImage(UIImage(named: "tag_bookmarked.png") , forState: .Normal)
        }
    }
    @IBAction func onClickOfLeftBarButton(sender:UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func onBookmarkClickBtnEnvent(sender:UIButton){
        if eventsDataDict?.isBookmarked != true{
            eventsDataDict?.isBookmarked = true
            sender.setImage(UIImage(named: "tag_bookmarked.png") , forState: .Normal)
            bookmarkAnimatedView("Bookmarked")
        }
        else{
            eventsDataDict?.isBookmarked = false
            sender.setImage(UIImage(named: "tagT.png") , forState: .Normal)
            
        }
        DatabaseManager.sharedInstance.saveContext()
    }
    
    
    @IBAction func onShareClickBtnEvent(sender:UIButton){
        
        AppCommonFunctions.sharedInstance.share(eventsDataDict?.name, description: eventsDataDict?.agenda, url: eventsDataDict?.img, fromViewController: self)
        
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell=UITableViewCell()
        
        switch indexPath.row
        {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier("headerCell", forIndexPath: indexPath) as! HeaderCell
            cell.titleLabel.text = eventsDataDict?.valueForKey("name") as? String
            return cell
        case 1:
            cell=tableView.dequeueReusableCellWithIdentifier("c2", forIndexPath: indexPath)
            let  eventImage = cell.viewWithTag(94) as! UIImageView
            let  eventTime = cell.viewWithTag(95) as! UILabel
            let  eventVenue = (cell.viewWithTag(96) as! UILabel)
            
            
            if let eventsDetaiDict = eventsDataDict{
                eventTime.text =  eventsDetaiDict.valueForKey("timestamp") as? String
                eventVenue.text = eventsDetaiDict.valueForKey("location") as? String
               
                if let eventsImageData = eventsDetaiDict.valueForKey("img") as? String {
                    if isNotNull(eventsImageData)
                    {
                        if let url = NSURL(string: eventsImageData) {
                            eventImage.sd_setImageWithURL(url)
                        }else{
                            eventImage.image = UIImage(named: "placeholderImage")
                            
                        }
                    }else{
                        eventImage.image = UIImage(named: "placeholderImage")
   
                    }
                }
                
            }
            
       
        default:
            cell=tableView.dequeueReusableCellWithIdentifier("c1", forIndexPath: indexPath)
            let  eventImage = cell.viewWithTag(4010) as! UIImageView
            let  eventTitle = (cell.viewWithTag(4011) as! UILabel)
            //SETTING VALUES
            eventImage.image = UIImage(named: optionsImages[indexPath.row])
            eventTitle.text = eventDatailArray[indexPath.row]
        }
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        var eventDetailInstance:UIViewController!       
        
        switch indexPath.row
        {
        case 2:
          let  eventAgendaInstance = storyboard?.instantiateViewControllerWithIdentifier("AgendaViewController") as! AgendaViewController
        eventAgendaInstance.eventDetailData = eventsDataDict
            eventDetailInstance = eventAgendaInstance
        case 3:
         let   eventSpeakertsInstance = storyboard?.instantiateViewControllerWithIdentifier("SpeakersViewController") as! SpeakersViewController
        
         eventSpeakertsInstance.SpeakerData = eventsDataDict?.speakrs?.array as? [Speaker]
         eventDetailInstance = eventSpeakertsInstance
 
        case 4:
         let   eventParticipantInstance = storyboard?.instantiateViewControllerWithIdentifier("ParticipantsViewController") as! ParticipantsViewController
         print(eventsDataDict)
         print(eventsDataDict?.description)

			eventParticipantInstance.eventId = eventsDataDict?.valueForKey("id") as? String
            eventParticipantInstance.participantsData = eventsDataDict?.participants?.array as? [Participant]
            eventDetailInstance = eventParticipantInstance
        case 5:
          let  eventScheduleInstance = storyboard?.instantiateViewControllerWithIdentifier("ScheduleViewController") as! ScheduleViewController
            eventScheduleInstance.eventSchedule = eventsDataDict
          eventDetailInstance = eventScheduleInstance

        case 6:
          let  eventSponsersInstance = storyboard?.instantiateViewControllerWithIdentifier("ExhibitorsViewController") as! ExhibitorsViewController
            eventSponsersInstance.sponsersData = eventsDataDict
          eventDetailInstance = eventSponsersInstance

        case 7:
         let eventBookInstance = storyboard?.instantiateViewControllerWithIdentifier("BookTicketViewController") as! BookTicketViewController
			 eventBookInstance.eventId = eventsDataDict?.valueForKey("id") as? String
			 eventDetailInstance = eventBookInstance
        case 8:
            let storyBoard = UIStoryboard(name: "Secondary", bundle: nil)
            let eventCardScanner = storyBoard.instantiateViewControllerWithIdentifier("CamCardViewController")
            //self.navigationController?.navigationBarHidden = false
            
            //eventBookInstance.eventId = eventsDataDict?.valueForKey("id") as? String
           eventDetailInstance = eventCardScanner
        default: break
			
		}
        
        if eventDetailInstance != nil
        {
            
            self.navigationController?.pushViewController(eventDetailInstance, animated: true)
            
        }
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if indexPath.row==0
        {
            return UITableViewAutomaticDimension
        }else if indexPath.row == 1{
            return 241
        }
        else
        {
            return 44
        }
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if eventsDataDict?.enable == false{
            return eventDatailArray.count - 2
        }
        return eventDatailArray.count - 1
    }
  
    
    // MARK: EVENTS API HIT FOR DESCRIPTION
    
    func getEvents(){
        if eventsDataDict?.participants?.count == 0{
            // activity indicator setup
            indicator = UIActivityIndicatorView  (activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            indicator.frame = CGRectMake( GlobalConstants.SCREEN_WIDTH/2-10,  GlobalConstants.SCREEN_HEIGHT/2-10, 10.0, 10.0)
            tempview = UIView(frame: tableView.frame)
            tempview.backgroundColor = UIColor.whiteColor()
           showNativeActivityIndicator()
        }
        if isInternetConnectivityAvailable(false) == false{
            if indicator.isAnimating(){
                stopActivityIndicator()
            }
            return
        }
        if  let idEvents = eventsDataDict?.valueForKey("id") as? String{
        WebServices().getEventsIdWise(NSMutableDictionary(),id: idEvents, completionBlock: {
            (responseData) -> ()in
            if self.indicator.isAnimating(){
                self.stopActivityIndicator()
            }
            if responseData != nil{
                if let eventsData = responseData?.valueForKey("data#") as? NSDictionary{
                    DatabaseManager.sharedInstance.addEvents(eventsData)
                    }
                
                // push management
                
                if self.viaPushToScreen != nil{
                    
                    switch self.viaPushToScreen!
                    {
                    case "agenda":
                        let  eventAgendaInstance = self.storyboard?.instantiateViewControllerWithIdentifier("AgendaViewController") as! AgendaViewController
                        eventAgendaInstance.eventDetailData = self.eventsDataDict
                        self.viaPushToScreen = nil
                        self.navigationController?.pushViewController(eventAgendaInstance, animated: true)
                    case "speakers":
                        let   eventSpeakertsInstance = self.storyboard?.instantiateViewControllerWithIdentifier("SpeakersViewController") as! SpeakersViewController
                        
                        eventSpeakertsInstance.SpeakerData = self.eventsDataDict?.speakrs?.array as? [Speaker]
                        self.viaPushToScreen = nil

                        self.navigationController?.pushViewController(eventSpeakertsInstance, animated: true)
                        
                    case "sponsers":
                        let  eventSponsersInstance = self.storyboard?.instantiateViewControllerWithIdentifier("ExhibitorsViewController") as! ExhibitorsViewController
                        eventSponsersInstance.sponsersData = self.eventsDataDict
                        self.viaPushToScreen = nil

                        self.navigationController?.pushViewController(eventSponsersInstance, animated: true)
                    case "participants":
                        let   eventParticipantInstance = self.storyboard?.instantiateViewControllerWithIdentifier("ParticipantsViewController") as! ParticipantsViewController
                        eventParticipantInstance.eventId = self.eventsDataDict?.valueForKey("id") as? String
                        eventParticipantInstance.participantsData = self.eventsDataDict?.participants?.array as? [Participant]
                        self.viaPushToScreen = nil
                        self.navigationController?.pushViewController(eventParticipantInstance, animated: true)
                    default: break
                    }}
                
                }
            
        })
        }
    }
    
    //MARK: ACTIVITY INDICATOR
    func showNativeActivityIndicator(){
        
        view.addSubview(tempview)
        view.addSubview(indicator)
        indicator.startAnimating()
    }
    
    func stopActivityIndicator() {
        indicator.stopAnimating()
        tempview.removeFromSuperview()
    }
    
    func bookmarkAnimatedView(str:String?){
        let bookmarkLabel = UILabel()
                bookmarkLabel.frame = CGRect(x: (GlobalConstants.SCREEN_WIDTH/2)-50, y: (GlobalConstants.SCREEN_HEIGHT)-80, width: 110, height: 20)

        bookmarkLabel.text = str
        bookmarkLabel.textColor = UIColor.whiteColor()
        bookmarkLabel.backgroundColor = GlobalConstants.APP_THEME_BLUE_COLOR
        bookmarkLabel.layer.cornerRadius = 5.0
        bookmarkLabel.clipsToBounds = true
        bookmarkLabel.textAlignment = .Center
        self.view.addSubview(bookmarkLabel)
        bookmarkLabel.layer.opacity = 0
        UIView.animateWithDuration(0.5) { () -> Void in
            bookmarkLabel.layer.opacity = 1.0
        }
        
        dispatch_after(1, dispatch_get_main_queue()) { () -> Void in
            UIView.animateWithDuration(1.2) { () -> Void in
                bookmarkLabel.layer.opacity = 0
            }
        }
        
    }
}
