//
//  EventsViewController.swift
//  screens
//
//  Created by Authorised Personnel on 26/10/15.
//  Copyright © 2015 Authorised Personnel. All rights reserved.
//

import UIKit

class EventViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var viaPushId:String?
    var viaPushToScreen:String?


    var refreshControlDefault = UIRefreshControl()
    @IBOutlet weak var tableView: UITableView!
    
    var upcomingevents = NSMutableArray()
    var passedevents = NSMutableArray()
    var highestEventsId = 0
    var eventDataArray:NSArray?
    var upcomingClicked = true
    var expiredClicked = true
    var indicator = UIActivityIndicatorView()
    var tempview = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPullToRefresh()
        initialSetupCheck()
        getEvents()
        tableView.estimatedRowHeight = 85
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    func initialSetupCheck(){
        //OFFLINE HANDLER
        getContentsFromDB()
        // Managing Last Events ID For api request
        if let offlineData = DatabaseManager.sharedInstance.getAllEventsId() {
            for objects in offlineData{
                if let id = objects["id"] as? String{
                    let val = NSNumberFormatter().numberFromString(id)!.integerValue
                    if val > highestEventsId{
                        highestEventsId = val
                    }
                }
            }
        }
  
    }
    @IBAction func onClickOfLeftBarButton(sender:UIButton){
        openLeft()
    }
    
    
    @IBOutlet weak var btnUpcomingEvnet: UIButton!
    
    @IBAction func btnUpcomingEvnetAction(sender: UIButton) {
        if upcomingevents.count>0{
        if upcomingClicked{
        sender.setTitleColor( GlobalConstants.APP_THEME_BLUE_COLOR, forState: .Normal)
        sender.backgroundColor = UIColor.whiteColor()
        btnPastEvnet.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
        btnPastEvnet.backgroundColor =  GlobalConstants.APP_THEME_LIGHTGREY_COLOR
        eventDataArray = upcomingevents
           

        UIView.transitionWithView(self.tableView, duration: 0.5, options: UIViewAnimationOptions.TransitionFlipFromLeft, animations: { () -> Void in
            
           
            
            }, completion: nil)

        tableView.reloadData()
        upcomingClicked = false
            expiredClicked = true
        }
        }
    }
    
    
    @IBOutlet weak var btnPastEvnet: UIButton!
    
    @IBAction func btnPastEvnetAction(sender: UIButton)
    {
        if passedevents.count>0{
        if expiredClicked{
        sender.backgroundColor = UIColor.whiteColor()
        sender.setTitleColor( GlobalConstants.APP_THEME_BLUE_COLOR, forState: .Normal)
        btnUpcomingEvnet.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
        btnUpcomingEvnet.backgroundColor =  GlobalConstants.APP_THEME_LIGHTGREY_COLOR
        eventDataArray = passedevents
        UIView.transitionWithView(self.tableView, duration: 0.5, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
            }, completion: nil)
        tableView.reloadData()
            upcomingClicked = true

        expiredClicked = false
        }
        }
    }
    
    
    func setupPullToRefresh(){
        self.refreshControlDefault.addTarget(self, action: "updateContents", forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControlDefault)
        
        // activity indicator setup
        indicator = UIActivityIndicatorView  (activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        indicator.frame = CGRectMake( GlobalConstants.SCREEN_WIDTH/2-10,  GlobalConstants.SCREEN_HEIGHT/2-10, 10.0, 10.0)
        tempview = UIView(frame: tableView.frame)
        tempview.backgroundColor = UIColor.whiteColor()
        
    }
    
    func updateContents(){
        NSObject.cancelPreviousPerformRequestsWithTarget(self)
        performSelector("getEvents", withObject: nil, afterDelay: 1)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
   
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell=tableView.dequeueReusableCellWithIdentifier("c1", forIndexPath: indexPath)
        //  CONNECTING OUTLETS
        let eventTitle = (cell.viewWithTag(4002) as! UILabel)
        let  eventDate = (cell.viewWithTag(4003) as! UILabel)
        let  eventVenue = (cell.viewWithTag(4004) as! UILabel)
        let eventsImageView = cell.viewWithTag(4001) as! UIImageView
        //  SETTING OUTLETS
        if let eventsData = eventDataArray?[indexPath.row] as? Event{
//
//          let fromdateString =  getDateFromTimestamp2(eventsData.valueForKey("timestamp") as? NSNumber ,dateFormat: "dd MMM YYYY")
//            

            eventTitle.text = eventsData.valueForKey("name") as? String
            eventVenue.text = eventsData.valueForKey("location") as? String
            eventDate.text = eventsData.valueForKey("timestamp") as? String
            if let eventsImageData = eventsData.valueForKey("thumb_img") as? String {
                if isNotNull(eventsImageData)
                {
                    
                    if let urlThere = NSURL(string: eventsImageData){
                        eventsImageView.sd_setImageWithURL(urlThere)}else{
                        eventsImageView.image = UIImage(named: "placeholderImage")
                    }
                    
                }else{
                    eventsImageView.image = UIImage(named: "placeholderImage")
 
                }
            }
        }
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let numberOfRows = eventDataArray {
            return numberOfRows.count
        }
        else
        {
            return 0
        }
    }
        
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let eventDetailInstance = storyboard?.instantiateViewControllerWithIdentifier("EventDetailsViewController") as! EventsDetailsViewController
        if let passdict = eventDataArray{
            eventDetailInstance.eventsDataDict = passdict[indexPath.row] as? Event
            self.navigationController?.pushViewController(eventDetailInstance, animated: true)
        }
    }
    
    // MARK: EVENTS DB MANAGEMENT
    
    func getContentsFromDB(){
        
        // Managing Last Events ID For api request
        if let offlineData = DatabaseManager.sharedInstance.getAllEventsId() {
            for objects in offlineData{
                if let id = objects["id"] as? String{
                    let val = NSNumberFormatter().numberFromString(id)!.integerValue
                    if val > highestEventsId{
                        highestEventsId = val
                    }
                }
            }
        }
        
        guard let offlineData = DatabaseManager.sharedInstance.getAllEvents() else{
                    if indicator.isAnimating() == false{
                showNativeActivityIndicator()
            }
            return
        }
        passedevents.removeAllObjects()
        upcomingevents.removeAllObjects()
        
        for objects in offlineData{
            if let dateToCompare = objects.valueForKey("enable") as? Bool{
                
                if dateToCompare
                    {
                        upcomingevents.addObject(objects)
                    }
                    else{
                    passedevents.addObject(objects)

                    }
        }
    
        eventDataArray = upcomingevents
        if !expiredClicked{
          eventDataArray = passedevents
        }
        reloadTableView()
        }
       
        if let via = self.viaPushId {
            if offlineData.count > 0 {
            for index in  offlineData{
                    if let passdict = index as? Event {
                        if via == passdict.id {
                        
                            let eventDetailInstance = storyboard?.instantiateViewControllerWithIdentifier("EventDetailsViewController") as! EventsDetailsViewController
                            eventDetailInstance.viaPushToScreen = viaPushToScreen
                            eventDetailInstance.eventsDataDict = passdict
                            
                            self.viaPushToScreen = nil
                            self.viaPushId = nil
                            
                            navigationController?.pushViewController(eventDetailInstance, animated: true)
                        
                        }
               
                        
                    }
                    
                
                
                }}
        }
    }
    
    
    // MARK: EVENTS API HIT
    
    func getEvents(){
        
        if isInternetConnectivityAvailable(true)==false {
            
            self.refreshControlDefault.endRefreshing()
            if indicator.isAnimating(){
                stopActivityIndicator()
            }
            return;
        }
        //TEMPORARY POSTFROM IS DISABLED
        //let information = NSMutableDictionary(object: "new:\(highestEventsId)", forKey: "postfrom")
        WebServices().getEvents(NSDictionary(), completionBlock: {
            (responseData) -> ()in
            self.refreshControlDefault.endRefreshing()
            if self.indicator.isAnimating(){
                self.stopActivityIndicator()
            }
            if responseData != nil{
                if let eventsData = responseData?.valueForKey("data#") as? NSArray{
                    DatabaseManager.sharedInstance.deleteAllEvents()
                    for objects in eventsData{
                        if objects is NSDictionary{
                            DatabaseManager.sharedInstance.addEvents(objects as! NSDictionary)
                        }
                    }
                    if eventsData.count>0{
                    self.getContentsFromDB()
                    }
                }
                
            }
        })
    }
	
    func reloadTableView(){
        tableView.reloadData()
    }
    
    
    //MARK: ACTIVITY INDICATOR
    func showNativeActivityIndicator(){
        tableView.allowsSelection = false
        view.addSubview(tempview)
        view.addSubview(indicator)
        indicator.startAnimating()
    }
    
    func stopActivityIndicator() {
      
        tableView.allowsSelection = true
        indicator.stopAnimating()
        tempview.removeFromSuperview()
    }
    // MARK: Push Management
    func pushNotificationManagement(){
        if let _ = viaPushId{
          //  UIApplication.sharedApplication().beginIgnoringInteractionEvents()
            getEvents()
        }
    }
}
