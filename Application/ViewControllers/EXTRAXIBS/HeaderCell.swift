//
//  HeaderCell.swift
//  Application
//
//  Created by Ankit Nandal on 15/01/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//

import UIKit

class HeaderCell: UITableViewCell {

    @IBOutlet weak var titleLabel:UILabel!

}
