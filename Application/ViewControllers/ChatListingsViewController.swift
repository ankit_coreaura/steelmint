//
//  ChatListingsViewController.swift
//  Application
//
//  Created by Ankit Nandal on 05/04/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//

import UIKit
import Parse

class ChatListingsViewController: UIViewController {
    @IBOutlet weak var tableView:UITableView!
    var arrayOfParseUsers = NSMutableArray()
    var viaPush:Bool?
    var emailViaPush:String?
    var indicator = UIActivityIndicatorView()
    var tempVIew = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.layer.borderColor = GlobalConstants.APP_THEME_BLUE_COLOR.CGColor
        tableView.layer.borderWidth = 1
        indicator = UIActivityIndicatorView  (activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        indicator.frame = CGRectMake(GlobalConstants.SCREEN_WIDTH/2-10, GlobalConstants.SCREEN_HEIGHT/2-10, 10.0, 10.0)
        tempVIew = UIView(frame: tableView.frame)
        tempVIew.backgroundColor = UIColor.whiteColor()
        getObjects()
        // Setup ActivityIdicator
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func getObjects(){
        if indicator.isAnimating() == false{
            showNativeActivityIndicator()
        }
        if  CacheManager.sharedInstance.loadObject("ChatUsers") != nil{
        self.arrayOfParseUsers = CacheManager.sharedInstance.loadObject("ChatUsers")! as! NSMutableArray
            tableView.reloadData()
        }
        if isNotNull(PFUser.currentUser()){
            let query = PFQuery(className: "Messages")
            query.whereKey("groupId", containsString: PFUser.currentUser()!.objectId!)
            query.findObjectsInBackgroundWithBlock {(objects,error) -> Void in
                if error == nil && objects?.count > 0 {
                    if self.indicator.isAnimating() == true{
                        self.stopActivityIndicator()
                    }
                    self.arrayOfParseUsers.removeAllObjects()
                    for users in objects!{
                        
                        if users["description"] as! String == PFUser.currentUser()!.username!{
                            continue
                        }
                      //  let dict = NSDictionary(objects: [users["user"] as! PFUser,(users["description"] as! String)], forKeys: ["user","name"])
                        
                        
                        self.arrayOfParseUsers.addObject(users["description"] as! String)
                    }
                    CacheManager.sharedInstance.saveObject(self.arrayOfParseUsers, identifier: "ChatUsers")
                    
                    if self.viaPush == true{
                      self.pushManagement(self.emailViaPush)
                    }
                   self.tableView.reloadData()
                }
                    
                    
                else{
                    
                    
                   showAlertController("Steelmint", message: "Unable to chat right now. Please try again later.", reference: self)
                }
            }
        }else{
            if self.indicator.isAnimating() == true{
                self.stopActivityIndicator()
            }
        }
    }
    // MARK:  TABLE VIEW DELEGTES
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayOfParseUsers.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("c1", forIndexPath: indexPath)
        let Username = cell.viewWithTag(287) as! UILabel
        
        // SETTING OUTLETS:
        let nameOfUser = arrayOfParseUsers[indexPath.row] as? String//(arrayOfParseUsers[indexPath.row] as? NSDictionary)?["name"] as? String
        
         if nameOfUser == "info@steelmint.com"{
            Username.text = "Customer Care"
         }else{
            var str = ""
            for char in nameOfUser!.characters{
                if char == "@"{
                    break
                }
                str = str + "\(char)"
            }
            Username.text = str
        }
       
        Username.text = Username.text?.capitalizedString

        return cell
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.allowsSelection = false
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(4 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            tableView.allowsSelection = true
        }
        
       
        if  let otherUserId = (arrayOfParseUsers[indexPath.row] as? String){ //(arrayOfParseUsers[indexPath.row] as? NSDictionary)?["name"] as? String{
            
            if isNotNull(PFUser.currentUser()){
                let query = PFQuery(className: GlobalConstants.PF_USER_CLASS_NAME)
                query.whereKey(GlobalConstants.PF_USER_EMAIL, equalTo:otherUserId)
                query.findObjectsInBackgroundWithBlock {(objects,error) -> Void in
                    if error == nil && objects?.count > 0 {
                        let otherUser = objects![0] as! PFUser
                        let chatViewController = ChatViewController()
                        let user1 = PFUser.currentUser()
                        let user2 = otherUser
                        let groupId = Messages.startPrivateChat(user1!, user2: user2)
                        chatViewController.groupId = groupId
                        chatViewController.hidesBottomBarWhenPushed = true
                        chatViewController.titleToshow = user2.username
                        chatViewController.user2 =  user2
                        chatViewController.user2EmailId =  otherUserId
                        //  chatViewController.senderImageUrl = participant.img
                        
                        
                        self.navigationController?.pushViewController(chatViewController, animated: true)
                    }
                        
                        
                    else{
                        print("error encountered while chatting")
                        AppCommonFunctions.sharedInstance.signupInParseWith(otherUserId, email: otherUserId)
                        showNotification("Initiating your chat with Customer Care . Please wait.....", showOnNavigation: true, showAsError: false)
                        
                        var notiCheck = false
                        NSNotificationCenter.defaultCenter().addObserverForName("kParseUserSignUp", object: nil, queue: nil, usingBlock: {(notification) -> Void in
                            
                            
                            print("notification for parse chat getting")
                            NSNotificationCenter.defaultCenter().removeObserver(self, name: "kParseUserSignUp", object: nil)
                            
                            
                            if notiCheck == false{
                                print("one time chat done")
                                query.findObjectsInBackgroundWithBlock {(objects,error) -> Void in
                                    
                                    if error == nil && objects?.count > 0 {
                                        let otherUser = objects![0] as! PFUser
                                        let chatViewController = ChatViewController()
                                        let user1 = PFUser.currentUser()
                                        let user2 = otherUser
                                        let groupId = Messages.startPrivateChat(user1!, user2: user2)
                                        chatViewController.groupId = groupId
                                        chatViewController.hidesBottomBarWhenPushed = true
                                        chatViewController.titleToshow = user2.username
                                        chatViewController.user2 =  user2
                                        chatViewController.user2EmailId =  otherUserId
                                        //  chatViewController.senderImageUrl = participant.img
                                        
                                       
                                        self.navigationController?.pushViewController(chatViewController, animated: true)
                                    }
                                }
                                
                            }
                            
                            
                            
                            notiCheck = true
                            
                            
                            
                        })
                        
                    }
                }
            }else{
                showNotification("connecting... try again after some time", showOnNavigation: true, showAsError: false)
                AppCommonFunctions.sharedInstance.prepareUserForParseChat()
                
                
            }
        }else{
            showAlertController("Steelmint", message: "Temporary unable to chat with this user", reference: self)
        }

        
    }
    @IBAction func openLeftMenu(sender:UIButton){
    openLeft()
    }
    func pushManagement(email:String?){
        if  let otherUserID = email{
            
            if isNotNull(PFUser.currentUser()){
                let query = PFQuery(className: GlobalConstants.PF_USER_CLASS_NAME)
                query.whereKey(GlobalConstants.PF_USER_EMAIL, equalTo:otherUserID)
                query.findObjectsInBackgroundWithBlock {(objects,error) -> Void in
                    if error == nil && objects?.count > 0 {
                        let otherUser = objects![0] as! PFUser
                        let chatViewController = ChatViewController()
                        let user1 = PFUser.currentUser()
                        let user2 = otherUser
                        let groupId = Messages.startPrivateChat(user1!, user2: user2)
                        chatViewController.groupId = groupId
                        chatViewController.hidesBottomBarWhenPushed = true
                        chatViewController.titleToshow = user2.username
                        chatViewController.user2 =  user2
                        chatViewController.user2EmailId =  otherUserID
                        //  chatViewController.senderImageUrl = participant.img
                        
                        
                        self.navigationController?.pushViewController(chatViewController, animated: true)
                    }
                        
                        
                    else{
                        print("error encountered while chatting")
                        AppCommonFunctions.sharedInstance.signupInParseWith(otherUserID, email: otherUserID)
                        showNotification("Initiating your chat with Customer Care . Please wait.....", showOnNavigation: true, showAsError: false)
                        
                        var notiCheck = false
                        NSNotificationCenter.defaultCenter().addObserverForName("kParseUserSignUp", object: nil, queue: nil, usingBlock: {(notification) -> Void in
                            
                            
                            print("notification for parse chat getting")
                            NSNotificationCenter.defaultCenter().removeObserver(self, name: "kParseUserSignUp", object: nil)
                            
                            
                            if notiCheck == false{
                                print("one time chat done")
                                query.findObjectsInBackgroundWithBlock {(objects,error) -> Void in
                                    
                                    if error == nil && objects?.count > 0 {
                                        let otherUser = objects![0] as! PFUser
                                        let chatViewController = ChatViewController()
                                        let user1 = PFUser.currentUser()
                                        let user2 = otherUser
                                        let groupId = Messages.startPrivateChat(user1!, user2: user2)
                                        chatViewController.groupId = groupId
                                        chatViewController.hidesBottomBarWhenPushed = true
                                        chatViewController.titleToshow = user2.username
                                        chatViewController.user2 =  user2
                                        chatViewController.user2EmailId =  otherUserID
                                        //  chatViewController.senderImageUrl = participant.img
                                        
                                        
                                        self.navigationController?.pushViewController(chatViewController, animated: true)
                                    }
                                }
                                
                            }
                            
                            
                            
                            notiCheck = true
                            
                            
                            
                        })
                        
                    }
                }
            }else{
                showNotification("connecting... try again after some time", showOnNavigation: true, showAsError: false)
                AppCommonFunctions.sharedInstance.prepareUserForParseChat()
                
                
            }
        }else{
            showAlertController("Steelmint", message: "Temporary unable to chat with this user", reference: self)
        }
    }
    //MARK: ACTIVITY INDICATOR
    func showNativeActivityIndicator(){
        self.tableView.allowsSelection = false
         view.addSubview(tempVIew)
        view.addSubview(indicator)
        indicator.startAnimating()
    }
    
    func stopActivityIndicator() {
       
        self.tableView.allowsSelection = true
        indicator.stopAnimating()
        tempVIew.removeFromSuperview()

    }
}
