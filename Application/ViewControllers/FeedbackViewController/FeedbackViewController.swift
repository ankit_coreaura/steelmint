//
//  SubscribeViewController.swift
//  SteelMint
//
//  Created by Ankit Nandal on 03/11/15.
//  Copyright © 2015 Saurabh Singh. All rights reserved.
//

import UIKit
import SCLAlertView

class FeedbackViewController: UIViewController,UITextViewDelegate {
    //CONNECTING OUTLETS
    
    @IBOutlet weak var nameTextField: UITextField!

    @IBOutlet weak var feedbackTextView: HolderTextView!
 
    //END
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextField.layer.borderColor=UIColor.darkGrayColor().CGColor
        feedbackTextView.placeHolder="Type here..."
        // Do any additional setup after loading the view.
    }

        @IBAction func onClickOfLeftBarButton(sender:UIButton){
                openLeft()
        }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

        
    @IBAction func submitBtnEvent(sender: UIButton) {
        let information = NSMutableDictionary()

        
        if nameTextField.text!.isEmpty || feedbackTextView.text!.isEmpty {
            showAlertController("SteelMint", message: "Please complete all the details", reference: self)
            
        }
        else{
                information.setObject("feedback", forKey: "type")
                information.setObject(nameTextField.text!, forKey: "name")
                information.setObject(feedbackTextView.text!, forKey: "message")
                
                let webServicesObject = WebServices()
                webServicesObject.progresssIndicatorText = "Getting your feedback.."
                webServicesObject.feedback(information, completionBlock:
                    {
                        (responseData) -> () in
                        
                        print(responseData)
                        if let response = responseData{
                            print(response.valueForKey("data#"))
                            
                            showAlertDifferentType("Thankyou for your feedback")
                            
                            
                             self.slideMenuController()?.changeMainViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("TabController"), close: true)
                        }
                        else{
                            let alertView = SCLAlertView()
                            
                            alertView.showTitle("SteelMint", subTitle: "Network Error", style: SCLAlertViewStyle.Error, closeButtonTitle: "Cancel", duration: 20, colorStyle: 0xE4E4E4, colorTextButton: 0xFFFFFF)
                        }
                })

        
        }
   }
}
