//
//  SingleLabelTableViewCell.swift
//  
//
//  Created by Alok Kumar Singh on 28/11/15.
//
//

import Foundation
import UIKit

class SingleLabelTableViewCell : UITableViewCell {
    var isInitialisedOnce = false
    @IBOutlet var titleLabel : UILabel?
    @IBOutlet var checkBox : UIButton?
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}
