//
//  FilterViewController.swift
//  Application
//
//  Created by Ankit Nandal on 19/11/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit
import RATreeView

typealias FVCCompletionBlock = (appliedFilters :NSDictionary) ->()

class FilterViewController: UIViewController,RATreeViewDataSource,RATreeViewDelegate {
    var treeView : RATreeView?
    var data =  NSArray()
    var completionBlock : FVCCompletionBlock?
    @IBOutlet var filterView:UIView?
    @IBOutlet var leadingConstraintFilterView:NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTreeView()
        self.setupLevelIntoTheData(self.data,level:0)
        self.treeView?.reloadData()
    }
    
    func setupLevelIntoTheData(data:NSArray,level:Int){
        for obj in data {
            if let objAgain = obj as? NSMutableDictionary {
                objAgain.setObject("\(level)", forKey: "level")
                if let subContents = objAgain.objectForKey("sub_filter"){
                    self.setupLevelIntoTheData(subContents as! NSArray,level:level+1)
                }
            }
        }
    }
    
    func resetSelectionIntoTheData(data:NSArray){
        for obj in data {
            if let objAgain = obj as? NSMutableDictionary {
                objAgain.removeObjectForKey("isSelected")
                if let subContents = objAgain.objectForKey("sub_filter"){
                    self.resetSelectionIntoTheData(subContents as! NSArray)
                }
            }
        }
    }
    
    func setupTreeView() -> Void {
        treeView = RATreeView(frame:CGRectMake(0,40,(filterView?.frame.size.width)!, (filterView?.frame.size.height)!-40))
        leadingConstraintFilterView?.constant = self.view.frame.size.width/3
        treeView!.registerNib(UINib.init(nibName: "SingleLabelTableViewCell", bundle: nil), forCellReuseIdentifier: "SingleLabelTableViewCell")
        treeView!.autoresizingMask = UIViewAutoresizing(rawValue:UIViewAutoresizing.FlexibleWidth.rawValue | UIViewAutoresizing.FlexibleHeight.rawValue)
        treeView!.delegate = self;
        treeView!.dataSource = self;
        treeView!.treeFooterView = UIView()
        treeView!.backgroundColor = UIColor.whiteColor()
        filterView!.layer.cornerRadius = 3.0
        filterView!.layer.borderColor = UIColor.lightGrayColor().CGColor
        filterView!.layer.borderWidth = 0.5
        filterView!.layer.shadowColor = UIColor.blackColor().CGColor
        filterView!.layer.shadowOpacity = 0.8
        filterView!.layer.shadowRadius = 3.0
        filterView!.layer.shadowOffset = CGSizeMake(2.0, 2.0)
        filterView!.addSubview(treeView!)
    }
    
    @IBAction func cancelBtnEvent(sender: UIButton) {
        resetSelectionIntoTheData(self.data)
        treeView?.reloadData()
        if completionBlock != nil {
            completionBlock!(appliedFilters: NSDictionary())
        }
    }
    
    @IBAction func applyBtnEvent(sender: UIButton) {
        if isInternetConnectivityAvailable(false){
            let seperator = ";"
            let appliedFiltersArray = NSMutableArray()
            let finalArray = NSMutableArray()
            getSelectedfiltersIntoTheArray(appliedFiltersArray,filterName: "initial", currentArray:data)
            
            for (_,object) in appliedFiltersArray.enumerate()
            {
                var shouldAdd = true
                for objectNew in finalArray
                {
                    let key = ((objectNew as! NSDictionary).allKeys).first as! String
                    if key == ((object as! NSDictionary).allKeys).first as! String
                    {
                        shouldAdd = false
                        let updateInValue = "\(((object as! NSDictionary).allValues).first!)\(seperator)\(((objectNew as! NSDictionary).allValues).first!)"
                        (objectNew as! NSMutableDictionary).setObject(updateInValue, forKey:key)
                        print(updateInValue)
                    }
                }
                if shouldAdd
                {
                    finalArray.addObject(object)
                }
            }
            
            if completionBlock != nil
            {
                let filteredDict=NSMutableDictionary()
                for index in finalArray{
                    let filter = (index as! NSDictionary).allKeys[0] as? String
                    filteredDict.setObject(index[filter!] as! String, forKey: filter!)
                    
                }
                completionBlock!(appliedFilters:filteredDict)
            }
   
        }else{
            completionBlock!(appliedFilters:NSDictionary(object: "no connection", forKey: "NO_INT"))
            resetSelectionIntoTheData(self.data)
            treeView?.reloadData()
        }
        removeAnimated()
    }
    
    func getSelectedfiltersIntoTheArray(selectedfilters:NSMutableArray,filterName:NSString,currentArray:NSArray){
        for obj in currentArray {
            if let objAgain = obj as? NSMutableDictionary {
                if objAgain.objectForKey("isSelected") as? String == "1" {
                    if filterName == "initial" {
                        selectedfilters.addObject([objAgain.objectForKey("filter")! as! String: "true"] as NSMutableDictionary)
                    }else{
                        selectedfilters.addObject([filterName:objAgain.objectForKey("filter")! as! String] as NSMutableDictionary)
                    }
                }
                if let subContents = objAgain.objectForKey("sub_filter"){
                    getSelectedfiltersIntoTheArray(selectedfilters,filterName:objAgain.objectForKey("filter")! as! String, currentArray:subContents as! NSArray)
                }
            }
        }
    }
    
    
    //MARK: RATreeView data source
    func treeView(treeView: RATreeView, numberOfChildrenOfItem item: AnyObject?) -> Int {
        if let item = item as? NSDictionary {
            if let _ = item.objectForKey("sub_filter"){
                return ((item.objectForKey("sub_filter") as? NSArray)?.count)!
            }
            else{
                return 0
            }
        } else {
            return self.data.count
        }
    }
    
    func treeView(treeView: RATreeView, child index: Int, ofItem item: AnyObject?) -> AnyObject {
        if let item = item as? NSDictionary {
            return (item.objectForKey("sub_filter") as? NSArray)![index]
        } else {
            return data[index] as AnyObject
        }
    }
    
    func treeView(treeView: RATreeView, cellForItem item: AnyObject?) -> UITableViewCell {
        let cell = treeView.dequeueReusableCellWithIdentifier("SingleLabelTableViewCell") as! SingleLabelTableViewCell
        let item = item as! NSDictionary
        let level = item.objectForKey("level")?.intValue
		if level == 0{
			cell.titleLabel?.text = "\(item.objectForKey("filter")!)"
            cell.titleLabel?.textColor = UIColor.blackColor()
			cell.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.3)
			
		}
		else if level == 1{
			cell.titleLabel?.text = "    \(item.objectForKey("filter")!)"
            cell.titleLabel?.textColor = UIColor.darkGrayColor()

			cell.backgroundColor = UIColor(red: 238.0/255, green: 238.0/255.0, blue: 238.0/255, alpha: 1.0)
		}
		else if level == 2{
			cell.titleLabel?.text = "       \(item.objectForKey("filter")!)"
			cell.backgroundColor = UIColor.whiteColor()
		}
        cell.titleLabel?.text = cell.titleLabel?.text?.stringByReplacingOccurrencesOfString("_", withString: " ")
        cell.titleLabel?.text = cell.titleLabel?.text?.capitalizedString
        cell.selectionStyle = .None
        if let _ = item.objectForKey("sub_filter"){
            cell.accessoryType=UITableViewCellAccessoryType.DisclosureIndicator
            cell.checkBox?.hidden = true
            cell.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.3)
        }else{
            cell.checkBox?.hidden = false
            cell.backgroundColor = UIColor.whiteColor()
            if level==0{
                cell.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.3)
            }
        if (item as! NSMutableDictionary).objectForKey("isSelected") as? String == "1" {
                cell.checkBox?.selected = true
            }else{
                cell.checkBox?.selected = false
            }
            cell.accessoryType=UITableViewCellAccessoryType.None

        }
        return cell
    }
    
    func treeView(treeView: RATreeView, heightForRowForItem item: AnyObject) -> CGFloat{
        if let _ = item.objectForKey("sub_filter"){
            return 40
        }else{
            return 36
        }
    }
    
    func treeView(treeView: RATreeView, didSelectRowForItem item: AnyObject){
        if let _ = item.objectForKey("sub_filter"){
            (item as! NSMutableDictionary).setObject("0", forKey: "isSelected")
        }else{
            if (item as! NSMutableDictionary).objectForKey("isSelected") as? String == "1" {
                (item as! NSMutableDictionary).setObject("0", forKey: "isSelected")
            }else{
                (item as! NSMutableDictionary).setObject("1", forKey: "isSelected")
            }
            treeView.reloadRowsForItems([item], withRowAnimation:RATreeViewRowAnimationAutomatic)
        }
    }
    func treeView(treeView: RATreeView, canEditRowForItem item: AnyObject) -> Bool{
        return false
    }
    
    @IBAction func removeFiltersView(){
       removeAnimated() 
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        removeAnimated()
    }
   
    func showAnimated(){
        self.view.layer.opacity = 0
         GlobalConstants.APPDELEGATE.window?.addSubview(self.view)
        UIView.animateWithDuration(0.3, animations: { () -> Void in
            self.view.layer.opacity = 1
            }) { (completion) -> Void in
        }
    }
    
    func removeAnimated(){
        UIView.animateWithDuration(0.3, animations: { () -> Void in
            self.view.layer.opacity = 0
            }) { (completion) -> Void in
                self.view.removeFromSuperview()
                self.view.layer.opacity = 1
        }
    }
}

