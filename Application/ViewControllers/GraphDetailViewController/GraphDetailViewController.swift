//
//  PriceDetailViewController.swift
//  Application
//
//  Created by Ankit Nandal on 05/11/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit
import BEMSimpleLineGraph
import ActionSheetPicker_3_0
import ScrollableGraphView
class GraphDetailViewController: UIViewController{
    
    @IBOutlet weak var navigationtitle: UILabel!
    @IBOutlet weak var graphDataOption: UIButton!

    @IBOutlet weak var containerView: UIView!
    let arrayOfGraphDataOptions = ["Weekly","Monthly","3 Months","Yearly"]
    var selectedGraphDataOptionValue = 0
    var arrayOfValues = NSMutableArray()
    var arrayOfDates = NSMutableArray()
    var priceObject:Price?
    var priceLogisticsObject:PriceTabWise?
    var graphDataConstantDayFrom:Double = 7

    var priceDetailDict:NSDictionary?
    var graphScrollView: ScrollableGraphView!
    
    private func createDarkGraph(frame: CGRect) -> ScrollableGraphView {
        let graphView = ScrollableGraphView.init(frame: frame)
        
        
        graphView.lineWidth = 1
        graphView.lineStyle = ScrollableGraphViewLineStyle.Straight
        
        graphView.shouldFill = false
        graphView.lineColor=UIColorFromRGB(0x1e88e5)
        //        graphView.fillType = ScrollableGraphViewFillType.Gradient
        //        graphView.fillGradientType = ScrollableGraphViewGradientType.Linear
        //        graphView.fillGradientEndColor=UIColorFromRGB(0x1e88e5)
        //        graphView.fillGradientStartColor=UIColor.blueColor()
        graphView.dataPointSpacing=GlobalConstants.SCREEN_WIDTH/6-10
        graphView.dataPointSize = 2
        graphView.dataPointFillColor = UIColor.blackColor()
        
        graphView.referenceLineLabelFont = UIFont.boldSystemFontOfSize(8)
        graphView.referenceLineColor = UIColor.grayColor()
        graphView.referenceLineLabelColor = UIColor.blackColor()
        graphView.numberOfIntermediateReferenceLines = 5
        graphView.dataPointLabelColor = UIColor.blackColor().colorWithAlphaComponent(1.0)
        graphView.shouldAnimateOnStartup = true
        graphView.shouldAdaptRange = true
        graphView.adaptAnimationType = ScrollableGraphViewAnimationType.EaseOut
        graphView.animationDuration = 1
        graphView.dataPointLabelFont = UIFont.systemFontOfSize(8)
        return graphView
    }

    func drawGraph(showSideLabels:Bool)
    {
        if(graphScrollView == nil)
        {
            graphScrollView = createDarkGraph(CGRectMake(0, 0, GlobalConstants.SCREEN_WIDTH,  GlobalConstants.SCREEN_HEIGHT-65))
            graphScrollView.backgroundColor = UIColor.whiteColor()
            graphScrollView.shouldAutomaticallyDetectRange = true
            if(!showSideLabels)
            {
                graphScrollView.dataPointLabelColor=UIColor.clearColor()
                graphScrollView.referenceLineLabelColor=UIColor.clearColor()
                
            }
            self.containerView.addSubview(graphScrollView)
            
        }
        
        
        graphScrollView.setData(arrayOfValues as NSArray as! [Double], withLabels: arrayOfDates as NSArray as! [String])
        
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
       
               updateGraph()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        var width = Int(scrollView.bounds.size.width)
//        let dateWidth = arrayOfDates[0] as? String
//        if let dateThere = dateWidth{
//            let wid = getSizeBasedOnText(230, heightOfContainer: 12, string: dateThere, fontSize: 11).size.width
//            if width < Int(wid) * arrayOfDates.count {
//                width = Int(wid) * arrayOfDates.count
//            }
//            graphView.translatesAutoresizingMaskIntoConstraints = true
//            scrollView.contentSize = CGSizeMake(CGFloat(width), scrollView.bounds.height)
//            if Int(graphView.frame.size.width) != width {
//                graphView.frame = CGRectMake(0, 0, CGFloat(width), scrollView.frame.size.height)
//                graphView.reloadGraph()
//            }else{
//                print("skipping")
//            }
//        }
    }
    
    
    func updateGraph(){
        self.arrayOfValues.removeAllObjects()
        self.arrayOfDates.removeAllObjects()
        var tempGraphArray : NSArray?
        print(self.priceObject?.description)

        var userAuthorisedForInteraction:NSNumber? = NSNumber()
        
        if self.priceObject?.graphData != nil{
            guard let graphData = self.priceObject?.graphData as? NSMutableArray else{return}
            userAuthorisedForInteraction = self.priceObject?.graph_view
            
            tempGraphArray = graphData.sort { ($0[0] as? Int) < ($1[0] as? Int) }

            for value in graphData {
                self.arrayOfDates.addObject(getDateFromTimestamp2(value[0] as? NSNumber, dateFormat: "dd/MM/yy"))
                self.arrayOfValues.addObject(CGFloat(value[1] as! Int))
            }
        }else if self.priceLogisticsObject?.graphData != nil{
            guard let graphData = self.priceLogisticsObject?.graphData as? NSMutableArray else{return}
            userAuthorisedForInteraction = self.priceLogisticsObject?.graph_view

            tempGraphArray =  graphData.sort { ($0[0] as? Int) < ($1[0] as? Int) }

            for value in graphData {
                self.arrayOfDates.addObject(getDateFromTimestamp2(value[0] as? NSNumber, dateFormat: "dd/MM/yy"))
                self.arrayOfValues.addObject(CGFloat(value[1] as! Int))
            }
        }
        
       
        if tempGraphArray?.count > 0{
            if selectedGraphDataOptionValue == 0 {
                // we have to show data weekly
                self.arrayOfValues.removeAllObjects()
                self.arrayOfDates.removeAllObjects()
                let now: NSDate = NSDate()
                for i in 1...6
                {
                    
                    let daysAgo: NSDate = now.dateByAddingTimeInterval(-Double(i)*graphDataConstantDayFrom*24*60*60)
                    let daysTo: NSDate = daysAgo.dateByAddingTimeInterval(graphDataConstantDayFrom*24*60*60)
                    
                    let filterdArray=tempGraphArray?.filteredArrayUsingPredicate(NSPredicate(format: "ANY SELF > %@ && ANY SELF < %@", argumentArray:[daysAgo.timeIntervalSince1970,daysTo.timeIntervalSince1970]))
                    print(filterdArray)
                    var sumAvgValue:Int = 0
                    for value in filterdArray! as! [[Int]]
                    {
                        sumAvgValue = sumAvgValue + value[1]
                    }
                    if(filterdArray?.count>0)
                    {
                    sumAvgValue=sumAvgValue/(filterdArray?.count)!
                    }
                    self.arrayOfValues.insertObject(sumAvgValue, atIndex: 0)
                    self.arrayOfDates.insertObject(getDateFromTimestamp2(daysAgo.timeIntervalSince1970 as? NSNumber, dateFormat: "dd/MM/yy"), atIndex: 0)
                    print(self.arrayOfValues)
                    print(self.arrayOfDates)
                    
                    
                }
                
                
                
                
                
            }
                
            else if selectedGraphDataOptionValue == 2 {
                self.arrayOfValues.removeAllObjects()
                self.arrayOfDates.removeAllObjects()
                graphDataConstantDayFrom=90
                let now: NSDate = NSDate()
                for i in 1...6
                {
                    
                    let daysAgo: NSDate = now.dateByAddingTimeInterval(-Double(i)*graphDataConstantDayFrom*24*60*60)
                    let daysTo: NSDate = daysAgo.dateByAddingTimeInterval(graphDataConstantDayFrom*24*60*60)
                    
                    let filterdArray=tempGraphArray?.filteredArrayUsingPredicate(NSPredicate(format: "ANY SELF > %@ && ANY SELF < %@", argumentArray:[daysAgo.timeIntervalSince1970,daysTo.timeIntervalSince1970]))
                    print(filterdArray)
                    var sumAvgValue:Int = 0
                    for value in filterdArray! as! [[Int]]
                    {
                        sumAvgValue = sumAvgValue + value[1]
                    }
                    if(filterdArray?.count>0)
                    {
                    sumAvgValue=sumAvgValue/(filterdArray?.count)!
                    }
                    
                    self.arrayOfValues.insertObject(sumAvgValue, atIndex: 0)
                    self.arrayOfDates.insertObject(getDateFromTimestamp2(daysAgo.timeIntervalSince1970 as? NSNumber, dateFormat: "MMM yyyy"), atIndex: 0)
                    print(self.arrayOfValues)
                    print(self.arrayOfDates)
                    
                    
                }
                
                
            }
                
            else if selectedGraphDataOptionValue == 1 {
                self.arrayOfValues.removeAllObjects()
                self.arrayOfDates.removeAllObjects()
                graphDataConstantDayFrom=30
                let now: NSDate = NSDate()
                for i in 1...6
                {
                    
                    let daysAgo: NSDate = now.dateByAddingTimeInterval(-Double(i)*graphDataConstantDayFrom*24*60*60)
                    let daysTo: NSDate = daysAgo.dateByAddingTimeInterval(graphDataConstantDayFrom*24*60*60)
                    
                    let filterdArray=tempGraphArray?.filteredArrayUsingPredicate(NSPredicate(format: "ANY SELF > %@ && ANY SELF < %@", argumentArray:[daysAgo.timeIntervalSince1970,daysTo.timeIntervalSince1970]))
                    print(filterdArray)
                    var sumAvgValue:Int = 0
                    for value in filterdArray! as! [[Int]]
                    {
                        sumAvgValue = sumAvgValue + value[1]
                    }
                    if(filterdArray?.count>0)
                    {
                    sumAvgValue=sumAvgValue/(filterdArray?.count)!
                    }
                    self.arrayOfValues.insertObject(sumAvgValue, atIndex: 0)
                    self.arrayOfDates.insertObject(getDateFromTimestamp2(daysAgo.timeIntervalSince1970 as? NSNumber, dateFormat: "MMM yyyy"), atIndex: 0)
                    print(self.arrayOfValues)
                    print(self.arrayOfDates)
                    
                    
                }
                
            }else if selectedGraphDataOptionValue == 3 {
                self.arrayOfValues.removeAllObjects()
                self.arrayOfDates.removeAllObjects()
                graphDataConstantDayFrom=366
                let now: NSDate = NSDate()
                for i in 1...6
                {
                    
                    let daysAgo: NSDate = now.dateByAddingTimeInterval(-Double(i)*graphDataConstantDayFrom*24*60*60)
                    let daysTo: NSDate = daysAgo.dateByAddingTimeInterval(graphDataConstantDayFrom*24*60*60)
                    
                    let filterdArray=tempGraphArray?.filteredArrayUsingPredicate(NSPredicate(format: "ANY SELF > %@ && ANY SELF < %@", argumentArray:[daysAgo.timeIntervalSince1970,daysTo.timeIntervalSince1970]))
                    print(filterdArray)
                    var sumAvgValue:Int = 0
                    for value in filterdArray! as! [[Int]]
                    {
                        sumAvgValue = sumAvgValue + value[1]
                    }
                    if(filterdArray?.count>0)
                    {
                        sumAvgValue=sumAvgValue/(filterdArray?.count)!
                    }
                    self.arrayOfValues.insertObject(sumAvgValue, atIndex: 0)
                    self.arrayOfDates.insertObject(getDateFromTimestamp2(daysAgo.timeIntervalSince1970 as? NSNumber, dateFormat: "yyyy"), atIndex: 0)
                    print(self.arrayOfValues)
                    print(self.arrayOfDates)
                    
                }
            }
        }
//        if userAuthorisedForInteraction == true{
//            self.graphView.enableTouchReport = true;
//            self.graphView.enablePopUpReport = true;
//
//        }else{
//            self.graphView.colorXaxisLabel = UIColor.clearColor()
//            self.graphView.colorYaxisLabel = UIColor.clearColor()
//        }
//        self.graphView.labelFont = UIFont.systemFontOfSize(9.0)
//        self.graphView.enableYAxisLabel = true;
//        self.graphView.enableXAxisLabel = true;
//        self.graphView.autoScaleYAxis = true;
//        self.graphView.alwaysDisplayDots = false;
//        self.graphView.enableReferenceYAxisLines = true;
//        self.graphView.enableReferenceAxisFrame = true;
//        self.graphView.animationGraphStyle = .Fade
//        self.graphView.formatStringForValues = "%.1f"
//        self.graphView.delegate = self
//        self.graphView.dataSource = self
//        self.graphView.reloadGraph()
//        self.view.setNeedsDisplay()
//        self.view.setNeedsLayout()
        
//        var width = Int(scrollView.bounds.size.width)
//        let dateWidth = arrayOfDates[0] as? String
//        if let dateThere = dateWidth{
//            let wid = getSizeBasedOnText(230, heightOfContainer: 12, string: dateThere, fontSize: 11).size.width
//            if width < Int(wid) * arrayOfDates.count {
//                width = Int(wid) * arrayOfDates.count
//            }
//            graphView.translatesAutoresizingMaskIntoConstraints = true
//            scrollView.contentSize = CGSizeMake(CGFloat(width), scrollView.bounds.height)
//            if Int(graphView.frame.size.width) != width {
//                graphView.frame = CGRectMake(0, 0, CGFloat(width), scrollView.frame.size.height)
//                graphView.reloadGraph()
//            }else{
//                print("skipping")
//            }
//        }
        let dateWidth = arrayOfDates[0] as? String
        if let dateThere = dateWidth{
            self.drawGraph((userAuthorisedForInteraction?.boolValue)!)
            
        }

    }
    
    @IBAction func onClickOfLeftBarButton(sender:UIButton){
        //GlobalConstants.APPDELEGATE.tforPortraitFforLandscape = true
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:  GRAPH DELEGATES
       @IBAction func onClickOfGraphDataOptions(sender:UIButton){
        ActionSheetStringPicker.showPickerWithTitle(arrayOfGraphDataOptions[selectedGraphDataOptionValue], rows: arrayOfGraphDataOptions, initialSelection: selectedGraphDataOptionValue, doneBlock: { (picker, index, value) -> Void in
            self.selectedGraphDataOptionValue = index
            self.graphDataOption.setTitle(self.arrayOfGraphDataOptions[self.selectedGraphDataOptionValue], forState: UIControlState.Normal)
            self.updateGraph()
            }, cancelBlock: { (picker) -> Void in
            }, origin: self.view)
    }
}
