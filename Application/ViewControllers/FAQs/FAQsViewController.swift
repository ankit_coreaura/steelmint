//
//  FAQsViewController.swift
//  Application
//
//  Created by Ankit Nandal on 29/12/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit

class FAQsViewController: UIViewController , UITableViewDelegate , UITableViewDataSource{
    @IBOutlet weak var navigationLeftButton:UIButton!
    @IBOutlet weak var tableView:UITableView!
    var faqDict:NSArray?
    var viaOption = ""
    var selectedIndexPath:NSIndexPath?
    var indicator = UIActivityIndicatorView()
    var tempVIew = UIView()
    //var selectedIndexPath: Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
        initialSetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func initialSetup()
    {
        indicator = UIActivityIndicatorView  (activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        indicator.frame = CGRectMake(GlobalConstants.SCREEN_WIDTH/2-10, GlobalConstants.SCREEN_HEIGHT/2-10, 10.0, 10.0)
        tempVIew = UIView(frame: tableView.frame)
        tempVIew.backgroundColor = UIColor.whiteColor()
        if viaOption == "PLANTBEE"{
            navigationLeftButton.setImage(UIImage(named: "back_button"), forState: .Normal)
        }
        if let offlineData = CacheManager.sharedInstance.loadObject("FAQs") as? NSArray {
          faqDict = offlineData
            self.tableView.reloadData()

        }
        
        getFAQs()
        // Setup ActivityIdicator
       
    }
    
    @IBAction func OpenLeftMenu(){
        if viaOption == "PLANTBEE"{
            navigationController?.popViewControllerAnimated(true)
            return
        }
        openLeft()
    }
    
    
    // MARK:  TABLE VIEW DELEGTES
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let numberOFRows = faqDict?.count{
            return numberOFRows
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("c1", forIndexPath: indexPath)
        let questionNumber = cell.viewWithTag(2) as! UILabel
        let questionLabel = cell.viewWithTag(3) as! UILabel
        let answerLabel = cell.viewWithTag(4) as! UILabel
        // SETTING OUTLETS:
        
        guard let questionAnswersDict = faqDict?[indexPath.row] else{return cell}
        questionNumber.text = String(indexPath.row + 1) + "."
        questionLabel.text = questionAnswersDict["ques"] as? String
        
        if let selectIndexPath = selectedIndexPath{
            if selectIndexPath == indexPath{
                if let str = questionAnswersDict["ans"] as? String{
                    answerLabel.attributedText = getHTMlRenderedString(str)
                }
            }
            else{
                answerLabel.backgroundColor = UIColor.whiteColor()
                answerLabel.text = ""
            }
        }else{
            answerLabel.backgroundColor = UIColor.whiteColor()
            
            answerLabel.text = ""
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       // let previous = selectedIndexPath
        if selectedIndexPath == indexPath {
            selectedIndexPath = nil
        }
        else{
            selectedIndexPath = indexPath
        }
        tableView.reloadData()
//        NSObject.cancelPreviousPerformRequestsWithTarget(self)
//        performSelector("scroll:", withObject: indexPath, afterDelay: 1)

    }
    func scroll(index:NSIndexPath){
        tableView.scrollToRowAtIndexPath(index, atScrollPosition: .Bottom, animated: true)
    }
    func getFAQs()
    {
        if CacheManager.sharedInstance.loadObject("FAQs") == nil{
        showNativeActivityIndicator()
        }
        if isInternetConnectivityAvailable(false) == false{
            stopActivityIndicator()
        }
        
        let webserviceInstance = WebServices()
        webserviceInstance.FAQs(NSDictionary(), completionBlock: {
            (responseData)->() in
            if self.indicator.isAnimating(){
                self.stopActivityIndicator()
   
            }
            if responseData != nil{
                guard let info = responseData?.objectForKey("data#") as? NSArray else{return}
                if CacheManager.sharedInstance.loadObject("FAQs") == nil{
                    CacheManager.sharedInstance.saveObject(info, identifier: "FAQs")
                    self.faqDict = info
                    self.tableView.reloadData()
                }else{
                  CacheManager.sharedInstance.saveObject(info, identifier: "FAQs")
                }
               
            }
        })
    }

    
    //MARK: ACTIVITY INDICATOR
    func showNativeActivityIndicator(){
       
        view.addSubview(tempVIew)
        view.addSubview(indicator)
        indicator.startAnimating()
    }
    
    func stopActivityIndicator() {
        
        indicator.stopAnimating()
        tempVIew.removeFromSuperview()
    }
  }
