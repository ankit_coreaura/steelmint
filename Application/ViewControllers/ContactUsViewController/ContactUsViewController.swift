//
//  SubscribeViewController.swift
//  SteelMint
//
//  Created by Ankit Nandal on 03/11/15.
//  Copyright © 2015 Saurabh Singh. All rights reserved.
//

import UIKit
import SCLAlertView

class ContactUsViewController: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate {
    //Connecting Outlets============
    @IBOutlet weak var nameTextField: UITextField!

    @IBOutlet weak var interestedInOutlet: UIButton!
    @IBOutlet weak var lastNameTextField: UITextField!
   
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var mobileTextField: UITextField!
    var tempview = UIView()
    var picker = UIPickerView()
    var pickerArray = NSMutableArray()
    var selectedRow :Int = 3
    //END=============================
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        pickerArray = ["Others","Coal","Semi Finished","Steel Materials","Iron ore","Raw Material","Logistics" ,"Ferro alloy"]
    }
    override func viewDidLoad() {
        super.viewDidLoad()

    }
        @IBAction func onClickOfLeftBarButton(sender:UIButton){
                openLeft()
           // addressTextView.textContainerInset = UIEdgeInsetsZero
        }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func interestedInEvent(sender: UIButton) {
        picker = AppCommonFunctions.sharedInstance.showPicker(self)
        picker.dataSource = self
        picker.delegate = self
        
    }
    @IBAction func popToMenuEvent(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func submitBtnEvent(sender: UIButton) {
        let information = NSMutableDictionary()

        if nameTextField.text!.isEmpty || lastNameTextField.text!.isEmpty || emailTextField.text!.isEmpty || mobileTextField.text!.isEmpty  {
            showAlertController("SteelMint", message: "Please complete all the details", reference: self)
            
        }
        else{
            var isEverythingValidated = true
            //Validating Email==========================================
            if emailTextField.text!.isEmail{
            information.setObject("enquiry", forKey: "type")
            information.setObject(emailTextField.text!, forKey: "email")
            }
            else{
                isEverythingValidated = false
                showAlertController("SteelMint",message: "Please provide valid email", reference: self)
            }
            //Validating Phone Number===================================

            if mobileTextField.text!.isPhoneNumber{
                information.setObject(mobileTextField.text!, forKey: "phoneno")
                information.setObject(nameTextField.text!, forKey: "name")
                information.setObject(lastNameTextField.text!, forKey: "lastname")
                information.setObject("....", forKey: "address")
                information.setObject(pickerArray[selectedRow] as! String, forKey: "interest")


            }
            else{
                if isEverythingValidated{
                isEverythingValidated = false
                showAlertController("SteelMint",message: "Please provide valid Phone Number", reference: self)
                }
            }
            if isEverythingValidated{
                
                
                let webServicesObject = WebServices()
                webServicesObject.progresssIndicatorText = "Please wait.. "
                webServicesObject.contactUs(information, completionBlock:
                    {
                        (responseData) -> () in
                
                print(responseData)
                        if let response = responseData{
                            print(response.valueForKey("data#"))
                            
                            showAlertDifferentType("Thankyou for your information. We will contact you soon")
                            
                            self.slideMenuController()?.changeMainViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("TabController"), close: true)
                        }
                        else{
                            let alertView = SCLAlertView()

                            alertView.showTitle("SteelMint", subTitle: "Network Error", style: SCLAlertViewStyle.Error, closeButtonTitle: "Cancel", duration: 20, colorStyle: 0xE4E4E4, colorTextButton: 0xFFFFFF)
                        }
                
                
                
                
                })
                
                
            //hit service===================================
                }
        }
        
    }
    
    func donePickerButton(sender:UIButton){
        interestedInOutlet.setTitle(pickerArray[selectedRow] as? String, forState: .Normal)
        picker.superview?.removeFromSuperview()
        
    }
    func cancelPickerButton(sender:UIButton){
        picker.superview?.removeFromSuperview()
    }
    // MARK:  PICKER DELEGATES
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return pickerArray.count
   
    }

    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       let selectedText = pickerArray[row]
        print(selectedText)
        selectedRow = row
        pickerView.reloadAllComponents()

    }
        
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let color = (row == pickerView.selectedRowInComponent(component)) ? UIColor.orangeColor() : UIColor(red: 30.0/255, green: 136.0/255.0, blue: 229.0/255, alpha: 1.0)
        return NSAttributedString(string: pickerArray[row] as! String, attributes: [NSForegroundColorAttributeName: color])
    }

}