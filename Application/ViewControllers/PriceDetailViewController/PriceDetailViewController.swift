//
//  PriceDetailViewController.swift
//  Application
//
//  Created by Ankit Nandal on 05/11/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit
import BEMSimpleLineGraph
import ActionSheetPicker_3_0
import ScrollableGraphView
class PriceDetailViewController: UIViewController {
    @IBOutlet weak var tableView:UITableView!
    
    @IBOutlet weak var tableviewBottomLayoutConstraint: NSLayoutConstraint!
    var jsonObj:NSDictionary?
    var graphData:Bool?
    @IBOutlet weak var navigationtitle: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var flagImageview: UIImageView!
    @IBOutlet weak var oreLabel: UILabel!
    @IBOutlet weak var deliveryLabel: UILabel!
    @IBOutlet weak var changeLabel: UILabel!
    @IBOutlet weak var gradeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    //@IBOutlet weak var scrollView: UIScrollView!
    //@IBOutlet weak var graphView: BEMSimpleLineGraphView!
    
    @IBOutlet weak var containerGraph: UIView!
    @IBOutlet weak var viewGraphInDetail: UIButton!
    @IBOutlet weak var graphDataOption: UIButton!
    
    var indicator = UIActivityIndicatorView()
   // var tempview = UIView()
    var graphDataConstantDayFrom:Double = 7
    let arrayOfGraphDataOptions = ["Weekly","Monthly","3 Months","Yearly"]//["Daily","Weekly","Monthly","Yearly"]
    var selectedGraphDataOptionValue = 0
    var arrayOfValues = NSMutableArray()
    var arrayOfDates = NSMutableArray()
    var priceObject:Price?
    var priceLogisticsObject:PriceTabWise?
    
    var priceDetailDict:NSDictionary?
    var allKeys:NSArray?
    var viaOption:Bool?
    var graphScrollView: ScrollableGraphView!

    private func createDarkGraph(frame: CGRect) -> ScrollableGraphView {
        let graphView = ScrollableGraphView.init(frame: frame)
        
        
        graphView.lineWidth = 1
        graphView.lineStyle = ScrollableGraphViewLineStyle.Straight
        
        graphView.shouldFill = false
        graphView.lineColor=UIColorFromRGB(0x1e88e5)
//        graphView.fillType = ScrollableGraphViewFillType.Gradient
//        graphView.fillGradientType = ScrollableGraphViewGradientType.Linear
//        graphView.fillGradientEndColor=UIColorFromRGB(0x1e88e5)
//        graphView.fillGradientStartColor=UIColor.blueColor()
        graphView.dataPointSpacing=GlobalConstants.SCREEN_WIDTH/6-10
        graphView.dataPointSize = 2
        graphView.dataPointFillColor = UIColor.blackColor()
        
        graphView.referenceLineLabelFont = UIFont.boldSystemFontOfSize(8)
        graphView.referenceLineColor = UIColor.grayColor()
        graphView.referenceLineLabelColor = UIColor.blackColor()
        graphView.numberOfIntermediateReferenceLines = 5
        graphView.dataPointLabelColor = UIColor.blackColor().colorWithAlphaComponent(1.0)
        graphView.shouldAnimateOnStartup = true
        graphView.shouldAdaptRange = false
        graphView.adaptAnimationType = ScrollableGraphViewAnimationType.EaseOut
        graphView.animationDuration = 1
        graphView.dataPointLabelFont = UIFont.systemFontOfSize(8)
        return graphView
    }
    func drawGraph(showSideLabels:Bool)
{
    if(graphScrollView == nil)
    {
        graphScrollView = createDarkGraph(CGRectMake(0, 0, GlobalConstants.SCREEN_WIDTH, 159))
        graphScrollView.backgroundColor = UIColor.whiteColor()
        graphScrollView.shouldAutomaticallyDetectRange = true
        if(!showSideLabels)
        {
            graphScrollView.dataPointLabelColor=UIColor.clearColor()
            graphScrollView.referenceLineLabelColor=UIColor.clearColor()

        }
        self.containerGraph.addSubview(graphScrollView)

    }
    
    
    graphScrollView.setData(arrayOfValues as NSArray as! [Double], withLabels: arrayOfDates as NSArray as! [String])
    
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    
        //
        if let path = NSBundle.mainBundle().pathForResource("countries", ofType: "json") {
            do {
                let data = try NSData(contentsOfURL: NSURL(fileURLWithPath: path), options: NSDataReadingOptions.DataReadingMappedIfSafe)
                jsonObj = parsedJsonFrom(data)
                
                
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        } else {
            print("Invalid filename/path.")
        }

        
        
        settingOutlets()
        getGraphData()
//(self.view.viewWithTag(8008) as! UILabel).transform = CGAffineTransformMakeRotation(CGFloat(-M_PI_2))
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let _ = priceDetailDict?["tabs"] as? NSArray{
            
        }else{
            (view.viewWithTag(937) as! UIButton).hidden = true
            tableviewBottomLayoutConstraint.constant = -37

        }
        
        if viaOption == true{
            (view.viewWithTag(937) as! UIButton).hidden = true
            tableviewBottomLayoutConstraint.constant = -37
        }
       
    }
    
    func getGraphData(){
        guard let priceDetailData = priceDetailDict else{return}
        let webserviceInstance = WebServices()
        
        // activity indicator setup
        indicator = UIActivityIndicatorView  (activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        indicator.frame = CGRectMake( GlobalConstants.SCREEN_WIDTH/2-10,215, 10.0, 10.0)
        
        if viaOption == true{
        self.priceLogisticsObject = DatabaseManager.sharedInstance.getPriceTabWise(priceDetailData)
            if (priceLogisticsObject?.graphData == nil) {
                if let  priceId = priceDetailData.valueForKey("graph_id") as? String{
                    let information = ["f_price_column":priceDetailData["f_price_column"] as! String,"assesment":priceDetailData["assesment"] as! String]
                    showNativeActivityIndicator()

                    if isInternetConnectivityAvailable(false)==false {
                       stopActivityIndicator()
                        
                    }
                    webserviceInstance.getPriceDetailById(information, id: priceId, completionBlock: {
                        (responseData) ->() in
                        self.stopActivityIndicator()
                        guard let responseDict = responseData as? NSDictionary else{return}
            self.priceLogisticsObject?.graphData = responseDict.objectForKey("data#") as? NSArray
                        DatabaseManager.sharedInstance.saveContext()
                        self.updateGraph()
                    })
                    
                }
            }else{
                updateGraph()
            }
        }else{
            self.priceObject = DatabaseManager.sharedInstance.getPrice(priceDetailData)
            if (priceObject?.graphData == nil) {
                if let  priceId = priceDetailData.valueForKey("graph_id") as? String{
                    let information = ["f_price_column":priceDetailData["f_price_column"] as! String,"assesment":priceDetailData["assesment"] as! String]
                    showNativeActivityIndicator()
                    
                    if isInternetConnectivityAvailable(false)==false {
                            self.stopActivityIndicator()
                    }
                    webserviceInstance.getPriceDetailById(information, id: priceId, completionBlock: {
                        (responseData) ->() in
                        self.stopActivityIndicator()

                        guard let responseDict = responseData as? NSDictionary else{return}
                        self.priceObject?.graphData = responseDict.objectForKey("data#") as? NSArray
                        DatabaseManager.sharedInstance.saveContext()
                        self.updateGraph()
                    })
                    
                    
                }
            }else{
                updateGraph()
            }
        }
       
       
   
    }
    
    func updateGraph(){

        self.arrayOfValues.removeAllObjects()
        self.arrayOfDates.removeAllObjects()
        let tempGraphArray:NSArray?
        let userAuthorisedForInteraction:NSNumber?
        if viaOption == true{
            guard let graphArray = self.priceLogisticsObject?.graphData as? NSArray else{return}
            
            userAuthorisedForInteraction = self.priceLogisticsObject?.graph_view
            tempGraphArray = graphArray.sort { ($0[0] as? Int) < ($1[0] as? Int) }
            if tempGraphArray?.count > 0{
                for value in tempGraphArray! {
                    self.arrayOfDates.addObject(getDateFromTimestamp2(value[0] as? NSNumber, dateFormat: "dd/MM/yy"))
                    self.arrayOfValues.addObject(CGFloat(value[1] as! Int))
                }
                            //   graphScrollView.setData(arrayOfValues as NSArray as! [Double], withLabels: arrayOfDates as NSArray as! [String])



            }
            
        }
        else{
            guard let graphArray = self.priceObject?.graphData as? NSArray else{return}
            userAuthorisedForInteraction = self.priceObject?.graph_view

            tempGraphArray = graphArray.sort { ($0[0] as? Int) < ($1[0] as? Int) }
             if tempGraphArray?.count > 0{
            for value in tempGraphArray! {
               
                self.arrayOfDates.addObject(getDateFromTimestamp2(value[0] as? NSNumber, dateFormat: "dd/MM/yy"))
                self.arrayOfValues.addObject(CGFloat(value[1] as! Int))
            }
               // graphScrollView.setData(arrayOfValues as NSArray as! [Double], withLabels: arrayOfDates as NSArray as! [String])

            }
        }
        
        if tempGraphArray?.count > 0{
            if selectedGraphDataOptionValue == 0 {
                // we have to show data weekly
                self.arrayOfValues.removeAllObjects()
                self.arrayOfDates.removeAllObjects()
                let now: NSDate = NSDate()
                for i in 1...6
                {
                    
                    let daysAgo: NSDate = now.dateByAddingTimeInterval(-Double(i)*graphDataConstantDayFrom*24*60*60)
                    let daysTo: NSDate = daysAgo.dateByAddingTimeInterval(graphDataConstantDayFrom*24*60*60)

                    let filterdArray=tempGraphArray?.filteredArrayUsingPredicate(NSPredicate(format: "ANY SELF > %@ && ANY SELF < %@", argumentArray:[daysAgo.timeIntervalSince1970,daysTo.timeIntervalSince1970]))
                    print(filterdArray)
                    var sumAvgValue:Int = 0
                    for value in filterdArray! as! [[Int]]
                    {
                        sumAvgValue = sumAvgValue + value[1]
                    }
                   
                    if(filterdArray?.count>0)
                    {
                    sumAvgValue=sumAvgValue/(filterdArray?.count)!
                    }
                    
                    self.arrayOfValues.insertObject(sumAvgValue, atIndex: 0)
                    self.arrayOfDates.insertObject(getDateFromTimestamp2(daysAgo.timeIntervalSince1970 as? NSNumber, dateFormat: "dd/MM/yy"), atIndex: 0)
                    print(self.arrayOfValues)
                    print(self.arrayOfDates)
                    

                   
                }
                
                


                
            }
            
            else if selectedGraphDataOptionValue == 2 {
                self.arrayOfValues.removeAllObjects()
                self.arrayOfDates.removeAllObjects()
                graphDataConstantDayFrom=90
                let now: NSDate = NSDate()
                for i in 1...6
                {
                    
                    let daysAgo: NSDate = now.dateByAddingTimeInterval(-Double(i)*graphDataConstantDayFrom*24*60*60)
                    let daysTo: NSDate = daysAgo.dateByAddingTimeInterval(graphDataConstantDayFrom*24*60*60)
                    
                    let filterdArray=tempGraphArray?.filteredArrayUsingPredicate(NSPredicate(format: "ANY SELF > %@ && ANY SELF < %@", argumentArray:[daysAgo.timeIntervalSince1970,daysTo.timeIntervalSince1970]))
                    print(filterdArray)
                    var sumAvgValue:Int = 0
                    for value in filterdArray! as! [[Int]]
                    {
                        sumAvgValue = sumAvgValue + value[1]
                    }
                    if(filterdArray?.count>0)
                    {
                    sumAvgValue=sumAvgValue/(filterdArray?.count)!
                    }
                    self.arrayOfValues.insertObject(sumAvgValue, atIndex: 0)
                    self.arrayOfDates.insertObject(getDateFromTimestamp2(daysAgo.timeIntervalSince1970 as? NSNumber, dateFormat: "MMM yyyy"), atIndex: 0)
                    print(self.arrayOfValues)
                    print(self.arrayOfDates)
                    
                    
                }

                
            }
            
            else if selectedGraphDataOptionValue == 1 {
                self.arrayOfValues.removeAllObjects()
                self.arrayOfDates.removeAllObjects()
                graphDataConstantDayFrom=30
                let now: NSDate = NSDate()
                for i in 1...6
                {
                    
                    let daysAgo: NSDate = now.dateByAddingTimeInterval(-Double(i)*graphDataConstantDayFrom*24*60*60)
                    let daysTo: NSDate = daysAgo.dateByAddingTimeInterval(graphDataConstantDayFrom*24*60*60)
                    
                    let filterdArray=tempGraphArray?.filteredArrayUsingPredicate(NSPredicate(format: "ANY SELF > %@ && ANY SELF < %@", argumentArray:[daysAgo.timeIntervalSince1970,daysTo.timeIntervalSince1970]))
                    print(filterdArray)
                    var sumAvgValue:Int = 0
                    for value in filterdArray! as! [[Int]]
                    {
                        sumAvgValue = sumAvgValue + value[1]
                    }
                    if(filterdArray?.count>0)
                    {
                    sumAvgValue=sumAvgValue/(filterdArray?.count)!
                    }
                    self.arrayOfValues.insertObject(sumAvgValue, atIndex: 0)
                    self.arrayOfDates.insertObject(getDateFromTimestamp2(daysAgo.timeIntervalSince1970 as? NSNumber, dateFormat: "MMM yyyy"), atIndex: 0)
                    
                    print(self.arrayOfValues)
                    print(self.arrayOfDates)
                    
                    
                }

            }else if selectedGraphDataOptionValue == 3 {
                self.arrayOfValues.removeAllObjects()
                self.arrayOfDates.removeAllObjects()
                graphDataConstantDayFrom=366
                let now: NSDate = NSDate()
                for i in 1...6
                {
                    
                    let daysAgo: NSDate = now.dateByAddingTimeInterval(-Double(i)*graphDataConstantDayFrom*24*60*60)
                    let daysTo: NSDate = daysAgo.dateByAddingTimeInterval(graphDataConstantDayFrom*24*60*60)
                    
                    let filterdArray=tempGraphArray?.filteredArrayUsingPredicate(NSPredicate(format: "ANY SELF > %@ && ANY SELF < %@", argumentArray:[daysAgo.timeIntervalSince1970,daysTo.timeIntervalSince1970]))
                    print(filterdArray)
                    var sumAvgValue:Int = 0
                    for value in filterdArray! as! [[Int]]
                    {
                        sumAvgValue = sumAvgValue + value[1]
                    }
                    if(filterdArray?.count>0)
                    {
                    sumAvgValue=sumAvgValue/(filterdArray?.count)!
                    }
                    self.arrayOfValues.insertObject(sumAvgValue, atIndex: 0)
                    self.arrayOfDates.insertObject(getDateFromTimestamp2(daysAgo.timeIntervalSince1970 as? NSNumber, dateFormat: "yyyy"), atIndex: 0)
                    
                    
                    print(self.arrayOfValues)
                    print(self.arrayOfDates)
            
        }
            }
        }
         self.view.setNeedsDisplay()
        self.view.setNeedsLayout()
        
       // var width = Int(scrollView.bounds.size.width)
        let dateWidth = arrayOfDates[0] as? String
        if let dateThere = dateWidth{
           // graphScrollView.setData(arrayOfValues as NSArray as! [Double], withLabels: arrayOfDates as NSArray as! [String])
            self.drawGraph((userAuthorisedForInteraction?.boolValue)!)

            }
        
        //   var userAuthorisedForInteraction:NSNumber? = NSNumber()
        //  userAuthorisedForInteraction = self.priceObject?.graph_view
    }
    
    func settingOutlets()
    {
        guard let priceDetailData = priceDetailDict else{return}
        if let countryCode = priceDetailData["country"] as? String{
            
            if countryCode.lowercaseString == "global"{
                
                flagImageview.image = UIImage(named: "globe.png")
                
            }else{
            
            
            let predicate = NSPredicate(format: "name = [C] %@", argumentArray: [countryCode])
            if let countMap = jsonObj?["data#"] as? NSArray{
                let arrayOfMappedCountries = countMap.filteredArrayUsingPredicate(predicate)
                if arrayOfMappedCountries.count > 0{
                    if let code =  arrayOfMappedCountries[0]["code"] as? String{
                        if code.characters.count == 2 {
                            let countryBundle = "CountryPicker.bundle/" + code
                            flagImageview.image = UIImage(named: countryBundle)
                        }
                    }
                    
                }
                }
            }
            
        }else{
            flagImageview.image = UIImage()
        }
        oreLabel.text = (priceDetailData.valueForKey("sub_category") as? String)?.uppercaseString
        oreLabel.text = oreLabel.text?.stringByReplacingOccurrencesOfString("_", withString: " ")
        // navigationtitle.text = (priceDetailData.valueForKey("category") as? String)?.uppercaseString
        if let price = priceDetailData["price"] as? String{
            priceLabel.text = price
        }
        changeLabel.text = priceDetailData.valueForKey("change") as? String
        
        if changeLabel.text!.containsString("+"){
            changeLabel.textColor = UIColor(red: 0, green: 178.0/255.0, blue: 0, alpha: 1.0)
        }else if changeLabel.text == "0"{
            changeLabel.textColor = UIColor.blackColor()
            
        }else{
            changeLabel.textColor = UIColor.redColor()
        }
        if let size = priceDetailData["size"] as? String{
            sizeLabel.text = size
        }
        if let grade = (priceDetailData.valueForKey("grade") as? String)?.capitalizedString{
            
            if sizeLabel.text?.characters.count > 0{
                gradeLabel.text = ", " + grade
 
            }else{
               gradeLabel.text = grade
            }
 
        }
        

            dateLabel.text = priceDetailData.valueForKey("date") as? String
      //  }

        
        countryLabel.text = priceDetailData.valueForKey("country") as? String
        
       
        deliveryLabel.text = (priceDetailData.valueForKey("delivery") as? String)?.capitalizedString
      
        allKeys = (priceDetailData["details"] as? NSDictionary)?.allKeys
    }
    
    @IBAction func onClickOfLeftBarButton(sender:UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: TABLEVIEW DELEGATES METHODS
    
    // MARK: TABLEVIEW DELEGATES METHODS
  
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let numberOfRows = priceDetailDict?["details"] as? NSDictionary else{return 0}
        return numberOfRows.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
            let  cell = tableView.dequeueReusableCellWithIdentifier("c1", forIndexPath:indexPath)
        let title = cell.viewWithTag(90) as! UILabel
        let desc = cell.viewWithTag(91) as! UILabel

        title.text = allKeys![indexPath.row] as? String
        if title.text?.characters.count>0 {
            if let x  = (priceDetailDict?["details"] as! NSDictionary)[title.text!] as? String{
                desc.attributedText = getHTMlRenderedString(x)
 
            }
        }
        return cell
   
    }
  
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    

    
    //MARK:  GRAPH DELEGATES

//    func noDataLabelEnableForLineGraph(graph: BEMSimpleLineGraphView) -> Bool {
//        return true
//    }
//
//    func numberOfPointsInLineGraph(graph: BEMSimpleLineGraphView) -> Int{
//        return arrayOfValues.count
//    }
//    
//    func lineGraph(graph: BEMSimpleLineGraphView, valueForPointAtIndex index: Int) -> CGFloat {
//        if let value = arrayOfValues.objectAtIndex(index) as? CGFloat{
//         return value
//        }
//        return 0
//    }
//    
//    func lineGraph(graph: BEMSimpleLineGraphView, labelOnXAxisForIndex index: Int) -> String {
//        if let xaxis = self.arrayOfDates[index] as? String{
//            return xaxis
//        }
//        return ""
//    }
//    func yAxisPrefixOnLineGraph(graph: BEMSimpleLineGraphView) -> String {
//        return "  "
//    }
    
    
    @IBAction func viewAllPricesEvent(sender:UIButton){
        let viewAllPricesInstance = self.storyboard?.instantiateViewControllerWithIdentifier("PriceCountryWiseViewController") as! PriceCountryWiseViewController
        guard let priceDetailData = priceDetailDict else{return}
        if let tabArray = priceDetailData["tabs"] as? NSArray{
        viewAllPricesInstance.tabOption = tabArray
        }
        viewAllPricesInstance.priceCountryDict = priceDetailData
        self.navigationController?.pushViewController(viewAllPricesInstance, animated: true)
    }
    
    @IBAction func onClickOfViewFullScreen(sender:UIButton){
        
        
        
       // GlobalConstants.APPDELEGATE.tforPortraitFforLandscape = false
        let graphDetailViewController  = UIStoryboard(name: "Main", bundle:nil).instantiateViewControllerWithIdentifier("GraphDetailViewController") as! GraphDetailViewController
        
        graphDetailViewController.priceDetailDict = self.priceDetailDict
        
        
        
        if viaOption == true{
            if  self.priceLogisticsObject?.graphData == nil{
                showAlertController(nil, message: "Please wait while graph data is being loading", reference: self)
                return
            }

            graphDetailViewController.priceLogisticsObject =  self.priceLogisticsObject
            
        }else{
            if  self.priceObject?.graphData == nil{
                showAlertController(nil, message: "Please wait while graph data is being loading", reference: self)
                return
            }
            graphDetailViewController.priceObject = self.priceObject
        }
        
        self.navigationController?.presentViewController(graphDetailViewController, animated: true, completion: nil)
    }
    
    @IBAction func onClickOfGraphDataOptions(sender:UIButton){
        ActionSheetStringPicker.showPickerWithTitle(arrayOfGraphDataOptions[selectedGraphDataOptionValue], rows: arrayOfGraphDataOptions, initialSelection: selectedGraphDataOptionValue, doneBlock: { (picker, index, value) -> Void in
            self.selectedGraphDataOptionValue = index
            self.graphDataOption.setTitle(self.arrayOfGraphDataOptions[self.selectedGraphDataOptionValue], forState: UIControlState.Normal)
            self.updateGraph()
            }, cancelBlock: { (picker) -> Void in
            }, origin: self.view)
    }
    //MARK: ACTIVITY INDICATOR
    func showNativeActivityIndicator(){

       // self.tableView.addSubview(tempview)
        self.tableView.addSubview(indicator)
        indicator.startAnimating()
    }
    
    func stopActivityIndicator() {
        
        indicator.stopAnimating()
      //  tempview.removeFromSuperview()
        
    }
    // Data Generation
  
   }
