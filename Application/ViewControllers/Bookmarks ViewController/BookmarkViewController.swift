//
//  BookmarkViewController.swift
//  Application
//
//  Created by Ankit Nandal on 30/11/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit
enum bookmarkType:Int{
    case NEWS = 0
    case PRICE = 5
    case TENDERS = 1
    case PLANTBEE = 2
    case EVENTS = 3
}
class BookmarkViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var plantImageUrl = ""

    @IBOutlet weak var segmentControlOutlet: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    var bookmarkArray:NSArray?
    var databaseInstance = DatabaseManager.sharedInstance
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentControlOutlet.tintColor=UIColor(red: 30.0/255.0, green: 136.0/255.0, blue: 229.0/255.0, alpha: 1)
        segmentControlOutlet.selectedSegmentIndex = bookmarkType.NEWS.rawValue
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        selectBookmarks(segmentControlOutlet)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func openleftMenu(sender:UIButton){
        openLeft()
        
    }
    @IBAction func selectBookmarks(sender: UISegmentedControl) {
        print(sender.selectedSegmentIndex)
        bookmarkArray = NSArray()
        let bookmarkTypeValue = bookmarkType(rawValue: sender.selectedSegmentIndex)
        if let bookmarkTypeValue=bookmarkTypeValue {
            switch bookmarkTypeValue{
            case .NEWS:
                print("news")
                let predicate = NSPredicate(format: "isBookmarked = %@", argumentArray: [true])
                if let dict = databaseInstance.getAllNews(){
                    bookmarkArray=dict.filter{predicate.evaluateWithObject($0)}
                }
                tableView.reloadData()
            case .PRICE:
                
                print("prices")
                
            case .TENDERS:
                
                print("tender")
                let predicate = NSPredicate(format: "isBookmarked = %@", argumentArray: [true])
                if let dict = databaseInstance.getAllTenders(){
                    bookmarkArray=dict.filter{predicate.evaluateWithObject($0)}
                }
                tableView.reloadData()
                
            case .PLANTBEE:
                print("plantbee")
                if let dict = databaseInstance.getAllPlantbeePost(){
                    bookmarkArray = dict
                }
                tableView.reloadData()
            case .EVENTS:
                print("events")
                let predicate = NSPredicate(format: "isBookmarked = %@", argumentArray: [true])
                if let dict = databaseInstance.getAllEvents(){
                    bookmarkArray=dict.filter{predicate.evaluateWithObject($0)}
                }
                tableView.reloadData()
            }
        }
        
    }
    
    //MARK: TABLEVIEW DELEGATES METHODS
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let bookmarkTypeValue = bookmarkType(rawValue:segmentControlOutlet.selectedSegmentIndex)
        if let bookmarkTypeValue=bookmarkTypeValue {
            switch bookmarkTypeValue{
                
            case .NEWS:
                let newsDetailInstance = storyboard?.instantiateViewControllerWithIdentifier("NewsDetailViewController") as! NewsDetailViewController
                if let passdict = bookmarkArray?.objectAtIndex(indexPath.row) as? NSDictionary{
                    newsDetailInstance.newsDetailData = passdict as? NSMutableDictionary
                   
                    navigationController?.pushViewController(newsDetailInstance, animated: true)
                }
                
            case .PRICE:
                print("events")
 
            case .TENDERS:
                let tenderDetailInstance = storyboard?.instantiateViewControllerWithIdentifier("TenderDetailsViewController") as? TenderDetailsViewController
                guard let tenderData = bookmarkArray?.objectAtIndex(indexPath.row) as? NSDictionary else{return}
                tenderDetailInstance?.tenderDetailDict = tenderData 
                navigationController?.pushViewController(tenderDetailInstance!, animated: true)
                
            case .PLANTBEE:
                let storyboard = UIStoryboard(name: "Secondary", bundle: nil)
                let  listingInstance = storyboard.instantiateViewControllerWithIdentifier("ListingDetailsViewController") as! ListingDetailsViewController
                if let data = bookmarkArray?[indexPath.row]  as? NSDictionary{
                    listingInstance.sellPlantDetailDict = data
                }
                self.navigationController?.pushViewController(listingInstance, animated: true)
            case .EVENTS:
                let eventDetailInstance = storyboard?.instantiateViewControllerWithIdentifier("EventDetailsViewController") as! EventsDetailsViewController
                if let passdict = bookmarkArray?.objectAtIndex(indexPath.row) as? Event{
                    eventDetailInstance.eventsDataDict = passdict
                    self.navigationController?.pushViewController(eventDetailInstance, animated: true)
                
                print("events")

            }
        }
        
    }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let bookmarkTypeValue = bookmarkType(rawValue:segmentControlOutlet.selectedSegmentIndex)
        if let bookmarkTypeValue=bookmarkTypeValue {
            switch bookmarkTypeValue{
            case .NEWS:
                    let newsData = bookmarkArray?.objectAtIndex(indexPath.row) as? NSDictionary
                    let cell = tableView.dequeueReusableCellWithIdentifier("cell1", forIndexPath:indexPath)
                    let newsImageView=(cell.viewWithTag(2001) as! UIImageView)
                    let newsLabel=(cell.viewWithTag(2002) as! UILabel)
                    let newsDateLabel=(cell.viewWithTag(2003) as! UILabel)
                    if isNotNull(newsData?.valueForKey("image") as? String){
                        if let url = NSURL(string:newsData?.valueForKey("image") as! String){
                            newsImageView.sd_setImageWithURL(url)
                        }else{
                            newsImageView.image = UIImage(named: "placeholderImage")
                            
                        }
                        
                    }else{
                        newsImageView.image = UIImage(named: "placeholderImage")
                        
                    }
                    
            newsDateLabel.text = getDateFromTimestamp2(newsData?.valueForKey("post_date") as? NSNumber, dateFormat: "MMM dd, yyyy HH:mm")
            newsLabel.text = newsData!.valueForKey("post_title") as? String
                    return cell
                
                
            case .PRICE:
                
                print("prices")
                
            case .TENDERS:
                        let cell=tableView.dequeueReusableCellWithIdentifier("c1", forIndexPath: indexPath) as! TenderTableViewCell
                        
                        if let tenders = bookmarkArray?[indexPath.row] as? NSDictionary  {
                            cell.tenderTitle.text = tenders.valueForKey("title") as? String
                            cell.tenderName.text = tenders.valueForKey("company_name") as? String
                            cell.tenderQuantity.text = tenders.valueForKey("qty") as? String
                            cell.tenderGrade.text = tenders.valueForKey("grade") as? String
                            cell.tenderRegion.text = tenders.valueForKey("region") as? String
cell.tenderDate.text = getDateFromTimestamp2(tenders.valueForKey("due_date") as? NSNumber, dateFormat: "dd MMM, yyyy")
                            
                            if let urlData = tenders.valueForKey("imgpath") as? String{
                                if let url = NSURL(string:urlData){
                                    cell.tenderImageView.sd_setImageWithURL(url)
                                }else{
                                    cell.tenderImageView.image = UIImage(named: "placeholderImage")
                                    
                                }
  
                            }else{
                                cell.tenderImageView.image = UIImage(named: "placeholderImage")
 
                            }
                            
                    return cell
                    }
                
                
            case .PLANTBEE:
                
                
                let  cell = tableView.dequeueReusableCellWithIdentifier("cellPlant", forIndexPath: indexPath)
                
                let  buyplantTitle = (cell.viewWithTag(4031) as! UILabel)
                let  buyplantCompany = (cell.viewWithTag(4032) as! UILabel)
                let  buyplantPhoneNo = (cell.viewWithTag(4033) as! UILabel)
                let  buyplantDate = (cell.viewWithTag(4040) as! UILabel)
                let  plantImage = (cell.viewWithTag(4035) as! UIImageView)
                
                
                guard let plantData = bookmarkArray?[indexPath.row] else{return UITableViewCell()}
                
                buyplantDate.text = getDateFromTimestamp2(plantData["datetime"] as? NSNumber, dateFormat: "dd MMM yyyy")
                if  buyplantDate.text?.isEmpty == true{
                    buyplantDate.text = plantData["datetime"] as? String
                }
                
                buyplantPhoneNo.text = plantData["mobile"] as? String
                
                buyplantTitle.text = plantData["title"] as? String
                buyplantCompany.text = plantData["company_name"] as? String ?? "Unavailable"
                
                if let dataLeadArray = plantData["dataLead"] as? NSArray{
                    if dataLeadArray.count>0{
                        if let imageUrlArray =  dataLeadArray[0]["images"] as? NSArray {
                            if imageUrlArray.count>0{
                                if let imageURL = imageUrlArray[0]["image_url"] as? String{
                                    if isNotNull(imageURL){
                                        plantImageUrl = imageURL
                                        plantImage.sd_setImageWithURL(NSURL(string: imageURL)!)
                                    }else{
                                        plantImage.image = UIImage(named:"placeholderImage")
                                    }
                                    
                                }
                            }
                            
                        }else{
                            plantImage.image = UIImage(named:"placeholderImage")
                        }
                    }else{
                        plantImage.image = UIImage(named:"placeholderImage")
                        
                    }
                    
                }else{
                    plantImage.image = UIImage(named:"placeholderImage")
                }
  
                print("plantbee")
                return cell
                
            case .EVENTS:
                let cell=tableView.dequeueReusableCellWithIdentifier("eventsCell", forIndexPath: indexPath)
                //  CONNECTING OUTLETS
                let eventTitle = (cell.viewWithTag(4002) as! UILabel)
                let  eventDate = (cell.viewWithTag(4003) as! UILabel)
                let  eventVenue = (cell.viewWithTag(4004) as! UILabel)
                let eventsImageView = cell.viewWithTag(4001) as! UIImageView
                //  SETTING OUTLETS
                if let eventsData = bookmarkArray?[indexPath.row] as? Event{
                    
                    
                    eventTitle.text = eventsData.valueForKey("name") as? String
                    eventVenue.text = eventsData.valueForKey("location") as? String
                    eventDate.text = eventsData.valueForKey("timestamp") as? String
                    if let eventsImageData = eventsData.valueForKey("thumb_img") as? String {
                        if isNotNull(eventsImageData)
                        {
                            
                            if let urlThere = NSURL(string: eventsImageData){
                                eventsImageView.sd_setImageWithURL(urlThere)}else{
                                eventsImageView.image = UIImage(named: "placeholderImage")
                            }
                            
                        }else{
                            eventsImageView.image = UIImage(named: "placeholderImage")
                            
                        }
                    }
                }
                
                
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let numberOfRows = bookmarkArray else{return 0}
        return numberOfRows.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let bookmarkTypeValue = bookmarkType(rawValue:segmentControlOutlet.selectedSegmentIndex)
        if let bookmarkTypeValue=bookmarkTypeValue {
            switch bookmarkTypeValue{
                
            case .NEWS:
               return 85
                
            case .PRICE:
                return 85

            case .TENDERS:
                return 100
                
            case .PLANTBEE:
                return 100
                
            case .EVENTS:
                return 85
            }
        }
        
        return 100
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    
    // MARK: PLANTBEE BTNS 
    
    @IBAction func callBtnEvent(sender: UIButton) {
        let x = sender.convertPoint(CGPointZero, toView: tableView)
        let indexpath =  tableView.indexPathForRowAtPoint(x)
        guard let plantData = bookmarkArray?[indexpath!.row] else{return}
        
        let phoneNumber = plantData["mobile"] as? String
        if let pNo = Int(phoneNumber!){
            if let phoneCallURL:NSURL = NSURL(string: "tel://\(pNo)") {
                
                let alert = UIAlertController(title: "", message: "Carrier charges may apply. Do you want to call?", preferredStyle: UIAlertControllerStyle.Alert)
                
                let callAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                    let application = UIApplication.sharedApplication()
                    if (application.canOpenURL(phoneCallURL)) {
                        application.openURL(phoneCallURL);
                    }
                })
                
                let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
                
                alert.addAction(callAction)
                alert.addAction(cancel)
                
                navigationController?.presentViewController(alert, animated: true, completion: nil)
                
                
            }}
    }
    @IBAction func chatBtnevent(sender: UIButton) {
        
    }
    @IBAction func expressiNterestBtnEvent(sender: UIButton) {
        let x = sender.convertPoint(CGPointZero, toView: tableView)
        let indexpath =  tableView.indexPathForRowAtPoint(x)
        guard let plantData = bookmarkArray?[indexpath!.row] else{return}
        
        let expressInstance = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ExpressInterestViewController") as?ExpressInterestViewController
        expressInstance?.leadId = plantData["id"] as? String ?? ""
        navigationController?.pushViewController(expressInstance!, animated: true)
    }
}
