//
//  SubscribeViewController.swift
//  SteelMint
//
//  Created by Ankit Nandal on 03/11/15.

import UIKit
import DKImagePickerController
import ActionSheetPicker_3_0

class PostOffersViewController: UIViewController {
    @IBOutlet weak var addressTableview:UITableView!
    @IBOutlet weak var addressTextField: UITextField!

    @IBOutlet weak var mobileTextField: UITextField!
    
    @IBOutlet weak var docTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var imageTExtField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var companyTextField: UITextField!
    let filterArrayOfCountries = NSMutableArray()
    var plantCode:String?
    var completecountryList = NSDictionary()
    var arrayOfAllValues = NSMutableDictionary()
    var dictOfLocation = ["country":"","state":"","city":""]

        var imagesArray : NSMutableArray?
        var documentImagesArray : NSMutableArray?
        var buyORsell:String?
        override func viewDidLoad() {
                super.viewDidLoad()
             if DatabaseManager.sharedInstance.getAllPlantbee()?.count == nil || DatabaseManager.sharedInstance.getAllPlantbee()?.count  == 0{
                getPLantCode()
 
             }else{
                rearrangeAllPlantsCategories()
            }
            initialSetup()
                imagesArray = NSMutableArray()
                documentImagesArray = NSMutableArray()
        }
    
    func getPLantCode(){
        WebServices().getBuyTypePlants(NSDictionary(), completionBlock: {
            (responseData)->() in
            
            if responseData != nil{
                guard let responseArray = responseData?["data#"] as? NSArray else{return}
                for objects in responseArray{
                    if objects is NSDictionary{
                        DatabaseManager.sharedInstance.addPlantbee(objects as! NSDictionary, type: "buy")
                        
                        self.rearrangeAllPlantsCategories()
                    }
                    
                }
                
            }
            
        })
    }
    
    func initialSetup(){
        addressTableview.hidden = true
        addressTableview.layer.borderColor = UIColor.lightGrayColor().CGColor
        addressTableview.layer.borderWidth = 1.0
        addressTableview.backgroundColor = UIColor(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        
        let userInfoDict = CacheManager.sharedInstance.loadObject("userinfo") as?NSMutableDictionary
        if userInfoDict?.count>0{
            nameTextField.text = userInfoDict?.valueForKey("data#")?.valueForKey("name") as?String
            emailTextField.text = userInfoDict?.valueForKey("data#")?.valueForKey("username") as?String
            mobileTextField.text = userInfoDict?.valueForKey("data#")?.valueForKey("phoneno") as?String
    companyTextField.text = userInfoDict?.valueForKey("data#")?.valueForKey("company") as?String

           

        }
        
        
        let s = try! String(contentsOfFile: NSBundle.mainBundle()
            .pathForResource("countryList", ofType: "txt")!,
            encoding: NSUTF8StringEncoding)
        let x = parsedJsonFrom2(s.dataUsingEncoding(NSUTF8StringEncoding))
        if x != nil{
                if let dict = x?.valueForKey("data#") as? NSDictionary{
            if self.completecountryList.count == 0{
            self.completecountryList = dict
            }
                }}
        
       descriptionTextView.layer.borderColor = UIColor.lightGrayColor().CGColor
        docTextField.layer.borderColor = UIColor.lightGrayColor().CGColor
        emailTextField.layer.borderColor = UIColor.lightGrayColor().CGColor
        nameTextField.layer.borderColor = UIColor.lightGrayColor().CGColor
        imageTExtField.layer.borderColor = UIColor.lightGrayColor().CGColor
        companyTextField.layer.borderColor = UIColor.lightGrayColor().CGColor
        mobileTextField.layer.borderColor = UIColor.lightGrayColor().CGColor
        addressTextField.layer.borderColor = UIColor.lightGrayColor().CGColor
        addressTextField.layer.borderWidth = 1.0

        docTextField.layer.borderWidth = 1.0
        emailTextField.layer.borderWidth = 1.0
        nameTextField.layer.borderWidth = 1.0
        imageTExtField.layer.borderWidth = 1.0
        companyTextField.layer.borderWidth = 1.0
        mobileTextField.layer.borderWidth = 1.0
        
       
    }
    
        override func didReceiveMemoryWarning() {
                super.didReceiveMemoryWarning()
        }
        @IBAction func onClickOfLeftBarButton(sender:UIButton){
navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func onClickOfImages(sender:UIButton){
        let pickerController = DKImagePickerController()
        pickerController.allowMultipleTypes=false
        pickerController.singleSelect=false
        pickerController.showsCancelButton=true
        pickerController.navigationBar.tintColor = UIColor.whiteColor()
        pickerController.assetType=DKImagePickerControllerAssetType.AllPhotos
        pickerController.didSelectAssets = { [unowned self] (assets: [DKAsset]) in
            self.imagesArray?.removeAllObjects()
            for asset in assets{
                asset.fetchFullScreenImageWithCompleteBlock({ (image, info) -> Void in
                    self.imagesArray?.addObject(image!)
                    self.imageTExtField.text = " images selected"
                })
            }
        if self.imagesArray?.count > 0{
                self.imageTExtField.text = " images selected"
 
            }
        }
        self.presentViewController(pickerController, animated: true) {}
        
    }
    @IBAction func onClickOfDocument(sender:UIButton){
        let pickerController = DKImagePickerController()
        pickerController.allowMultipleTypes=false
        pickerController.singleSelect=false
        pickerController.showsCancelButton=true

        pickerController.assetType=DKImagePickerControllerAssetType.AllPhotos
        pickerController.didSelectAssets = { [unowned self] (assets: [DKAsset]) in
            self.documentImagesArray?.removeAllObjects()

            for asset in assets{
                asset.fetchFullScreenImageWithCompleteBlock({ (image, info) -> Void in
                    self.documentImagesArray?.addObject(image!)
                    self.docTextField.text = " docs selected"
                })
            }
            if self.documentImagesArray?.count > 0{
                
                
            }
        }
        self.presentViewController(pickerController, animated: true) {}
    }
    
    @IBAction func submitBtnEvent(sender: UIButton) {
        
        guard let type = buyORsell else{
        showAlertController("SteelMint", message: "Please specify whether you want to buy or sell", reference: self)
            return
            //
        }
        if emailTextField.text!.isEmpty || nameTextField.text!.isEmpty  || descriptionTextView.text!.isEmpty  || mobileTextField.text!.isEmpty || dictOfLocation["country"] == "" || companyTextField.text!.isEmpty {
            showAlertController("SteelMint", message: "Please fill all mendatory information", reference: self)
            return
        }
        
        if emailTextField.text!.isEmail == false{
            showAlertController("SteelMint", message: "Please provide valid email id", reference: self)
            return
        }
        
        if mobileTextField.text!.isPhoneNumber == false{
            showAlertController("SteelMint", message: "Please provide valid Phone Number", reference: self)
            return
        }
        let information = NSMutableDictionary()

        if let typeOfPlant = plantCode{
            information.setObject(typeOfPlant, forKey: "plant_type")
 
        }else{
           showAlertController("SteelMint", message: "Please Select Product Type", reference: self)
            return
        }
   // let arrayOfImages = imagesArray?.arrayByAddingObjectsFromArray(documentImagesArray as! NSMutableArray)
     
        information.setObject(type, forKey: "category")
        information.setObject(descriptionTextView.text, forKey: "description")
        information.setObject(emailTextField.text!, forKey: "email")
        information.setObject(mobileTextField.text!, forKey: "phone")
        information.setObject(nameTextField.text!, forKey: "name")
        information.setObject(companyTextField.text!, forKey: "company")
        information.setObject(dictOfLocation["country"]!, forKey: "country")
        information.setObject(dictOfLocation["state"]!, forKey: "state")
        information.setObject(dictOfLocation["city"]!, forKey: "city")
        //information.setObject(type, forKey: "plant_type")
//        if imagesArray?.count > 0 {
//            information.setObject(imagesArray!, forKey: "images")
//        }
//        if imagesArray?.count > 0 {
//            information.setObject(type, forKey: "docs")
//        }
        let webservicesInstance = WebServices()
        webservicesInstance.progresssIndicatorText = "Posting your Offer..."
        webservicesInstance.PostYourOffer(information,imagesArray: imagesArray,docsArray: documentImagesArray) { (responseData) -> () in
            if responseData != nil{
                guard let status = responseData?["status#"] as? String else{return}
              
                if status == "1"{
                    information.setObject(type, forKey: "plant_type")
                            if self.imagesArray?.count > 0 {
                                information.setObject(self.imagesArray!, forKey: "images")
                            }
                    let date = NSDate().timeIntervalSince1970
                    DatabaseManager.sharedInstance.addMyListings(information,time:date)
                showAlertDifferentTypeFailure("Your post has been saved successfully")
                self.navigationController?.popViewControllerAnimated(true)
                }
            }else{
               showAlertDifferentTypeFailure("Your post has not been saved successfully. Please try again")
            }
        }
        
    }
    @IBAction func buySellBtnEvent(sender: UIButton) {
        switch sender.tag{
        case 88:
            view.viewWithTag(571)?.hidden = true
            view.viewWithTag(572)?.hidden = true
            let tmpButton = self.view.viewWithTag(89) as? UIButton
tmpButton!.setImage(UIImage(named: "unfill.png"), forState: UIControlState.Normal)
        buyORsell = "Sell"
        case 89:
            buyORsell = "Buy"
            view.viewWithTag(571)?.hidden = false
            view.viewWithTag(572)?.hidden = false
            imagesArray?.removeAllObjects()
            documentImagesArray?.removeAllObjects()
            docTextField.text = nil
            imageTExtField.text = nil
            let tmpButton = self.view.viewWithTag(88) as? UIButton
            tmpButton!.setImage(UIImage(named: "unfill.png"), forState: UIControlState.Normal)
        default:
            break
        }
        sender.setImage(UIImage(named: "fill.png"), forState: UIControlState.Normal)

    }
    
    
    @IBAction func textFieldValueChanged(sender: UITextField) {
        print("outside")
        filterArrayOfCountries.removeAllObjects()
        
        if sender.text?.characters.count > 2{
            if addressTableview.hidden == true{
                
                addressTableview.hidden = false
               let yAxis = ((sender.superview?.superview as! UIScrollView).contentOffset.y + 100)
                (sender.superview?.superview as! UIScrollView).setContentOffset(CGPointMake(0, yAxis), animated: true)
                
            }
            dictOfLocation["city"] = ""
            dictOfLocation["state"] = ""
            dictOfLocation["country"] = ""
            
            
            print("inside")
            for values in completecountryList.allValues{
                let dictOfLocation = NSMutableDictionary()
                
                //stringCheck = false
                //country
                if (values["name"] as! String).lowercaseString.containsString(sender.text!.lowercaseString){
                    // stringCheck = true
                    dictOfLocation.setObject(values["name"] as! String, forKey: "cntName")
                    dictOfLocation.setObject(values["id"] as! String, forKey: "cntId")
                }
                //state
                if values["state"] != nil {
                    for valuesState in (values["state"] as! NSDictionary).allValues{
                        if (valuesState["name"] as! String).lowercaseString.containsString(sender.text!.lowercaseString){
                            // stateCheck = true
                            dictOfLocation.setObject(values["name"] as! String, forKey: "cntName")
                            dictOfLocation.setObject(values["id"] as! String, forKey: "cntId")
                            
                            dictOfLocation.setObject(valuesState["name"] as! String, forKey: "stateName")
                            dictOfLocation.setObject(valuesState["id"] as! String, forKey: "stateId")
                        }
                        
                        
                        
                        //city
                        if valuesState["city"] != nil  {
                            for valuesCity in valuesState["city"] as! NSArray{
                                if (valuesCity["name"] as! String).lowercaseString.containsString(sender.text!.lowercaseString){
                                    //  cityCheck = true
                                    
                                    
                                    dictOfLocation.setObject(values["name"] as! String, forKey: "cntName")
                                    dictOfLocation.setObject(values["id"] as! String, forKey: "cntId")
                                    
                                    dictOfLocation.setObject(valuesState["name"] as! String, forKey: "stateName")
                                    dictOfLocation.setObject(valuesState["id"] as! String, forKey: "stateId")
                                    
                                    dictOfLocation.setObject(valuesCity["name"] as! String, forKey: "cityName")
                                    dictOfLocation.setObject(valuesCity["id"] as! String, forKey: "cityId")
                                }
                            }
                            
                        }
                    }
                    
                    
                    
                    
                    
                }
                
                
                
                // print(filterArrayOfCountries)
                if dictOfLocation.count > 0{
                    filterArrayOfCountries.addObject(dictOfLocation)
                    
                }
            }
            
            //
        }else{
            dictOfLocation["city"] = ""
            dictOfLocation["state"] = ""
            dictOfLocation["country"] = ""
            addressTableview.hidden = true
        }
        addressTableview.reloadData()
    }
    
    
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("addressCell", forIndexPath: indexPath)
        let city = cell.viewWithTag(76) as! UILabel
        let state = cell.viewWithTag(77) as! UILabel
        let country = cell.viewWithTag(78) as! UILabel
        
        city.text = nil
        state.text = nil
        country.text = nil
        
        if let dict = filterArrayOfCountries[indexPath.row] as? NSDictionary{
            if addressTextField.text?.characters.count > 2{
                //city
                if dict["cityName"]  == nil{
                    
                    if dict["stateName"]  == nil{
                        city.text = (dict["cntName"] as? String)?.capitalizedString
                    }else{
                        city.text = (dict["stateName"] as? String)?.capitalizedString
                    }
                    
                }else{
                    city.text = (dict["cityName"] as? String)?.capitalizedString
                }
                
                
                //state
                if city.text == (dict["stateName"] as? String)?.capitalizedString{
                    state.text = (dict["cntName"] as? String)?.capitalizedString
                }
                else if city.text == (dict["cntName"] as? String)?.capitalizedString{
                    
                }
                else{
                    if dict["stateName"] == nil {
                        state.text = (dict["cntName"] as? String)?.capitalizedString
                        
                        
                    }else{
                        state.text = (dict["stateName"] as? String)?.capitalizedString
                        
                    }
                }
                
                
                //country
                if state.text == dict["cntName"] as? String || city.text == dict["cntName"] as? String{
                    
                }else{
                    country.text = (dict["cntName"] as? String)?.capitalizedString
                }
            }
        }
        
        
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print(filterArrayOfCountries[indexPath.row])
        let dict = filterArrayOfCountries[indexPath.row]
        addressTableview.hidden = true
        var address = ""
        if dict["cityName"] as? String != nil{
            dictOfLocation["city"] = (dict["cityId"] as! String)
            address = dict["cityName"] as! String + ", "
        }
        if dict["stateName"]as? String != nil{
            dictOfLocation["state"] = (dict["stateId"] as! String)
            
            address += dict["stateName"] as! String + ", "
        }
        if dict["cntName"]as? String != nil{
            dictOfLocation["country"] = (dict["cntId"] as! String)
            address += dict["cntName"] as! String
        }
        
        addressTextField.text = address.capitalizedString
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filterArrayOfCountries.count > 10 {
            return 7
        }
        return filterArrayOfCountries.count
    }
    
    func rearrangeAllPlantsCategories(){
      
            guard let offlineData = DatabaseManager.sharedInstance.getAllPlantbee() else{return}
        
        print("data filtered:\(offlineData)")
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { () -> Void in
            if let data = offlineData as? [NSDictionary]{
                for objects in data{
                    self.arrayOfAllValues.setObject(objects["plant_type_code"] as? String ?? "", forKey: objects["name"] as? String ?? "")
                    
                    if objects["children"] != nil {
                        if let childrenValues = objects["children"] as? NSArray{
                            
                            for childObjects in childrenValues{
                                
                                self.arrayOfAllValues.setObject(childObjects["plant_type_code"] as? String ?? "", forKey: childObjects["name"] as? String ?? "")
                                
                                
                                
                                if childObjects["children"] != nil {
                                    if let childrenValues2 = childObjects["children"] as? NSArray{
                                        
                                        for childObjects2 in childrenValues2{
                                            
                                            self.arrayOfAllValues.setObject(childObjects2["plant_type_code"] as? String ?? "", forKey: childObjects2["name"] as? String ?? "")
                                            
                                        }
                                    }
                                    
                                }
                                
                                
                                
                                
                                
                                
                            }
                        }
                        
                    }
                }
            }
        }
        
        
    }
    
    @IBAction func selectPlantcode(sender:UIButton){
        
        ActionSheetStringPicker.showPickerWithTitle("", rows: arrayOfAllValues.allKeys, initialSelection: 0, doneBlock: { (picker, index, value) -> Void in
            print(value)
           sender.setTitle(value as? String ?? "", forState: UIControlState.Normal)
            self.plantCode = self.arrayOfAllValues[value as? String ?? ""] as? String
            }, cancelBlock: { (picker) -> Void in
            }, origin: self.view)

        
    }
    
   
}
