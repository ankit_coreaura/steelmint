//
//  TenderViewController.swift
//  screens
//
//  Created by Authorised Personnel on 26/10/15.
//  Copyright © 2015 Authorised Personnel. All rights reserved.
//

import UIKit
import CarbonKit
import DCPathButton


class TenderViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource,searchDelegate{
    
    @IBOutlet weak var navigationView:UIView!
    var viaPushId:String?
    var apiSucess:Bool?

        var refreshControlDefault = UIRefreshControl()
        let webservicesInstance=WebServices()
       var arrayOfTenders:[AnyObject]?
        var lastCategoryButtonPressed=""
        var filterVC : FilterViewController?
        var highestTenderId = 0
        var searchedArray :[AnyObject]?
        var searchActive = false
        var tempFilterVIew:UIView?
        var filterActive:Bool?
        var filterDictionary:NSDictionary?
        var noResults:Bool?
    var tempArray: [AnyObject]?

    var indicator = UIActivityIndicatorView()
    var tempVIew = UIView()

        @IBOutlet weak var tableView: UITableView!
    var lastButtonPressed :Int?=2


    @IBOutlet weak var scrollView: UIScrollView!
    //MARK: DEFAULT METHODS
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        viaPushId = nil
    }
        override func viewDidLoad() {
                super.viewDidLoad()
                setupPullToRefresh()
            startUpInitialisations()
                setupScrollview()
            tableView.estimatedRowHeight = 100
            tableView.rowHeight = UITableViewAutomaticDimension

        }
    
    
    func startUpInitialisations(){
        
        getContentsToShow()
        
        //GETTING TENDER CATEGORIES:
         getTendersCategories()

        // Managing Filters
        filterVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("FilterViewController") as? FilterViewController

        self.filterVC!.completionBlock = {
   
            (appliedFilters) -> ()in
            
            if appliedFilters.count>0{
                if appliedFilters["NO_INT"] != nil{
                    self.filterActive = false
                    showAlertController("Steelmint", message: "You cannot use filters in offline mode. Please connect to internet", reference: self)
                    return
                }
                if isInternetConnectivityAvailable(false){
                    if self.tempArray?.count == 0  || self.tempArray == nil{
                        self.tempArray = self.arrayOfTenders
                    }
                    self.arrayOfTenders?.removeAll()
                    self.tableView.reloadData()
                    self.filterActive = true
                    self.filterDictionary = appliedFilters
                    self.setupFilterView()
                    self.getCategoryWiseTender(self.lastCategoryButtonPressed, category:appliedFilters)
                }
                else{
                    self.filterActive = false
                    showAlertController("Steelmint", message: "You cannot use filters in offline mode. Please connect to internet", reference: self)
                }
                //print(appliedFilters)
            }
            else{
                
               // if self.tempArray?.count > 0{
                    self.arrayOfTenders = self.tempArray
                    self.tempArray?.removeAll()
              //  }
                self.filterActive = false
                self.scrollView.userInteractionEnabled = true
                self.tempFilterVIew?.removeFromSuperview()
                self.tempFilterVIew = nil
                self.tableView.reloadData()
                self.getContentsToShow()
                self.getCategoryWiseTenderWithoutFilter(self.lastCategoryButtonPressed)
                
            }
            ////////
        
            //
            //
            //
        }
        
        //OFFLINE HANDLER:
        if CacheManager.sharedInstance.loadObject("tenderFilters") != nil{
            filterVC!.data = CacheManager.sharedInstance.loadObject("tenderFilters") as! NSArray
        }
        
        let information = NSMutableDictionary()
        WebServices().getTenderFilters(information, completionBlock: {
            (responseData) -> ()in
            if responseData != nil{
                if self.filterVC!.data.count == 0{
                self.filterVC!.data = responseData?.objectForKey("data#") as! NSArray
                }
                CacheManager.sharedInstance.saveObject(responseData?.valueForKey("data#"), identifier: "tenderFilters")
            }
        })
        
   
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupScrollview(){
        
        if CacheManager.sharedInstance.loadObject("tenderCategories")?.count>0{
            let arr = CacheManager.sharedInstance.loadObject("tenderCategories") as! NSMutableArray
            print(arr.dynamicType)
            arr.insertObject("All Categories", atIndex: 0)
            apiSucess = true
            getCategoryWiseTenderWithoutFilter(lastCategoryButtonPressed)
            AppCommonFunctions.sharedInstance.buttonsetup(arr,fontSize: 16, scrollView: scrollView, refrence: self)
        }
        else{
//        let arr = ["All Categories","RAW MATERIAL","SEMI FINISHED","FERRO ALLOY","FINISH FLAT","FINISH LONG"]
//        AppCommonFunctions.sharedInstance.buttonsetup(arr,fontSize: 16, scrollView: scrollView, refrence: self)
        }
    }
        @IBAction func onClickOfLeftBarButton(sender:UIButton){
                openLeft()
        }
        func setupPullToRefresh(){
            self.refreshControlDefault.addTarget(self, action: "updateContents", forControlEvents: UIControlEvents.ValueChanged)
            self.tableView.addSubview(refreshControlDefault)
            
            // Setup ActivityIdicator
            indicator = UIActivityIndicatorView  (activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            indicator.frame = CGRectMake(GlobalConstants.SCREEN_WIDTH/2-10, GlobalConstants.SCREEN_HEIGHT/2-10, 10.0, 10.0)
            tempVIew = UIView(frame: tableView.frame)
            tempVIew.backgroundColor = UIColor.whiteColor()

        }
    
    func updateContents(){
        // GETTING ALL TENDERS
        
        
        if filterActive == true{
            NSObject.cancelPreviousPerformRequestsWithTarget(self)
            performSelector("callFilterDataApi", withObject: nil, afterDelay: 1)
        }else{
        NSObject.cancelPreviousPerformRequestsWithTarget(self)
            if apiSucess == true{
                performSelector("getCategoryWiseTenderWithoutFilter:", withObject: lastCategoryButtonPressed, afterDelay: 1)
            }else{
                getTendersCategories()
            }
       
        }
    }
    
    
    //MARK: TABLE VIEW DELEGATES
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        return 100
//    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell=tableView.dequeueReusableCellWithIdentifier("c1", forIndexPath: indexPath) as! TenderTableViewCell
        if searchActive {
            if let tenders = searchedArray?[indexPath.row] as? NSDictionary  {
                cell.tenderTitle.text = tenders.valueForKey("title") as? String
                cell.tenderName.text = tenders.valueForKey("company_name") as? String
                cell.tenderQuantity.text = tenders.valueForKey("qty") as? String
                cell.tenderGrade.text = tenders.valueForKey("grade") as? String
                cell.tenderRegion.text = tenders.valueForKey("region") as? String
                cell.tenderDate.text = tenders.valueForKey("due_date") as? String
                if isNotNull(tenders.valueForKey("imgpath")!){
                    if let url = NSURL(string:tenders.valueForKey("imgpath") as! String){
                        cell.tenderImageView.sd_setImageWithURL(url)
                    }else{
                        cell.tenderImageView.image = UIImage(named: "placeholderImage")
                        
                    }
                    
                }else{
                    cell.tenderImageView.image = UIImage(named: "placeholderImage")
                    
                }
                
            }
            }else{
        if let tenders = arrayOfTenders?[indexPath.row] as? NSDictionary  {
            cell.tenderTitle.text = tenders.valueForKey("title") as? String
            cell.tenderName.text = tenders.valueForKey("company_name") as? String
            cell.tenderQuantity.text = tenders.valueForKey("qty") as? String
            cell.tenderGrade.text = tenders.valueForKey("grade") as? String ?? "  N/A"
            cell.tenderRegion.text = tenders.valueForKey("region") as? String ?? "  N/A"
            cell.tenderDate.text = getDateFromTimestamp2(tenders.valueForKey("due_date") as? NSNumber, dateFormat: "dd MMM, yyyy")
            
            if cell.tenderDate.text?.characters.count == 0 {
                cell.tenderDate.text = tenders.valueForKey("due_date") as? String
            }
            
            if isNotNull(tenders.valueForKey("imgpath")!){
                if let url = NSURL(string:tenders.valueForKey("imgpath") as! String){
                  cell.tenderImageView.sd_setImageWithURL(url)
                }else{
                    cell.tenderImageView.image = UIImage(named: "placeholderImage")
                    
                }
               
            }else{
                cell.tenderImageView.image = UIImage(named: "placeholderImage")
  
            }

        }
        }
        return cell
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView   = nil

        if searchActive {
            if let items = searchedArray?.count {
                if items == 0{
                    noDataToShow()
                }
                return items
            }else{
                noDataToShow()            }
        }else{
        if arrayOfTenders?.count>0{
            return arrayOfTenders!.count
            }}
        noDataToShow()
        return 0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if searchActive{
            let exists = indexPath.row < searchedArray?.count ? true : false
            if exists{
            let tenderDetail = storyboard?.instantiateViewControllerWithIdentifier("TenderDetailsViewController") as? TenderDetailsViewController
            guard let tenderData = searchedArray else{return}
            tenderDetail?.tenderDetailDict = tenderData[indexPath.row] as? NSDictionary
            navigationController?.pushViewController(tenderDetail!, animated: true)
            }}else{
            let tenderDetail = storyboard?.instantiateViewControllerWithIdentifier("TenderDetailsViewController") as? TenderDetailsViewController
            guard let tenderData = arrayOfTenders else{return}
            tenderDetail?.tenderDetailDict = tenderData[indexPath.row] as? NSDictionary
            navigationController?.pushViewController(tenderDetail!, animated: true)

        }
      
    }

    
    // MARK:  CATEGORIES CLICKED

    func butonIndexed(sender:UIButton){
        refreshControlDefault.endRefreshing()

        var offsetX = sender.frame.origin.x+sender.frame.size.width/2 -  GlobalConstants.SCREEN_WIDTH/2
        if offsetX > scrollView.contentSize.width -  GlobalConstants.SCREEN_WIDTH {
            offsetX = scrollView.contentSize.width -  GlobalConstants.SCREEN_WIDTH
        }
        if offsetX < 0 {
            offsetX = 0
        }
        scrollView.setContentOffset(CGPointMake(offsetX, 0), animated: true)
        if let buttonPressed = lastButtonPressed{
            (scrollView.viewWithTag(buttonPressed) as? UIButton)?.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
            (scrollView.viewWithTag(buttonPressed) as? UIButton)?.titleLabel?.font=UIFont.systemFontOfSize(13)
            (scrollView.viewWithTag(buttonPressed+15) as? UIImageView)?.removeFromSuperview()
        }
        lastButtonPressed = sender.tag
        sender.titleLabel?.font=UIFont.boldSystemFontOfSize(14)
        sender.setTitleColor(UIColor(red: 30.0/255.0, green: 136.0/255.0, blue: 229.0/255.0, alpha: 1), forState: .Normal)
        print(sender.currentTitle)
        lastCategoryButtonPressed=sender.currentTitle!
        getContentsToShow()
        NSObject.cancelPreviousPerformRequestsWithTarget(self)
        performSelector("getCategoryWiseTenderWithoutFilter:", withObject: sender.currentTitle!, afterDelay: 1)
        
    }
    func getTendersCategories(){
        if isInternetConnectivityAvailable(false) == false{
            self.refreshControlDefault.endRefreshing()
            return
        }
        if CacheManager.sharedInstance.loadObject("tenderCategories") == nil{
            showNativeActivityIndicator()
        }
        webservicesInstance.getTenderCategories(["":""], completionBlock: {
            (responseData) -> ()in
            self.stopActivityIndicator()
            guard let categories = responseData?.valueForKey("data#") as? NSArray else{return}
            
            print(categories)
            let tempCategoryArray = NSMutableArray()
            for name in categories{
                tempCategoryArray.addObject(name.valueForKey("cat_name")!)
            }
            if CacheManager.sharedInstance.loadObject("tenderCategories") == nil{
                CacheManager.sharedInstance.saveObject(tempCategoryArray, identifier: "tenderCategories")
                self.scrollView.subviews.forEach({ $0.removeFromSuperview() })
                self.setupScrollview()
            }
            CacheManager.sharedInstance.saveObject(tempCategoryArray, identifier: "tenderCategories")
        })
    }
    
    
    func callFilterDataApi(){
        if filterDictionary?.count>0{
            self.getCategoryWiseTender(self.lastCategoryButtonPressed, category:filterDictionary!)
        }
    }
    func getContentsToShow()
    {
        // Managing LAst Tender ID For api request
        highestTenderId = 0
        
        if lastCategoryButtonPressed=="All Categories" || lastCategoryButtonPressed=="" {
            
            if let offlineData = DatabaseManager.sharedInstance.getAllTendersId("") {
                for objects in offlineData{
                    if let id = objects["tender_id"] as? String{
                        let val = NSNumberFormatter().numberFromString(id)!.integerValue
                        if val > highestTenderId{
                            highestTenderId = val
                        }
                    }
                }
            }
            
        }else{
        
        
        
        if let offlineData = DatabaseManager.sharedInstance.getAllTendersId(lastCategoryButtonPressed) {
            
            if offlineData.count > 30 {
            
                for objects in offlineData{
                    if let id = objects["tender_id"] as? String{
                        let val = NSNumberFormatter().numberFromString(id)!.integerValue
                        if val > highestTenderId{
                            highestTenderId = val
                        }
                    }
                }

            
            }else{
                highestTenderId = 0
            }
            
        }}
        
        guard let offlineData = DatabaseManager.sharedInstance.getAllTenders() else{return}
        
        
        if lastCategoryButtonPressed=="All Categories" || lastCategoryButtonPressed=="" {
            let predicate = NSPredicate(format: "viaSearchOrFilter = %@", argumentArray:[false])
            let filteredArray = offlineData.filter { predicate.evaluateWithObject($0) };
            // print(" filtered data is: \(filteredArray)")
            arrayOfTenders = filteredArray
        }
        else{
            let predicate = NSPredicate(format: "category_all contains[c] %@ || category contains[c] %@ && viaSearchOrFilter = %@", argumentArray:[lastCategoryButtonPressed,lastCategoryButtonPressed,false])
            let filteredArray = offlineData.filter { predicate.evaluateWithObject($0) };
           // print(" filtered data is: \(filteredArray)")
            arrayOfTenders = filteredArray
        }
        
        if arrayOfTenders?.count == 0{
            if indicator.isAnimating() == false{
                showNativeActivityIndicator()
            }
        }
        reloadTableView()
        
        // PUSH TO DETAIL VC WHEN PUSH NOTIFICATION ARRIVES:
        guard let tenderData = arrayOfTenders else{return}
        if let via = self.viaPushId {
            for index in  tenderData{
                
                if via == index["tender_id"] as? String{
                if let passdict = index as? NSDictionary {

                        let tenderDetail = storyboard?.instantiateViewControllerWithIdentifier("TenderDetailsViewController") as? TenderDetailsViewController
                        tenderDetail?.tenderDetailDict = passdict
                        self.viaPushId = nil
                        navigationController?.pushViewController(tenderDetail!, animated: true)
 
                    }
                    
                }
                
            }
        }
       
    }
    
    
    func getCategoryWiseTender(var filter:String,category:NSDictionary)
    {
        self.noResults = false
        let information = NSMutableDictionary()
        if filter == "All Categories"{filter = ""}
        let tempFilter="category:\(filter)"
       
        information.setObject(tempFilter, forKey: "filter")
        information.setObject(category, forKey: "arr_filter")
        if filterActive == true{
            showNativeActivityIndicator()
  
        }else{
        if !indicator.isAnimating() && DatabaseManager.sharedInstance.getAllTenders()?.count == nil {
            showNativeActivityIndicator()
        }
        }
        if isInternetConnectivityAvailable(true)==false {
            self.refreshControlDefault.endRefreshing()
            if indicator.isAnimating(){
                self.stopActivityIndicator()
                
            }
            return;
        }
        webservicesInstance.getTenderCategoryWise(information, completionBlock: {
            (responseData) -> ()in
            
            self.refreshControlDefault.endRefreshing()
            if self.indicator.isAnimating(){
                self.stopActivityIndicator()
                
            }
            if responseData != nil{
                //FILTER HANDLE

                if category.count>0{
                    
                    guard let tenderData = responseData?.valueForKey("data#") as?NSMutableArray else{self.noResults = true;return}
                    if tenderData.count == 0{
                        self.noResults = true
                    }else{
                    self.arrayOfTenders = tenderData as [AnyObject]
                    }
                    self.reloadTableView()
                }
            }
            
        })
    }
    
    func getCategoryWiseTenderWithoutFilter(var filter:String)
    {
        self.noResults = false

        print("tender api hit without filter \n \n *********+++++")
        let information = NSMutableDictionary()
        if filter == "All Categories"{filter = ""}
        let tempFilter="category:\(filter)"
        
        information.setObject(tempFilter, forKey: "filter")
        
           // information.setObject("new:\(highestTenderId)", forKey: "postfrom")
    
        if indicator.isAnimating() == false && DatabaseManager.sharedInstance.getAllTenders()?.count == nil {
            showNativeActivityIndicator()
        }
        if isInternetConnectivityAvailable(true)==false {
            self.refreshControlDefault.endRefreshing()
            if indicator.isAnimating(){
                self.stopActivityIndicator()
                
            }
            return;
        }
        webservicesInstance.getTenderCategoryWise(information, completionBlock: {
            (responseData) -> ()in
            UIApplication.sharedApplication().endIgnoringInteractionEvents()

                        self.refreshControlDefault.endRefreshing()
            if self.indicator.isAnimating(){
                self.stopActivityIndicator()
                
            }
            if responseData != nil{

                guard let tender = responseData?.valueForKey("data#") as? NSMutableArray
                        else{self.noResults = true
                            return}
               DatabaseManager.sharedInstance.deleteTenderforCategory(self.lastCategoryButtonPressed)
                    for object in tender
                    {
                        DatabaseManager.sharedInstance.addTender(object as! NSDictionary)
                    }
                if let _ = self.viaPushId {
                self.getContentsToShow()
                }
               else if tender.count > 0{
                    self.getContentsToShow()
                }
                }
            
        })
    }
    
    
    func pushApihit(){
        
        if isInternetConnectivityAvailable(true)==false {
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
            return;
        }

        webservicesInstance.getTenderCategoryWise(nil, completionBlock: {
            (responseData) -> ()in
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
            
            if responseData != nil{
                
                guard let tender = responseData?.valueForKey("data#") as? NSMutableArray
                    else{return}
                
                for object in tender
                {
                    DatabaseManager.sharedInstance.addTender(object as! NSDictionary)
                }
                    self.getContentsToShow()
            }
            
        })
    }

    func reloadTableView()
    {
        self.tableView.reloadData()
    }
    @IBAction func onClickRightBarButton(sender: UIButton) {
        filterVC?.showAnimated()
    }

    
    // MARK: Search Management

    
    @IBAction func searchBtnEvent(sender:UIButton){
        self.navigationView.userInteractionEnabled = false

        let searchView: SearchView = SearchView()
        searchView.frame = CGRectMake(0, -99, UIScreen.mainScreen().bounds.width, 99)
        
        UIView.animateWithDuration(0.3) { () -> Void in
            searchView.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 99)
        }
        searchView.delegate = self
        self.view.addSubview(searchView)
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissMode.OnDrag
    }
    // MARK: Search View Delegates
    func textForSearch(text: String) {
        self.tableView.allowsSelection = false
        NSObject.cancelPreviousPerformRequestsWithTarget(self)
        performSelector("searchApiHit:", withObject: text, afterDelay: 1)
    }
    func textForSearchButtonClicked(text: String) {
        self.tableView.allowsSelection = false
        NSObject.cancelPreviousPerformRequestsWithTarget(self)
        performSelector("searchApiHit:", withObject: text, afterDelay: 1)
    }
    func cancelCallBack(flag: Bool) {
        self.navigationView.userInteractionEnabled = true
        searchActive = false
        tableView.addSubview(refreshControlDefault)
        tableView.reloadData()
    }
    func searchDidBegin(serachBar: UISearchBar) {
        if refreshControlDefault.refreshing{
            refreshControlDefault.endRefreshing()
        }
        refreshControlDefault.removeFromSuperview()
        searchedArray?.removeAll()
        searchActive = true
        tableView.reloadData()
    }
    
    
    func searchApiHit(searchText:String){
        self.tableView.allowsSelection = true
        if noResults == true{
            self.tableView.backgroundView = nil
        }
        self.noResults = false
        if !indicator.isAnimating(){
            showNativeActivityIndicator()
        }
        if !isInternetConnectivityAvailable(true){
            if indicator.isAnimating(){
                self.stopActivityIndicator()
            }
            return
        }
        let searchDict = NSDictionary(object: "search:\(searchText)", forKey: "filter")
        webservicesInstance.getTendersSearchWise(searchDict) { (responseData) -> () in
            self.searchedArray?.removeAll()
            if self.indicator.isAnimating(){
                self.stopActivityIndicator()
            }
            if responseData != nil{
                guard let response = responseData?["data#"] as? [NSDictionary] else{ self.noResults = true;return}
                if response.count == 0{
                    self.noResults = true
                }else{
                    self.searchedArray = response}
                self.tableView.reloadData()
            }
        }
    }
    //MARK: ACTIVITY INDICATOR
    func showNativeActivityIndicator(){
        scrollView.userInteractionEnabled = false
        self.tableView.allowsSelection = false
        scrollView.layer.opacity = 0.3

        view.addSubview(tempVIew)
        view.addSubview(indicator)
        indicator.startAnimating()
    }
    
    func stopActivityIndicator() {
        scrollView.userInteractionEnabled = true
        scrollView.layer.opacity = 1.0

        self.tableView.allowsSelection = true
        indicator.stopAnimating()
        tempVIew.removeFromSuperview()
    }

   //MARK: FILTERVIEW
    func setupFilterView(){
        if tempFilterVIew == nil{
            self.scrollView.userInteractionEnabled = false
            tempFilterVIew = UIView(frame: CGRectMake(0, 64, GlobalConstants.SCREEN_WIDTH,35))
            tempFilterVIew!.backgroundColor = UIColor.whiteColor()
            let label = UILabel(frame: CGRectMake(10, tempFilterVIew!.bounds.minY, 180, 30))
            let clearAllBtn = UIButton(frame: CGRectMake(GlobalConstants.SCREEN_WIDTH-35, tempFilterVIew!.bounds.minY+3, 30, 30))
            clearAllBtn.setImage(UIImage.image(UIImage(named: "cancelImage")!, withColor: UIColor.whiteColor()), forState: .Normal)
            clearAllBtn.addTarget(self, action: "clearFilters", forControlEvents: UIControlEvents.TouchUpInside)
            clearAllBtn.backgroundColor = GlobalConstants.APP_THEME_BLUE_COLOR
            clearAllBtn.layer.cornerRadius = 15.0
            clearAllBtn.layer.borderWidth = 2.0
            clearAllBtn.layer.borderColor = UIColor.whiteColor().CGColor
            clearAllBtn.tintColor = UIColor.whiteColor()
            
            tempFilterVIew!.addSubview(clearAllBtn)
            tempFilterVIew!.addSubview(label)
            label.text = "Filtered Results:"
            label.font = UIFont.systemFontOfSize(13)
            label.textColor = UIColor.darkGrayColor()
            tempFilterVIew!.layer.borderColor = UIColor.grayColor().CGColor
            tempFilterVIew!.layer.borderWidth = 1.0

            self.view.addSubview(tempFilterVIew!)
        }
    }
    func clearFilters(){
       // if tempArray?.count > 0{
            self.arrayOfTenders = tempArray
            tempArray?.removeAll()
       // }
        self.filterActive = false
        self.scrollView.userInteractionEnabled = true
        self.tempFilterVIew?.removeFromSuperview()
        self.tempFilterVIew = nil
        self.tableView.reloadData()
        filterVC!.cancelBtnEvent(UIButton())
        getContentsToShow()
        self.getCategoryWiseTenderWithoutFilter(self.lastCategoryButtonPressed)
    }
    // MARK: No data Management
    func noDataToShow(){
        //if noResults == true{
            let noDataLabel = UILabel (frame: CGRectMake(0, 0,tableView.bounds.size.width,tableView.bounds.size.height))
//            noDataLabel.text             = "No Live Tenders"
//            noDataLabel.textColor        = UIColor.lightGrayColor()
//            noDataLabel.textAlignment    = .Center
//            tableView.backgroundView = noDataLabel;
       // }
    }
    
    // MARK: Push Management
    func pushNotificationManagement(){
        if let _ = viaPushId{
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        pushApihit()
        }
    }
}
