//
//  TenderTableViewCell.swift
//  Application
//
//  Created by Ankit Nandal on 01/12/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit

class TenderTableViewCell: UITableViewCell {

    @IBOutlet weak var tenderTitle: UILabel!
   
    @IBOutlet weak var tenderName: UILabel!
   
    @IBOutlet weak var tenderGrade: UILabel!

    @IBOutlet weak var tenderDate: UILabel!
    @IBOutlet weak var tenderQuantity: UILabel!

    @IBOutlet weak var tenderRegion: UILabel!

    @IBOutlet weak var tenderDetailName: UILabel!

    @IBOutlet weak var tenderImageView: UIImageView!
    @IBOutlet weak var tenderDetailQuantity: UILabel!

    @IBOutlet weak var tenderDetailImageView: UIImageView!
    @IBOutlet weak var tenderDetailRegion: UILabel!

    @IBOutlet weak var tenderDetailGrade: UILabel!
    @IBOutlet weak var tenderDetailDate: UILabel!
    @IBOutlet weak var tenderDetailTitle: UILabel!
    @IBOutlet weak var tenderDetailDescription: UILabel!
}
