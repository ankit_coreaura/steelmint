//
//  TermsViewController.swift
//  Application
//
//  Created by Ankit Nandal on 18/01/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//

import UIKit

class TermsViewController: UIViewController {
    var terms = [NSDictionary]()
    var indicator:UIActivityIndicatorView?
    var tempVIew: UIView?
    @IBOutlet var tableVIew: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       tableVIew.estimatedRowHeight = 74
       tableVIew.rowHeight = UITableViewAutomaticDimension
        getTnC()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getTnC(){
        let webserviceInstance =  WebServices()
        if CacheManager.sharedInstance.loadObject("TnC") != nil{
            self.terms = CacheManager.sharedInstance.loadObject("TnC") as! [NSDictionary]
        }
        if terms.count < 1{
        showNativeActivityIndicator()
        }
        webserviceInstance.getTermsAndConditions(NSDictionary(), completionBlock: {
            (responseData) ->() in
            if ((self.indicator?.isAnimating()) != nil){
            self.stopActivityIndicator()
            }
            if responseData != nil{
                guard let response = responseData?.valueForKey("data#") as? [NSDictionary] else{return}
                var formattedArray = [NSDictionary]()
                
                for objects in response{
                    var value = String()
                    if let x =  objects["value"] as? String{
                    value = getHTMlRenderedString(x).string
                    }
                 let dict = NSDictionary(objects: [objects["name"]!,value], forKeys: ["name","value"])
                    formattedArray.append(dict)
                }
                self.terms = formattedArray
                CacheManager.sharedInstance.saveObject(self.terms, identifier: "TnC")
                self.tableVIew.reloadData()
            }
            
        })

    }
    // MARK: TABLEVIEW DELEGATES
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableVIew.dequeueReusableCellWithIdentifier("c1", forIndexPath: indexPath)
        let title = (cell.viewWithTag(2) as! UILabel)
        let desc = (cell.viewWithTag(3) as! UILabel)
        title.text  = terms[indexPath.row]["name"] as? String
        desc.text  = terms[indexPath.row]["value"] as? String
        return cell
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return terms.count
    }
    
    @IBAction func dismissVIewController(sender:UIButton){
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    //MARK: ACTIVITY INDICATOR
    
    func showNativeActivityIndicator(){
        indicator = UIActivityIndicatorView  (activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        indicator!.frame = CGRectMake(GlobalConstants.SCREEN_WIDTH/2-10, GlobalConstants.SCREEN_HEIGHT/2-10, 10.0, 10.0)
        tempVIew = UIView(frame: tableVIew.frame)
        tempVIew!.backgroundColor = UIColor.whiteColor()
        view.addSubview(tableVIew)
        view.addSubview(indicator!)
        indicator!.startAnimating()
    }
    
    func stopActivityIndicator() {
        indicator?.stopAnimating()
        tempVIew?.removeFromSuperview()
    }
}




