//
//  AddressViewController.swift
//  Application
//
//  Created by Ankit Nandal on 17/01/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//

import UIKit
 protocol AddressDelegate{
      func returnAddressWithCode(value:String,name:String,via:String)
}
class AddressViewController: UIViewController,UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet var navigationTitle: UILabel!
    @IBOutlet var tableVIew: UITableView!
    var delegate:AddressDelegate?=nil
    var predicate = NSPredicate()
    var tempdict = NSDictionary()
    var dataArray = [String]()
    var navigationTitleText = ""
    @IBOutlet var searchBar: UISearchBar!
    var filteredArray = [String]()
    //var dataArray = [NSDictionary]()
    var shouldShowSearchResults = false
    var via = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationTitle.text = navigationTitleText
       let sortedArray:[String] = tempdict.allKeys as! [String]
        dataArray = sortedArray.sort{ $0.localizedCaseInsensitiveCompare($1) == NSComparisonResult.OrderedAscending }
        ////print(tempdict)
        //print(dataArray)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func dismissController(sender:UIButton){
       
        dismissViewControllerAnimated(true, completion: nil)
        
    }
    // MARK: TABLEVIEW DELEGATES
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        self.tableVIew.keyboardDismissMode = UIScrollViewKeyboardDismissMode.Interactive
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableVIew.dequeueReusableCellWithIdentifier("c1", forIndexPath: indexPath)
        cell.textLabel?.textColor = UIColor.darkGrayColor()
        cell.textLabel?.font = cell.textLabel?.font.fontWithSize(15)
        
        if shouldShowSearchResults {
            cell.textLabel?.text = filteredArray[indexPath.row] as String
        }
        else {
            cell.textLabel?.text = dataArray[indexPath.row] as String
            
        }
        cell.textLabel?.text = cell.textLabel?.text?.capitalizedString
        return cell
 
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if shouldShowSearchResults {
            
            return filteredArray.count
            
        }
        else {
            return dataArray.count
        }
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var from = ""
        switch via{
            case "c":
            from = via
        case "s":
            from = via
        case "d":
            from = via
        case "ct":
            from = via
        case "p":
            from = via
        case "state":
            from = via
        case "city":
            from = via
        case "dpt":
            from = via
        case "c":
            from = via
        case "c":
            from = via
        default:
            from = ""
        }
        
        if shouldShowSearchResults{
            let exists = indexPath.row < filteredArray.count ? true : false
            
            if exists{//print(filteredArray[indexPath.row])
                let code = tempdict.valueForKey(filteredArray[indexPath.row]) as! String
                delegate!.returnAddressWithCode(code,name: filteredArray[indexPath.row],via: from )
                dismissViewControllerAnimated(true, completion: nil)
            return
            }
            
            
            return
        }
        let code = tempdict.valueForKey(dataArray[indexPath.row]) as! String
        delegate!.returnAddressWithCode(code,name: dataArray[indexPath.row],via: from )
        //print(dataArray[indexPath.row])
        dismissViewControllerAnimated(true, completion: nil)

    }
    // MARK: Searchbar Delegates Methods
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        shouldShowSearchResults = true
        tableVIew.reloadData()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar){
        tableVIew.reloadData()
        
    }
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        shouldShowSearchResults = false
        tableVIew.reloadData()
        
        searchBar.resignFirstResponder()
    }
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        filteredArray.removeAll()
                if searchBar.text?.characters.count>0{
            let searchString = searchBar.text
            
            // Filter the data array and get only those countries that match the search text.
            filteredArray = dataArray.filter({ (country) -> Bool in
                let countryText: NSString = country
                
                return (countryText.rangeOfString(searchString!, options: NSStringCompareOptions.CaseInsensitiveSearch).location) != NSNotFound
            })
        }
        tableVIew.reloadData()
    }
   
}
