//
//  RegisterViewController.swift
//  SteelMint
//
//  Created by Saurabh Singh on 08/09/15.
//  Copyright (c) 2015 Saurabh Singh. All rights reserved.
//



import UIKit
import CoreLocation
import SwiftLocation
import SCLAlertView
import ActionSheetPicker_3_0
class RegisterViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate{
    @IBOutlet weak var registerBTNOutlet:UIButton!
    var completecountryList = NSDictionary()
    var dictOfLocation = ["country":"","state":"","city":""]
    let filterArrayOfCountries = NSMutableArray()
    var isSubscribeNavigation:Bool?
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var companyTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var addressTableview: UITableView!
    
    var termsconditionsVerified = false
    
    var isEverythingValidated = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchcountryList()
    }
    func fetchcountryList(){
        let s = try! String(contentsOfFile: NSBundle.mainBundle()
            .pathForResource("countryList", ofType: "txt")!,
            encoding: NSUTF8StringEncoding)
        let x = parsedJsonFrom2(s.dataUsingEncoding(NSUTF8StringEncoding))
        if x != nil{
            if let dict = x?.valueForKey("data#") as? NSDictionary{
                if self.completecountryList.count == 0{
                    self.completecountryList = dict
                }
            }
        }
        
        addressTableview.hidden = true
        addressTableview.layer.borderColor = UIColor.lightGrayColor().CGColor
        addressTableview.layer.borderWidth = 1.0
        addressTableview.backgroundColor = UIColor(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
        
    }
    
    @IBAction func onClickOfLeftBarButton(sender:UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func onClickOfRegisterUser(sender: UIButton){
        isEverythingValidated = true
        let information = NSMutableDictionary()
        
        if nameTextField.text!.isEmpty || emailTextField.text!.isEmpty || mobileTextField.text!.isEmpty || companyTextField.text!.isEmpty || passwordTextField.text!.isEmpty || confirmPasswordTextField.text!.isEmpty || dictOfLocation["country"] == ""
        {
			
			var blankFieldName: String = ""
			if nameTextField.text!.isEmpty{
				nameTextField.becomeFirstResponder()
				blankFieldName = "Name"
			}else if emailTextField.text!.isEmpty {
				blankFieldName = "Email"
				emailTextField.becomeFirstResponder()
			}else if passwordTextField.text!.isEmpty {
				blankFieldName = "Password"
				passwordTextField.becomeFirstResponder()
			}else if confirmPasswordTextField.text!.isEmpty {
				blankFieldName = "Confirm Password"
				confirmPasswordTextField.becomeFirstResponder()
			}else if mobileTextField.text!.isEmpty {
				blankFieldName = "Mobile No"
				mobileTextField.becomeFirstResponder()
			}else if companyTextField.text!.isEmpty{
				blankFieldName = "Company Name"
				companyTextField.becomeFirstResponder()
			}else if dictOfLocation["country"] == ""{
				blankFieldName = "Location"
				addressTextField.becomeFirstResponder()
			}
			
			showAlertController("SteelMint", message: "Please Complete \(blankFieldName) field", reference: self)
			}
        else
        {
            information.setObject(nameTextField.text!, forKey: "name")
			
			   // validating email============================
            if (emailTextField.text!.isEmail)
            {
                let emailId = emailTextField.text!.lowercaseString
                information.setObject(emailId, forKey: "email")
            }
            else
            {
                isEverythingValidated = false
                showAlertController("SteelMint", message: "Please enter valid Email id", reference: self)
            }
            // validating phone============================
            
            if (mobileTextField.text!.isPhoneNumber){
                information.setObject(mobileTextField.text!, forKey: "phoneno")
            }
            else{
                if isEverythingValidated {
                    isEverythingValidated = false
                    showAlertController("SteelMint", message: "Please enter valid Phone Number", reference: self)}
            }
            information.setObject(companyTextField.text!, forKey: "company")
            
            information.setObject(dictOfLocation["state"]!, forKey: "state")
            
            information.setObject(dictOfLocation["country"]!, forKey: "country")
            
            
            // validating password============================
            if passwordTextField.text! == confirmPasswordTextField.text!{
                if passwordTextField.text!.isValidPassword{
                    //                                    isEverythingValidated = true
                    information.setObject(passwordTextField.text!, forKey: "password")}
                else{if isEverythingValidated {
                    isEverythingValidated = false
                    showAlertController("SteelMint", message: "Please enter valid password containing minimum 6 alphanumeric character", reference: self)}}
                
            }
            else{
                if isEverythingValidated {
                    isEverythingValidated = false
                    showAlertController("SteelMint", message: "Password does not match", reference: self)}
            }
            
            
            // if everything validated hit service
            if isEverythingValidated{
                let webServicesObject = WebServices()
                webServicesObject.progresssIndicatorText = "Registering"
                
                    webServicesObject.registerUser(information)
                        { (responseData) -> () in
                            
                            if let _ = responseData{
                                let alertView = SCLAlertView()
                                alertView.showTitle("SteelMint", subTitle: "Please check your mail & Click on the link to activate your account", style: SCLAlertViewStyle.Success, closeButtonTitle: nil, duration: 100, colorStyle: 0xE4E4E4, colorTextButton: 0xFFFFFF)
                                let button =  alertView.addButton("Login Now") { () -> Void in
                                    self.navigationController?.popViewControllerAnimated(true)
                                }
                                button.backgroundColor =  GlobalConstants.APP_THEME_DARK_BLUE_COLOR
                            }
                    }
                
                
                
                
            }
        }
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
  
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if filterArrayOfCountries.count > 10 {
            return 7
        }
        return filterArrayOfCountries.count
        
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("addressCell", forIndexPath: indexPath)
        let city = cell.viewWithTag(76) as! UILabel
        let state = cell.viewWithTag(77) as! UILabel
        let country = cell.viewWithTag(78) as! UILabel
        
        city.text = nil
        state.text = nil
        country.text = nil
        
        if let dict = filterArrayOfCountries[indexPath.row] as? NSDictionary{
            if addressTextField.text?.characters.count > 2{
                //city
                if dict["cityName"]  == nil{
                    
                    if dict["stateName"]  == nil{
                        city.text = (dict["cntName"] as? String)?.capitalizedString
                    }else{
                        city.text = (dict["stateName"] as? String)?.capitalizedString
                    }
                    
                }else{
                    city.text = (dict["cityName"] as? String)?.capitalizedString
                }
                
                
                //state
                if city.text == (dict["stateName"] as? String)?.capitalizedString{
                    state.text = (dict["cntName"] as? String)?.capitalizedString
                }
                else if city.text == (dict["cntName"] as? String)?.capitalizedString{
                    
                }
                else{
                    if dict["stateName"] == nil {
                        state.text = (dict["cntName"] as? String)?.capitalizedString
                        
                        
                    }else{
                        state.text = (dict["stateName"] as? String)?.capitalizedString
                        
                    }
                }
                
                
                //country
                if state.text == dict["cntName"] as? String || city.text == dict["cntName"] as? String{
                    
                }else{
                    country.text = (dict["cntName"] as? String)?.capitalizedString
                }
            }
        }
        
        
        return cell
        
    }
    
    @IBAction func checkBoxBtnEvent(sender: UIButton) {
        if termsconditionsVerified{
            termsconditionsVerified = false
            sender.setImage(UIImage(named: "unCheckedBox"), forState: UIControlState.Normal)
            registerBTNOutlet.enabled = false
            
        }else{
            termsconditionsVerified = true
            sender.setImage(UIImage(named: "checkedBox"), forState: UIControlState.Normal)
            registerBTNOutlet.enabled = true
        }
        
        
    }
    
    @IBAction func terms(sender: UIButton) {
        
        let termsINStance = storyboard?.instantiateViewControllerWithIdentifier("TermsViewController") as! TermsViewController
        presentViewController(termsINStance, animated: true, completion: nil)
    }
    
    // MARK: Country Selection Management:
    
    @IBAction func textFieldValueChanged(sender: UITextField) {
        print("outside")
        filterArrayOfCountries.removeAllObjects()
        
        if sender.text?.characters.count > 2{
            if addressTableview.hidden == true{
                let yAxis = ((sender.superview?.superview as! UIScrollView).contentOffset.y + 100)
                addressTableview.hidden = false
                (sender.superview?.superview as! UIScrollView).setContentOffset(CGPointMake(0, yAxis), animated: true)
                
            }
            dictOfLocation["city"] = ""
            dictOfLocation["state"] = ""
            dictOfLocation["country"] = ""
            
            
            print("inside")
            for values in completecountryList.allValues{
                let dictOfLocation = NSMutableDictionary()
                
                //stringCheck = false
                //country
                if (values["name"] as! String).lowercaseString.containsString(sender.text!.lowercaseString){
                    // stringCheck = true
                    dictOfLocation.setObject(values["name"] as! String, forKey: "cntName")
                    dictOfLocation.setObject(values["id"] as! String, forKey: "cntId")
                }
                //state
                if values["state"] != nil {
                    for valuesState in (values["state"] as! NSDictionary).allValues{
                        if (valuesState["name"] as! String).lowercaseString.containsString(sender.text!.lowercaseString){
                            // stateCheck = true
                            dictOfLocation.setObject(values["name"] as! String, forKey: "cntName")
                            dictOfLocation.setObject(values["id"] as! String, forKey: "cntId")
                            
                            dictOfLocation.setObject(valuesState["name"] as! String, forKey: "stateName")
                            dictOfLocation.setObject(valuesState["id"] as! String, forKey: "stateId")
                        }
                        
                        
                        
                        //city
                        if valuesState["city"] != nil  {
                            for valuesCity in valuesState["city"] as! NSArray{
                                if (valuesCity["name"] as! String).lowercaseString.containsString(sender.text!.lowercaseString){
                                    //  cityCheck = true
                                    
                                    
                                    dictOfLocation.setObject(values["name"] as! String, forKey: "cntName")
                                    dictOfLocation.setObject(values["id"] as! String, forKey: "cntId")
                                    
                                    dictOfLocation.setObject(valuesState["name"] as! String, forKey: "stateName")
                                    dictOfLocation.setObject(valuesState["id"] as! String, forKey: "stateId")
                                    
                                    dictOfLocation.setObject(valuesCity["name"] as! String, forKey: "cityName")
                                    dictOfLocation.setObject(valuesCity["id"] as! String, forKey: "cityId")
                                }
                            }
                            
                        }
                    }
                    
                    
                    
                    
                    
                }
                
                
                
                // print(filterArrayOfCountries)
                if dictOfLocation.count > 0{
                    filterArrayOfCountries.addObject(dictOfLocation)
                    
                }
            }
            
            //
        }else{
            dictOfLocation["city"] = ""
            dictOfLocation["state"] = ""
            dictOfLocation["country"] = ""
        addressTableview.hidden = true
        }
        addressTableview.reloadData()
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if tableView == addressTableview{
            print(filterArrayOfCountries[indexPath.row])
            let dict = filterArrayOfCountries[indexPath.row]
            addressTableview.hidden = true
            var address = ""
            if dict["cityName"] as? String != nil{
                dictOfLocation["city"] = (dict["cityId"] as! String)
                address = dict["cityName"] as! String + ", "
            }
            if dict["stateName"]as? String != nil{
                dictOfLocation["state"] = (dict["stateId"] as! String)
                
                address += dict["stateName"] as! String + ", "
            }
            if dict["cntName"]as? String != nil{
                dictOfLocation["country"] = (dict["cntId"] as! String)
                address += dict["cntName"] as! String
            }
            
            addressTextField.text = address.capitalizedString
        }
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
}





