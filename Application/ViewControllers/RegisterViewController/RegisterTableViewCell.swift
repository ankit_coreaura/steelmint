//
//  RegisterTableViewCell.swift
//  Application
//
//  Created by Ankit Nandal on 15/12/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit

class RegisterTableViewCell: UITableViewCell {

    @IBOutlet weak var countryTitle: UIButton!
  
    @IBOutlet weak var stateTitle: UIButton!

}
