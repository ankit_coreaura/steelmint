//
//  PlantbeeViewController.swift
//  Application
//
//  Created by Ankit Nandal on 25/11/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit
import MessageUI
import Parse
import Bolts
import ZDCChat

class PlantbeeViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource ,MFMailComposeViewControllerDelegate,searchDelegate {
    @IBOutlet weak var navigationView:UIView!
    var pageControl:UIPageControl?
    var indicator:UIActivityIndicatorView?
    var searchedArray :[AnyObject]?
    var filterVC : FilterViewController?
    var viaPushId:String?
    var pushDIct:NSDictionary?
    var tempScrollView:UIScrollView?
    var searchActive = false
    let webserviceInstance = WebServices()
    @IBOutlet weak var tableVIew:UITableView!
    var plantOptionsArray = ["Buyers","Sellers","My Listings","Post Your Offers","Contact Us","FAQ's"]//,"Plan And Pricings"]
    var scrollViewImages = [NSDictionary]()
    var optionsImages = ["buy_a_plant","sell_a_plant","my_listings","post_ur_offers","call_us","faqs"]//"plan_Price"]
    var currentPage = 0
    let secondaryStoryboard = UIStoryboard(name: "Secondary", bundle: nil)
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
       tableVIew.registerClass(UITableViewCell.self, forCellReuseIdentifier: "searchCell")
    }
    func initialSetup(){
       
        getPlantbeeImages()
        
        getPlantBeeFilters()
       
        if DatabaseManager.sharedInstance.getAllPlantbee()?.count == nil || DatabaseManager.sharedInstance.getAllPlantbee()?.count  == 0{
            getPLantCode()
   
        }
        
        
    }
    func getPLantCode(){
        WebServices().getBuyTypePlants(NSDictionary(), completionBlock: {
            (responseData)->() in
            
            if responseData != nil{
                guard let responseArray = responseData?["data#"] as? NSArray else{return}
                for objects in responseArray{
                    if objects is NSDictionary{
                        DatabaseManager.sharedInstance.addPlantbee(objects as! NSDictionary, type: "buy")
                    }
                    
                }
                
            }
            
        })
    }
    func getPlantBeeFilters(){
     
        WebServices().getPlantbeeFilters(NSDictionary(), completionBlock: {
            (responseData) -> ()in
            if responseData != nil{
                
                CacheManager.sharedInstance.saveObject(responseData?.valueForKey("data#"), identifier: "plantbeeFilters")
            }
        })

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func openLeftMenuBtnEvent(sender: UIButton) {
        openLeft()
    }
    
    //MARK: TABLE VIEW DELEGATES
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchActive{
            guard let numberOfRows = searchedArray?.count else{return 0}
                return numberOfRows            }

        return plantOptionsArray.count+1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        print(indexPath.row)
        if searchActive{
            let cell = tableView.dequeueReusableCellWithIdentifier("searchCell", forIndexPath: indexPath)
            guard let searchedData = searchedArray?[indexPath.row] else{return cell}
            cell.textLabel?.text = searchedData["name"] as? String
            cell.textLabel?.textColor = UIColor.darkGrayColor()
            cell.textLabel?.font = cell.textLabel?.font.fontWithSize(11)
            return cell
        
        }else{
        switch indexPath.row
        {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier("cell1", forIndexPath: indexPath) as! PlantTableCell
            pageControl = cell.imagePagecontrol
            setupScrollView(cell)

            tempScrollView = cell.imageScrollview
            cell.imageScrollview.showsHorizontalScrollIndicator = false
            return cell
            
        default:
            let cell = tableView.dequeueReusableCellWithIdentifier("cell2", forIndexPath: indexPath)
            cell.textLabel?.text = plantOptionsArray[indexPath.row-1]
            cell.textLabel?.textColor = UIColor.blueColor()
            cell.accessoryType = .DisclosureIndicator
            switch indexPath.row{
                
            case 5...7:
                cell.backgroundColor = UIColor(red: 237.0/255, green: 237.0/255, blue: 237.0/255, alpha: 1)
                cell.textLabel?.textColor = UIColor.darkGrayColor()
                
            default:
                break
            }
            cell.imageView?.image = UIImage(named: optionsImages[indexPath.row-1])
            return cell
            
        }
        }
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if searchActive{
            let storyboard = UIStoryboard(name: "Secondary", bundle: nil)
            let  buyPlantListingInstance = storyboard.instantiateViewControllerWithIdentifier("BuyPlantListingViewController") as! BuyPlantListingViewController
            guard let text = searchedArray?[indexPath.row]["name"] as? String else{return}
           buyPlantListingInstance.searchDict = NSDictionary(object:text , forKey: "search")
                buyPlantListingInstance.totalListings = "Search Results"
            self.navigationController?.pushViewController(buyPlantListingInstance, animated: true)
          
           
        }else{
        
        print(indexPath.row)
        switch indexPath.row{
        case 1:
            let buyPlantInstance = secondaryStoryboard.instantiateViewControllerWithIdentifier("BuyCategoryViewController") as! BuyCategoryViewController
            navigationController?.pushViewController(buyPlantInstance, animated: true)
        case 2:
            let sellPlantInstance = secondaryStoryboard.instantiateViewControllerWithIdentifier("SellCategoryViewController") as! SellCategoryViewController
            navigationController?.pushViewController(sellPlantInstance, animated: true)
        case 3:
            let myListingPlantInstance = secondaryStoryboard.instantiateViewControllerWithIdentifier("MyListingsViewController") as! MyListingsViewController
            navigationController?.pushViewController(myListingPlantInstance, animated: true)
        case 4:
            let postOfferinstance = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("PostOffersViewController") as! PostOffersViewController
            postOfferinstance.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(postOfferinstance, animated: true)
        case 5:
            dispatch_async(dispatch_get_main_queue(), {()->() in
                self.showActionSheet()
            })
        case 6:
            let faqsinstance = UIStoryboard(name: "Secondary", bundle: nil).instantiateViewControllerWithIdentifier("FAQsViewController") as! FAQsViewController
            faqsinstance.viaOption = "PLANTBEE"
            navigationController?.pushViewController(faqsinstance, animated: true)
//        case 7:
//            if isUserLoggedIn(){
//                let planinstance = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("SubscribeViewController") as! SubscribeViewController
//                planinstance.hidesBottomBarWhenPushed = true
//                planinstance.viaOption = "PLANTBEE"
//                navigationController?.pushViewController(planinstance, animated: true)
//            }
        default:
            return
        }
        }
        
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if searchActive{
          return 34
        }else{
        switch indexPath.row{
        case 0:
            return 200
        default:
            return 64
        }
        }
    }
    func setupScrollView(cell:PlantTableCell){
        
        if let imageData = NSUserDefaults.standardUserDefaults().valueForKey("plantbeeImages") as? [NSDictionary]{
            scrollViewImages = imageData
        }
        print(scrollViewImages.description)
        cell.imagePagecontrol.numberOfPages = scrollViewImages.count
        
        for var i=0;i<scrollViewImages.count;i++ {
            let imageview = UIImageView()
            
            imageview.frame =  CGRect(x: i*Int( GlobalConstants.SCREEN_WIDTH) , y: Int(cell.contentView.bounds.origin.y), width: Int( GlobalConstants.SCREEN_WIDTH), height: Int(cell.contentView.bounds.height))
            imageview.clipsToBounds = true
            imageview.contentMode = UIViewContentMode.ScaleToFill
            if let imageURl = scrollViewImages[i]["image"] as? String{
                imageview.sd_setImageWithURL(NSURL(string: imageURl)!)
                
            }
            else{
                imageview.image = UIImage(named: "placeholderImage")
                
            }
            cell.imageScrollview.addSubview(imageview)
            
        }
        cell.imageScrollview.contentSize = CGSize(width:  GlobalConstants.SCREEN_WIDTH*CGFloat(scrollViewImages.count), height:cell.imageScrollview.bounds.height)
    }
    func autoScroll(timer:NSTimer){
        let cell = timer.userInfo as! PlantTableCell
        cell.imageScrollview.setContentOffset(CGPoint(x: currentPage*Int( GlobalConstants.SCREEN_WIDTH), y: 0), animated: true)
        cell.imagePagecontrol.currentPage = currentPage
        currentPage++
        if currentPage == 5{
            currentPage=0
        }
        
    }
    //MARK: -ScrollView Delegate
    func scrollViewDidScroll(scrollView: UIScrollView){
        //print("times")
        if scrollView == tempScrollView{
        let pageWidth : CGFloat = view.frame.size.width;
        let page: CGFloat = floor((scrollView.contentOffset.x - pageWidth/2)/pageWidth)+1;
        pageControl?.currentPage = Int(page);
        }
        
    }
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        self.tableVIew.keyboardDismissMode = .Interactive
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        //timer.invalidate()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        //  timer.fire()
    }
    func showActionSheet(){
        let alert = UIAlertController(title: "Contact Us", message: "Plase select an option to serve you", preferredStyle: .ActionSheet) // 1
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        let firstAction = UIAlertAction(title: "Email", style: .Default) { (alert: UIAlertAction!) -> Void in
            self.sendEmail()
        }
        
        let secondAction = UIAlertAction(title: "Call", style: .Default) { (alert: UIAlertAction!) -> Void in
            
            let alertCall = UIAlertController(title: " ", message: "Do you want to call. Carrier charges may apply", preferredStyle: .Alert) // 1
            
            let cancelAxn = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
            let callAction = UIAlertAction(title: "Call", style: .Default, handler: { (UIAlertAction) -> Void in
                let phoneNumber = "0091-9575100530"
                
                if let phoneCallURL = NSURL(string: "tel://\(phoneNumber)") {
                    let application = UIApplication.sharedApplication()
                    if (application.canOpenURL(phoneCallURL)) {
                        application.openURL(phoneCallURL);
                    }
                    
                }
            })
            
            alertCall.addAction(callAction)
            alertCall.addAction(cancelAxn)
            self.presentViewController(alertCall, animated: true, completion: nil)
            
        }
        let thirdAction = UIAlertAction(title: "Live Chat", style: .Default) { (alert: UIAlertAction!) -> Void in
            ZDCChat.updateVisitor({ (visitor:ZDCVisitorInfo!) -> Void in
					
					if let userInfo = CacheManager.sharedInstance .loadObject("userinfo") as! NSDictionary?{
						let username = ((userInfo.objectForKey("data#") as? NSDictionary)?.objectForKey("username") as? String)
						
						visitor.email = username
						visitor.name = ((userInfo.objectForKey("data#") as? NSDictionary)?.objectForKey("name") as? String)!
						visitor.phone = ((userInfo.objectForKey("data#") as? NSDictionary)?.objectForKey("phoneno") as? String)!
						
					}else{
						
						visitor.name = "Guest"
						visitor.email = "Guest@example.com"
						visitor.phone = "0123456789"}
            })
            
            self.navigationController!.navigationBar.tintColor=UIColor.whiteColor()
            ZDCChat.startChatIn(self.navigationController, withConfig: nil)
        }
        alert.addAction(cancelAction)
        alert.addAction(firstAction)
        alert.addAction(secondAction)
        alert.addAction(thirdAction)
        presentViewController(alert, animated: true, completion:nil)
    }
    
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["info@steelmint.com"])
            mail.setMessageBody("", isHTML: true)
            mail.navigationBar.tintColor=UIColor.whiteColor()
            
            presentViewController(mail, animated: true, completion: nil)
        } else {
            showAlertController("Steelmint", message: "Email not supported", reference: self)        }
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    func logout(){
        AppCommonFunctions.sharedInstance.logOutUser(self)
        
    }
    
    
    
    // MARK:  PLANTBEE IMAGES API HIT
    
    func getPlantbeeImages(){
        if NSUserDefaults.standardUserDefaults().valueForKey("plantbeeImages") == nil {
            showNativeActivityIndicator()
        }
        webserviceInstance.getPlantBeeImages(NSDictionary(), completionBlock: {
            (responseData) -> Void in
            if responseData != nil{
                let imagesArray = responseData?["data#"] as? [NSDictionary]
                print(responseData)

                print(responseData!.description)
                NSUserDefaults.standardUserDefaults().setValue(imagesArray, forKey: "plantbeeImages")
            }
            self.tableVIew.reloadData()
            self.stopActivityIndicator()
        })
        webserviceInstance.getPlantBeeTags(nil, completionBlock: {
            (responseData) -> Void in
            
            if responseData != nil{
                guard let tagsData = responseData?["data#"] as? [NSDictionary] else{return}
                NSUserDefaults.standardUserDefaults().setValue(tagsData, forKey: "tagsData")
            }
        })

        
    }
    
    // MARK: Search Management
        @IBAction func searchBtnEvent(sender: UIButton) {
        self.navigationView.userInteractionEnabled = false
        
        let searchView: SearchView = SearchView()
        searchView.frame = CGRectMake(0, -64, UIScreen.mainScreen().bounds.width, 64)
        
        UIView.animateWithDuration(0.3) { () -> Void in
            searchView.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 64)
        }
        searchView.delegate = self
        self.view.addSubview(searchView)
    }
    
    // MARK: Search View Delegates
    func textForSearchButtonClicked(text: String) {
        let storyboard = UIStoryboard(name: "Secondary", bundle: nil)

        let  buyPlantListingInstance = storyboard.instantiateViewControllerWithIdentifier("BuyPlantListingViewController") as! BuyPlantListingViewController
        buyPlantListingInstance.searchDict = NSDictionary(object:text , forKey: "search")
        buyPlantListingInstance.totalListings = "Search Results"
        self.navigationController?.pushViewController(buyPlantListingInstance, animated: true)
    }
    
    func textForSearch(text: String) {
        searchedArray?.removeAll()
        if let tagsData = NSUserDefaults.standardUserDefaults().valueForKey("tagsData") as? [NSDictionary]{
            searchedArray = (tagsData as NSArray).filteredArrayUsingPredicate(NSPredicate(format: "name contains [C] %@", argumentArray: [text]))
            tableVIew.reloadData()
        }
    }
    
    func cancelCallBack(flag: Bool) {
        self.navigationView.userInteractionEnabled = true
        searchActive = false
        tableVIew.reloadData()
    }
    func searchDidBegin(serachBar: UISearchBar) {
       
        searchedArray?.removeAll()
        searchActive = true
        tableVIew.reloadData()
    }
    
    

    //MARK: ACTIVITY INDICATOR
    func showNativeActivityIndicator(){
        if indicator == nil{
            indicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
            indicator?.frame = CGRectMake(GlobalConstants.SCREEN_WIDTH/2, 100, 10, 10)
            self.tableVIew.addSubview(indicator!)
        }
        indicator?.startAnimating()
    }
    
    func stopActivityIndicator() {
        indicator?.stopAnimating()
    }
    
    
    
    // MARK: Push Management
    func pushNotificationManagement(){
        if let _ = viaPushId{
            let storyboard = UIStoryboard(name: "Secondary", bundle: nil)
            let  buyPlantListingInstance = storyboard.instantiateViewControllerWithIdentifier("BuyPlantListingViewController") as! BuyPlantListingViewController
            buyPlantListingInstance.viaDictPush = pushDIct
            buyPlantListingInstance.totalListings = "Plants:"
            self.navigationController?.pushViewController(buyPlantListingInstance, animated: true)
        }
    }
}
