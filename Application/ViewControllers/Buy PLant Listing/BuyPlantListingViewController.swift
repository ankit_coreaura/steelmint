//
//  BuyPlantListingViewController.swift
//  Application
//
//  Created by Ankit Nandal on 09/01/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//

import UIKit
import Parse

class BuyPlantListingViewController: UIViewController {
    
    @IBOutlet weak var navigationTypeProperty: UILabel!
    @IBOutlet weak var navigationTitle:UILabel!
    @IBOutlet weak var tableView:UITableView!
    var  sellcategoryWiseDataArray :[AnyObject]?
    var plant_type = ""
    var parent_Code : String?
    var plantDate = ""
    var indicator = UIActivityIndicatorView()
    var tempVIew = UIView()
    var totalListings = ""
    var searchDict:NSDictionary?
    var filterVC : FilterViewController?
    var viaDictPush:NSDictionary?
    @IBOutlet weak var clearAllBtn:UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        setupImageForFilterBtn()
        // Do any additional setup after loading the view.
    }
    
    func setupImageForFilterBtn(){
        clearAllBtn.setImage(UIImage.image(UIImage(named: "cancelImage")!, withColor: UIColor.whiteColor()), forState: UIControlState.Normal)
        clearAllBtn.layer.cornerRadius = 15.0
        clearAllBtn.layer.borderWidth = 2.0
        clearAllBtn.layer.borderColor = UIColor.whiteColor().CGColor
    }
    func initialSetup(){
        // Setup ActivityIdicator
        indicator = UIActivityIndicatorView  (activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        indicator.frame = CGRectMake(GlobalConstants.SCREEN_WIDTH/2-10, GlobalConstants.SCREEN_HEIGHT/2-10, 10.0, 10.0)
        tempVIew = UIView(frame: tableView.frame)
        tempVIew.backgroundColor = UIColor.whiteColor()
        if searchDict?.count > 0{
           
           
            navigationTitle.text = "Searched Results:"
            (view.viewWithTag(9990) as! UIButton).hidden = true
            navigationTypeProperty.text = "Plants Listings"
            getPlantsFromGenricSearch()
        }
        else if viaDictPush?.count>0{
            (view.viewWithTag(9990) as! UIButton).hidden = true

        navigationTypeProperty.text = viaDictPush?["p_type"] as? String ?? "Plants"
        navigationTypeProperty.text = navigationTypeProperty.text?.uppercaseString
         pushApiHit(viaDictPush)
        }
        
        else{
            if CacheManager.sharedInstance.loadObject("buyFilters") != nil{
                
                navigationTitle.text = "Filtered Results:"
                clearAllBtn.hidden = false
            }
                
            
            else{
                navigationTitle.text = totalListings
                getContentsFromDB()
            }
            
            if totalListings.componentsSeparatedByString("Listings")[0].stringByReplacingOccurrencesOfString(" ", withString: "") != "0"{
                getPlantTypeBuy()
            }
        }
        
        
        
        
        filterVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("FilterViewController") as? FilterViewController
        if CacheManager.sharedInstance.loadObject("plantbeeFilters") != nil{
            filterVC!.data = CacheManager.sharedInstance.loadObject("plantbeeFilters") as! NSArray
            
            self.filterVC!.completionBlock = {
                (appliedFilters) -> ()in
                
                if appliedFilters.count>0{
                    if appliedFilters["NO_INT"] != nil{
                        showAlertController("Steelmint", message: "You cannot use filters in offline mode. Please connect to internet", reference: self)
                        return
                    }
                    if isInternetConnectivityAvailable(false){
                        self.showNativeActivityIndicator()
                        CacheManager.sharedInstance.saveObject(appliedFilters, identifier: "buyFilters")
                        self.clearAllBtn.hidden = false
                        self.getPlantTypeBuy()
                        self.filterVC?.removeAnimated()
                        
                    }
                    else{
                        showAlertController("Steelmint", message: "You cannot use filters in offline mode. Please connect to internet", reference: self)
                    }
                    //print(appliedFilters)
                }
                else{
                    CacheManager.sharedInstance.saveObject(nil, identifier: "buyFilters")
                    self.clearAllBtn.hidden = true
                    self.getContentsFromDB()
                    self.navigationTitle.text = self.totalListings
                    self.filterVC?.removeAnimated()
                    
                }
            }
        }
    }
    
   

    @IBAction func popBtnClickEventy(sender: UIButton) {
        navigationController?.popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 100
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = nil
        if let numberOfRows = sellcategoryWiseDataArray?.count {
            if numberOfRows == 0
            {
                noDataToShow()
            }
            return numberOfRows;
        }
        noDataToShow()
       return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
      let  cell = tableView.dequeueReusableCellWithIdentifier("cellPlant", forIndexPath: indexPath)
     
        let  buyplantTitle = (cell.viewWithTag(4031) as! UILabel)
        let  buyplantCompany = (cell.viewWithTag(4032) as! UILabel)
        let  buyplantPhoneNo = (cell.viewWithTag(4033) as! UILabel)
        let  buyplantDate = (cell.viewWithTag(4040) as! UILabel)
        let  plantImage = (cell.viewWithTag(4035) as! UIImageView)
      
        
      
        
        
        guard let plantData = sellcategoryWiseDataArray?[indexPath.row] else{return UITableViewCell()}

        buyplantDate.text = getDateFromTimestamp2(plantData["datetime"] as? NSNumber, dateFormat: "dd MMM yyyy")
        if  buyplantDate.text?.isEmpty == true{
           buyplantDate.text = plantData["datetime"] as? String
        }
        
        buyplantPhoneNo.text = plantData["mobile"] as? String

        buyplantTitle.text = plantData["title"] as? String
        buyplantCompany.text = plantData["company_name"] as? String ?? "Unavailable"
        
        plantImage.image = nil
        plantImage.image = UIImage(named:"placeholderImage")

        if let dataLeadArray = plantData["dataLead"] as? NSArray{
            if dataLeadArray.count>0{
                
                if dataLeadArray[0]["thumb_img"]  != nil{
                    if let imageURL = dataLeadArray[0]["thumb_img"] as? String{
                        if isNotNull(imageURL){
                            print("plantbee image url found....")
                            if let urlExists = NSURL(string: imageURL){
                                plantImage.sd_setImageWithURL(urlExists)
    
                            }
                        }
                }
                
                
                
            }else{
                plantImage.image = UIImage(named:"placeholderImage")
 
            }
           
        }else{
            plantImage.image = UIImage(named:"placeholderImage")
        }
        
        }
        
        
        
        
        
        if searchDict?.count>0{
            (cell.viewWithTag(9001) as! UIImageView).hidden = false
            (cell.viewWithTag(9009) as! UILabel).hidden = false
            
            
            (cell.viewWithTag(9009) as! UILabel).text = (plantData["category"] as? String)?.capitalizedString
            if (cell.viewWithTag(9009) as! UILabel).text?.lowercaseString == "sell"{
            (cell.viewWithTag(9001) as! UIImageView).image = UIImage(named: "buyorSellBlue")
                (cell.viewWithTag(9009) as! UILabel).text = "Sell"
					(cell.viewWithTag(9009) as! UILabel).textAlignment = .Center
            
            }else{
                (cell.viewWithTag(9001) as! UIImageView).image = UIImage(named: "buyOrSell")
                (cell.viewWithTag(9009) as! UILabel).text = "Buy"
					(cell.viewWithTag(9009) as! UILabel).textAlignment = .Center
            }
        }
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        let  listingInstance = storyboard?.instantiateViewControllerWithIdentifier("ListingDetailsViewController") as! ListingDetailsViewController
        if let data = sellcategoryWiseDataArray?[indexPath.row]  as? NSDictionary{
            listingInstance.sellPlantDetailDict = data
        }
        if searchDict?.count>0{
         listingInstance.viaSearch = true
        }
        self.navigationController?.pushViewController(listingInstance, animated: true)

    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    // MARK: Hit web service to get plants detail
    func pushApiHit(pushDictData:NSDictionary?){
        showNativeActivityIndicator()
        if isInternetConnectivityAvailable(true)==false {
            if indicator.isAnimating(){
                self.stopActivityIndicator()
                
            }
            return
        }
        
        let webservicesInstance = WebServices()
        let information = NSMutableDictionary()
        information.setObject([pushDictData?["p_code"] as! String], forKey: "plant_type")
        information.setObject(pushDictData?["p_type"] as! String, forKey: "lead_type")
        
        webservicesInstance.getPostDetails(information, completionBlock: {
            (responseData)-> () in
            
            
            if responseData != nil{
                guard let responseArray = responseData?["data#"] as? NSArray else{return}
                
                for index in responseArray{
                    if index["id"] as! String == pushDictData?["id"] as! String{
                        let  listingInstance = self.storyboard?.instantiateViewControllerWithIdentifier("ListingDetailsViewController") as! ListingDetailsViewController
                        if let data = index  as? NSDictionary{
                            listingInstance.sellPlantDetailDict = data
                        }
                        listingInstance.viaSearch = true
                        self.navigationController?.pushViewController(listingInstance, animated: true)
                    }
                }
                
                
            }
            if self.indicator.isAnimating(){
                self.stopActivityIndicator()
            }
        })
    }
    func getPlantTypeBuy(){
     
        
        if let parentCode = self.parent_Code{
            if !indicator.isAnimating() && DatabaseManager.sharedInstance.getAllPlantbeePostCodeWise(plant_type,parentCode:parentCode, type: "Buy")?.count == nil {
                showNativeActivityIndicator()
            }
           
        }else{
            if !indicator.isAnimating() && DatabaseManager.sharedInstance.getAllPlantbeePostCodeWise(plant_type,parentCode:"no value", type: "Buy")?.count == nil {
                showNativeActivityIndicator()
            }
        }
        
        if isInternetConnectivityAvailable(true)==false {
            if indicator.isAnimating(){
                self.stopActivityIndicator()
                
            }
            return
        }
       
        
        let webservicesInstance = WebServices()
        var information = NSMutableDictionary()
        
        if CacheManager.sharedInstance.loadObject("buyFilters") != nil{
            let dict = CacheManager.sharedInstance.loadObject("buyFilters") as! NSDictionary
            information = dict as! NSMutableDictionary
        }
        information.setObject([plant_type], forKey: "plant_type")
        information.setObject("buy", forKey: "lead_type")

        webservicesInstance.getPostDetails(information, completionBlock: {
            (responseData)-> () in
            if self.indicator.isAnimating(){
                self.stopActivityIndicator()
            }

            if responseData != nil{
                guard let responseArray = responseData?["data#"] as? NSArray else{return}
                self.sellcategoryWiseDataArray?.removeAll()
                if CacheManager.sharedInstance.loadObject("buyFilters") != nil{
       
                self.navigationTitle.text = "Filtered Results:"

                self.sellcategoryWiseDataArray = responseArray as [AnyObject]
                    self.tableView.reloadData()
                }else{ self.navigationTitle.text = self.totalListings
                    for objects in responseArray{
                        if let parentCode = self.parent_Code{
                            if objects is NSDictionary{
                                
                                let tempdict = NSMutableDictionary(dictionary: objects as! NSDictionary)
                                tempdict.setObject(parentCode, forKey: "parent_code")
                                DatabaseManager.sharedInstance.addPlantbeePost(tempdict)
                                
                            }
                        }else{
                            DatabaseManager.sharedInstance.addPlantbeePost(objects as! NSDictionary)
                        }
                    }
                    self.getContentsFromDB()
                }
    
               
            }
        })
        
    }
    func getPlantsFromGenricSearch(){
        
        if !indicator.isAnimating(){
                showNativeActivityIndicator()
       
        }
        if !isInternetConnectivityAvailable(true) {
            if indicator.isAnimating(){
                self.stopActivityIndicator()
            }
            return
        }
        let webservicesInstance = WebServices()
        webservicesInstance.getPlantsSearchWise(searchDict!) { (responseData) -> () in
            if self.indicator.isAnimating(){
                self.stopActivityIndicator()
                
            }
            if responseData != nil{
                
               self.sellcategoryWiseDataArray = responseData?["data#"] as? [AnyObject]
                self.tableView.reloadData()
            }
        }
    }
    // MARK: SHORTCUT METHODS
    
    @IBAction func callEvent(sender: UIButton) {
        let x = sender.convertPoint(CGPointZero, toView: tableView)
        let indexpath =  tableView.indexPathForRowAtPoint(x)
        guard let plantData = sellcategoryWiseDataArray?[indexpath!.row] else{return}

        let phoneNumber = plantData["mobile"] as? String
        if let pNo = Int(phoneNumber!) {
            if let phoneCallURL:NSURL = NSURL(string: "tel://\(pNo)") {
                
                let alert = UIAlertController(title: "", message: "Carrier charges may apply. Do you want to call?", preferredStyle: UIAlertControllerStyle.Alert)
                
                let callAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                    let application = UIApplication.sharedApplication()
                    if (application.canOpenURL(phoneCallURL)) {
                        application.openURL(phoneCallURL);
                    }
                })
                
                let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
                
                alert.addAction(callAction)
                alert.addAction(cancel)
                
                navigationController?.presentViewController(alert, animated: true, completion: nil)
                
                
            }}
    }
    
    @IBAction func chatEvent(sender: UIButton) {
        let x = sender.convertPoint(CGPointZero, toView: tableView)
        let indexpath =  tableView.indexPathForRowAtPoint(x)
        guard let plantData = sellcategoryWiseDataArray?[indexpath!.row] else{return}
        
        guard let secondUserEmail = plantData["email_id"] as? String else{showAlertController("SteelMint", message: "Cannot chat with this user right now. Please try again later", reference: self);return}
        
        var userName = ""
        
        for char in secondUserEmail.characters{
            if char == "@"{break}
            userName.append(char)
        }
        if isNotNull(PFUser.currentUser()){
            let query = PFQuery(className: GlobalConstants.PF_USER_CLASS_NAME)
            query.whereKey(GlobalConstants.PF_USER_EMAIL, equalTo:secondUserEmail)
            query.findObjectsInBackgroundWithBlock {(objects,error) -> Void in
                if error == nil && objects?.count > 0 {
                    let otherUser = objects![0] as! PFUser
                    let chatViewController = ChatViewController()
                    let user1 = PFUser.currentUser()
                    let user2 = otherUser
                    let groupId = Messages.startPrivateChat(user1!, user2: user2)
                    chatViewController.groupId = groupId
                    chatViewController.hidesBottomBarWhenPushed = true
                    chatViewController.titleToshow = user2.username
                    chatViewController.user2 =  user2
                    chatViewController.user2EmailId =  secondUserEmail
                    //  chatViewController.senderImageUrl = participant.img
                    
                    
                    self.navigationController?.pushViewController(chatViewController, animated: true)
                }
                
                
                else{
                    print("error encountered while chatting")
                    AppCommonFunctions.sharedInstance.signupInParseWith(secondUserEmail, email: secondUserEmail)
                    showNotification("Initiating your chat with \(userName) . Please wait.....", showOnNavigation: true, showAsError: false)
            
                    
                    
                    var notiCheck = false
            NSNotificationCenter.defaultCenter().addObserverForName("kParseUserSignUp", object: nil, queue: nil, usingBlock: {(notification) -> Void in
              
                
                print("notification for parse chat getting")
                NSNotificationCenter.defaultCenter().removeObserver(self, name: "kParseUserSignUp", object: nil)

                
                if notiCheck == false{
                    print("one time chat done")
                    query.findObjectsInBackgroundWithBlock {(objects,error) -> Void in
                        
                        if error == nil && objects?.count > 0 {
                            let otherUser = objects![0] as! PFUser
                            let chatViewController = ChatViewController()
                            let user1 = PFUser.currentUser()
                            let user2 = otherUser
                            let groupId = Messages.startPrivateChat(user1!, user2: user2)
                            chatViewController.groupId = groupId
                            chatViewController.hidesBottomBarWhenPushed = true
                            chatViewController.titleToshow = user2.username
                            chatViewController.user2 =  user2
                            chatViewController.user2EmailId =  secondUserEmail
                            //  chatViewController.senderImageUrl = participant.img
                            
                            
                            self.navigationController?.pushViewController(chatViewController, animated: true)
                        }
                    }
                
                }
                
                
              
                notiCheck = true

            
            
            })
            
                }
            }
        }else{
            showNotification("connecting... try again after some time", showOnNavigation: true, showAsError: false)
            AppCommonFunctions.sharedInstance.prepareUserForParseChat()
            
            
        }
        
        
    }
    @IBAction func expressInterestEvent(sender: UIButton) {
        let x = sender.convertPoint(CGPointZero, toView: tableView)
        let indexpath =  tableView.indexPathForRowAtPoint(x)
        guard let plantData = sellcategoryWiseDataArray?[indexpath!.row] else{return}
        
        let expressInstance = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ExpressInterestViewController") as?ExpressInterestViewController
        expressInstance?.leadId = plantData["id"] as? String ?? ""
        navigationController?.pushViewController(expressInstance!, animated: true)

    }
   // MARK: DB MANAGEMENT
    
    func getContentsFromDB(){
        sellcategoryWiseDataArray?.removeAll()
        if let parentCode = self.parent_Code{
            guard let offlineData = DatabaseManager.sharedInstance.getAllPlantbeePostCodeWise(plant_type,parentCode:parentCode, type: "Buy") else{return}
            sellcategoryWiseDataArray = offlineData

   
        }else{
            guard let offlineData = DatabaseManager.sharedInstance.getAllPlantbeePostCodeWise(plant_type,parentCode:"no value", type: "Buy") else{return}
            sellcategoryWiseDataArray = offlineData

        }
        
        tableView.reloadData()
    }
    //MARK: ACTIVITY INDICATOR
    func showNativeActivityIndicator(){
        self.tableView.allowsSelection = false
        view.addSubview(tableView)
        view.addSubview(indicator)
        indicator.startAnimating()
    }
    
    func stopActivityIndicator() {
        self.tableView.allowsSelection = true
        indicator.stopAnimating()
        tempVIew.removeFromSuperview()
    }
    @IBAction func filterBtnEvent(sender: UIButton) {
      filterVC?.showAnimated()
    }
    
    @IBAction func clearFilters(sender: UIButton){
        stopActivityIndicator()
        CacheManager.sharedInstance.saveObject(nil, identifier: "buyFilters")
        self.getContentsFromDB()
        self.clearAllBtn.hidden = true
        self.navigationTitle.text = self.totalListings
        filterVC!.cancelBtnEvent(UIButton())
    }
    func noDataToShow(){
        if indicator.isAnimating() == false{
            let noDataLabel = UILabel (frame: CGRectMake(0, 0,tableView.bounds.size.width,tableView.bounds.size.height))
//            noDataLabel.text             = "No data available"
//            noDataLabel.textColor        = UIColor.lightGrayColor()
//            noDataLabel.textAlignment    = .Center
//            tableView.backgroundView = noDataLabel;
        }
        
    }
}
