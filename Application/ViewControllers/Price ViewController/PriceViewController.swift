//
//  PriceViewController.swift
//  Application
//
//  Created by Ankit Nandal on 05/11/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//testing

import UIKit

class PriceViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate {
    var lastButtonPressed :Int?=2
    var lastLogisticsButtonPressed :Int?=888
    var lastLogisticsCategoryButtonPressed = "iron ore"
    var jsonObj:NSDictionary?
    var logisticsScrollView: UIScrollView?
    var shouldAnimate : Bool?
    var refreshControlDefault = UIRefreshControl()
    var lastCategoryButtonPressed = "RAW MATERIAL"
    var arrayOfPrices:NSArray?
    var webserviceInstance = WebServices()
    var indicator = UIActivityIndicatorView()
    var tempview = UIView()
    var apiSucess:Bool?

    @IBOutlet var tableView: UITableView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // INITIAL SETUPS:
        setupPullToRefresh()
       initialsetup()
        setupScrollview()
       
 
    }
    
    func initialsetup(){
        getContentsToShow()
            getPriceCategories()
        
        
        
        //
        if let path = NSBundle.mainBundle().pathForResource("countries", ofType: "json") {
            do {
                let data = try NSData(contentsOfURL: NSURL(fileURLWithPath: path), options: NSDataReadingOptions.DataReadingMappedIfSafe)
                jsonObj = parsedJsonFrom(data)
               
                
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        } else {
            print("Invalid filename/path.")
        }
        }
    
    func setupScrollview(){
        if CacheManager.sharedInstance.loadObject("priceCategories") != nil{
            let arr = CacheManager.sharedInstance.loadObject("priceCategories") as! NSMutableArray
            apiSucess = true
            getPrice("RAW MATERIAL")
            AppCommonFunctions.sharedInstance.buttonsetup(arr,fontSize: 16, scrollView: scrollView, refrence: self)
        }
        else{
//            let arr = ["Raw Material","Semi Finished","Logistics"]
//            AppCommonFunctions.sharedInstance.buttonsetup(arr,fontSize: 16, scrollView: scrollView, refrence: self)
        }
    }
    @IBAction func onClickOfLeftBarButton(sender:UIButton){
        openLeft()
    }
    
    func setupPullToRefresh(){
        self.refreshControlDefault.addTarget(self, action: "updateContents", forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControlDefault)
        
        // activity indicator setup
        indicator = UIActivityIndicatorView  (activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        indicator.frame = CGRectMake( GlobalConstants.SCREEN_WIDTH/2-10,  GlobalConstants.SCREEN_HEIGHT/2-10, 10.0, 10.0)
        tempview = UIView(frame: tableView.frame)
        tempview.backgroundColor = UIColor.whiteColor()
    }
    
    func updateContents(){
            NSObject.cancelPreviousPerformRequestsWithTarget(self)
        if apiSucess == true{
           performSelector("getPrice:", withObject: lastCategoryButtonPressed, afterDelay: 1)
        }else{
            getPriceCategories()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: TABLE VIEW DELEGATES METHODS
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if lastCategoryButtonPressed == "Logistics" || lastCategoryButtonPressed == "Logistic"{
            if indexPath.row == 0{
                return 39
            }
        }
        return 80
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = nil
        if lastCategoryButtonPressed == "Logistics"{
            guard let numberOfRows = arrayOfPrices?.count else{noDataToShow();return 1}
            if numberOfRows == 0{
                noDataToShow()
            }
            return numberOfRows + 1
        }else{
            guard let numberOfRows = arrayOfPrices?.count else{noDataToShow();return 0}
            if numberOfRows == 0{
                noDataToShow()
            }
            return numberOfRows

        }
    }
//    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if lastCategoryButtonPressed == "Logistics" || lastCategoryButtonPressed == "logistics" {return 70}
//        return 0
//    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if lastCategoryButtonPressed == "Logistics" || lastCategoryButtonPressed == "Logistic"{
            if indexPath.row == 0 {
                
                let  cell = tableView.dequeueReusableCellWithIdentifier("logisticsCell", forIndexPath:indexPath) as! PriceTableViewCell
                if logisticsScrollView == nil{
                    logisticsScrollView =  cell.viewWithTag(700) as? UIScrollView
                    if CacheManager.sharedInstance.loadObject("priceLogisticsCategories") != nil{
                        //logisticsScrollView!.subviews.forEach({ $0.removeFromSuperview() })
                            AppCommonFunctions.sharedInstance.buttonsetupForLogistics(CacheManager.sharedInstance.loadObject("priceLogisticsCategories") as! NSArray, fontSize: 16, scrollView: logisticsScrollView!, refrence: self)
                    }
                }
                
                return cell
            }
            if arrayOfPrices?.count > 0{
                guard let pricedDataDict = arrayOfPrices?[indexPath.row - 1] as? NSDictionary else{return UITableViewCell() }
              
                let  cell = tableView.dequeueReusableCellWithIdentifier("c2", forIndexPath:indexPath) as! PriceTableViewCell
                
                cell.categoryName.text = (pricedDataDict["delivery"] as? String)?.capitalizedString
               
                if let assesment =  pricedDataDict.valueForKey("assesment") as? String{
                    let dateString = pricedDataDict.valueForKey("date") as? String
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                   
                    let date = dateFormatter.dateFromString(dateString!)
                    
                    switch assesment{
                        
                        case "w","W":
                            dateFormatter.dateFormat = "dd MMM"
                            if date != nil{
                            let dateStr = dateFormatter.stringFromDate(date!)
                            cell.datendeliveryLabel.text = dateStr
                        }
                        case "d","D":
                            dateFormatter.dateFormat = "dd MMM, HH:mm"
                            if date != nil{
                                let dateStr = dateFormatter.stringFromDate(date!)
                                cell.datendeliveryLabel.text = dateStr
                        }
                        case "y","Y":
                            dateFormatter.dateFormat = "MMM yyyy"
                            if date != nil{
                                let dateStr = dateFormatter.stringFromDate(date!)
                                cell.datendeliveryLabel.text = dateStr
                        }
                    default :
                        break
                    }
                    /*
                    2016-01-13 10:55:43
                    [05/02/16, 1:37:42 PM] varun palekar: If assessment W = 19 Jan, 2 Week
                    [05/02/16, 1:38:04 PM] varun palekar: Detail 19 Jan 2016, Week 2
                    [05/02/16, 1:39:13 PM] varun palekar: If assessment is ‘Y’, then show data as Jan 2016, But in detail 19 Jan 2016,
                    [05/02/16, 1:39:37 PM] varun palekar: if assessment is D then show  date as 12 Jan, 23:30
                    [05/02/16, 1:39:45 PM] varun palekar: Same in details*/
  
                }else{
                    cell.datendeliveryLabel.text = pricedDataDict.valueForKey("date") as? String
                }
                cell.deliveryLabel.text = (pricedDataDict.valueForKey("s_country") as? String)?.capitalizedString
                cell.gradeLAbel.text = (pricedDataDict["grade"] as? String)?.capitalizedString 
                cell.sizeLabel.text = (pricedDataDict["size"] as? String)?.capitalizedString
                
                cell.priceOutlet.setTitle(pricedDataDict["price"] as? String , forState: .Normal)
                cell.changeLabel.text = pricedDataDict["change"] as? String 
                cell.changeLabel.text = cell.changeLabel.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
                if cell.changeLabel.text!.containsString("+"){
                    cell.changeLabel.textColor = UIColor(red: 0, green: 178.0/255.0, blue: 0, alpha: 1.0)
                }else if cell.changeLabel.text == "0"{
                    cell.changeLabel.textColor = UIColor.blackColor()
                    
                }else{
                    cell.changeLabel.textColor = UIColor.redColor()
                }
               
                if let countryCode = pricedDataDict["country"] as? String{
                    let predicate = NSPredicate(format: "name = [C] %@", argumentArray: [countryCode])
                    if let countMap = jsonObj?["data#"] as? NSArray{
                        let arrayOfMappedCountries = countMap.filteredArrayUsingPredicate(predicate)
                        if arrayOfMappedCountries.count > 0{
                            if let code =  arrayOfMappedCountries[0]["code"] as? String{
                                if code.characters.count == 2 {
                                    let countryBundle = "CountryPicker.bundle/" + code
                                    cell.flagImageview.image = UIImage(named: countryBundle)
                                }
                            }
                            
 
                        }
                    }

                }
                if let countryCode2 = pricedDataDict["s_country"] as? String{
                    let predicate = NSPredicate(format: "name = [C] %@", argumentArray: [countryCode2])
                    if let countMap = jsonObj?["data#"] as? NSArray{
                        let arrayOfMappedCountries = countMap.filteredArrayUsingPredicate(predicate)
                        if arrayOfMappedCountries.count > 0{
                            if let code =  arrayOfMappedCountries[0]["code"] as? String{
                                if code.characters.count == 2 {
                                    let countryBundle = "CountryPicker.bundle/" + code
                                    cell.flag2.image = UIImage(named: countryBundle)
                                }
                            }
                            
                            
                        }
                    }
                    
                }
                cell.flagImageview.layer.borderColor = UIColor.lightGrayColor().CGColor
                cell.flag2.layer.borderColor = UIColor.lightGrayColor().CGColor

                return cell
            }
            return UITableViewCell()
        }
        else{
            
            guard let pricedDataDict = arrayOfPrices?[indexPath.row] as? NSDictionary else{return UITableViewCell() }
            let  cell = tableView.dequeueReusableCellWithIdentifier("c1", forIndexPath:indexPath) as! PriceTableViewCell
            
            cell.categoryName.text = (pricedDataDict["sub_category"] as? String)?.capitalizedString
            cell.categoryName.text = cell.categoryName.text?.stringByReplacingOccurrencesOfString("_", withString: " ")
            if let assesment =  pricedDataDict.valueForKey("assesment") as? String{
                let dateString = pricedDataDict.valueForKey("date") as? String
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                let date = dateFormatter.dateFromString(dateString!)
                
                switch assesment{
                    
                case "w","W":
                    dateFormatter.dateFormat = "dd MMM"
                    if date != nil{
                        let dateStr = dateFormatter.stringFromDate(date!)
                        cell.datendeliveryLabel.text = dateStr
                    }
                case "d","D":
                    dateFormatter.dateFormat = "dd MMM, HH:mm"
                    if date != nil{
                        let dateStr = dateFormatter.stringFromDate(date!)
                        cell.datendeliveryLabel.text = dateStr
                    }
                case "y","Y":
                    dateFormatter.dateFormat = "MMM yyyy"
                    if date != nil{
                        let dateStr = dateFormatter.stringFromDate(date!)
                        cell.datendeliveryLabel.text = dateStr
                    }
                default :
                    break
                }
                               
            }else{
                cell.datendeliveryLabel.text = pricedDataDict.valueForKey("date") as? String
            }
            cell.deliveryLabel.text = (pricedDataDict.valueForKey("delivery") as? String)?.capitalizedString 
            cell.gradeLAbel.text = (pricedDataDict["grade"] as? String)?.capitalizedString 
            cell.sizeLabel.text = (pricedDataDict["size"] as? String)?.capitalizedString
            
            cell.priceOutlet.setTitle(pricedDataDict["price"] as? String , forState: .Normal)
            cell.changeLabel.text = pricedDataDict["change"] as? String 
            cell.changeLabel.text = cell.changeLabel.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            if cell.changeLabel.text!.containsString("+"){
            cell.changeLabel.textColor = UIColor(red: 0, green: 178.0/255.0, blue: 0, alpha: 1.0)
            }else if cell.changeLabel.text == "0"{
                cell.changeLabel.textColor = UIColor.blackColor()

            }else{
                cell.changeLabel.textColor = UIColor.redColor()
            }
            
            if let countryCode = pricedDataDict["country"] as? String{
                
                if countryCode.lowercaseString == "global"{
                    
                    cell.flagImageview.image = UIImage(named: "globe.png")

                }else{
                    let predicate = NSPredicate(format: "name = [C] %@", argumentArray: [countryCode])
                    if let countMap = jsonObj?["data#"] as? NSArray{
                        let arrayOfMappedCountries = countMap.filteredArrayUsingPredicate(predicate)
                        if arrayOfMappedCountries.count > 0{
                            if let code =  arrayOfMappedCountries[0]["code"] as? String{
                                if code.characters.count == 2 {
                                    let countryBundle = "CountryPicker.bundle/" + code
                                    cell.flagImageview.image = UIImage(named: countryBundle)
                                }
                            }
                            
                            
                        }
                    }
                }
                
                
                
            }else{
              cell.flagImageview.image = UIImage()  
            }
            cell.flagImageview.layer.borderColor = UIColor.lightGrayColor().CGColor
            return cell

        }
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if arrayOfPrices?.count > 0 {
            if lastCategoryButtonPressed == "Logistics" || lastCategoryButtonPressed == "Logistic"{
                if indexPath.row != 0 {
                guard let pricedDataDict = arrayOfPrices?[indexPath.row - 1] as? NSDictionary else{return}

                let viewDetailsInstance =
                self.storyboard?.instantiateViewControllerWithIdentifier("PriceDetailViewController") as! PriceDetailViewController
                viewDetailsInstance.priceDetailDict = pricedDataDict
                viewDetailsInstance.viaOption = true
                viewDetailsInstance.hidesBottomBarWhenPushed = true
                
                self.navigationController?.pushViewController(viewDetailsInstance, animated: true)
                }
           
            }
            else{
                guard let pricedDataDict = arrayOfPrices?[indexPath.row] as? NSDictionary else{return}

                let viewDetailsInstance =
                self.storyboard?.instantiateViewControllerWithIdentifier("PriceDetailViewController") as! PriceDetailViewController
                viewDetailsInstance.priceDetailDict = pricedDataDict
                viewDetailsInstance.hidesBottomBarWhenPushed = true
                
                self.navigationController?.pushViewController(viewDetailsInstance, animated: true)
            }
        }

    }
    
    // MARK:  CATEGORIES CLICKED
    
    func butonIndexed(sender:UIButton){
        refreshControlDefault.endRefreshing()
        var offsetX = sender.frame.origin.x+sender.frame.size.width/2 -  GlobalConstants.SCREEN_WIDTH/2
        if offsetX > scrollView.contentSize.width -  GlobalConstants.SCREEN_WIDTH {
            offsetX = scrollView.contentSize.width -  GlobalConstants.SCREEN_WIDTH
        }
        if offsetX < 0 {
            offsetX = 0
        }
        scrollView.setContentOffset(CGPointMake(offsetX, 0), animated: true)
        if let buttonPressed = lastButtonPressed{
            (scrollView.viewWithTag(buttonPressed) as? UIButton)?.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
            (scrollView.viewWithTag(buttonPressed) as? UIButton)?.titleLabel?.font=UIFont.systemFontOfSize(13)
        }
        lastButtonPressed = sender.tag
        sender.titleLabel?.font=UIFont.boldSystemFontOfSize(14)
        sender.setTitleColor(UIColor(red: 30.0/255.0, green: 136.0/255.0, blue: 229.0/255.0, alpha: 1), forState: .Normal)
        print(sender.currentTitle)
        lastCategoryButtonPressed=sender.currentTitle!
            getContentsToShow()
       
        
            NSObject.cancelPreviousPerformRequestsWithTarget(self)
            performSelector("getPrice:", withObject: sender.currentTitle!, afterDelay: 1)
        
        
    }
    func buttonIndexedForLogistics(sender:UIButton){
        refreshControlDefault.endRefreshing()
        var offsetX = sender.frame.origin.x+sender.frame.size.width/2 -  GlobalConstants.SCREEN_WIDTH/2
        if offsetX > logisticsScrollView!.contentSize.width -  GlobalConstants.SCREEN_WIDTH {
            offsetX = logisticsScrollView!.contentSize.width -  GlobalConstants.SCREEN_WIDTH
        }
        if offsetX < 0 {
            offsetX = 0
        }
        logisticsScrollView!.setContentOffset(CGPointMake(offsetX, 0), animated: true)
        if let buttonPressed = lastLogisticsButtonPressed{
            (view.viewWithTag(buttonPressed) as? UIButton)?.setTitleColor(UIColor.blackColor(), forState: .Normal)
            (view.viewWithTag(buttonPressed) as? UIButton)?.titleLabel?.font=UIFont.systemFontOfSize(13)
        }
        lastLogisticsButtonPressed = sender.tag
        sender.titleLabel?.font=UIFont.boldSystemFontOfSize(14)
        sender.setTitleColor(UIColor(red: 30.0/255.0, green: 136.0/255.0, blue: 229.0/255.0, alpha: 1), forState: .Normal)
        print(sender.currentTitle)
        lastLogisticsCategoryButtonPressed = sender.currentTitle!
        
        getContentsToShow()
        NSObject.cancelPreviousPerformRequestsWithTarget(self)
        performSelector("getPrice:", withObject: sender.currentTitle!, afterDelay: 1)
    }
    
    @IBAction func viweAllMenuOptionEvent(sender: UIButton) {
        
        let x = sender.convertPoint(CGPointZero, toView: tableView)
        let indexpath =  tableView.indexPathForRowAtPoint(x)
        guard let pricedDataDict = arrayOfPrices?[indexpath!.row] as? NSDictionary else{return }
        print(arrayOfPrices?.description)
        
        let actionSheet = UIAlertController(title: "", message: "View particular price detail or view all prices", preferredStyle:UIAlertControllerStyle.ActionSheet)
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        
        let viewDetailsAxn = UIAlertAction(title: "View Details", style:UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
            let viewDetailsInstance = self.storyboard?.instantiateViewControllerWithIdentifier("PriceDetailViewController") as! PriceDetailViewController
            viewDetailsInstance.priceDetailDict = pricedDataDict
            viewDetailsInstance.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewDetailsInstance, animated: true)
        }
        let viewAllPricesAxn = UIAlertAction(title: "View All Prices", style:UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
            let viewAllPricesInstance = self.storyboard?.instantiateViewControllerWithIdentifier("PriceCountryWiseViewController") as! PriceCountryWiseViewController
            viewAllPricesInstance.hidesBottomBarWhenPushed = true
            viewAllPricesInstance.priceCountryDict = pricedDataDict
            if let tabArray = pricedDataDict["tabs"] as? NSArray{
                viewAllPricesInstance.tabOption = tabArray
            }
            self.navigationController?.pushViewController(viewAllPricesInstance, animated: true)
        }
        
//        if let _ = pricedDataDict["tabs"] as? NSArray{
//            actionSheet.addAction(viewAllPricesAxn)
//        }
        
        actionSheet.addAction(viewAllPricesAxn)
        actionSheet.addAction(viewDetailsAxn)
        actionSheet.addAction(cancelAction)
        
        presentViewController(actionSheet, animated: true, completion: nil)
        
    }
    
    // MARK : GETTING PRICES VIA API
    
    func getPriceCategories(){
        if isInternetConnectivityAvailable(false) == false{
            self.refreshControlDefault.endRefreshing()
            return
        }
        if CacheManager.sharedInstance.loadObject("priceCategories") == nil{
         showNativeActivityIndicator()
        }
        webserviceInstance.getPriceCategories(NSDictionary(), completionBlock: {
            (responseData) ->() in
            
            guard let responseValue = responseData as? NSDictionary else{return}
            
            let categories = responseValue.valueForKey("data#")?.valueForKey("category") as? NSArray
            let arrayOfCAtegories = NSMutableArray()
            for catName in categories! {
                arrayOfCAtegories.addObject(catName.valueForKey("name") as! String)
                if (catName.valueForKey("name") as! String).caseInsensitiveCompare("logistics") == NSComparisonResult.OrderedSame{
                    let arr1 = (catName.valueForKey("E_I") as! NSDictionary)["LOGISTICS"] as? NSArray
                    CacheManager.sharedInstance.saveObject(arr1, identifier: "priceLogisticsCategories")
                }

            }
            
            if CacheManager.sharedInstance.loadObject("priceCategories") == nil{
                CacheManager.sharedInstance.saveObject(arrayOfCAtegories, identifier: "priceCategories")
                self.scrollView.subviews.forEach({ $0.removeFromSuperview() })
                self.setupScrollview()
            }
            
            CacheManager.sharedInstance.saveObject(arrayOfCAtegories, identifier: "priceCategories")
       
        })
    }
    func getPrice(category:String){
        
      print("price api hit \n \n *******************************")
        if !indicator.isAnimating() && DatabaseManager.sharedInstance.getAllPrice()?.count == nil || shouldAnimate == true{
            showNativeActivityIndicator()
        }
        if isInternetConnectivityAvailable(true)==false {
            self.refreshControlDefault.endRefreshing()
            if indicator.isAnimating(){
                self.stopActivityIndicator()
                
            }
            return
        }
        var information = ["category":"\(lastCategoryButtonPressed)"]
        if lastCategoryButtonPressed.caseInsensitiveCompare("logistics") == NSComparisonResult.OrderedSame{
           
            information["tab"] = lastLogisticsCategoryButtonPressed
        }
        webserviceInstance.getPrices(information, completionBlock: {
            (responseData) ->() in
            self.refreshControlDefault.endRefreshing()
            if self.indicator.isAnimating(){
                self.stopActivityIndicator()
                
            }
            guard let responseValue = responseData?.valueForKey("data#") as? NSArray else{return}
            if responseValue.count>0{
                if((responseValue[0]["category"] as! String).caseInsensitiveCompare("Logistics") == NSComparisonResult.OrderedSame){
                    for object in responseValue
                    {
                     DatabaseManager.sharedInstance.addPriceTabWise(object as! NSDictionary)
                    }
                }else{
                    for object in responseValue
                    {
                    DatabaseManager.sharedInstance.addPrice(object as! NSDictionary)
                    }
                }
                self.getContentsToShow()
            }
            
        })
    }
    
    
    func reloadTableView()
    {
        tableView.reloadData()
    }
    
    
    
    // MARK: DB CONTENTS TO SHOW

    func getContentsToShow()
    {
        shouldAnimate = false
        arrayOfPrices = nil
        if lastCategoryButtonPressed.caseInsensitiveCompare("logistics") == NSComparisonResult.OrderedSame{
            guard let offlineData = DatabaseManager.sharedInstance.getAllPriceTabWise(lastLogisticsCategoryButtonPressed) else{
                if indicator.isAnimating()  == false{
                    showNativeActivityIndicator()
                }
                self.tableView.reloadData()
                return}

            let predicate = NSPredicate(format: "category == [C]%@ AND tab = [C] %@", argumentArray:[lastCategoryButtonPressed,lastLogisticsCategoryButtonPressed])
            
            let filteredArray = offlineData.filter { predicate.evaluateWithObject($0) };
            arrayOfPrices = filteredArray
            
        }else{
            guard let offlineData = DatabaseManager.sharedInstance.getAllPrice() else{ self.tableView.reloadData();return}
            let predicate = NSPredicate(format: "category == [C]%@", argumentArray:[lastCategoryButtonPressed])
            
            let filteredArray = offlineData.filter { predicate.evaluateWithObject($0) };
            arrayOfPrices = filteredArray
        }
       
        if arrayOfPrices?.count == 0{
            if indicator.isAnimating() == false{
                shouldAnimate = true
            }
        }
            self.reloadTableView()

        
    }

    //MARK: ACTIVITY INDICATOR
    func showNativeActivityIndicator(){
        scrollView.userInteractionEnabled = false
        scrollView.layer.opacity = 0.3
        tableView.allowsSelection = false
        view.addSubview(tempview)
        view.addSubview(indicator)
        indicator.startAnimating()
    }
    
    func stopActivityIndicator() {
        scrollView.userInteractionEnabled = true
        scrollView.layer.opacity = 1.0
        tableView.allowsSelection = true
        indicator.stopAnimating()
        tempview.removeFromSuperview()
    }
    
    func pushNotificationManagement(viaPushCategory:String?,viaPushTab:String?){
        if let cat = viaPushCategory ,  tab = viaPushTab{
            let viewAllPricesInstance = self.storyboard?.instantiateViewControllerWithIdentifier("PriceCountryWiseViewController") as! PriceCountryWiseViewController
            viewAllPricesInstance.hidesBottomBarWhenPushed = true
            
            viewAllPricesInstance.vaiPushTab = tab
            viewAllPricesInstance.vaiPushCategory = cat
navigationController?.pushViewController(viewAllPricesInstance, animated: true)
           // viewAllPricesInstance.priceCountryDict = pricedDataDict
            
        }}
    // MARk: No data Management
    func noDataToShow(){
       
//            let noDataLabel = UILabel (frame: CGRectMake(0, 0,tableView.bounds.size.width,tableView.bounds.size.height))
//            noDataLabel.text             = "No data available"
//            noDataLabel.textColor        = UIColor.lightGrayColor()
//            noDataLabel.textAlignment    = .Center
//            tableView.backgroundView = noDataLabel;
        
    }
    
}



