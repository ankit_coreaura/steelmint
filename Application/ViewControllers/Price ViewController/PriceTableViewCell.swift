//
//  PriceTableViewCell.swift
//  Application
//
//  Created by Ankit Nandal on 16/12/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit

class PriceTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var datendeliveryLabel: UILabel!
    @IBOutlet weak var flagImageview: UIImageView!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var gradeLAbel: UILabel!
    @IBOutlet weak var priceOutlet: UIButton!
    @IBOutlet weak var changeLabel: UILabel!
    @IBOutlet weak var deliveryLabel: UILabel!
    
    @IBOutlet weak var flag2: UIImageView!
   
}
