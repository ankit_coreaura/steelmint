//
//  SubscribeViewController.swift
//  SteelMint
//
//  Created by Ankit Nandal on 03/11/15.
//  Copyright © 2015 Saurabh Singh. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    //CONNECTING OUTLETS
let webservicesInstance = WebServices()
    override func viewDidLoad() {
        super.viewDidLoad()
if NSUserDefaults.standardUserDefaults().valueForKey("notificationCheck") != nil {
    if NSUserDefaults.standardUserDefaults().valueForKey("notificationCheck") as! String == "On" {
        (view.viewWithTag(12) as! UISwitch).on = true
    }else{
        (view.viewWithTag(12) as! UISwitch).on = false
 
    }
        }
    
    }
        @IBAction func onClickOfLeftBarButton(sender:UIButton){
                openLeft()
        }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }    
    
        
    @IBAction func setNotifications(sender: UISwitch) {
        guard let token = NSUserDefaults.standardUserDefaults().valueForKey("deviceToken") as? String else{return}
        if sender.on{
            
            let tokenDict = NSDictionary(objects: [sessionId(),token,"ios","enable"], forKeys: ["token","device_id","device","status"])
        webservicesInstance.progresssIndicatorText = "Please Wait.."
            webservicesInstance.setPushNotificationStatus(tokenDict, completionBlock: { (responseData) -> () in
                if responseData != nil{
                    if let response = responseData{
                        if response["status#"] as! String == "1"{
                            
                            NSUserDefaults.standardUserDefaults().setValue("On", forKey: "notificationCheck")
                            
                            showAlertDifferentType("Your notifications are set on. ")
                            self.openLeft()
                        }else{
                            sender.on = true
                            showAlertDifferentType("Your notifications are not set off.Please try again ")
                        }
                    }
                    else{
                        showAlertDifferentType("Your notifications are not set.Please try again ")
                    }
                }else{
                    showAlertDifferentType("Your notifications are not set.Please try again ")
                }
                
            })
            print("on")
        }
        else{
            let tokenDict = NSDictionary(objects: [sessionId(),token,"ios","disable"], forKeys: ["token","device_id","device","status"])
            webservicesInstance.progresssIndicatorText = "Push Notification Off.."
            webservicesInstance.setPushNotificationStatus(tokenDict, completionBlock: { (responseData) -> () in
                
                
                if responseData != nil{
                    if let response = responseData{
                        if response["status#"] as! String == "1"{
                            NSUserDefaults.standardUserDefaults().setValue("Off", forKey: "notificationCheck")
                            showAlertDifferentType("Your notifications are set off. ")
                            self.openLeft()
                        }else{
                            sender.on = false
                            showAlertDifferentType("Your notifications are not set off.Please try again ")
                        }
                        
                    }
                    else{
                        showAlertDifferentType("Your notifications are not set off.Please try again ")
                    }
                }else{
                    showAlertDifferentType("Your notifications are not set off.Please try again ")
                }
                
                
                
            })
            print("off")

        }
    }
        
    

}
