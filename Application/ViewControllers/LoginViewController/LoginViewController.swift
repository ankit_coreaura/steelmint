//
//  ViewController.swift
//  SteelMint
//
//  Created by Saurabh Singh on 07/09/15.
//  Copyright (c) 2015 Saurabh Singh. All rights reserved.
//

import UIKit
import SCLAlertView

class LoginViewController: UIViewController{
    //OUTLETS SETUP
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var heightLoginImageViewOutlet: NSLayoutConstraint!
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
        
   
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if UIScreen.mainScreen().bounds.size.height<=480{
            heightLoginImageViewOutlet.constant = 130}
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onClickOfLeftBarButton(sender:UIButton){
        openLeft()
    }
    
    @IBAction func login(sender: UIButton) {
        let userInfo = NSMutableDictionary()
        if usernameTextField.text!.isEmpty || passwordTextField.text!.isEmpty{
            showAlertController("SteelMint", message: "Please complete all the details", reference: self)
        }
        else{
            if usernameTextField.text!.isEmail {
                let emailId = usernameTextField.text!.lowercaseString
                userInfo.setObject(emailId, forKey: "username")
                userInfo.setObject(passwordTextField.text!, forKey: "password")
                //Saving USERNAME AND PASSWORD
                 NSUserDefaults.standardUserDefaults().setValue(emailId, forKey: "username")
                NSUserDefaults.standardUserDefaults().setValue(passwordTextField.text!,forKey: "password")
                //==================================================================
                
                
                if NSUserDefaults.standardUserDefaults().valueForKey("token") != nil {
                
                    let tokenDict = NSMutableDictionary()
                tokenDict.setObject(NSUserDefaults.standardUserDefaults().valueForKey("token")!, forKey: "refreshToken")
                    
                    let webservicesInstance = WebServices()
                    webservicesInstance.progresssIndicatorText = "Please Wait.."
                    webservicesInstance.getSessionId(tokenDict, completionBlock: {
                        (responseData) -> () in
                        if let _ = responseData{
                            NSUserDefaults.standardUserDefaults().setValue(responseData?.valueForKey("data#")?.valueForKey("token"), forKey: "sessionId")
                            let sessionID:String? = sessionId()
                            let webservicesInstance = WebServices()
                            
                            webservicesInstance.progresssIndicatorText = "Please Wait.."
                            webservicesInstance.getUserInfo(["token":sessionID!], completionBlock: {
                                (responseData) -> () in
                                if let _ = responseData{
                                    CacheManager.sharedInstance .saveObject(responseData as! NSDictionary, identifier: "userinfo")
                                    if let _ = sessionID{
                                        DatabaseManager.sharedInstance.resetCoreDataEntities()
                                        
                                        self.slideMenuController()?.changeMainViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("TabController"), close: true)
                                    }
                                }
                            })
                        }
                        }
                    )
                
                }else{
                    let webservicesInstance = WebServices()
                    webservicesInstance.progresssIndicatorText = "Signing in"
                    sender.userInteractionEnabled = false
                    webservicesInstance.login(userInfo, completionBlock:{(responseData) -> () in
                        if let _ = responseData{
                            NSUserDefaults.standardUserDefaults().setValue(responseData?.valueForKey("data#")?.valueForKey("refreshToken"), forKey: "token")
                            let tokenDict = NSMutableDictionary()
                            tokenDict.setObject(NSUserDefaults.standardUserDefaults().valueForKey("token")!, forKey: "refreshToken")
                            
                            let webservicesInstance = WebServices()
                            webservicesInstance.progresssIndicatorText = "Please Wait.."
                            webservicesInstance.getSessionId(tokenDict, completionBlock: {
                                (responseData) -> () in
                                if let _ = responseData{
                                    sender.userInteractionEnabled = true
                                    NSUserDefaults.standardUserDefaults().setValue(responseData?.valueForKey("data#")?.valueForKey("token"), forKey: "sessionId")
                                    let sessionID:String? = sessionId()
                                    let webservicesInstance = WebServices()
                                    
                                    webservicesInstance.progresssIndicatorText = "Please Wait.."
                                    webservicesInstance.getUserInfo(["token":sessionID!], completionBlock: {
                                        (responseData) -> () in
                                        if let _ = responseData{
                                            CacheManager.sharedInstance .saveObject(responseData as! NSDictionary, identifier: "userinfo")
                                            AppCommonFunctions.sharedInstance.prepareUserForParseChat()
                                            if let _ = sessionID{
                                                DatabaseManager.sharedInstance.resetCoreDataEntities()
                                                
                                                
                                                if (UIApplication.sharedApplication().delegate as! AppDelegate).subscriptionFlag == true{
                                                  (UIApplication.sharedApplication().delegate as! AppDelegate).subscriptionFlag = nil
                                                    
                                                     self.slideMenuController()?.changeMainViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("SubscribeViewController"), close: true)
                                                }else{
                                                    
                                                    
                                                  self.registerForPush()
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                 self.slideMenuController()?.changeMainViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("TabController"), close: true)   
                                                }
                                            }
                                        }
                                    })
                                }else{sender.userInteractionEnabled = true}
                                }
                            )
                        }else{
                            sender.userInteractionEnabled = true}
                        }
                    )
                }
                
                
            }
            else{
                showAlertController("SteelMint", message: "Please enter valid email", reference: self)
            }
        }
    }
    
    
    func registerForPush(){
        if sessionId().characters.count > 1{
                let token = NSUserDefaults.standardUserDefaults().valueForKey("deviceToken") as? String
                if let token = token{
                    let webservicesInstance = WebServices()
                    let information = NSDictionary(objects: ["ios",token,sessionId()], forKeys: ["device","device_id","token"])
                    webservicesInstance.setPushNotification(information) { (responseData) -> () in
                        if responseData != nil{
                            if let status = responseData?["status#"] as? String{
                                if status == "1"{
                                    
                                }
                            }
                        }
                        
                    }
                    
                }
                
            
        }

    }
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func forgetPasswordEvent(sender: UIButton) {
        var inputTextField :UITextField?
        let passwordPrompt = UIAlertController(title: "SteelMint", message: "Please enter your email id", preferredStyle:.Alert)
        passwordPrompt.addTextFieldWithConfigurationHandler({(textField: UITextField!) in
            textField.placeholder = "Email Id"
            textField.keyboardType = UIKeyboardType.EmailAddress
            inputTextField = textField
            inputTextField?.keyboardType = .EmailAddress
        })
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: nil)
        passwordPrompt.addAction(cancelButton)
        passwordPrompt.addAction(UIAlertAction(title: "Send Password", style: .Default, handler: { (action) -> Void in
            if  inputTextField?.text?.characters.count>0 && inputTextField!.text!.isEmail{
                print("count is there") // send mail
                let forgetPasswordDict = NSMutableDictionary()
                let emailId = inputTextField?.text!.lowercaseString
                forgetPasswordDict.setObject(emailId!, forKey: "email")
                
                let webservicesInstance = WebServices()
                webservicesInstance.progresssIndicatorText = "Sending Password"
                webservicesInstance.forgetPassword(forgetPasswordDict){ (responseData) -> () in
                    if let _ = responseData{
                        showAlertDifferentType("Password succesfully sent on your email id please check")
                                               
                    }
                }
            }
            else{
                showAlertController("SteelMint", message: "Please enter valid email id", reference: self)
            }
        }))
        presentViewController(passwordPrompt, animated: true, completion: nil)
        
    }
    @IBAction func onClickOfSignup(sender: UIButton) {
        
        let registerInstance = storyboard?.instantiateViewControllerWithIdentifier("RegisterViewController") as? RegisterViewController
        
        if(sender.tag==1)
        {
            registerInstance?.isSubscribeNavigation=true
        }
        else
        {
            registerInstance?.isSubscribeNavigation=false

        }
        self.navigationController?.pushViewController(registerInstance!, animated: true)
    
    }
}

