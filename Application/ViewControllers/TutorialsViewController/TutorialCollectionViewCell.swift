//
//  TutorialCollectionViewCell.swift
//  Application
//
//  Created by Ankit Nandal on 20/12/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit

class TutorialCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLayout: NSLayoutConstraint!
    @IBOutlet weak var imageViewLayout: NSLayoutConstraint!
    @IBOutlet weak var titleLAbel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageViewOutlet: UIImageView!
    
    
}
