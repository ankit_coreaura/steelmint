//
//  TutorialsViewController.swift
//  Application
//
//  Created by Ankit Nandal on 20/12/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit

class TutorialsViewController: UIViewController ,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    var contentArray = [
        ["title": "For Those Who Mean Business ", "desc": "","image": "walkthrough1"],
        ["title": "NEWS ", "desc": "Timely, Prompt and Decisive News and Information ","image": "newsWalkthrough"],
        ["title": "PRICES ", "desc":"Stay Updated with Spot Steel and Raw Material Price Across the Globe","image": "priceWalkthrough"],
        ["title": "TENDERS ", "desc": "Search and Participate From Thousands of Global Tenders","image": "tenderWalkthrough"],
        ["title": "PLANTBEE ", "desc": "Buy,Sell,Rent Used Plant & Machines through our Strong Network ","image": "plantbeeWalkthrough"],
        ["title": "EVENTS", "desc": "Knowledge, Innovation, Networking experience it at Steelmint Events" ,"image": "eventsWalkthrough"],
    ]
    var viaOption = "splash"
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var walkthroughCollectionView:UICollectionView!
    
    
    // MARK: PARENTS METHODS
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
//        centerAlignUsername.constant -= view.bounds.width
//        centerAlignPassword.constant -= view.bounds.width
//        loginButton.alpha = 0.0
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
//        UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
//            self.centerAlignUsername.constant += self.view.bounds.width
//            self.centerAlignPassword.constant += self.view.bounds.width
//            self.loginButton.alpha = 1
//            self.view.layoutIfNeeded()
//            }, completion: nil)
//        
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func skipBtnEvent(sender: UIButton) {
        if viaOption == "leftMenu"{
        openLeft()
        }
        else{
            openTabViewController()
        }
        
    }

    @IBAction func nextBtnEvent(sender: UIButton) {
        if pageControl.currentPage == 5{
            if viaOption == "leftMenu"{
                openLeft()
            }
            else{
                openTabViewController()
            }
        }
        else{
            
        let pageCount = pageControl.currentPage+1;
        let indexPath = NSIndexPath(forItem: pageCount, inSection: 0)
        walkthroughCollectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition:UICollectionViewScrollPosition.None, animated: true)
        }
    }
    
    // MARK: COLLECTION VIEW DELEGATES
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return contentArray.count
    }
    
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("c1", forIndexPath: indexPath) as! TutorialCollectionViewCell
        cell.titleLAbel.text = (contentArray[indexPath.row])["title"]
        cell.descriptionLabel.text = (contentArray[indexPath.row] )["desc"]

        cell.descriptionLabel.layer.opacity = 0

        UIView.animateWithDuration(1, animations: {
            cell.descriptionLabel.layer.opacity = 1
            self.view.layoutIfNeeded()


        })

        cell.imageViewOutlet.image = UIImage(named: (contentArray[indexPath.row])["image"]!)
                if cell.descriptionLabel.text == ""{
                    cell.imageViewLayout.constant = -80
                }
                else{
                    cell.imageViewLayout.constant = 20
    
        }
        

        return cell
        
        
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat{
            return 0.0;
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat{
            return 0.0;
    }
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
            return walkthroughCollectionView.frame.size;
    }
    //MARK: -ScrollView Delegate
    func scrollViewDidScroll(scrollView: UIScrollView){
        //print("times")
        let pageWidth : CGFloat = walkthroughCollectionView.frame.size.width;
        let page: CGFloat = floor((walkthroughCollectionView.contentOffset.x - pageWidth/2)/pageWidth)+1;
            pageControl.currentPage = Int(page);
        
    }
    
    //MARK: - other functions
    @IBAction func pageControlValueChanged(sender: UIPageControl){
        let pageCount = sender.currentPage;
        let indexPath = NSIndexPath(forItem: pageCount, inSection: 0);
        walkthroughCollectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition:UICollectionViewScrollPosition.None, animated: true);
        
    }
    func openTabViewController(){
        AppCommonFunctions.sharedInstance.showHomeScreen()
    }

}
