//
//  MyListingsViewController.swift
//  Application
//
//  Created by Chandan Singh on 1/4/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//

import UIKit 

class MyListingsViewController: UIViewController {
    var myListingsArray:[AnyObject]?
    
    @IBOutlet weak var tableView: UITableView!
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        myListingsArray?.removeAll()
        myListingsArray = DatabaseManager.sharedInstance.getAllMyListings()
        tableView.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onClickOfLeftBarButton(sender:UIButton)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
//        if let count = myListingsArray?.count {
//            if indexPath.row == count-1 {
//                return 40
//            }
//        }else{
//          return 40
//        }
        
        return 78
    }
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let numberOfRows = myListingsArray?.count else {return 1}
        return numberOfRows + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let p = tableView.numberOfRowsInSection(0)
        
        if indexPath.row == p-1 {
            let cell = tableView.dequeueReusableCellWithIdentifier("postOffer", forIndexPath: indexPath)
            return cell
        }
    let cell = tableView.dequeueReusableCellWithIdentifier("c1", forIndexPath: indexPath)
        guard let myListingDict = myListingsArray?[indexPath.row] as? NSDictionary else{return cell}
        
        
        let  listingDesc = (cell.viewWithTag(4031) as! UILabel)
        let  listingCategory = (cell.viewWithTag(4032) as! UILabel)
        let  listingDate = (cell.viewWithTag(4033) as! UILabel)
        let  listingImage = (cell.viewWithTag(4034) as! UIImageView)
        let image = myListingDict["information"]?["images"] as? NSArray
        if image?.count>0{
            listingImage.image = image![0] as? UIImage
        }
        let date =  myListingDict["time"] as? Double
        let strDate = getDateFromTimestamp2(date, dateFormat: "dd MMM YYYY")
        
        listingDesc.text = myListingDict["information"]?["description"] as? String ?? "--"
        listingCategory.text = myListingDict["information"]?["category"] as? String ?? "--"
        listingDate.text = strDate

        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let p = tableView.numberOfRowsInSection(0)
        if indexPath.row == p-1{
            let postOfferinstance = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("PostOffersViewController") as! PostOffersViewController
            postOfferinstance.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(postOfferinstance, animated: true)
            return
        }
        guard let desc = myListingsArray?[indexPath.row]["information"]??["description"] as? String else{return}
      let alertView = UIAlertController(title: "Description of offer:", message:desc , preferredStyle: UIAlertControllerStyle.Alert)
        let alertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler: nil)
        alertView.addAction(alertAction)
        presentViewController(alertView, animated: true, completion: nil)
        
    }
    
    
}
