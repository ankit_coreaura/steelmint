//
//  ListingDetailsViewController.swift
// for both buy and sell
//

import UIKit
import MapKit
import Parse

class ListingDetailsViewController: UIViewController {
    var height = 32
    var sellPlantDetailDict = NSDictionary()
    var sizeOfHTMLText = CGRect()
    //var sizeOfHTMLTextOffer = CGRect()
    var plantDetailObject:PlantbeePost?
    var defaultSegmentControl = 0
    var viaSearch :Bool?
    var location:String?
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if viaSearch == true{
          (view.viewWithTag(880) as? UIButton)?.hidden = true
            return
        }
        if plantDetailObject?.isBookmarked == true{
            (view.viewWithTag(880) as? UIButton)?.setImage(UIImage(named: "tag_bookmarked.png") , forState: .Normal)
        }
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       
 
        calculateHeight()
        tableView.registerNib(UINib(nibName: "headerCell", bundle: nil), forCellReuseIdentifier: "headerCell")
        tableView.estimatedRowHeight = 45
        tableView.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }
    
    func leadImpression(){
        if  let id = sellPlantDetailDict["id"] as? String{
            let webserInstance = WebServices()
            webserInstance.setLeadImpression(nil, id: id) { (responseData) -> () in
                
            }
        }
        
        
    }
    func calculateHeight(){
        if let plantIdExists = sellPlantDetailDict["id"] as? String{
            plantDetailObject = DatabaseManager.sharedInstance.getPlantbeePostID(plantIdExists)
        }
        if let titleString = sellPlantDetailDict["title"] as? String{
            sizeOfHTMLText =  getSizeBasedOnText( GlobalConstants.SCREEN_WIDTH-50, heightOfContainer: 100, string: getHTMlRenderedString(titleString).string, fontSize: 14)
        }
//        if let titleString = sellPlantDetailDict["offer_detail"] as? String{
//            sizeOfHTMLTextOffer =  getSizeBasedOnText( GlobalConstants.SCREEN_WIDTH-50, heightOfContainer: 100, string: getHTMlRenderedString(titleString).string, fontSize: 14)
//        }
    }
    
    @IBAction func onClickOfLeftBarButton(sender:UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func segmentControlAction(sender:UISegmentedControl) {
        
        switch sender.selectedSegmentIndex
        {
        case 0:
            height = 32
            defaultSegmentControl = 0
            tableView.reloadData()
        case 1:
            height = 153
            defaultSegmentControl = 1

            tableView.reloadData()

        default:
            height = 300
            defaultSegmentControl = 2

            tableView.reloadData()

        }
    
    }
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch(indexPath.row)
        {
        case 0:
            return UITableViewAutomaticDimension
        case 1:
            return 200
        case 2:
            return 53
        case 3:
            return UITableViewAutomaticDimension
//            if sizeOfHTMLTextOffer.height > 1 {
//                
//                return sizeOfHTMLTextOffer.height+70
//                
//            }else{
//                return 85
//            }
        case 4:
            return CGFloat(height)
            
        default:
            if defaultSegmentControl == 0{
                return 60
            }

            return 0
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch(indexPath.row)
        {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier("headerCell", forIndexPath: indexPath) as! HeaderCell
            if let title = sellPlantDetailDict["title"] as? String
            {
                cell.titleLabel.text = getHTMlRenderedString(title).string
                
            }
            return cell
            
        case 1:
         let   cell=tableView.dequeueReusableCellWithIdentifier("c1", forIndexPath: indexPath) as! PlantbeeDetailTableViewCell
            
         if let dataLeadArray = sellPlantDetailDict["dataLead"] as? NSArray{
            if dataLeadArray.count>0{
                if let imageUrlArray =  dataLeadArray[0]["images"] as? NSArray {
                    if imageUrlArray.count>0{
                        if let imageURL = imageUrlArray[0]["image_url"] as? String{
                            if isNotNull(imageURL){
                                //plantImageUrl = imageURL
                                cell.imageVIew.sd_setImageWithURL(NSURL(string: imageURL)!)
                            }else{
                                cell.imageVIew.image = UIImage(named:"placeholderImage")
                            }
                        }else{
                          cell.imageVIew.image = UIImage(named:"placeholderImage")  
                        }
                    }else{
                        cell.imageVIew.image = UIImage(named:"placeholderImage")
                    }
                    
                }else{
                    cell.imageVIew.image = UIImage(named:"placeholderImage")
                }
            }else{
                cell.imageVIew.image = UIImage(named:"placeholderImage")
                
            }
            
         }else{
            cell.imageVIew.image = UIImage(named:"placeholderImage")
         }
            return cell
  
        case 2:
         let cell=tableView.dequeueReusableCellWithIdentifier("c2", forIndexPath: indexPath) as! PlantbeeDetailTableViewCell
            
        cell.company.text = sellPlantDetailDict["company"] as? String ?? "Unavailable"
         var city = ""
         var state = ""
         var country = ""
         if let tcity = sellPlantDetailDict["city_name"] as? String{ city = tcity + ","}
         if let tstate = sellPlantDetailDict["state_name"] as? String{state = tstate + ","}
         if let tcountry = sellPlantDetailDict["country_name"] as? String{country = tcountry }
         cell.location.text = (city+state+country).capitalizedString
         
         location = cell.location.text
         cell.date.text = "Posted: " + getDateFromTimestamp2(sellPlantDetailDict["datetime"] as? NSNumber, dateFormat: "dd MMM yyyy")
         if getDateFromTimestamp2(sellPlantDetailDict["datetime"] as? NSNumber, dateFormat: "dd MMM yyyy").isEmpty == true{
            if let date = sellPlantDetailDict["datetime"] as? String{
                cell.date.text = "Posted: " + date
   
            }
         }
         return cell

            
        case 3:
           let cell=tableView.dequeueReusableCellWithIdentifier("c3", forIndexPath: indexPath) as! PlantbeeDetailTableViewCell
            if let title = sellPlantDetailDict["offer_detail"] as? String
            {
                cell.offer.attributedText = getHTMlRenderedString(getHTMlRenderedString(title).string)
                cell.offer.font = UIFont.systemFontOfSize(12)
            }
           return cell

        case 4:
            switch defaultSegmentControl{
            case 0:
                let cell=tableView.dequeueReusableCellWithIdentifier("specificationHeader", forIndexPath: indexPath) as! PlantbeeDetailTableViewCell
                
                return cell
                
                
            case 1:
                let cell=tableView.dequeueReusableCellWithIdentifier("aboutUs", forIndexPath: indexPath) as! PlantbeeDetailTableViewCell
                cell.nameOFCompany.text = sellPlantDetailDict["company_name"] as? String ?? "-"
                cell.aboutCompany.text = sellPlantDetailDict["about_company"] as? String ?? "-"
                cell.businessType.text = sellPlantDetailDict["business_type"] as? String ?? "-"
                cell.addressOfCompany.text = sellPlantDetailDict["address"] as? String ?? "-"

                return cell
                
            case 2:
                let cell=tableView.dequeueReusableCellWithIdentifier("map", forIndexPath: indexPath) as! PlantbeeDetailTableViewCell
                
                cell.mapLocation.text = location 
                
                if  (sellPlantDetailDict["latitude"] as? String)?.characters.count > 1 && (sellPlantDetailDict["longitude"] as? String)?.characters.count > 1 {
                    let lat = NSNumberFormatter().numberFromString((sellPlantDetailDict["latitude"] as! String))?.doubleValue
                    let long = NSNumberFormatter().numberFromString((sellPlantDetailDict["longitude"] as! String))?.doubleValue
                    
                    if let longitudeCoordinates = long , latitudeCoordinates = lat{
                        let location = CLLocationCoordinate2D(
                            latitude:latitudeCoordinates,
                            longitude:longitudeCoordinates)
                        // 2
                        let span = MKCoordinateSpanMake(0.05, 0.05)
                        let region = MKCoordinateRegion(center: location, span: span)
                        cell.mapkitView.setRegion(region, animated: true)
                        
                        //3
                        let annotation = MKPointAnnotation()
                        annotation.coordinate = location
                        //                    annotation.title = "Sell"
                        //                    annotation.subtitle = "Machine"
                        cell.mapkitView.addAnnotation(annotation)
                        
                    }else{
                        let location = CLLocationCoordinate2D(
                            latitude:21.25274,
                            longitude:81.667887)
                        // 2
                        let span = MKCoordinateSpanMake(0.05, 0.05)
                        let region = MKCoordinateRegion(center: location, span: span)
                        cell.mapkitView.setRegion(region, animated: true)
                        
                        //3
                        let annotation = MKPointAnnotation()
                        annotation.coordinate = location
                        //                    annotation.title = "Sell"
                        //                    annotation.subtitle = "Machine"
                        cell.mapkitView.addAnnotation(annotation)
                    }
                    
                }else{
                    if cell.mapLocation.text?.characters.count > 2{
                        let address = cell.mapLocation.text!
                        let geocoder = CLGeocoder()
                        
                        geocoder.geocodeAddressString(address,completionHandler: {(placemarks: [CLPlacemark]?, error: NSError?) -> Void in
                            if placemarks?.count > 0{
                                placemarks![0].postalCode
                                
                                if let coordinates = placemarks![0].location?.coordinate{
                                    let span = MKCoordinateSpanMake(0.05, 0.05)
                                    let region = MKCoordinateRegion(center: coordinates, span: span)
                                    cell.mapkitView.setRegion(region, animated: true)
                                    
                                    //3
                                    let annotation = MKPointAnnotation()
                                    annotation.coordinate = coordinates
                                    //                    annotation.title = "Sell"
                                    //                    annotation.subtitle = "Machine"
                                    cell.mapkitView.addAnnotation(annotation)
                                    
                                }else{let location = CLLocationCoordinate2D(
                                    latitude:21.25274,
                                    longitude:81.667887)
                                    // 2
                                    let span = MKCoordinateSpanMake(0.05, 0.05)
                                    let region = MKCoordinateRegion(center: location, span: span)
                                    cell.mapkitView.setRegion(region, animated: true)
                                    
                                    //3
                                    let annotation = MKPointAnnotation()
                                    annotation.coordinate = location
                                    //                    annotation.title = "Sell"
                                    //                    annotation.subtitle = "Machine"
                                    cell.mapkitView.addAnnotation(annotation)
                                    
                                    
                                }
                                
                                
                                
                            }
                            
                        })
                        
                        
                    }else{
                        cell.mapLocation.text = "Unavailable"
                        let location = CLLocationCoordinate2D(
                            latitude:21.25274,
                            longitude:81.667887)
                        // 2
                        let span = MKCoordinateSpanMake(0.05, 0.05)
                        let region = MKCoordinateRegion(center: location, span: span)
                        cell.mapkitView.setRegion(region, animated: true)
                        
                        //3
                        let annotation = MKPointAnnotation()
                        annotation.coordinate = location
                        //                    annotation.title = "Sell"
                        //                    annotation.subtitle = "Machine"
                        cell.mapkitView.addAnnotation(annotation)
                        
                    }
                    
                }
                
               
                
                
                
                
                
                
                return cell
                
                
            default:
                break
            }
            if defaultSegmentControl != 0{
             break
            }
        default:
           let cell=tableView.dequeueReusableCellWithIdentifier("specificationData", forIndexPath: indexPath) as! PlantbeeDetailTableViewCell
           if sellPlantDetailDict["dataLead"] != nil && (sellPlantDetailDict["dataLead"] as? NSArray)?.count>0{
            
            if let data = (sellPlantDetailDict["dataLead"] as! NSArray)[indexPath.row-5] as? NSDictionary{
                cell.machineDetails.text = data["particular"] as? String ?? "-"
                cell.numberOfMachines.text = data["numbers"] as? String ?? String(data["numbers"] as? NSNumber)
                cell.numberOfMachines.text = cell.numberOfMachines.text?.stringByReplacingOccurrencesOfString("Optional(", withString: "")
                cell.numberOfMachines.text = cell.numberOfMachines.text?.stringByReplacingOccurrencesOfString(")", withString: "")
                cell.capacityOfMAchine.text = data["capacity"] as? String ?? "-"
                cell.makeOfMAchine.text = data["make"] as? String ?? "-"
                cell.yearOFMachine.text = data["year"] as? String ?? "-"

            }
            
           }
            
            return cell
            
        }
        return UITableViewCell()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if defaultSegmentControl == 0{
        if sellPlantDetailDict["dataLead"] != nil && (sellPlantDetailDict["dataLead"] as? NSArray)?.count>0{
        return (sellPlantDetailDict["dataLead"] as? NSArray)!.count + 5
        }
        else{
            return 5
            }

        }else{
            return 5
        }
    }
    @IBAction func contactSellerBtnEvent(sender: UIButton) {
        let phoneNumber = sellPlantDetailDict["mobile"] as? String
        if let pNo = Int(phoneNumber!) {
            if let phoneCallURL:NSURL = NSURL(string: "tel://\(pNo)") {
                
                let alert = UIAlertController(title: "", message: "Carrier charges may apply. Do you want to call?", preferredStyle: UIAlertControllerStyle.Alert)
                
                let callAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                    let application = UIApplication.sharedApplication()
                    if (application.canOpenURL(phoneCallURL)) {
                        application.openURL(phoneCallURL);
                    }
                })
                
                let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
                
                alert.addAction(callAction)
                alert.addAction(cancel)
                
                navigationController?.presentViewController(alert, animated: true, completion: nil)
                
                
            }
            
        }
        
        
    }
    @IBAction func expressIntersetBtnEvent(sender: UIButton) {
        
        let expressInstance = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ExpressInterestViewController") as?ExpressInterestViewController
        if let leadID = sellPlantDetailDict["id"] as? String{
            expressInstance?.leadId = leadID
            navigationController?.pushViewController(expressInstance!, animated: true)
        }else{
         showAlertController("Plantbee", message: "Temporarily unable to submit your interest contact customer care or try again later", reference: self)
        }
        
    }
    
    
    @IBAction func chatBtnEvent(sender: UIButton) {
        sender.userInteractionEnabled = false
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(2 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            sender.userInteractionEnabled = true
        }
        
        
        guard let secondUserEmail = sellPlantDetailDict["email_id"] as? String else{showAlertController("SteelMint", message: "Cannot chat with this user right now. Please try again later", reference: self);return}
        
        var userName = ""
        
        for char in secondUserEmail.characters{
            if char == "@"{break}
            userName.append(char)
        }
        if isNotNull(PFUser.currentUser()){
            let query = PFQuery(className: GlobalConstants.PF_USER_CLASS_NAME)
            query.whereKey(GlobalConstants.PF_USER_EMAIL, equalTo:secondUserEmail)
            query.findObjectsInBackgroundWithBlock {(objects,error) -> Void in
                if error == nil && objects?.count > 0 {
                    let otherUser = objects![0] as! PFUser
                    let chatViewController = ChatViewController()
                    let user1 = PFUser.currentUser()
                    let user2 = otherUser
                    let groupId = Messages.startPrivateChat(user1!, user2: user2)
                    chatViewController.groupId = groupId
                    chatViewController.hidesBottomBarWhenPushed = true
                    chatViewController.titleToshow = user2.username
                    chatViewController.user2 =  user2
                    chatViewController.user2EmailId =  secondUserEmail
                    //  chatViewController.senderImageUrl = participant.img
                    
                    
                    self.navigationController?.pushViewController(chatViewController, animated: true)
                }
                    
                    
                else{
                    print("error encountered while chatting")
                    AppCommonFunctions.sharedInstance.signupInParseWith(secondUserEmail, email: secondUserEmail)
                    showNotification("Initiating your chat with \(userName) . Please wait.....", showOnNavigation: true, showAsError: false)
                    
                    
                    
                    var notiCheck = false
                    NSNotificationCenter.defaultCenter().addObserverForName("kParseUserSignUp", object: nil, queue: nil, usingBlock: {(notification) -> Void in
                        
                        
                        print("notification for parse chat getting")
                        NSNotificationCenter.defaultCenter().removeObserver(self, name: "kParseUserSignUp", object: nil)
                        
                        
                        if notiCheck == false{
                            print("one time chat done")
                            query.findObjectsInBackgroundWithBlock {(objects,error) -> Void in
                                
                                if error == nil && objects?.count > 0 {
                                    let otherUser = objects![0] as! PFUser
                                    let chatViewController = ChatViewController()
                                    let user1 = PFUser.currentUser()
                                    let user2 = otherUser
                                    let groupId = Messages.startPrivateChat(user1!, user2: user2)
                                    chatViewController.groupId = groupId
                                    chatViewController.hidesBottomBarWhenPushed = true
                                    chatViewController.titleToshow = user2.username
                                    chatViewController.user2 =  user2
                                    chatViewController.user2EmailId =  secondUserEmail
                                    //  chatViewController.senderImageUrl = participant.img
                                    
                                    
                                    self.navigationController?.pushViewController(chatViewController, animated: true)
                                }
                            }
                            
                        }
                        
                        
                        
                        notiCheck = true
                        
                        
                        
                    })
                    
                }
            }
        }else{
            showNotification("connecting... try again after some time", showOnNavigation: true, showAsError: false)
            AppCommonFunctions.sharedInstance.prepareUserForParseChat()
            
            
        }
        
        
        
        
        
        
        


        
    }
    
    // MARK: BOOKMARK AND SHARE
    
    @IBAction func bookmarkBtnEvent(sender: UIButton) {
        
        if plantDetailObject?.isBookmarked != true{
            plantDetailObject?.isBookmarked = true
            sender.setImage(UIImage(named: "tag_bookmarked.png") , forState: .Normal)
            bookmarkAnimatedView("Bookmarked")
        }
        else{
            plantDetailObject?.isBookmarked = false
            sender.setImage(UIImage(named: "tagT") , forState: .Normal)
            
        }
        DatabaseManager.sharedInstance.saveContext()
    }
    
    @IBAction func shareBtnEvent(sender: AnyObject) {
        
        var title : NSString?
        var description : NSString?
        var url : NSString?
        if let title = sellPlantDetailDict["offer_detail"] as? String
        {
        description = getHTMlRenderedString(title).string
        }
        title = sellPlantDetailDict["title"] as? String
        url = sellPlantDetailDict.valueForKey("href") as? NSString
        AppCommonFunctions.sharedInstance.share(title, description: description, url: url, fromViewController: self)
    }
    func bookmarkAnimatedView(str:String?){
        let bookmarkLabel = UILabel()
                bookmarkLabel.frame = CGRect(x: (GlobalConstants.SCREEN_WIDTH/2)-50, y: (GlobalConstants.SCREEN_HEIGHT)-80, width: 110, height: 20)

        bookmarkLabel.text = str
        bookmarkLabel.textColor = UIColor.whiteColor()

        bookmarkLabel.backgroundColor = GlobalConstants.APP_THEME_BLUE_COLOR

        bookmarkLabel.layer.cornerRadius = 5.0
        bookmarkLabel.clipsToBounds = true
        bookmarkLabel.textAlignment = .Center
        self.view.addSubview(bookmarkLabel)
        bookmarkLabel.layer.opacity = 0
        UIView.animateWithDuration(0.5) { () -> Void in
            bookmarkLabel.layer.opacity = 1.0
        }
        
        dispatch_after(1, dispatch_get_main_queue()) { () -> Void in
            UIView.animateWithDuration(1.2) { () -> Void in
                bookmarkLabel.layer.opacity = 0
            }
        }
        
    }
}
