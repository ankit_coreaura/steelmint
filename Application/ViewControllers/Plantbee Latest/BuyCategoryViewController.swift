//
//  BuyCategoryViewController.swift
//  Application
//
//  Created by Chandan Singh on 12/30/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//



import UIKit

class BuyCategoryViewController: UIViewController,UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate
{
    var indicator = UIActivityIndicatorView()
    var tempVIew = UIView()
    @IBOutlet weak var searchBtnOutlet: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var dataBuyPlants :[AnyObject]?
    var arrayOfAllValues = [NSDictionary]()

    var filterVC : FilterViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        
    }
    
    @IBAction func onClickOfLeftBarButton(sender:UIButton)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func initialSetup(){
        // Setup ActivityIdicator
        indicator = UIActivityIndicatorView  (activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        indicator.frame = CGRectMake(GlobalConstants.SCREEN_WIDTH/2-10, GlobalConstants.SCREEN_HEIGHT/2-10, 10.0, 10.0)
        tempVIew = UIView(frame: tableView.frame)
        tempVIew.backgroundColor = UIColor.whiteColor()
        getBuyCategories()
        
        
        //GETTING Plantbee filters:
        filterVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("FilterViewController") as? FilterViewController
        if CacheManager.sharedInstance.loadObject("plantbeeFilters") != nil{
            filterVC!.data = CacheManager.sharedInstance.loadObject("plantbeeFilters") as! NSArray
            
            self.filterVC!.completionBlock = {
                (appliedFilters) -> ()in
                
                if appliedFilters.count>0{
                                    if appliedFilters["NO_INT"] != nil{
                                        showAlertController("Steelmint", message: "You cannot use filters in offline mode. Please connect to internet", reference: self)
                                        return
                                    }
                                    if isInternetConnectivityAvailable(false){
                                        CacheManager.sharedInstance.saveObject(appliedFilters, identifier: "buyFilters")
                                        self.filterVC?.removeAnimated()
                                       
                                    }
                                    else{
                                        showAlertController("Steelmint", message: "You cannot use filters in offline mode. Please connect to internet", reference: self)
                                    }
                                    //print(appliedFilters)
                                }
                                else{
                    CacheManager.sharedInstance.saveObject(nil, identifier: "buyFilters")
                    self.filterVC?.removeAnimated()
                                }
                }
            }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: TABLEVIEW DELEGATES METHODS
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 54
    }
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
            if let numberOfRows = dataBuyPlants?.count {
                return numberOfRows
            }
            return 0
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
       
            guard let categoriesData = dataBuyPlants?[indexPath.row] as? NSDictionary else{return UITableViewCell()}
          let  cell = tableView.dequeueReusableCellWithIdentifier("cell2", forIndexPath: indexPath)
            let  categoryTitle = (cell.viewWithTag(4020) as! UILabel)
            let  leadsTitle = (cell.viewWithTag(4021) as! UILabel)
            let  imageView = (cell.viewWithTag(4019) as! UIImageView)
            
            categoryTitle.text = categoriesData["name"] as? String
            leadsTitle.text = String(categoriesData["total_leads"]!)
            if leadsTitle.text?.characters.count > 0{
                leadsTitle.text = leadsTitle.text! + "  Listings"
            }
            
            if isNotNull(categoriesData["image"]){
                if let str = categoriesData["image"] as? String{
                    if let url = NSURL(string:str) {
                        imageView.sd_setImageWithURL(url)
                    }else{
                     imageView.image=UIImage(named: "otherCategory")   
                    }
                }
                
                
            }else{
                imageView.image=UIImage(named: "otherCategory")
            }
            
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        
            if dataBuyPlants?[indexPath.row]["children"] as? [NSDictionary]  == nil {
                let  buyPlantListingInstance = storyboard?.instantiateViewControllerWithIdentifier("BuyPlantListingViewController") as! BuyPlantListingViewController
                if let code = dataBuyPlants?[indexPath.row]["plant_type_code"] as? String{
                    buyPlantListingInstance.plant_type = code
                }
                if let leads = dataBuyPlants?[indexPath.row]["total_leads"] as? NSNumber {
                    let name = dataBuyPlants?[indexPath.row]["name"] as? String ?? ""
                    buyPlantListingInstance.totalListings = String(leads) + " Listings Of " + name
                }
                self.navigationController?.pushViewController(buyPlantListingInstance, animated: true)
                return
            }
            else{
                let  subCategoryInstance = storyboard?.instantiateViewControllerWithIdentifier("BuySubCategoryViewController") as! BuySubCategoryViewController
                subCategoryInstance.mainCategory = dataBuyPlants?[indexPath.row]["name"] as? String ?? ""
                if let dataPresent = dataBuyPlants?[indexPath.row]["children"] as? [NSDictionary]{
                    
                    subCategoryInstance.subCategoryArray = dataPresent
                }
                self.navigationController?.pushViewController(subCategoryInstance, animated: true)
                
                
                
                
            }
            
      
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    // MARK: Web Services Hit
    
    func getBuyCategories(){
        let webserviceInsatnce = WebServices()
        if DatabaseManager.sharedInstance.getAllPlantbee()?.count > 0{
            getBuyCategoriesFromDb()
        }
        if !indicator.isAnimating() && DatabaseManager.sharedInstance.getAllPlantbee()?.count == nil {
            showNativeActivityIndicator()
        }
        if isInternetConnectivityAvailable(true)==false {
            if indicator.isAnimating(){
                self.stopActivityIndicator()
                
            }
            return
        }
        
        webserviceInsatnce.getBuyTypePlants(NSDictionary(), completionBlock: {
            (responseData)->() in
            if self.indicator.isAnimating(){
                self.stopActivityIndicator()
                
            }
            if responseData != nil{
                guard let responseArray = responseData?["data#"] as? NSArray else{return}
                for objects in responseArray{
                    if objects is NSDictionary{
                        DatabaseManager.sharedInstance.addPlantbee(objects as! NSDictionary, type: "buy")
                    }
                    
                }
                self.getBuyCategoriesFromDb()
            }
            
        })
    }
    
    // MARK: Db Management
    
    func getBuyCategoriesFromDb(){
        guard let offlineData = DatabaseManager.sharedInstance.getAllPlantbee() else{return}
        dataBuyPlants = offlineData
        
        print("data filtered:\(dataBuyPlants)")
        tableView.reloadData()

        
    }

    @IBAction func searchBtnEvent(sender: UIButton) {
        
        if sender.currentTitle == "Cancel"{

            sender.setTitle("Search", forState: .Normal)
            searchBar.resignFirstResponder()
            searchBar.text = nil
        }else{
            searchBar.becomeFirstResponder()
        }
    }
    
    // MARK: Search Bar Delegates
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        if searchBtnOutlet.currentTitle == "Cancel"{
            searchBar.resignFirstResponder()
        }
    }
    
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {

        return true
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchBtnOutlet.setTitle("Cancel", forState: .Normal)
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar){
        
        if  let serachtext = searchBar.text {
        let storyboard = UIStoryboard(name: "Secondary", bundle: nil)
        
        let  buyPlantListingInstance = storyboard.instantiateViewControllerWithIdentifier("BuyPlantListingViewController") as! BuyPlantListingViewController
        buyPlantListingInstance.searchDict = NSDictionary(object:"buy:?" + serachtext , forKey: "search")
        buyPlantListingInstance.totalListings = "Search Results"
        self.navigationController?.pushViewController(buyPlantListingInstance, animated: true)
        
        }
        //tableView.reloadData()
    }

    //MARK: ACTIVITY INDICATOR
    func showNativeActivityIndicator(){
        view.addSubview(tableView)
        view.addSubview(indicator)
        indicator.startAnimating()
    }
    
    func stopActivityIndicator() {
        indicator.stopAnimating()
        tempVIew.removeFromSuperview()
    }
    @IBAction func postYourOffer(sender: UIButton) {
        
        let postOfferinstance = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("PostOffersViewController") as! PostOffersViewController
        postOfferinstance.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(postOfferinstance, animated: true)
    }
    
    @IBAction func filterBtnEvent(sender: UIButton) {
        
     // filterVC?.showAnimated()
        
    }
    
}