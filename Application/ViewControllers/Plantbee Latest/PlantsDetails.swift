//
//  PlantsDetails.swift
//  screens
//
//  Created by Authorised Personnel on 26/10/15.
//  Copyright © 2015 Authorised Personnel. All rights reserved.
//

import UIKit

class PlantsDetails: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var buyPlantDetailDict = NSDictionary()
    var sizeOfHTMLText = CGRect()
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        calculateHeight()
        tableView.estimatedRowHeight = 216
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    func calculateHeight(){
        if let titleString = buyPlantDetailDict["offer_detail"] as? String{
            
             sizeOfHTMLText =  getSizeBasedOnText( GlobalConstants.SCREEN_WIDTH-50, heightOfContainer: 100, string: getHTMlRenderedString(titleString).string, fontSize: 14)
    }
    }
    @IBAction func onClickOfLeftBarButton(sender:UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch(indexPath.row)
        {
        case 0:
            return 200
        case 1:
            if let titleString = buyPlantDetailDict["title"] as? String{
                let size =  getSizeBasedOnText( GlobalConstants.SCREEN_WIDTH-80, heightOfContainer: 100, string: titleString, fontSize: 15)
                return size.height+110
                
            }else{
                return 134
            }
        default:
            if sizeOfHTMLText.height > 1 {
                
                return sizeOfHTMLText.height+230
                
            }else{
                return 216
            }

        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell=UITableViewCell()
        switch(indexPath.row)
            
        {
        case 0:
            cell=tableView.dequeueReusableCellWithIdentifier("c1", forIndexPath: indexPath)
            let  plantImage = (cell.viewWithTag(8) as! UIImageView)
            
            if let imageUrl  = buyPlantDetailDict["image"] as? String{
                plantImage.sd_setImageWithURL(NSURL(string:imageUrl)!, placeholderImage: UIImage(named: "placeholderImage"))
            }else{
                plantImage.image = UIImage(named: "placeholderImage")
            }
            
        case 1:
            cell=tableView.dequeueReusableCellWithIdentifier("c2", forIndexPath: indexPath)
            
          

            
            let  buyplantTitle = (cell.viewWithTag(9) as! UILabel)
            let  buyplantCompany = (cell.viewWithTag(10) as! UILabel)
            let  buyplantLocation = (cell.viewWithTag(11) as! UILabel)
            let  buyplantPhoneNo = (cell.viewWithTag(12) as! UILabel)
            let  buyplantDate = (cell.viewWithTag(13) as! UILabel)
            let  buyplantPrice = (cell.viewWithTag(14) as! UILabel)
            if let title = buyPlantDetailDict["title"] as? String
            {
                buyplantTitle.attributedText = getHTMlRenderedString(title)
                buyplantTitle.font = UIFont.boldSystemFontOfSize(13)

            }
            buyplantPrice.text = buyPlantDetailDict["prices_detail"] as? String
            buyplantCompany.text = buyPlantDetailDict["company"] as? String ?? "Unavailable"
            buyplantLocation.text = buyPlantDetailDict["country_name"] as? String
            buyplantPhoneNo.text = buyPlantDetailDict["mobile"] as? String
            buyplantDate.text = buyPlantDetailDict["datetime"] as? String
        case 2:
          let  cell=tableView.dequeueReusableCellWithIdentifier("c3", forIndexPath: indexPath) as! BuyPlantTableViewCell
          if let title = buyPlantDetailDict["offer_detail"] as? String
          {
            cell.offerDetail.text = getHTMlRenderedString(title).string
            cell.offerDetail.font = UIFont.systemFontOfSize(12)
          }
          if let leadDetails = (buyPlantDetailDict["dataLead"] as? NSArray){
            if leadDetails.count > 0 {
                let leadDict = leadDetails[0] as? NSDictionary
                cell.particulars.text = leadDict?["particular"] as? String ?? "-"
                cell.capacity.text = leadDict?["capacity"] as? String ?? "-"
                cell.type.text = leadDict?["type"] as? String ?? "-"
                cell.number.text = leadDict?["numbers"] as? String ?? "-"
                cell.location.text = leadDict?["location"] as? String ?? "-"
                cell.remarks.text = leadDict?["remarks"] as? String ?? "-"
                cell.year.text = leadDict?["year"] as? String ?? "-"
                cell.make.text = leadDict?["make"] as? String ?? "-"


            }
          }
            
            
        default:
            break
        }
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    @IBAction func contactSellerBtnEvent(sender: UIButton) {
        let phoneNumber = buyPlantDetailDict["mobile"] as? String
        if let pNo = NSNumberFormatter().numberFromString(phoneNumber!)?.intValue {
            if let phoneCallURL:NSURL = NSURL(string: "tel://\(pNo)") {
                
                let alert = UIAlertController(title: "", message: "Carrier charges may apply. Do you want to call?", preferredStyle: UIAlertControllerStyle.Alert)
                
                let callAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                    let application = UIApplication.sharedApplication()
                    if (application.canOpenURL(phoneCallURL)) {
                        application.openURL(phoneCallURL);
                    }
                })
                
                let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
                
                alert.addAction(callAction)
                alert.addAction(cancel)
                
                navigationController?.presentViewController(alert, animated: true, completion: nil)
                
                
            }
            
        }
      
        
    }
    @IBAction func expressIntersetBtnEvent(sender: UIButton) {
        
        let expressInstance = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ExpressInterestViewController") as?ExpressInterestViewController
        if let leadID = buyPlantDetailDict["id"] as? String{
            expressInstance?.leadId = leadID
            navigationController?.pushViewController(expressInstance!, animated: true)
        }else{
            showAlertController("Plantbee", message: "Temporarily unable to submit your interest contact customer care or try again later", reference: self)
        }
        
        
    }
}
