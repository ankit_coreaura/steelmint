//
//  PlantListingViewController.swift
//  Application
//
//  Created by Chandan Singh on 1/2/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//

import UIKit
import Parse
class PlantListingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var listinglabel:UILabel!
    var sellcategoryWiseDataArray: [AnyObject]?
    var filterVC : FilterViewController?
    var filtered:[String] = []
    var plant_type = ""
    var parent_code :String?
    var totalListings = ""
    var indicator = UIActivityIndicatorView()
    var tempVIew = UIView()
    var plantDate = ""

    @IBOutlet weak var clearAllBtn:UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        setupImageForFilterBtn()
    }
    func setupImageForFilterBtn(){
      clearAllBtn.setImage(UIImage.image(UIImage(named: "cancelImage")!, withColor: UIColor.whiteColor()), forState: UIControlState.Normal)
        clearAllBtn.layer.cornerRadius = 15.0
        clearAllBtn.layer.borderWidth = 2.0
        clearAllBtn.layer.borderColor = UIColor.whiteColor().CGColor

    }
    
    func initialSetup(){
        // Setup ActivityIdicator
        indicator = UIActivityIndicatorView  (activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        indicator.frame = CGRectMake(GlobalConstants.SCREEN_WIDTH/2-10, GlobalConstants.SCREEN_HEIGHT/2-10, 10.0, 10.0)
        tempVIew = UIView(frame: tableView.frame)
        tempVIew.backgroundColor = UIColor.whiteColor()
        if CacheManager.sharedInstance.loadObject("sellFilters") != nil{

            self.listinglabel.text = "Filtered Results: "
            clearAllBtn.hidden = false


        }else
        {
            listinglabel.text = totalListings.uppercaseString
            getContentsFromDB()
        }
        
        if totalListings.componentsSeparatedByString("Listings")[0].stringByReplacingOccurrencesOfString(" ", withString: "") != "0"{
            getPlantType()
        }

        
        // Filter Management:
        filterVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("FilterViewController") as? FilterViewController
        if CacheManager.sharedInstance.loadObject("plantbeeFilters") != nil{
            filterVC!.data = CacheManager.sharedInstance.loadObject("plantbeeFilters") as! NSArray
            
            self.filterVC!.completionBlock = {
                (appliedFilters) -> ()in
                
                if appliedFilters.count>0{
                    if appliedFilters["NO_INT"] != nil{
                        showAlertController("Steelmint", message: "You cannot use filters in offline mode. Please connect to internet", reference: self)
                        return
                    }
                    if isInternetConnectivityAvailable(false){
                        self.showNativeActivityIndicator()
                        CacheManager.sharedInstance.saveObject(appliedFilters, identifier: "sellFilters")
                        self.clearAllBtn.hidden = false
                        self.getPlantType()
                        self.filterVC?.removeAnimated()
                        
                    }
                    else{
                        showAlertController("Steelmint", message: "You cannot use filters in offline mode. Please connect to internet", reference: self)
                    }
                    //print(appliedFilters)
                }
                else{
                    CacheManager.sharedInstance.saveObject(nil, identifier: "sellFilters")
                    self.listinglabel.text = self.totalListings.uppercaseString
                    self.clearAllBtn.hidden = true
                    self.getContentsFromDB()
                    self.filterVC?.removeAnimated()
                    
                }
            }
        }

    }
  
    @IBAction func onClickOfLeftBarButton(sender:UIButton)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
            return 100
    }
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = nil
        if let numberOfRows = sellcategoryWiseDataArray?.count {
            if numberOfRows == 0
            {
                noDataToShow()
            }
            return numberOfRows;
        }
        noDataToShow()
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let  cell = tableView.dequeueReusableCellWithIdentifier("cellPlant", forIndexPath: indexPath)

        let  buyplantTitle = (cell.viewWithTag(4031) as! UILabel)
        let  buyplantCompany = (cell.viewWithTag(4032) as! UILabel)
        let  buyplantPhoneNo = (cell.viewWithTag(4033) as! UILabel)
        let  buyplantDate = (cell.viewWithTag(4040) as! UILabel)
        let  plantImage = (cell.viewWithTag(4035) as! UIImageView)
        
        guard let plantData = sellcategoryWiseDataArray?[indexPath.row] else{return UITableViewCell()}
        
        buyplantDate.text = getDateFromTimestamp2(plantData["datetime"] as? NSNumber, dateFormat: "dd MMM yyyy")
        buyplantPhoneNo.text = plantData["mobile"] as? String
        
        buyplantTitle.text = plantData["title"] as? String
        buyplantCompany.text = plantData["company"] as? String ?? "Unavailable"
       
        plantImage.image = nil
        plantImage.image = UIImage(named:"placeholderImage")
        
        if let dataLeadArray = plantData["dataLead"] as? NSArray{
            if dataLeadArray.count>0{
                
                if dataLeadArray[0]["thumb_img"]  != nil{
                    if let imageURL = dataLeadArray[0]["thumb_img"] as? String{
                        if isNotNull(imageURL){
                            print("plantbee image url found....")
                            if let urlExists = NSURL(string: imageURL){
                                plantImage.sd_setImageWithURL(urlExists)
                                
                            }
                        }
                    }
                    
                    
                    
                }else{
                    plantImage.image = UIImage(named:"placeholderImage")
                    
                }
                
            }else{
                plantImage.image = UIImage(named:"placeholderImage")
            }
            
        }
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let  listingInstance = storyboard?.instantiateViewControllerWithIdentifier("ListingDetailsViewController") as! ListingDetailsViewController
        if let data = sellcategoryWiseDataArray?[indexPath.row]  as? NSDictionary{
            listingInstance.sellPlantDetailDict = data
        }
        
        self.navigationController?.pushViewController(listingInstance, animated: true)
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    // MARK: Hit web service to get plants detail
   
    func getPlantType(){
        
        
        if let parentCode = self.parent_code{
            if !indicator.isAnimating() && DatabaseManager.sharedInstance.getAllPlantbeePostCodeWise(plant_type,parentCode:parentCode, type: "Sell")?.count == nil {
                showNativeActivityIndicator()
            }
            
        }else{
            if !indicator.isAnimating() && DatabaseManager.sharedInstance.getAllPlantbeePostCodeWise(plant_type,parentCode:"no value", type: "Sell")?.count == nil {
                showNativeActivityIndicator()
            }
        }
        
        if isInternetConnectivityAvailable(true)==false {
            if indicator.isAnimating(){
                self.stopActivityIndicator()
                
            }
            return
        }
        
        
        
        
        let webservicesInstance = WebServices()
       // let information = NSDictionary(objects: [[plant_type],"sell"], forKeys: ["plant_type","lead_type"])
       
        
        
        
        var information = NSMutableDictionary()
        
        if CacheManager.sharedInstance.loadObject("sellFilters") != nil{
            let dict = CacheManager.sharedInstance.loadObject("sellFilters") as! NSDictionary
            information = dict as! NSMutableDictionary
        }
        information.setObject([plant_type], forKey: "plant_type")
        information.setObject("sell", forKey: "lead_type")
        
        
        webservicesInstance.getPostDetails(information, completionBlock: {
        (responseData)-> () in
            if self.indicator.isAnimating(){
                self.stopActivityIndicator()
                
            }
            if responseData != nil{
                guard let responseArray = responseData?["data#"] as? NSArray else{return}
                self.sellcategoryWiseDataArray?.removeAll()
                if CacheManager.sharedInstance.loadObject("sellFilters") != nil{
//                    let array = (CacheManager.sharedInstance.loadObject("sellFilters")as! NSDictionary).allKeys
//                    var filtersText = ""
//
//                    for objects in array{
//                        filtersText += ((objects as! NSString).stringByReplacingOccurrencesOfString("_", withString: "-").capitalizedString) + "  "
//                    }
//                    self.listinglabel.text = "Filtered Results: \(filtersText)"
                    self.listinglabel.text = "Filtered Results:"
                    self.sellcategoryWiseDataArray = responseArray as [AnyObject]
                    self.tableView.reloadData()
                
                }else{ self.listinglabel.text = self.totalListings
                
                    for objects in responseArray{
                        if let parentCode = self.parent_code{
                            if objects is NSDictionary{
                                
                                let tempdict = NSMutableDictionary(dictionary: objects as! NSDictionary)
                                tempdict.setObject(parentCode, forKey: "parent_code")
                                DatabaseManager.sharedInstance.addPlantbeePost(tempdict)
                                
                            }
                        }else{
                            DatabaseManager.sharedInstance.addPlantbeePost(objects as! NSDictionary)
                        }
                        
                        
                    }
                    self.getContentsFromDB()
                
                }
               

            }
        })
        
    }
    
    
    
    func getContentsFromDB(){
        sellcategoryWiseDataArray?.removeAll()
        if let parentCode = self.parent_code{
            guard let offlineData = DatabaseManager.sharedInstance.getAllPlantbeePostCodeWise(plant_type,parentCode:parentCode, type: "Sell") else{return}
            sellcategoryWiseDataArray = offlineData
            
            
        }else{
            guard let offlineData = DatabaseManager.sharedInstance.getAllPlantbeePostCodeWise(plant_type,parentCode:"no value", type: "Sell") else{return}
            sellcategoryWiseDataArray = offlineData
            
        }
        tableView.reloadData()
    }
    // MARK: SHORTCUT METHODS
    
    @IBAction func callEvent(sender: UIButton) {
        
        let view = sender.superview!
        let cell = view.superview as! UITableViewCell
        
        let indexpath = tableView.indexPathForCell(cell)
        
        
        guard let plantData = sellcategoryWiseDataArray?[indexpath!.row] else{return}
        
        let phoneNumber = plantData["mobile"] as? String
        if let pNo = Int(phoneNumber!) {
            if let phoneCallURL:NSURL = NSURL(string: "tel://\(pNo)") {
                
                let alert = UIAlertController(title: "", message: "Carrier charges may apply. Do you want to call?", preferredStyle: UIAlertControllerStyle.Alert)
                
                let callAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                    let application = UIApplication.sharedApplication()
                    if (application.canOpenURL(phoneCallURL)) {
                        application.openURL(phoneCallURL);
                    }
                })
                
                let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
                
                alert.addAction(callAction)
                alert.addAction(cancel)
                
                navigationController?.presentViewController(alert, animated: true, completion: nil)
                
                
            }}
    }
    
    
    @IBAction func chatEvent(sender: UIButton) {
        
        let view = sender.superview!
        let cell = view.superview as! UITableViewCell
        
        let indexpath = tableView.indexPathForCell(cell)
        guard let plantData = sellcategoryWiseDataArray?[indexpath!.row] else{return}
        
//        guard let secondUserEmail = plantData["email_id"] as? String else{showAlertController("SteelMint", message: "Cannot chat with this user right now. Please try again later", reference: self);return}
//        
//        var userName = ""
//        
//        for char in secondUserEmail.characters{
//            if char == "@"{break}
//            userName.append(char)
//        }
//        if isNotNull(PFUser.currentUser()){
//            let query = PFQuery(className: GlobalConstants.PF_USER_CLASS_NAME)
//            query.whereKey(GlobalConstants.PF_USER_EMAIL, equalTo:secondUserEmail)
//            query.findObjectsInBackgroundWithBlock {(objects,error) -> Void in
//                if error == nil && objects?.count > 0 {
//                    let otherUser = objects![0] as! PFUser
//                    let chatViewController = ChatViewController()
//                    let user1 = PFUser.currentUser()
//                    let user2 = otherUser
//                    let groupId = Messages.startPrivateChat(user1!, user2: user2)
//                    chatViewController.groupId = groupId
//                    chatViewController.hidesBottomBarWhenPushed = true
//                    chatViewController.titleToshow = user2.username
//                    
//                    //  chatViewController.senderImageUrl = participant.img
//                    
//                    
//                    self.navigationController?.pushViewController(chatViewController, animated: true)
//                }else{
//                    AppCommonFunctions.sharedInstance.signupInParseWith(userName, email: secondUserEmail)
//                    showNotification("Initiating your chat with \(userName) . Please wait.....", showOnNavigation: true, showAsError: false)
//                }
//            }
//        }else{
//            showNotification("connecting... try again after some time", showOnNavigation: true, showAsError: false)
//            AppCommonFunctions.sharedInstance.prepareUserForParseChat()
//        }
        
        guard let secondUserEmail = plantData["email_id"] as? String else{showAlertController("SteelMint", message: "Cannot chat with this user right now. Please try again later", reference: self);return}
        
        var userName = ""
        
        for char in secondUserEmail.characters{
            if char == "@"{break}
            userName.append(char)
        }
        if isNotNull(PFUser.currentUser()){
            let query = PFQuery(className: GlobalConstants.PF_USER_CLASS_NAME)
            query.whereKey(GlobalConstants.PF_USER_EMAIL, equalTo:secondUserEmail)
            query.findObjectsInBackgroundWithBlock {(objects,error) -> Void in
                if error == nil && objects?.count > 0 {
                    let otherUser = objects![0] as! PFUser
                    let chatViewController = ChatViewController()
                    let user1 = PFUser.currentUser()
                    let user2 = otherUser
                    let groupId = Messages.startPrivateChat(user1!, user2: user2)
                    chatViewController.groupId = groupId
                    chatViewController.hidesBottomBarWhenPushed = true
                    chatViewController.titleToshow = user2.username
                    chatViewController.user2 =  user2
                    chatViewController.user2EmailId =  secondUserEmail
                    //  chatViewController.senderImageUrl = participant.img
                    
                    
                    self.navigationController?.pushViewController(chatViewController, animated: true)
                }
                    
                    
                else{
                    print("error encountered while chatting")
                    AppCommonFunctions.sharedInstance.signupInParseWith(secondUserEmail, email: secondUserEmail)
                    showNotification("Initiating your chat with \(userName) . Please wait.....", showOnNavigation: true, showAsError: false)
                    
                    
                    
                    var notiCheck = false
                    NSNotificationCenter.defaultCenter().addObserverForName("kParseUserSignUp", object: nil, queue: nil, usingBlock: {(notification) -> Void in
                        
                        
                        print("notification for parse chat getting")
                        NSNotificationCenter.defaultCenter().removeObserver(self, name: "kParseUserSignUp", object: nil)
                        
                        
                        if notiCheck == false{
                            print("one time chat done")
                            query.findObjectsInBackgroundWithBlock {(objects,error) -> Void in
                                
                                if error == nil && objects?.count > 0 {
                                    let otherUser = objects![0] as! PFUser
                                    let chatViewController = ChatViewController()
                                    let user1 = PFUser.currentUser()
                                    let user2 = otherUser
                                    let groupId = Messages.startPrivateChat(user1!, user2: user2)
                                    chatViewController.groupId = groupId
                                    chatViewController.hidesBottomBarWhenPushed = true
                                    chatViewController.titleToshow = user2.username
                                    chatViewController.user2 =  user2
                                    chatViewController.user2EmailId =  secondUserEmail
                                    //  chatViewController.senderImageUrl = participant.img
                                    
                                    
                                    self.navigationController?.pushViewController(chatViewController, animated: true)
                                }
                            }
                            
                        }
                        
                        
                        
                        notiCheck = true
                        
                        
                        
                    })
                    
                }
            }
        }else{
            showNotification("connecting... try again after some time", showOnNavigation: true, showAsError: false)
            AppCommonFunctions.sharedInstance.prepareUserForParseChat()
            
            
        }

        

        
        
    }
    @IBAction func expressInterestEvent(sender: UIButton) {
        let view = sender.superview!
        let cell = view.superview as! UITableViewCell
        
        let indexpath = tableView.indexPathForCell(cell)
         guard let plantData = sellcategoryWiseDataArray?[indexpath!.row] else{return}
        
        let expressInstance = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ExpressInterestViewController") as?ExpressInterestViewController
        expressInstance?.leadId = plantData["id"] as? String ?? ""
        navigationController?.pushViewController(expressInstance!, animated: true)
        
    }
    //MARK: ACTIVITY INDICATOR
    func showNativeActivityIndicator(){
        self.tableView.allowsSelection = false
        view.addSubview(tableView)
        view.addSubview(indicator)
        indicator.startAnimating()
    }
    
    func stopActivityIndicator() {
        self.tableView.allowsSelection = true
        indicator.stopAnimating()
        tempVIew.removeFromSuperview()
    }
    @IBAction func filterBtnEvent(sender: UIButton) {
        filterVC?.showAnimated()
    }
    
    
    @IBAction func clearFilters(sender:UIButton){
        stopActivityIndicator()
        CacheManager.sharedInstance.saveObject(nil, identifier: "sellFilters")
        self.listinglabel.text = self.totalListings.uppercaseString
        self.getContentsFromDB()
        clearAllBtn.hidden = false
        filterVC!.cancelBtnEvent(UIButton())
    }
    // MARk: No data Management
    func noDataToShow(){
        
        
        if indicator.isAnimating() == false{
            let noDataLabel = UILabel (frame: CGRectMake(0, 0,tableView.bounds.size.width,tableView.bounds.size.height))
//            noDataLabel.text             = "No data available"
//            noDataLabel.textColor        = UIColor.lightGrayColor()
//            noDataLabel.textAlignment    = .Center
//            tableView.backgroundView = noDataLabel;
        }
        
        
    }
}
