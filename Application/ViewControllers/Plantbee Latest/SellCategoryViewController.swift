//
//  SellCategoryViewController.swift
//  Application
//
//  Created by Chandan Singh on 12/31/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit

class SellCategoryViewController: UIViewController {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var arrayOfAllValues = [NSDictionary]()
    var dataSellPlants :[AnyObject]?
    var indicator = UIActivityIndicatorView()
    var tempVIew = UIView()
    var filterVC : FilterViewController?

    @IBOutlet weak var searchBtnOutlet: UIButton!
    var filtered:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        
    }
    func initialSetup(){
        // Setup ActivityIdicator
        indicator = UIActivityIndicatorView  (activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        indicator.frame = CGRectMake(GlobalConstants.SCREEN_WIDTH/2-10, GlobalConstants.SCREEN_HEIGHT/2-10, 10.0, 10.0)
        tempVIew = UIView(frame: tableView.frame)
        tempVIew.backgroundColor = UIColor.whiteColor()
        getSellCategories()
        
        
        //GETTING Plantbee filters:
        filterVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("FilterViewController") as? FilterViewController
        if CacheManager.sharedInstance.loadObject("plantbeeFilters") != nil{
            filterVC!.data = CacheManager.sharedInstance.loadObject("plantbeeFilters") as! NSArray
            
            self.filterVC!.completionBlock = {
                (appliedFilters) -> ()in
                
                if appliedFilters.count>0{
                    if appliedFilters["NO_INT"] != nil{
                        showAlertController("Steelmint", message: "You cannot use filters in offline mode. Please connect to internet", reference: self)
                        return
                    }
                    if isInternetConnectivityAvailable(false){
                        CacheManager.sharedInstance.saveObject(appliedFilters, identifier: "sellFilters")
                        self.filterVC?.removeAnimated()
                        
                        
                    }
                    else{
                        showAlertController("Steelmint", message: "You cannot use filters in offline mode. Please connect to internet", reference: self)
                    }
                    //print(appliedFilters)
                }
                else{
                    CacheManager.sharedInstance.saveObject(nil, identifier: "sellFilters")
                    self.filterVC?.removeAnimated()

                }
            }
        }
    }
    
    @IBAction func onClickOfLeftBarButton(sender:UIButton)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
       override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: TABLEVIEW DELEGATES METHODS
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
     
            return 54
    
    }
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            if let numberOfRows = dataSellPlants?.count {
                return numberOfRows
            }
            return 0
       // }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
      
        guard let categoriesData = dataSellPlants?[indexPath.row] as? NSDictionary else{return UITableViewCell()}
          let  cell = tableView.dequeueReusableCellWithIdentifier("cell2", forIndexPath: indexPath)
            let  categoryTitle = (cell.viewWithTag(4020) as! UILabel)
            let  leadsTitle = (cell.viewWithTag(4021) as! UILabel)
            let  imageView = (cell.viewWithTag(4019) as! UIImageView)
            
            categoryTitle.text = categoriesData["name"] as? String
            leadsTitle.text = String(categoriesData["total_leads"]!)
            if leadsTitle.text?.characters.count > 0{
                leadsTitle.text = leadsTitle.text! + "  Listings"
            }
            
            if isNotNull(categoriesData["image"]){
                if let str = categoriesData["image"] as? String{
                    if let url = NSURL(string:str) {
                        imageView.sd_setImageWithURL(url,placeholderImage:UIImage(named: "otherCategory"))
                        
                    }
                }
                
                
            }else{
                imageView.image=UIImage(named: "otherCategory")
            }
            
     
        
        return cell
    }
   
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    // MARK: Web Services Hit
    
    func getSellCategories(){
        let webserviceInsatnce = WebServices()
        if DatabaseManager.sharedInstance.getAllPlantbeeSell()?.count > 0{
            getSellCategoriesFromDb()
        }
        if !indicator.isAnimating() && DatabaseManager.sharedInstance.getAllPlantbeeSell()?.count == nil {
            showNativeActivityIndicator()
        }
        if isInternetConnectivityAvailable(true)==false {
            if indicator.isAnimating(){
                self.stopActivityIndicator()
                
            }
            return
        }
        webserviceInsatnce.getSellTypePlants(NSDictionary(), completionBlock: {
            (responseData)->() in
            if self.indicator.isAnimating(){
                self.stopActivityIndicator()
                
            }
            if responseData != nil{
                guard let responseArray = responseData?["data#"] as? NSArray else{return}
                for objects in responseArray{
                    if objects is NSDictionary{
                        DatabaseManager.sharedInstance.addPlantbeeSell(objects as! NSDictionary, type: "sell")
                    }
                    
                }
                self.getSellCategoriesFromDb()
            }
            
        })
    }
    
    // MARK: Db Management
    
    func getSellCategoriesFromDb(){
        guard let offlineData = DatabaseManager.sharedInstance.getAllPlantbeeSell() else{return}
        dataSellPlants = offlineData
        
        print("data filtered:\(dataSellPlants)")
        tableView.reloadData()

        
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {

        if dataSellPlants?[indexPath.row]["children"] as? [NSDictionary]  == nil {
            let  plantListingInstance = storyboard?.instantiateViewControllerWithIdentifier("PlantListingViewController") as! PlantListingViewController
            if let code = dataSellPlants?[indexPath.row]["plant_type_code"] as? String{
                plantListingInstance.plant_type = code
            }
            if let leads = dataSellPlants?[indexPath.row]["total_leads"] as? NSNumber {
                let name = dataSellPlants?[indexPath.row]["name"] as? String ?? ""
                plantListingInstance.totalListings = String(leads) + " Listings Of " + name
            }
            
            self.navigationController?.pushViewController(plantListingInstance, animated: true)
        }
        else{
            let  subCategoryInstance = storyboard?.instantiateViewControllerWithIdentifier("SellSubCategoryViewController") as! SellSubCategoryViewController
            if let nameOfMAinCAtegory = dataSellPlants?[indexPath.row]["name"] as? String{
                subCategoryInstance.mainCategory = nameOfMAinCAtegory
                
            }
            if let dataPresent = dataSellPlants?[indexPath.row]["children"] as? [NSDictionary]{
                
                subCategoryInstance.subCategoryArray = dataPresent
            }
            self.navigationController?.pushViewController(subCategoryInstance, animated: true)
            
            }//}
        
        
    }
    

    @IBAction func searchBtnEvent(sender: UIButton) {
        
        if sender.currentTitle == "Cancel"{

            sender.setTitle("Search", forState: .Normal)
            searchBar.resignFirstResponder()
            searchBar.text = nil

        }else{
            searchBar.becomeFirstResponder()
        }
    }
    
    // MARK: Search Bar Delegates
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        if searchBtnOutlet.currentTitle == "Cancel"{
            searchBar.resignFirstResponder()
        }
    }
    
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {

        return true
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchBtnOutlet.setTitle("Cancel", forState: .Normal)
        //tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar){
        if  let serachtext = searchBar.text {
            let storyboard = UIStoryboard(name: "Secondary", bundle: nil)
            
            let  buyPlantListingInstance = storyboard.instantiateViewControllerWithIdentifier("BuyPlantListingViewController") as! BuyPlantListingViewController
        buyPlantListingInstance.searchDict = NSDictionary(object:"sell:?" + serachtext , forKey: "search")
            buyPlantListingInstance.totalListings = "Search Results"
            self.navigationController?.pushViewController(buyPlantListingInstance, animated: true)
            
        }
    }
 
    //MARK: ACTIVITY INDICATOR
    func showNativeActivityIndicator(){
        view.addSubview(tableView)
        view.addSubview(indicator)
        indicator.startAnimating()
    }
    
    func stopActivityIndicator() {
        indicator.stopAnimating()
        tempVIew.removeFromSuperview()
    }
    @IBAction func filterBtnEvent(sender: UIButton) {
        
        
       // filterVC?.showAnimated()
    }
    @IBAction func postYourOffer(sender: UIButton) {
        
        let postOfferinstance = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("PostOffersViewController") as! PostOffersViewController
        postOfferinstance.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(postOfferinstance, animated: true)
    }
}
