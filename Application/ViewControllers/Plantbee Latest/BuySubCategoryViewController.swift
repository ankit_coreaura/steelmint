//
//  BuySubCategoryViewController.swift
//  Application
//
//  Created by Chandan Singh on 12/31/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit

class BuySubCategoryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var searchButtonOutlet: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var dataBuyPlants :[AnyObject]?
    var arrayOfAllValues = [NSDictionary]()
    var filterArray = [AnyObject]()
    var searchActive = false
    var subCategoryArray = [NSDictionary]()
    var mainCategory = ""
    var selectedIndexPath: Int?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        (view.viewWithTag(4019) as! UILabel).text = mainCategory.uppercaseString
       
     rearrangeAllPlantsCategories()
    }
    
    @IBAction func onClickOfLeftBarButton(sender:UIButton)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        
            return 54
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
       
        if searchActive == false
        {
           let cell = tableView.dequeueReusableCellWithIdentifier("cell2")! 
            let  categoryTitle = (cell.viewWithTag(4020) as! UILabel)
            let  leadsTitle = (cell.viewWithTag(4021) as! UILabel)
            guard let categoriesTitle = subCategoryArray[section]["name"] as? String else{return cell
            }
            categoryTitle.text = getHTMlRenderedString(categoriesTitle).string
            leadsTitle.text = String(subCategoryArray[section]["total_leads"]!)
            if leadsTitle.text?.characters.count > 0{
                leadsTitle.text = leadsTitle.text! + "  Listings"
            }
            if subCategoryArray[section]["children"] == nil{
                cell.accessoryView = UIImageView(image: UIImage(named: "arrowSideEvents"))
            }else{
               cell.accessoryView = UIImageView(image: UIImage(named: "arrowDownEvents"))
            }
            //add button on header
            let button   = UIButton(type: UIButtonType.System) as UIButton
            button.frame = CGRectMake(0, 0, 430, 45)
            button.tag = section;
            button.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
            cell.addSubview(button)
            return cell
            
        }
        return nil

    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if searchActive
        {
            
            return 0
        }else
        {
            return 54
        }
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if searchActive  {
        
                return 1
        }else
        {
            return subCategoryArray.count
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchActive
        {
            
            return filterArray.count
        }
        else
        {
            let ip = section
            if selectedIndexPath != nil {
                if ip == selectedIndexPath!
                {
                    if let children = subCategoryArray[section]["children"] as? NSArray
                    {
                        return children.count
                    }else
                    {
                        return 0
                    }
                }
                else
                {
                    return 0
                }
            }
            else
            {
                return 0
            }
        }
   }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        if searchActive{
            
           let cell = tableView.dequeueReusableCellWithIdentifier("cell3", forIndexPath: indexPath)
            guard let categoriesData = filterArray[indexPath.row] as? NSDictionary else{return cell}

            let  categoryTitle = (cell.viewWithTag(4020) as! UILabel)
            let  leadsTitle = (cell.viewWithTag(4021) as! UILabel)
            
            categoryTitle.text = categoriesData["name"] as? String
            leadsTitle.text = String(categoriesData["total_leads"]!)
            if leadsTitle.text?.characters.count > 0{
                leadsTitle.text = leadsTitle.text! + "  Listings"
            }
            return cell
            
        }else{
        
    let cell = tableView.dequeueReusableCellWithIdentifier("cell3", forIndexPath: indexPath)
            let  categoryTitle = (cell.viewWithTag(4020) as! UILabel)
            let  leadsTitle = (cell.viewWithTag(4021) as! UILabel)
            
           let childer = subCategoryArray[selectedIndexPath!]["children"]
            categoryTitle.text = childer![indexPath.row]["name"] as? String
            if let number = childer![indexPath.row]["total_leads"] as? NSNumber{
                leadsTitle.text = (number).stringValue
            }
            
            if leadsTitle.text?.characters.count > 0
            {
                leadsTitle.text = leadsTitle.text! + "  Listings"
            }
       return cell

        }
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        if searchActive{
            let exists = indexPath.row < filterArray.count ? true : false
            if exists{
            let  buyPlantListingInstance = storyboard?.instantiateViewControllerWithIdentifier("BuyPlantListingViewController") as! BuyPlantListingViewController
            if let code = filterArray[indexPath.row]["plant_type_code"] as? String{
                buyPlantListingInstance.plant_type = code
            }
            
            if let code = filterArray[indexPath.row]["plant_type_code"] as? String{
                buyPlantListingInstance.parent_Code = code
            }
            
            if let leads = filterArray[indexPath.row]["total_leads"] as? NSNumber {
                let name = filterArray[indexPath.row]["name"] as? String ?? ""
                buyPlantListingInstance.totalListings = String(leads) + " Listings Of " + name
            }
            self.navigationController?.pushViewController(buyPlantListingInstance, animated: true)
            return
            }
        
        }else{
            let  buyPlantListingInstance = storyboard?.instantiateViewControllerWithIdentifier("BuyPlantListingViewController") as! BuyPlantListingViewController
            let childer = subCategoryArray[selectedIndexPath!]["children"]
            if let code = childer![indexPath.row]["plant_type_code"] as? String {
                buyPlantListingInstance.plant_type = code
            }
            if let leads = childer![indexPath.row]["total_leads"] as? NSNumber{
                let name = childer![indexPath.row]["name"] as? String ?? ""
                buyPlantListingInstance.totalListings = String(leads) + "  Listings Of " + name
            }
            self.navigationController?.pushViewController(buyPlantListingInstance, animated: true)
        }
        

    }
    
    func buttonAction(sender:UIButton!)
    {
        if subCategoryArray[sender.tag]["children"] == nil{
            let  plantListingInstance = storyboard?.instantiateViewControllerWithIdentifier("BuyPlantListingViewController") as! BuyPlantListingViewController
            if let code = subCategoryArray[sender.tag]["plant_type_code"] as? String{
                plantListingInstance.plant_type = code
            }
            if let leads = subCategoryArray[sender.tag]["total_leads"] as? NSNumber {
                let name = subCategoryArray[sender.tag]["name"] as? String ?? ""
                plantListingInstance.totalListings = String(leads) + " Listings Of " + name
            }
            self.navigationController?.pushViewController(plantListingInstance, animated: true)
        }
        else{

        switch selectedIndexPath {
        case nil:
            selectedIndexPath = sender.tag
        default:
            if selectedIndexPath! == sender.tag
            {
                selectedIndexPath = nil
            } else
            {
                selectedIndexPath = sender.tag
            }
        }
        
        self.tableView.reloadData()
    }
    }
    
    
    @IBAction func searchButtonEvent(sender: UIButton) {
        
        
        if sender.currentTitle == "Cancel"{
            searchActive = false
            filterArray.removeAll()
            tableView.reloadData()
            sender.setTitle("Search", forState: .Normal)
            searchBar.resignFirstResponder()
            searchBar.text = nil

        }else{
            searchBar.becomeFirstResponder()
        }
    }
    
    func rearrangeAllPlantsCategories(){
            let dictTemp = NSMutableDictionary()
            for objects in subCategoryArray{
                dictTemp.setObject(objects["name"] as? String ?? "", forKey: "name")
                dictTemp.setObject(objects["plant_type_code"] as? String ?? "", forKey: "plant_type_code")
                dictTemp.setObject(objects["total_leads"] as? NSNumber ?? "", forKey: "total_leads")
                arrayOfAllValues.append(dictTemp.mutableCopy() as! NSDictionary)
                dictTemp.removeAllObjects()
                
                if objects["children"] != nil {
                    if let childrenValues = objects["children"] as? NSArray{
                    
                        for childObjects in childrenValues{
                            
                            dictTemp.setObject(childObjects["name"] as? String ?? "", forKey: "name")
                            dictTemp.setObject(childObjects["plant_type_code"] as? String ?? "", forKey: "plant_type_code")
                            dictTemp.setObject(childObjects["total_leads"] as? NSNumber ?? "", forKey: "total_leads")
                            arrayOfAllValues.append(dictTemp.mutableCopy() as! NSDictionary)
                            dictTemp.removeAllObjects()
                        }
                    }
                    
                }
            }
        
        
    }
    // MARK: Search Bar Delegates
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        if searchButtonOutlet.currentTitle == "Cancel"{
            searchBar.resignFirstResponder()
        }
    }
    
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
        if arrayOfAllValues.count == 0{
            showAlertController("Plantbee", message: "Please try again", reference: self)
            return false
        }
        return true
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchActive = true
        searchButtonOutlet.setTitle("Cancel", forState: .Normal)
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar){
        tableView.reloadData()
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        filterArray.removeAll()
        if searchBar.text?.characters.count>0{
            let searchString = searchBar.text
            
            // Filter the data array and get only those categories that match the search text.
            let predicate = NSPredicate(format: "name contains[c] %a", argumentArray:[searchString!])
            filterArray = (arrayOfAllValues as NSArray).filteredArrayUsingPredicate(predicate)
        }
        tableView.reloadData()
    }
    @IBAction func postYourOffers(sender: UIButton) {
        let postOfferinstance = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("PostOffersViewController") as! PostOffersViewController
        postOfferinstance.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(postOfferinstance, animated: true)
    }
 
}
