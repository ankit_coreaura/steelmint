//
//  TenderDetailsViewController.swift
//  screens
//
//  Created by Authorised Personnel on 26/10/15.
//  Copyright © 2015 Authorised Personnel. All rights reserved.
//

import UIKit

class TenderDetailsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var tenderDetailDict:NSDictionary?
    @IBOutlet weak var tableView:UITableView!
    var tenderObject:Tender?
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if tenderObject?.isBookmarked == true{
            (view.viewWithTag(880) as? UIButton)?.setImage(UIImage(named: "tag_bookmarked.png") , forState: .Normal)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.registerNib(UINib(nibName: "headerCell", bundle: nil), forCellReuseIdentifier: "headerCell")
        
        
        if let _ = tenderDetailDict?.valueForKey("phone_no"){
            
        }else{
            view.viewWithTag(571)?.hidden = false
        }
    }
    func initialSetup(){
        
        guard let tenderDetailData=tenderDetailDict else{return}
        let dict = NSMutableDictionary()
        if let _=tenderDetailData.valueForKey("tender_id"){
            dict.setObject(tenderDetailData.valueForKey("tender_id")!, forKey: "ID")
            
        }
        else{
            dict.setObject(tenderDetailData.valueForKey("ID")!, forKey: "ID")
        }
        
        tenderObject = DatabaseManager.sharedInstance.getTender(dict)
    }
    @IBAction func onClickOfLeftBarButton(sender:UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch(indexPath.row)
        {
            
        case 0:
            return UITableViewAutomaticDimension
        case 1:
            return 155
        case 2:
return 68
        case 3:
            return UITableViewAutomaticDimension

            
        default:
            return 0.0
        }
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell=UITableViewCell()
        switch(indexPath.row)
            
        {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier("headerCell", forIndexPath: indexPath) as! HeaderCell
           // cell.titleLabel.font = cell.textLabel?.font.fontWithSize(18)
            cell.titleLabel.text = tenderDetailDict?.valueForKey("title") as? String
            return cell
        case 1:
            let cell=tableView.dequeueReusableCellWithIdentifier("c1", forIndexPath: indexPath) as! TenderTableViewCell
            
            if isNotNull(tenderDetailDict?.valueForKey("imgpath") as? String){
                
                if let url = NSURL(string:tenderDetailDict?.valueForKey("imgpath") as! String){
                    cell.tenderDetailImageView.sd_setImageWithURL(url)
                    
                }else{
                    cell.tenderDetailImageView.image = UIImage(named: "placeholderImage")
                    cell.tenderDetailImageView.contentMode = UIViewContentMode.ScaleAspectFill
                }
                
            }else{
                cell.tenderDetailImageView.image = UIImage(named: "placeholderImage")
                
            }
            
            
        case 2:
            let  cell=tableView.dequeueReusableCellWithIdentifier("c2", forIndexPath: indexPath) as! TenderTableViewCell
            cell.tenderDetailName.text = tenderDetailDict?.valueForKey("company_name") as? String
            cell.tenderDetailQuantity.text = tenderDetailDict?.valueForKey("qty") as? String
            cell.tenderDetailGrade.text = tenderDetailDict?.valueForKey("grade") as? String
            cell.tenderDetailRegion.text = tenderDetailDict?.valueForKey("region") as? String
            cell.tenderDetailDate.text = getDateFromTimestamp2(tenderDetailDict?.valueForKey("due_date") as? NSNumber, dateFormat: "dd MMM, yyyy")
            
            if cell.tenderDetailDate.text == "" {
               cell.tenderDetailDate.text = tenderDetailDict?.valueForKey("due_date") as? String

            }
            return cell
            
        default:
            let  cell=tableView.dequeueReusableCellWithIdentifier("c3", forIndexPath: indexPath) as! TenderTableViewCell
            var str = (tenderDetailDict?.valueForKey("description_tender"))! as! String
            str = str.stringByReplacingOccurrencesOfString("<[^>]+>", withString: "", options: .RegularExpressionSearch, range: nil)
            cell.tenderDetailDescription.text = str
            
            if cell.tenderDetailDescription.text == ""{
             cell.tenderDetailDescription.text = tenderDetailDict?.valueForKey("description") as? String
            }
            
            //cell.tenderDetailDescription.text = cell.tenderDetailDescription.text?.capitalizedString
            return cell
        }
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        if indexPath.row == 1{
            //let cell = tableView.cellForRowAtIndexPath(indexPath)
            if let object = tenderDetailDict{
                if isNotNull(object.valueForKey("imgpath") as? String){
                    AppCommonFunctions.sharedInstance.showImages(self, images: [(view.viewWithTag(9890) as! UIImageView).image!], initialIndex: 0, zoomFromView: (view.viewWithTag(9890) as! UIImageView))
                    
                }
            }
        }
    }
    
    
    
    @IBAction func bidBtnEvent(sender: UIButton) {
//        if isUserLoggedIn(){
            guard let tenderDetailData=tenderDetailDict else{return}
            var id = ""
            if let tenID = tenderDetailData.valueForKey("tender_id") as? String{
                id =  tenID
                
            }
            else{
                id = tenderDetailData.valueForKey("ID") as! String
            }
            let bidInstance = storyboard?.instantiateViewControllerWithIdentifier("ParticipateViewController") as? ParticipateViewController
            bidInstance?.tenderID = id
            navigationController?.pushViewController(bidInstance!, animated: true)
            
       // }
    }
    
    @IBAction func contactSellerBtnEvent(sender: UIButton) {
        
        if let phoneNumber = tenderDetailDict?.valueForKey("phone_no"){
            
            
            let alertController = UIAlertController(title: "", message: "Do you want to call. Carrier charges may apply.", preferredStyle: .Alert)
            let cancelAction = UIAlertAction(title: "Cancel", style:.Cancel, handler: nil)
            let callAction = UIAlertAction(title: "Call", style:.Default, handler:{ (UIAlertAction) -> Void in
                
                if let phoneCallURL:NSURL = NSURL(string: "tel://\(phoneNumber)") {
                    let application:UIApplication = UIApplication.sharedApplication()
                    if (application.canOpenURL(phoneCallURL)) {
                        application.openURL(phoneCallURL);
                    }
                }
                
            })
            
            alertController.addAction(callAction)
            alertController.addAction(cancelAction)
            presentViewController(alertController, animated: true, completion: nil)
            
            
        }
        else{
            showAlertController("SteelMint", message: "Phone number unavailable", reference: self)
        }
        
    }
    
    @IBAction func bookmarkClickedEvent(sender: UIButton) {
        
        if (tenderObject == nil){
            guard let tenDetailDict=tenderDetailDict else {return}
            DatabaseManager.sharedInstance.addNews(tenDetailDict)
            let dict = NSMutableDictionary()
            if let _ = tenDetailDict.valueForKey("tender_id"){
                dict.setObject(tenDetailDict.valueForKey("tender_id")!, forKey: "ID")
                
            }
            else{
                dict.setObject(tenDetailDict.valueForKey("ID")!, forKey: "ID")
            }
            
            tenderObject = DatabaseManager.sharedInstance.getTender(dict)
            tenderObject?.viaSearchOrFilter = true

        }
        
        if tenderObject?.isBookmarked != true{
            tenderObject?.isBookmarked = true
            sender.setImage(UIImage(named: "tag_bookmarked.png") , forState: .Normal)
            bookmarkAnimatedView("Bookmarked")
        }
        else{
            tenderObject?.isBookmarked = false
            sender.setImage(UIImage(named: "tagT") , forState: .Normal)
            
        }
        DatabaseManager.sharedInstance.saveContext()
    }
    
    @IBAction func shareClickedEvent(sender: UIButton) {
        
        var title : NSString?
        var description : NSString?
        var url : NSString?
        title = tenderDetailDict?.valueForKey("title") as? NSString
        var str = (tenderDetailDict?.valueForKey("description_tender"))! as! String
        str = str.stringByReplacingOccurrencesOfString("<[^>]+>", withString: "", options: .RegularExpressionSearch, range: nil)
        description = str
        url = tenderDetailDict?.valueForKey("imgpath") as? NSString
        AppCommonFunctions.sharedInstance.share(title, description: description, url: url, fromViewController: self)
        
    }
    func bookmarkAnimatedView(str:String?){
        let bookmarkLabel = UILabel()

        bookmarkLabel.frame = CGRect(x: (GlobalConstants.SCREEN_WIDTH/2)-50, y: (GlobalConstants.SCREEN_HEIGHT)-80, width: 110, height: 20)
        bookmarkLabel.text = str
        bookmarkLabel.textColor = UIColor.whiteColor()
        bookmarkLabel.backgroundColor = GlobalConstants.APP_THEME_BLUE_COLOR
        bookmarkLabel.layer.cornerRadius = 5.0
        bookmarkLabel.clipsToBounds = true
        bookmarkLabel.textAlignment = .Center
        self.view.addSubview(bookmarkLabel)
        bookmarkLabel.layer.opacity = 0
        UIView.animateWithDuration(0.5) { () -> Void in
            bookmarkLabel.layer.opacity = 1.0
        }
        
        dispatch_after(1, dispatch_get_main_queue()) { () -> Void in
            UIView.animateWithDuration(1.2) { () -> Void in
                bookmarkLabel.layer.opacity = 0
            }
        }
        
    }
}
