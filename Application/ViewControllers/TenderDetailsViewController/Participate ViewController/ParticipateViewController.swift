//
//  ParticipateViewController.swift
//  Application
//
//  Created by Ankit Nandal on 13/11/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit

class ParticipateViewController: UIViewController ,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var addressTableview:UITableView!
    @IBOutlet weak var addressTextField: UITextField!

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var CommentsTextView: UITextView!
    var dictOfLocation = ["country":"","state":"","city":""]
    var tenderID = ""
    let filterArrayOfCountries = NSMutableArray()

    var completecountryList = NSDictionary()



    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        nameTextField.layer.borderColor = UIColor.lightGrayColor().CGColor
        emailTextField.layer.borderColor = UIColor.lightGrayColor().CGColor
        phoneNumberTextField.layer.borderColor = UIColor.lightGrayColor().CGColor
        phoneNumberTextField.layer.borderWidth = 1.0
        emailTextField.layer.borderWidth = 1.0
        addressTextField.layer.borderColor = UIColor.lightGrayColor().CGColor
        addressTextField.layer.borderWidth = 1.0
        
         (view.viewWithTag(489) as! UIImageView).layer.borderColor = UIColor.lightGrayColor().CGColor
        (view.viewWithTag(490) as! UIImageView).layer.borderColor = UIColor.lightGrayColor().CGColor
        (view.viewWithTag(491) as! UIImageView).layer.borderColor = UIColor.lightGrayColor().CGColor
        (view.viewWithTag(492) as! UIImageView).layer.borderColor = UIColor.lightGrayColor().CGColor
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let s = try! String(contentsOfFile: NSBundle.mainBundle()
            .pathForResource("countryList", ofType: "txt")!,
            encoding: NSUTF8StringEncoding)
        let x = parsedJsonFrom2(s.dataUsingEncoding(NSUTF8StringEncoding))
        if x != nil{
            if let dict = x?.valueForKey("data#") as? NSDictionary{
                if self.completecountryList.count == 0{
                    self.completecountryList = dict
                }
            }
        }
        addressTableview.hidden = true
        addressTableview.layer.borderColor = UIColor.lightGrayColor().CGColor
        addressTableview.layer.borderWidth = 1.0
        addressTableview.backgroundColor = UIColor(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        initialSetup()
    }
    
    func initialSetup(){
        let userInfoDict = CacheManager.sharedInstance.loadObject("userinfo") as?NSMutableDictionary
        if userInfoDict?.count>0{
            
            
            nameTextField.text = userInfoDict?.valueForKey("data#")?.valueForKey("name") as?String
            emailTextField.text = userInfoDict?.valueForKey("data#")?.valueForKey("username") as?String
            phoneNumberTextField.text = userInfoDict?.valueForKey("data#")?.valueForKey("phoneno") as?String
            
            if nameTextField.text?.characters.count > 0{
                nameTextField.userInteractionEnabled = false

            }
            if emailTextField.text?.characters.count > 0{
                emailTextField.userInteractionEnabled = false

            }
            if phoneNumberTextField.text?.characters.count > 0{
              phoneNumberTextField.userInteractionEnabled = false
            }
            
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func bidBtnEvent(sender: UIButton) {
        if nameTextField.text!.isEmpty || emailTextField.text!.isEmpty || phoneNumberTextField.text!.isEmpty || dictOfLocation["country"] == "" || CommentsTextView.text.isEmpty {
           showAlertController("SteelMint", message: "Please provide all the mendatory fields", reference: self)
        }else{
            
            
            if emailTextField.text!.isEmail{
                
            }else{

                showAlertController("SteelMint", message: "Please enter valid Email id", reference: self)
            
                return
            }
           
            if phoneNumberTextField.text!.isPhoneNumber{
                
            }else{
                
                showAlertController("SteelMint", message: "Please enter valid Phone Number ", reference: self)
                
                return}
            
            
            
          let webserviceInstance=WebServices()
            let information=NSMutableDictionary()
            information.setObject(sessionId(), forKey: "token")
            information.setObject(CommentsTextView.text, forKey: "comments")
            information.setObject(nameTextField.text!, forKey: "name")
            information.setObject(emailTextField.text!, forKey: "email")
            information.setObject(phoneNumberTextField.text!, forKey: "mobile")
            information.setObject(dictOfLocation["country"]!, forKey: "country_id")
           
            information.setObject(dictOfLocation["state"]!, forKey: "state_id")
            information.setObject(dictOfLocation["city"]!, forKey: "city_id")

            information.setObject(tenderID, forKey: "tender_id")

            webserviceInstance.progresssIndicatorText = "Bidding...."
            webserviceInstance.bidOnTender(information, completionBlock: {
                (responseData) -> ()in
                
                if responseData != nil{
                    if let response = responseData?["status#"] as? String{
                        if response == "1"{
                        showAlertDifferentType("Your bid has been successfully submitted. We will contact you soon.")
                        self.navigationController?.popViewControllerAnimated(true)
                        }else{
                            showAlertDifferentType("Your bid has not been successfully submitted. Please try again.")
                        }
                    }else{
                        showAlertDifferentType("Your bid has not been successfully submitted. Please try again.")
                    }
                }else{
                  showAlertDifferentType("Your bid has not been successfully submitted. Please try again.")
                }
            })
 
        }
        
        
    }

    @IBAction func backBtnEvent(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        //(textField.superview?.superview as! UIScrollView).scrollsToTop = true
        //(textField.superview?.superview as! //UIScrollView).setContentOffset(CGPointMake(0, -100), animated: true)
    }

   
    
    
    @IBAction func textFieldValueChanged(sender: UITextField) {
        print("outside")
        filterArrayOfCountries.removeAllObjects()

        if sender.text?.characters.count > 2{
            if addressTableview.hidden == true{
                let yAxis = ((sender.superview?.superview as! UIScrollView).contentOffset.y + 100)
               addressTableview.hidden = false
                (sender.superview?.superview as! UIScrollView).setContentOffset(CGPointMake(0, yAxis), animated: true)

            }
            dictOfLocation["city"] = ""
            dictOfLocation["state"] = ""
            dictOfLocation["country"] = ""

            
            print("inside")
            for values in completecountryList.allValues{
                let dictOfLocation = NSMutableDictionary()
                
                //stringCheck = false
                //country
                if (values["name"] as! String).lowercaseString.containsString(sender.text!.lowercaseString){
                   // stringCheck = true
                    dictOfLocation.setObject(values["name"] as! String, forKey: "cntName")
                    dictOfLocation.setObject(values["id"] as! String, forKey: "cntId")
                }
                //state
                if values["state"] != nil {
                    for valuesState in (values["state"] as! NSDictionary).allValues{
                        if (valuesState["name"] as! String).lowercaseString.containsString(sender.text!.lowercaseString){
                           // stateCheck = true
                            dictOfLocation.setObject(values["name"] as! String, forKey: "cntName")
                            dictOfLocation.setObject(values["id"] as! String, forKey: "cntId")
                            
                            dictOfLocation.setObject(valuesState["name"] as! String, forKey: "stateName")
                            dictOfLocation.setObject(valuesState["id"] as! String, forKey: "stateId")
                        }
                        
                        
                        
                        //city
                        if valuesState["city"] != nil  {
                            for valuesCity in valuesState["city"] as! NSArray{
                                if (valuesCity["name"] as! String).lowercaseString.containsString(sender.text!.lowercaseString){
                                  //  cityCheck = true
                                  
                                    
                                    dictOfLocation.setObject(values["name"] as! String, forKey: "cntName")
                                    dictOfLocation.setObject(values["id"] as! String, forKey: "cntId")
                                    
                                    dictOfLocation.setObject(valuesState["name"] as! String, forKey: "stateName")
                                    dictOfLocation.setObject(valuesState["id"] as! String, forKey: "stateId")
                                    
                                    dictOfLocation.setObject(valuesCity["name"] as! String, forKey: "cityName")
                                    dictOfLocation.setObject(valuesCity["id"] as! String, forKey: "cityId")
                                }
                            }
                            
                        }
                    }
                    
                    
                    
                    
                    
                }
                

                
               // print(filterArrayOfCountries)
                if dictOfLocation.count > 0{
                    filterArrayOfCountries.addObject(dictOfLocation)
 
                }
            }
            
          //
        }
        else{
            
            dictOfLocation["city"] = ""
            dictOfLocation["state"] = ""
            dictOfLocation["country"] = ""
            addressTableview.hidden = true
        }
        addressTableview.reloadData()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("addressCell", forIndexPath: indexPath)
        let city = cell.viewWithTag(76) as! UILabel
        let state = cell.viewWithTag(77) as! UILabel
        let country = cell.viewWithTag(78) as! UILabel
        
        city.text = nil
        state.text = nil
        country.text = nil
        
        if let dict = filterArrayOfCountries[indexPath.row] as? NSDictionary{
        if addressTextField.text?.characters.count > 2{
           //city
            if dict["cityName"]  == nil{
                
                if dict["stateName"]  == nil{
                    city.text = (dict["cntName"] as? String)?.capitalizedString
                }else{
                    city.text = (dict["stateName"] as? String)?.capitalizedString
                }
               
            }else{
                city.text = (dict["cityName"] as? String)?.capitalizedString
            }
            
            
            //state
            if city.text == (dict["stateName"] as? String)?.capitalizedString{
                state.text = (dict["cntName"] as? String)?.capitalizedString
            }
            else if city.text == (dict["cntName"] as? String)?.capitalizedString{
                
            }
            else{
            if dict["stateName"] == nil {
                    state.text = (dict["cntName"] as? String)?.capitalizedString
                    
                    
                }else{
                    state.text = (dict["stateName"] as? String)?.capitalizedString
                    
                }
            }
            
            
           //country
            if state.text == dict["cntName"] as? String || city.text == dict["cntName"] as? String{
                
            }else{
             country.text = (dict["cntName"] as? String)?.capitalizedString
            }
        }
        }
        

        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print(filterArrayOfCountries[indexPath.row])
        let dict = filterArrayOfCountries[indexPath.row]
            addressTableview.hidden = true
        var address = ""
        if dict["cityName"] as? String != nil{
        dictOfLocation["city"] = (dict["cityId"] as! String)
          address = dict["cityName"] as! String + ", "
        }
        if dict["stateName"]as? String != nil{
            dictOfLocation["state"] = (dict["stateId"] as! String)

            address += dict["stateName"] as! String + ", "
        }
        if dict["cntName"]as? String != nil{
            dictOfLocation["country"] = (dict["cntId"] as! String)
            address += dict["cntName"] as! String
        }
        
        addressTextField.text = address.capitalizedString
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filterArrayOfCountries.count > 10 {
            return 7
        }
        return filterArrayOfCountries.count
    }
    
}
