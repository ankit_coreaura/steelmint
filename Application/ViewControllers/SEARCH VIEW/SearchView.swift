//
//  SearchViewController.swift
//  Application
//
//  Created by Mohit Gaur on 1/20/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//

import UIKit



 @objc protocol searchDelegate{
 //func didFisnishSelectingRow(row: Int);
   optional func textForSearchButtonClicked(text: String);
   optional func textForSearch(text: String);
    func searchDidBegin(serachBar:UISearchBar)

    func cancelCallBack(flag: Bool);
}


class SearchView: UIView,UISearchBarDelegate {

	var delegate: searchDelegate?
	@IBOutlet var searchBar: UISearchBar?
	
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup(frame)
        self.frame = frame
        searchBar?.becomeFirstResponder()
        searchBar?.delegate = self
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    func xibSetup(frame: CGRect) {
        addSubview(loadViewFromNib(frame))
    }
    
    
    func loadViewFromNib(frame: CGRect) -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "SearchView", bundle: bundle)
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        view.frame = frame
       
        return view
    }


	// Mark: SearchBar Delegate
	// called when keyboard search button pressed
	func searchBarSearchButtonClicked(searchBar: UISearchBar)
        
	{
		if searchBar.text != nil{
              self.delegate?.textForSearchButtonClicked!(searchBar.text!)
			}
	}
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
      
            self.delegate?.searchDidBegin(searchBar)
    }
	// called when cancel button pressed
	func searchBarCancelButtonClicked(searchBar: UISearchBar)
	{
		self.delegate?.cancelCallBack(true)
		self.removeFromSuperview()
	}

    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text != nil && searchBar.text?.characters.count > 2 {
            self.delegate?.textForSearch!(searchBar.text!)
        }
    }
}
