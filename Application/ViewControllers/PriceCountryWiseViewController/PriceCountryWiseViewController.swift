//
//  PriceCountryWiseViewController.swift
//  Application
//
//  Created by Ankit Nandal on 05/11/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit

class PriceCountryWiseViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var tableView:UITableView!
    
    @IBOutlet weak var countrySelectionBtn:UIButton!
    var vaiPushCategory:String?
    var vaiPushTab:String?
    var jsonObj:NSDictionary?

    var indicator:UIActivityIndicatorView?
    var tempview:UIView?
    var arrayOfPrices:NSArray?
    var priceCountryDict:NSDictionary?
    var lastCategoryButtonPressed = ""
    var lastButtonPressed :Int?=2
    var tabOption = NSArray()
    var priceDetailData = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let path = NSBundle.mainBundle().pathForResource("countries", ofType: "json") {
            do {
                let data = try NSData(contentsOfURL: NSURL(fileURLWithPath: path), options: NSDataReadingOptions.DataReadingMappedIfSafe)
                jsonObj = parsedJsonFrom(data)
                
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        } else {
            print("Invalid filename/path.")
        }
        
        
        if let priceCountryDictExists = priceCountryDict{
            priceDetailData = priceCountryDictExists
        }
        if vaiPushTab != nil{tabOption = NSArray(object: vaiPushTab!)}
        
        setupScrollview()
        setupContents()
  
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK: INITIAL SETUPS
    
    func setupContents(){
        if vaiPushCategory == nil{
            titleLabel.text = (priceDetailData.valueForKey("sub_category") as? String)?.uppercaseString
            
            
            
            countrySelectionBtn.setTitle((priceCountryDict!["top_flag"] as? String)?.capitalizedString, forState: .Normal)
            
            
            if countrySelectionBtn.currentTitle?.characters.count>2{
                if countrySelectionBtn.currentTitle?.lowercaseString == "global" {
                 countrySelectionBtn.setTitle("Global".capitalizedString, forState: .Normal)
                }
                setFlag()
            }
            if tabOption.count > 0 {
              //  getDateFromTimestamp(timeStamp: String?, dateFormat: String)
                if  let _ = NSNumberFormatter().numberFromString(tabOption[0] as! String){
                    self.scrollView.subviews.forEach({$0.hidden = true})
                }
                
                lastCategoryButtonPressed = tabOption[0] as! String
                getContentsFromDB()
                getPrice(lastCategoryButtonPressed)
                
            }
   
        }else{
            titleLabel.text = (vaiPushCategory)?.uppercaseString
            countrySelectionBtn.setTitle("INDIA", forState: .Normal)
            if countrySelectionBtn.currentTitle?.characters.count>2{
                setFlag()
                pushApiHit()
            }
        }
        titleLabel.text = titleLabel.text?.stringByReplacingOccurrencesOfString("_", withString: " ")
        
    }
    func setFlag(){
        
        
        if let countryCode = priceCountryDict!["top_flag"]{
            
            if countryCode.lowercaseString == "global"{
                
                flagImageView.image = UIImage(named: "globe.png")
                
            }else{

            
            let predicate = NSPredicate(format: "name = [C] %@", argumentArray: [countryCode])
            if let countMap = jsonObj?["data#"] as? NSArray{
                let arrayOfMappedCountries = countMap.filteredArrayUsingPredicate(predicate)
                if arrayOfMappedCountries.count > 0{
                    if let code =  arrayOfMappedCountries[0]["code"] as? String{
                        if code.characters.count == 2 {
                            let countryBundle = "CountryPicker.bundle/" + code
                            flagImageView.image = UIImage(named: countryBundle)
                        }
                    }
                    
                    
                }
            }
            }
            
        }else{
            flagImageView.image = UIImage()
        }
     
    }
   
    func getContentsFromDB()
    {
        arrayOfPrices = nil
        guard let offlineData = DatabaseManager.sharedInstance.getAllPriceTabWise(lastCategoryButtonPressed) else{
            if indicator?.isAnimating() == false{
                showNativeActivityIndicator()
            }
            reloadTableView()
            return
        
        
        }
        
        if vaiPushCategory != nil{
          let predicate2 = NSPredicate(format: "category == [C] %@ ", argumentArray:[vaiPushCategory!])
            arrayOfPrices = offlineData.filter { predicate2.evaluateWithObject($0) }

        }else{
        
            let predicate2 = NSPredicate(format: "category == [C] %@ AND sub_category == [C] %@ ", argumentArray:[priceDetailData["category"] as! String,titleLabel.text!])
            
            arrayOfPrices = offlineData.filter { predicate2.evaluateWithObject($0) }
        if arrayOfPrices?.count == 0{
            if indicator?.isAnimating() == false{
                showNativeActivityIndicator()
            }
        }
        }
            reloadTableView()
    }
    
    func setupScrollview(){
        AppCommonFunctions.sharedInstance.buttonsetup(tabOption,fontSize: 20, scrollView: scrollView, refrence: self)
    }
    
    @IBAction func onClickOfLeftBarButton(sender:UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    // MARK: TABLEVIEW DELEGATES METHODS
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let numberOfRows = arrayOfPrices else{return 0}
        return numberOfRows.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let  cell = tableView.dequeueReusableCellWithIdentifier("c2", forIndexPath:indexPath) as! PriceTableViewCell

            guard let pricedDataDict = arrayOfPrices?[indexPath.row] as? NSDictionary else{return cell}
            
        
        cell.categoryName.text = (pricedDataDict["sub_category"] as? String)?.capitalizedString
        
        
        if let assesment =  pricedDataDict.valueForKey("assesment") as? String{
            let dateString = pricedDataDict.valueForKey("date") as? String
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let date = dateFormatter.dateFromString(dateString!)
            
            switch assesment{
                
            case "w","W":
                dateFormatter.dateFormat = "dd MMM"
                if date != nil{
                    let dateStr = dateFormatter.stringFromDate(date!)
                    cell.datendeliveryLabel.text = dateStr
                }
            case "d","D":
                dateFormatter.dateFormat = "dd MMM, HH:mm"
                if date != nil{
                    let dateStr = dateFormatter.stringFromDate(date!)
                    cell.datendeliveryLabel.text = dateStr
                }
            case "y","Y":
                dateFormatter.dateFormat = "MMM yyyy"
                if date != nil{
                    let dateStr = dateFormatter.stringFromDate(date!)
                    cell.datendeliveryLabel.text = dateStr
                }
            default :
                cell.datendeliveryLabel.text = pricedDataDict.valueForKey("date") as? String
            }
            
        }else{
            cell.datendeliveryLabel.text = pricedDataDict.valueForKey("date") as? String
        }

        
        
        cell.deliveryLabel.text = (pricedDataDict.valueForKey("delivery") as? String)?.capitalizedString
        cell.gradeLAbel.text = (pricedDataDict["grade"] as? String)?.capitalizedString
        cell.sizeLabel.text = (pricedDataDict["size"] as? String)?.capitalizedString
        
        cell.priceOutlet.setTitle(pricedDataDict["price"] as? String , forState: .Normal)
        cell.changeLabel.text = pricedDataDict["change"] as? String
        cell.changeLabel.text = cell.changeLabel.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        if cell.changeLabel.text!.containsString("+"){
            cell.changeLabel.textColor = UIColor(red: 0, green: 178.0/255.0, blue: 0, alpha: 1.0)
        }else if cell.changeLabel.text == "0"{
            cell.changeLabel.textColor = UIColor.blackColor()
            
        }else{
            cell.changeLabel.textColor = UIColor.redColor()
        }
        
        
        if let countryCode = pricedDataDict["country"] as? String{
            if countryCode.lowercaseString == "global"{
                
                cell.flagImageview.image = UIImage(named: "globe.png")
                
            }else{
            
            let predicate = NSPredicate(format: "name = [C] %@", argumentArray: [countryCode])
            if let countMap = jsonObj?["data#"] as? NSArray{
                let arrayOfMappedCountries = countMap.filteredArrayUsingPredicate(predicate)
                if arrayOfMappedCountries.count > 0{
                    if let code =  arrayOfMappedCountries[0]["code"] as? String{
                        if code.characters.count == 2 {
                            let countryBundle = "CountryPicker.bundle/" + code
                            cell.flagImageview.image = UIImage(named: countryBundle)
                        }
                    }
                    
                    
                }
            }
                }
            
        }else{
            cell.flagImageview.image = UIImage()
        }
        return cell
        
    }
    
    
    
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        guard let pricedDataDict = arrayOfPrices?[indexPath.row] as? NSDictionary else{return}
        
        let viewDetailsInstance =
        self.storyboard?.instantiateViewControllerWithIdentifier("PriceDetailViewController") as! PriceDetailViewController
        viewDetailsInstance.viaOption = true
        viewDetailsInstance.priceDetailDict = pricedDataDict
        viewDetailsInstance.hidesBottomBarWhenPushed = true
        
        self.navigationController?.pushViewController(viewDetailsInstance, animated: true)
    }
    // MARK:  CATEGORIES CLICKED
    
    
    func butonIndexed(sender:UIButton){
        
        var offsetX = sender.frame.origin.x+sender.frame.size.width/2 -  GlobalConstants.SCREEN_WIDTH/2
        if offsetX > scrollView.contentSize.width -  GlobalConstants.SCREEN_WIDTH {
            offsetX = scrollView.contentSize.width -  GlobalConstants.SCREEN_WIDTH
        }
        if offsetX < 0 {
            offsetX = 0
        }
        scrollView.setContentOffset(CGPointMake(offsetX, 0), animated: true)
        if let buttonPressed = lastButtonPressed{
            (scrollView.viewWithTag(buttonPressed) as? UIButton)?.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
            (scrollView.viewWithTag(buttonPressed) as? UIButton)?.titleLabel?.font=UIFont.systemFontOfSize(13)
            (scrollView.viewWithTag(buttonPressed+15) as? UIImageView)?.removeFromSuperview()
        }
        lastButtonPressed = sender.tag
        sender.titleLabel?.font=UIFont.boldSystemFontOfSize(14)
        sender.setTitleColor(UIColor(red: 30.0/255.0, green: 136.0/255.0, blue: 229.0/255.0, alpha: 1), forState: .Normal)
        
        print(sender.currentTitle)
        
        lastCategoryButtonPressed = sender.currentTitle!
        getContentsFromDB()
        NSObject.cancelPreviousPerformRequestsWithTarget(self)
        performSelector("getPrice:", withObject: lastCategoryButtonPressed, afterDelay: 1)
    }
    
    
    func reloadTableView()
    {
        tableView.reloadData()
    }
    
   
    // MARK: Api hit
    func getPrice(category:String){
        
        print("price api hit \n \n *******************************")
        if indicator?.isAnimating() == nil && DatabaseManager.sharedInstance.getAllPriceTabWise(lastCategoryButtonPressed)?.count == nil {
            showNativeActivityIndicator()
        }
        if isInternetConnectivityAvailable(true)==false {
            if indicator?.isAnimating() == true{
                self.stopActivityIndicator()
                
            }
            return
        }
        let information = ["tab":"\(lastCategoryButtonPressed.uppercaseString)","category":(priceDetailData.valueForKey("category") as! String)]
        let webserviceInstance = WebServices()
        webserviceInstance.getPrices(information, completionBlock: {
            (responseData) ->() in
            if self.indicator?.isAnimating() == true{
                self.stopActivityIndicator()
                
            }
            guard let responseValue = responseData?.valueForKey("data#") as? NSArray else{return}
            if responseValue.count>0{
                
                    for object in responseValue
                    {
                        DatabaseManager.sharedInstance.addPriceTabWise(object as! NSDictionary)
                    }
                self.getContentsFromDB()
            }
        })
    }
    func pushApiHit(){
        if vaiPushCategory != nil && vaiPushTab != nil{
            lastCategoryButtonPressed = vaiPushTab!
            let information = ["tab":"\(vaiPushTab!)","category":vaiPushCategory!]
            let webserviceInstance = WebServices()
            webserviceInstance.getPrices(information, completionBlock: {
                (responseData) ->() in
               
                guard let responseValue = responseData?.valueForKey("data#") as? NSArray else{return}
                if responseValue.count>0{
                    
                    for object in responseValue
                    {
                        DatabaseManager.sharedInstance.addPriceTabWise(object as! NSDictionary)
                    }
                    self.getContentsFromDB()
                }
            })
   
        }
    }
    //MARK: ACTIVITY INDICATOR
    func showNativeActivityIndicator(){
        scrollView.userInteractionEnabled = false
        scrollView.layer.opacity = 0.3
        tableView.allowsSelection = false
        if indicator == nil{
            // Setup ActivityIdicator
            indicator = UIActivityIndicatorView  (activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            indicator?.frame = CGRectMake(GlobalConstants.SCREEN_WIDTH/2-10, GlobalConstants.SCREEN_HEIGHT/2-10, 10.0, 10.0)
            tempview = UIView(frame: tableView.frame)
            tempview?.backgroundColor = UIColor.whiteColor()
            
            view.addSubview(tempview!)
            view.addSubview(indicator!)
        }
        indicator?.startAnimating()

       
    }
    
    func stopActivityIndicator() {
        scrollView.userInteractionEnabled = true
        scrollView.layer.opacity = 1.0
        tableView.allowsSelection = true
        indicator?.stopAnimating()
        tempview?.removeFromSuperview()
    }
    
    
  }
