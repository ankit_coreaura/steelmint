//
//  ProfileViewController.swift
//  Application
//
//  Created by Ankit Nandal on 05/11/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

import UIKit
import DKImagePickerController
import SCLAlertView
import SVWebViewController

class ProfileViewController: UIViewController,AddressDelegate,ProductOfInterestDelegate {
    var indicator = UIActivityIndicatorView()
    var tempVIew = UIView()
    var countryDict:NSDictionary?
    @IBOutlet weak var profilePicImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var companyName: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var designationTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var productOfInterest: UITextField!
    @IBOutlet weak var websiteTextField: UITextField!
    @IBOutlet weak var pinCodeTextField: UITextField!
    @IBOutlet weak var addressTextFiels: UITextField!
    @IBOutlet weak var companyType: UITextField!
    var countryStateListDict = NSDictionary()
    var hasProfileValue = false
    var dictOfUpdates = ["company":"", "company_type":"","product_of_interest":[""],"designation":"","department":"","address":"","state":" ","city":"","pin_code":"","website":""]
    @IBOutlet weak var departmentTextField: UITextField!
    
    var userInfo:NSMutableDictionary?
    var userListings = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiHit()
        
    }
    func apiHit() {
        
        
        let s = try! String(contentsOfFile: NSBundle.mainBundle()
            .pathForResource("countryList", ofType: "txt")!,
            encoding: NSUTF8StringEncoding)
        let x = parsedJsonFrom(s.dataUsingEncoding(NSUTF8StringEncoding))
        if x != nil{
                if let dict = x?.valueForKey("data#") as? NSDictionary{
             countryDict = dict
            //CacheManager.sharedInstance.saveObject(dict, identifier: "countryList")
            if let countryCode = self.userInfo?.valueForKey("data#")?.valueForKey("country") as?String{
                if let dict = dict.valueForKey(countryCode) as? NSDictionary{
                self.countryStateListDict = dict
                }
            }
        }
                
        }
       
        
        //if CacheManager.sharedInstance.loadObject("countryList") == nil{
//            let webserviceInstance=WebServices()
//            
//            webserviceInstance.countryList(NSDictionary(), completionBlock: {
//                (responseData) ->() in
//                if responseData != nil{
//                    if let dict = responseData?.valueForKey("data#") as? NSDictionary{
//                       CacheManager.sharedInstance.saveObject(dict, identifier: "countryList")
//                        if let countryCode = self.userInfo?.valueForKey("data#")?.valueForKey("country") as?String{
//                            if let countryList = CacheManager.sharedInstance.loadObject("countryList") as? NSDictionary {
//                                if let dict = countryList.valueForKey(countryCode) as? NSDictionary{
//                                    self.countryStateListDict = dict
//                                }
//                            }
//                        }
//                        
//                        
//                    }
//                
//                }
//            })
        //}
        let webinstance =  WebServices()
        if  (CacheManager.sharedInstance.loadObject("userListings") != nil){
            hasProfileValue = true
            self.userListings = CacheManager.sharedInstance.loadObject("userListings") as! NSDictionary
            setupUserProfile()
        }else{showNativeActivityIndicator()}
        if isInternetConnectivityAvailable(false)==false {
           stopActivityIndicator()
        }
        webinstance.getUserListings(NSDictionary()) { (responseData) -> () in
            if responseData != nil{
                
                guard let response = responseData?["data#"] as? NSDictionary else{return}
                self.userListings = response
                CacheManager.sharedInstance.saveObject(response, identifier: "userListings")
                self.stopActivityIndicator()
                if !self.hasProfileValue {
                    self.setupUserProfile()}
            }
        }
        
    }
    func setupUserProfile() {
        
        profilePicImageView.clipsToBounds=true
        profilePicImageView.layer.cornerRadius=profilePicImageView.frame.size.width/2
        profilePicImageView.layer.masksToBounds=true
        profilePicImageView.layer.borderColor = GlobalConstants.APP_THEME_DARK_BLUE_COLOR.CGColor
        profilePicImageView.layer.borderWidth=3
        userInfo = CacheManager.sharedInstance.loadObject("userinfo") as?NSMutableDictionary
        nameTextField.text = userInfo?.valueForKey("data#")?.valueForKey("name") as?String
        emailTextField.text = userInfo?.valueForKey("data#")?.valueForKey("username") as?String
        mobileTextField.text = userInfo?.valueForKey("data#")?.valueForKey("phoneno") as?String
        if mobileTextField.text == ""{
            mobileTextField.placeholder = "Number not available"
        }
        
      
        
        if let idData = userInfo?.valueForKey("data#")?.valueForKey("designation") as?String{
            let designationPredicate = NSPredicate(format: "id = %@", argumentArray: [idData])
            if let x  = self.userListings.valueForKey("designation")?.filteredArrayUsingPredicate(designationPredicate){
                if x.count>0{
                    designationTextField.text = x[0]["name"] as? String
                }

        if let idData = userInfo?.valueForKey("data#")?.valueForKey("department") as?String{
            let designationPredicate = NSPredicate(format: "id = %@", argumentArray: [idData])
            if let x  = self.userListings.valueForKey("department")?.filteredArrayUsingPredicate(designationPredicate){
                if x.count>0{
                    departmentTextField.text = x[0]["name"] as? String
                }
            }
        }
        if let idData = userInfo?.valueForKey("data#")?.valueForKey("company_type") as?String{
            let designationPredicate = NSPredicate(format: "id = %@", argumentArray: [idData])
            if let x  = self.userListings.valueForKey("company_type")?.filteredArrayUsingPredicate(designationPredicate){
                if x.count>0{
                    companyType.text = x[0]["name"] as? String
                }
            }
        }
        
        
        if NSUserDefaults.standardUserDefaults().valueForKey("userproductType") != nil{
            productOfInterest.text = NSUserDefaults.standardUserDefaults().valueForKey("userproductType") as?String
            
        }
        
        
        if let countryCode = userInfo?.valueForKey("data#")?.valueForKey("country") as?String{
            if let countryList = countryDict {
            if let dict = countryList.valueForKey(countryCode) as? NSDictionary{
                self.countryStateListDict = dict
            }
            }
        }
        
        
        dictOfUpdates["state"] = userInfo?.valueForKey("data#")?.valueForKey("state") as?String
        addressTextFiels.text = userInfo?.valueForKey("data#")?.valueForKey("address") as?String
        cityTextField.text = userInfo?.valueForKey("data#")?.valueForKey("city_name") as?String
        stateTextField.text = userInfo?.valueForKey("data#")?.valueForKey("state_name") as?String
        
        companyName.text = userInfo?.valueForKey("data#")?.valueForKey("company") as?String
        
        pinCodeTextField.text = userInfo?.valueForKey("data#")?.valueForKey("pin_code") as?String
        websiteTextField.text = userInfo?.valueForKey("data#")?.valueForKey("website") as?String
        profilePicImageView.sd_setImageWithURL(NSURL(gravatarEmail: emailTextField.text, size:512), placeholderImage: UIImage(named: "profile_menu.png"))
        
       
                
    }
        }
    }
    @IBAction func onClickOfLeftBarButton(sender:UIButton){
        openLeft()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func logoutBtnEvent(sender: UIButton) {
        //AppCommonFunctions.sharedInstance.logOutUser(self)
    }
    
    @IBAction func saveBtnEvent(sender: UIButton) {
        
        
        
        if nameTextField.text!.isEmpty {
            showAlertController("Steelmint", message: "Please provide your name", reference: self)
        }
            
        else{
            if oldPasswordTextField.text!.characters.count>0 || newPasswordTextField.text!.characters.count>0 || confirmPasswordTextField.text!.characters.count>0{
                
                if oldPasswordTextField.text!.isEmpty || newPasswordTextField.text!.isEmpty || confirmPasswordTextField.text!.isEmpty  {
                    showAlertController("SteelMint", message: "Please complete password details", reference: self)
                    
                }
                else{
                    var isEverythingValidated = true
                    let informationDict = NSMutableDictionary()
                    //Validating password==========================================
                    let oldPassword = NSUserDefaults.standardUserDefaults().valueForKey("password") as? String
                    if oldPassword == oldPasswordTextField.text{
                        
                        if oldPassword != newPasswordTextField.text!{
                            if newPasswordTextField.text! == confirmPasswordTextField.text!{
                                if newPasswordTextField.text!.isValidPassword{
                                    
                                    informationDict.setObject(sessionId(), forKey: "token")
                                    informationDict.setObject(nameTextField.text!, forKey: "name")
                                    
                                    informationDict.setObject(oldPasswordTextField.text!, forKey: "old_password")
                                    informationDict.setObject(newPasswordTextField.text!, forKey: "password")
                                }
                                else{
                                    isEverythingValidated = false
                                    showAlertController("SteelMint",message: "Password must be of 8 letters and alphanumeric", reference: self)
                                }
                                
                            }
                            else{
                                isEverythingValidated = false
                                showAlertController("SteelMint",message: "Password does not match", reference: self)
                            }
                        }
                        else{
                            isEverythingValidated = false
                            showAlertController("SteelMint",message: "Old password and new password are same", reference: self)
                        }
                        
                        
                        if isEverythingValidated{
                            
                            //hit service===================================
                            informationDict.setObject(dictOfUpdates["company_type"]! , forKey: "company_type")
                            informationDict.setObject(dictOfUpdates["product_of_interest"]! , forKey: "product_of_interest")
                            informationDict.setObject(dictOfUpdates["designation"]! , forKey: "designation")
                            informationDict.setObject(dictOfUpdates["department"]! , forKey: "department")
                            informationDict.setObject(dictOfUpdates["state"]! , forKey: "state")
                            informationDict.setObject(dictOfUpdates["city"]! , forKey: "city")
                            if let companyName = addressTextFiels.text{
                                informationDict.setObject(companyName, forKey: "address")
                            }
                            if let companyName = companyName.text{
                                informationDict.setObject(companyName, forKey: "company")
                            }
                            if let companyName = pinCodeTextField.text{
                                informationDict.setObject(companyName, forKey: "pin_code")
                            }
                            if let companyName = websiteTextField.text{
                                informationDict.setObject(companyName, forKey: "website")
                            }
                            
                            let webServicesObject = WebServices()
                            webServicesObject.progresssIndicatorText = "Changing User Info.. "
                            webServicesObject.changePassword(informationDict, completionBlock:
                                {
                                    (responseData) -> () in
                                    
                                    print(responseData)
                                    if let response = responseData?.valueForKey("status#") as? String{
                                        if response == "1"{
                                            // Saving things
                                            
                                            if self.productOfInterest.text?.characters.count > 0{
                                                NSUserDefaults.standardUserDefaults().setValue(self.productOfInterest.text, forKey: "userproductType")
                                            }
                                            
                                            webServicesObject.getUserInfo(["token":sessionId()], completionBlock: {
                                                (responseData) -> () in
                                                if let _ = responseData{
                                                    CacheManager.sharedInstance .saveObject(responseData as! NSDictionary, identifier: "userinfo")
                                                    
                                                    self.slideMenuController()?.changeMainViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("TabController"), close: true)
                                                    
                                                }})
                                            
                                            
                                            NSUserDefaults.standardUserDefaults().setValue(self.newPasswordTextField.text!,forKey: "password")
                                            showAlertDifferentType("User Info successfully changed")
                                            
                                            self.slideMenuController()?.changeMainViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("TabController"), close: true)
                                        }
                                    } //if let response = responseDa
                                    
                            })
                            
                        }
                    }
                    else{
                        showAlertController("SteelMint", message: "Your old password is invalid", reference: self)
                    }
                    
                }
            }
            else{
                let informationDict = NSMutableDictionary()
                informationDict.setObject(sessionId(), forKey: "token")
                informationDict.setObject(nameTextField.text!, forKey: "name")
                informationDict.setObject(dictOfUpdates["company_type"]! , forKey: "company_type")
                informationDict.setObject(dictOfUpdates["product_of_interest"]! , forKey: "product_of_interest")
                informationDict.setObject(dictOfUpdates["designation"]! , forKey: "designation")
                informationDict.setObject(dictOfUpdates["department"]! , forKey: "department")
                informationDict.setObject(dictOfUpdates["state"]! , forKey: "state")
                informationDict.setObject(dictOfUpdates["city"]! , forKey: "city")
                if let companyName = companyName.text{
                    informationDict.setObject(companyName, forKey: "company")
                }
                if let companyName = addressTextFiels.text{
                    informationDict.setObject(companyName, forKey: "address")
                }
                
                if let companyName = pinCodeTextField.text{
                    informationDict.setObject(companyName, forKey: "pin_code")
                }
                if let companyName = websiteTextField.text{
                    informationDict.setObject(companyName, forKey: "website")
                }
                informationDict.setObject("", forKey: "old_password")
                informationDict.setObject("", forKey: "password")
                let webServicesObject = WebServices()
                webServicesObject.progresssIndicatorText = "Updating User Info.. "
                webServicesObject.changePassword(informationDict, completionBlock:
                    {
                        (responseData) -> () in
                        
                        print(responseData)
                        if let response = responseData?.valueForKey("status#") as? String{
                            if response == "1"{
                                
                                
                                if self.productOfInterest.text?.characters.count > 0{
                                    NSUserDefaults.standardUserDefaults().setValue(self.productOfInterest.text, forKey: "userproductType")
                                }
                                webServicesObject.getUserInfo(["token":sessionId()], completionBlock: {
                                    (responseData) -> () in
                                    if let _ = responseData{
                                        CacheManager.sharedInstance .saveObject(responseData as! NSDictionary, identifier: "userinfo")
                                        
                                        self.slideMenuController()?.changeMainViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("TabController"), close: true)
                                        
                                    }})
                                
                                
                                showAlertDifferentType("User Info successfully changed")
                                
                                self.slideMenuController()?.changeMainViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("TabController"), close: true)
                            }
                        } //if let response = responseDa
                        
                })
            }
            
        }
    }
    
    @IBAction func changeProfilePicture(sender: UIButton) {
        self.navigationController?.pushViewController(SVWebViewController(address: "https://en.gravatar.com"), animated: true)
    }
    
    @IBAction func designationEvent(sender: UIButton) {
        
        let selectAddressInstance = storyboard?.instantiateViewControllerWithIdentifier("AddressViewController") as! AddressViewController
        let dict = NSMutableDictionary()
        if let designation = userListings["designation"] as? NSArray{
            for objects  in designation{
                if objects is NSDictionary{
                    dict.setObject(objects["id"] as! String, forKey: objects["name"] as! String)
                }
            }
        }
        
        selectAddressInstance.delegate = self
        selectAddressInstance.tempdict = dict
        selectAddressInstance.via = "d"
        selectAddressInstance.navigationTitleText = "SELECT DESIGNATION"
        
        presentViewController(selectAddressInstance, animated: true, completion: nil)
        
    }
    
    @IBAction func productOfInterestEvent(sender: UIButton) {
        let productOfIntereseInstance = UIStoryboard(name: "Secondary", bundle: nil).instantiateViewControllerWithIdentifier("ProductOfInterestViewController") as! ProductOfInterestViewController
        productOfIntereseInstance.delegate = self
        if let designation = userListings["product_type"] as? NSDictionary{
            productOfIntereseInstance.dictOfValues = designation
            presentViewController(productOfIntereseInstance, animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func departmentEvent(sender: UIButton) {
        let selectAddressInstance = storyboard?.instantiateViewControllerWithIdentifier("AddressViewController") as! AddressViewController
        let dict = NSMutableDictionary()
        if let designation = userListings["department"] as? NSArray{
            for objects  in designation{
                if objects is NSDictionary{
                    dict.setObject(objects["id"] as! String, forKey: objects["name"] as! String)
                }
            }
        }
        
        selectAddressInstance.delegate = self
        selectAddressInstance.tempdict = dict
        selectAddressInstance.via = "dpt"
        selectAddressInstance.navigationTitleText = "SELECT DEPARTMENT"
        
        presentViewController(selectAddressInstance, animated: true, completion: nil)
    }
    @IBAction func companyTypeEvent(sender: UIButton) {
        let selectAddressInstance = storyboard?.instantiateViewControllerWithIdentifier("AddressViewController") as! AddressViewController
        let dict = NSMutableDictionary()
        if let designation = userListings["company_type"] as? NSArray{
            for objects  in designation{
                if objects is NSDictionary{
                    dict.setObject(objects["id"] as! String, forKey: objects["name"] as! String)
                }
            }
        }
        
        selectAddressInstance.delegate = self
        selectAddressInstance.tempdict = dict
        selectAddressInstance.via = "ct"
        selectAddressInstance.navigationTitleText = "SELECT COMPANY TYPE"
        
        presentViewController(selectAddressInstance, animated: true, completion: nil)
    }
    
    @IBAction func stateBtnEvent(sender: UIButton) {
        
        let selectAddressInstance = storyboard?.instantiateViewControllerWithIdentifier("AddressViewController") as! AddressViewController
        let dict = NSMutableDictionary()
        guard let countryList = countryStateListDict["state"] as? NSDictionary else{return}
        let designation = countryList.allValues as NSArray
        for objects  in designation{
            if objects is NSDictionary{
                dict.setObject(objects["id"] as! String, forKey: objects["name"] as! String)
            }
        }
        selectAddressInstance.delegate = self
        selectAddressInstance.tempdict = dict
        selectAddressInstance.via = "state"
        selectAddressInstance.navigationTitleText = "SELECT STATE"
        
        presentViewController(selectAddressInstance, animated: true, completion: nil)
        
    }
    
    
    @IBAction func cityBtnEvent(sender: UIButton) {
        
        let selectAddressInstance = storyboard?.instantiateViewControllerWithIdentifier("AddressViewController") as! AddressViewController
        let dict = NSMutableDictionary()
        guard let countryList = countryStateListDict["state"] as? NSDictionary else{return}
        guard let  stateCode = dictOfUpdates["state"] as? String else{return}
        guard let cityList = countryList[stateCode]?["city"] as? NSArray else{return}
        for objects  in cityList{
            if objects is NSDictionary{
                dict.setObject(objects["id"] as! String, forKey: objects["name"] as! String)
            }
        }
        selectAddressInstance.delegate = self
        selectAddressInstance.tempdict = dict
        selectAddressInstance.via = "city"
        selectAddressInstance.navigationTitleText = "SELECT CITY"
        
        presentViewController(selectAddressInstance, animated: true, completion: nil)
        
        
        
    }
    
    func returnAddressWithCode(value: String, name: String, via: String) {
        switch via{
        case "ct":
            companyType.text = name
            dictOfUpdates["company_type"] = value
        case "dpt":
            departmentTextField.text = name
            dictOfUpdates["department"] = value
        case "p":
            productOfInterest.text = name
            dictOfUpdates["product_of_interest"] = value
        case "d":
            designationTextField.text = name
            dictOfUpdates["designation"] = value
        case "state":
            stateTextField.text = name
            dictOfUpdates["state"] = value
        case "city":
            cityTextField.text = name
            dictOfUpdates["city"] = value
        default:
            break
        }
        
    }
    func returnProductOfInterestArray(values: NSArray) {
        print(values)
        dictOfUpdates["product_of_interest"] = values
        switch values.count{
        case 0:
            productOfInterest.text = "Nothing Selected"
        case 1:
            productOfInterest.text = values[0] as? String
        default:
            productOfInterest.text = "Multuple Values Selected"
            
        }
    }
    
    //MARK: ACTIVITY INDICATOR

    
    func showNativeActivityIndicator(){
        indicator = UIActivityIndicatorView  (activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        indicator.frame = CGRectMake(GlobalConstants.SCREEN_WIDTH/2-10, GlobalConstants.SCREEN_HEIGHT/2-10, 10.0, 10.0)
        tempVIew = UIView(frame: CGRect(x: 0, y: 64, width: GlobalConstants.SCREEN_WIDTH, height: GlobalConstants.SCREEN_HEIGHT))
        tempVIew.backgroundColor = UIColor.whiteColor()
        view.addSubview(tempVIew)
        view.addSubview(indicator)
        indicator.startAnimating()
    }
    
    func stopActivityIndicator() {
        indicator.stopAnimating()
        tempVIew.removeFromSuperview()
    }

}








