//
//  ProductOfInterestViewController.swift
//  Application
//
//  Created by Ankit Nandal on 21/01/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//

import UIKit
protocol ProductOfInterestDelegate{
    func returnProductOfInterestArray(values:NSArray)
}
class ProductOfInterestViewController: UIViewController {
    var delegate:ProductOfInterestDelegate? = nil
    var selectedIndexPath: Int?
    @IBOutlet weak var tableView: UITableView!
    var dictOfValues = NSDictionary()
    var sectionTitles = [String]()
    var selectedCategories = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        if let titles = dictOfValues.allKeys as? [String]{
            sectionTitles = titles
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func dismissViewController(sender: UIButton) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    // MARK: TABLE VIEW DELEGATES
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 44
        
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 1;
    }
    
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier("sectionCell")!
        let  categoryTitle = (cell.viewWithTag(4020) as! UILabel)
        categoryTitle.text = sectionTitles[section]
        
        //add button on header
        let button   = UIButton(type: UIButtonType.System) as UIButton
        button.frame = cell.bounds
        button.tag = section
        button.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
        cell.addSubview(button)
        return cell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 35
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return sectionTitles.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let ip = section
        if selectedIndexPath != nil {
            if ip == selectedIndexPath!
            {
                if let children = dictOfValues[sectionTitles[section]] as? NSArray
                {
                    return children.count
                }else
                {
                    return 0
                }
            }
            else
            {
                return 0
            }
        }
        else
        {
            return 0
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let  cell = tableView.dequeueReusableCellWithIdentifier("childCell", forIndexPath: indexPath)
        let  categoryTitle = (cell.viewWithTag(4020) as! UILabel)
        guard let children = dictOfValues[sectionTitles[selectedIndexPath!]] as? NSArray else{return cell}
        
        categoryTitle.text = children[indexPath.row] as? String
        if selectedCategories.containsObject(categoryTitle.text!){
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        }else{
            cell.accessoryType = UITableViewCellAccessoryType.None
   
        }
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let indexPath = tableView.indexPathForSelectedRow
        if let currentCell = tableView.cellForRowAtIndexPath(indexPath!){
            if currentCell.accessoryType != UITableViewCellAccessoryType.Checkmark{
                currentCell.accessoryType = UITableViewCellAccessoryType.Checkmark
                let  categoryTitle = (currentCell.viewWithTag(4020) as! UILabel)
                selectedCategories.addObject(categoryTitle.text!)
            }else{
                currentCell.accessoryType = UITableViewCellAccessoryType.None
                let  categoryTitle = (currentCell.viewWithTag(4020) as! UILabel)
                selectedCategories.removeObject(categoryTitle.text!)
            }
        }
        
    }
    
    func buttonAction(sender:UIButton)
    {
        switch selectedIndexPath {
        case nil:
            selectedIndexPath = sender.tag
        default:
            if selectedIndexPath! == sender.tag
            {
                selectedIndexPath = nil
            } else
            {
                selectedIndexPath = sender.tag
            }
        }
        
        self.tableView.reloadData()
    }
    
    
    @IBAction func saveArryOFInterset(sender: UIButton) {
        delegate?.returnProductOfInterestArray(selectedCategories)
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
}
