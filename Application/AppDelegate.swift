///
//  AppDelegate.swift
//  Application
//
//  Created by Alok Singh on 25/06/15.
//  Copyright (c) 2015 Alok Singh. All rights reserved.
//
//testing GITHUB

import UIKit
import CoreData
import Parse
//import Bolts
//import Google
import ZDCChat


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate{
    var redirectUrlCCAvenue:String?
    var subscriptionFlag:Bool?

    var window: UIWindow?
    var gcmSenderID: String?
    var registrationOptions = [String: AnyObject]()
    let registrationKey = "onRegistrationCompleted"
    var tforPortraitFforLandscape = true
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        ZDCChat.configure { ( defaults: ZDCConfig! ) -> Void in
            defaults.accountKey="rYVPR5goWOiXootBVvgOj0n1mnt6HzmT";
        }
        
    MKStoreKit.sharedKit().startProductRequest()
        NSNotificationCenter.defaultCenter().addObserverForName(kMKStoreKitProductsAvailableNotification,
            object: nil, queue: NSOperationQueue.mainQueue()) { (note) -> Void in
                print(MKStoreKit.sharedKit().availableProducts)
        }
        
        NSNotificationCenter.defaultCenter().addObserverForName(kMKStoreKitProductPurchasedNotification,
            object: nil, queue: NSOperationQueue.mainQueue()) { (note) -> Void in
                print ("Purchased product: \(note.object)")
        }
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
//PayPalMobile.initializeWithClientIdsForEnvironments([PayPalEnvironmentSandbox:"AS6BZK_uZ8Goc5Bf6ZAxH-AAE-mLxSkZJEvp0qV-re30dDUPORSUVtlTnntvyu2Tn5W7Z8LP54FePJN1"])
        PayPalMobile.initializeWithClientIdsForEnvironments([PayPalEnvironmentProduction:"ASFpWCUOAjDmgeg_LjpeHVrVdVU765PZX_GAIQS00W6M2_RC6LI8gpf99bmsr1538ZRx1oVE9VdQ3gi8"])
        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]
        let pushNotificationSettings = UIUserNotificationSettings(forTypes: notificationTypes, categories: nil)
        
        application.registerUserNotificationSettings(pushNotificationSettings)
        application.registerForRemoteNotifications()
        
        
        
//        let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
//        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
//        UIApplication.sharedApplication().registerForRemoteNotifications()
        
        
        
    // PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : @"YOUR_CLIENT_ID_FOR_PRODUCTION",
           // PayPalEnvironmentSandbox : @"YOUR_CLIENT_ID_FOR_SANDBOX"}
    
       // setupForPushNotifications()
        Parse.enableLocalDatastore()
        Parse.setApplicationId("nYfW5DijG0OJppSCDJIYz4HHt98NISHXU4cQdVlf",
            clientKey: "f3lR3lM8uf29rRuj041qYe1HmRc5NrxubzUzqjvc")
        PFAnalytics.trackAppOpenedWithLaunchOptions(launchOptions)
        AppCommonFunctions.sharedInstance.prepareForStartUp()
        print("LAUNCH OPTIONS \(launchOptions)")
        if isNotNull(launchOptions){
            if isNotNull(launchOptions?[UIApplicationLaunchOptionsLocalNotificationKey]){
                AppCommonFunctions.sharedInstance.handleNotificationWhenTappedToView(launchOptions?[UIApplicationLaunchOptionsLocalNotificationKey] as? NSDictionary)
            }
        }
        return true
    }
    
    
    func applicationWillResignActive(application: UIApplication) {
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        //GCMService.sharedInstance().disconnect()
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        //connectToGCMServer()
    }
    
    func applicationWillTerminate(application: UIApplication) {
        self.saveContext()
    }
    
    func application(app: UIApplication, openURL url: NSURL, options: [String : AnyObject]) -> Bool {
        return true;
    }
    // MARK: PUSH NOTIFICATION
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
                
        if application.applicationState == UIApplicationState.Active{
            
        }else{
            AppCommonFunctions.sharedInstance.prepareForScreensToShow()
            let nav : UINavigationController = window?.rootViewController as! UINavigationController
            
            if let type = userInfo["type"] as? String{
                switch type{
                case "transactional" :
                    if let vc =  userInfo["category"] as? String{
                        switch vc{
                        case "news":
                            
                            let news =  (((nav.viewControllers[0] as! SlideMenuController).mainViewController as! TabController).viewControllers![0] as! UINavigationController).viewControllers[0] as! NewsViewController
                            news.viaPushId =  userInfo["data"]?["id"] as? String
                            news.pushNotificationManagement()
                            
                            //news pushed
                        case "price":
                             ((nav.viewControllers[0] as! SlideMenuController).mainViewController as! TabController).selectedIndex = 1
                            let price =  (((nav.viewControllers[0] as! SlideMenuController).mainViewController as! TabController).viewControllers![1] as! UINavigationController).viewControllers[0] as! PriceViewController
 
                        price.pushNotificationManagement(userInfo["data"]?["cat"] as? String,viaPushTab:userInfo["data"]?["tab"] as? String)
                            // price pushed
                        case "tender":
                            ((nav.viewControllers[0] as! SlideMenuController).mainViewController as! TabController).selectedIndex = 2
                            let tenders =  (((nav.viewControllers[0] as! SlideMenuController).mainViewController as! TabController).viewControllers![2] as! UINavigationController).viewControllers[0] as! TenderViewController
                            tenders.viaPushId =  userInfo["data"]?["id"] as? String
                            tenders.pushNotificationManagement()
                            
                            // tenders pushed
                        case "plantbee":
                             ((nav.viewControllers[0] as! SlideMenuController).mainViewController as! TabController).selectedIndex = 3
                            let plantbee =  (((nav.viewControllers[0] as! SlideMenuController).mainViewController as! TabController).viewControllers![3] as! UINavigationController).viewControllers[0] as! PlantbeeViewController
                            plantbee.viaPushId =  userInfo["data"]?["id"] as? String
                             plantbee.pushDIct = userInfo["data"] as? NSDictionary
                            plantbee.pushNotificationManagement()
                            
                            // plantbee pushed
                        case "event":
                            ((nav.viewControllers[0] as! SlideMenuController).mainViewController as! TabController).selectedIndex = 4
                            let events =  (((nav.viewControllers[0] as! SlideMenuController).mainViewController as! TabController).viewControllers![4] as! UINavigationController).viewControllers[0] as! EventViewController
                            events.viaPushToScreen =  userInfo["data"]?["show"] as? String
                            events.viaPushId =  userInfo["data"]?["id"] as? String
                            events.pushNotificationManagement()
                            
                            
                            
                            //                        (nav.viewControllers[0] as! SlideMenuController).mainViewController?.navigationController?.pushViewController(events, animated: true)
                            // events pushed
                            
                            
                        default:
                            break
                        }
                    }
                case "promotional" :
                    
                    if let vc =  userInfo["category"] as? String{
                        switch vc{
                        case "news":
                            
                            ((nav.viewControllers[0] as! SlideMenuController).mainViewController as! TabController).selectedIndex = 0
                            //news pushed
                        case "price":
                            
                            ((nav.viewControllers[0] as! SlideMenuController).mainViewController as! TabController).selectedIndex = 1
                            // price pushed
                        case "tender":
                            
                            ((nav.viewControllers[0] as! SlideMenuController).mainViewController as! TabController).selectedIndex = 2
                            // tenders pushed
                        case "plantbee":
                            
                            ((nav.viewControllers[0] as! SlideMenuController).mainViewController as! TabController).selectedIndex = 3
                            // plantbee pushed
                        case "event":
                            ((nav.viewControllers[0] as! SlideMenuController).mainViewController as! TabController).selectedIndex = 4
                            
                            // events pushed
                        default:
                            break
                        }
                    }
                    
                    
                default:
                    break
                }
            }
            else if userInfo["chat"] as? String == "true"{
                
            let slideController =  (nav.viewControllers[0] as! SlideMenuController)
                
             slideController.changeMainViewController(UIStoryboard(name: "Secondary", bundle: nil).instantiateViewControllerWithIdentifier("ChatListingsViewController"), close: true)
                let chatController =  slideController.mainViewController as! ChatListingsViewController
                chatController.emailViaPush = userInfo["emailId"] as? String
                chatController.viaPush = true

            }
  
        }

    }
    
    
//    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
//       
//    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        print("DEVICE TOKEN = \(deviceToken)")
        var token = NSString(format: "%@", deviceToken)
        token = token.stringByReplacingOccurrencesOfString("<", withString: "")
        token = token.stringByReplacingOccurrencesOfString(">", withString: "")
        token = token.stringByReplacingOccurrencesOfString(" ", withString: "")
        NSUserDefaults.standardUserDefaults().setValue(token, forKey: "deviceToken")
           
        let webservicesInstance = WebServices()
        let information = NSDictionary(objects: ["ios",token,sessionId()], forKeys: ["device","device_id","token"])
        webservicesInstance.setPushNotification(information) { (responseData) -> () in
            if responseData != nil{
                if let status = responseData?["status#"] as? String{
                    if status == "1"{
                        NSUserDefaults.standardUserDefaults().setValue("notifications registered", forKey: "notificationFlag")
                        
                    }
                }
            }
            
        }
        
        
        
        
        let installation = PFInstallation.currentInstallation()
        installation.setDeviceTokenFromData(deviceToken)
        installation.channels = ["global"]
        installation.saveInBackground()
        
        // send token to server
        
        
    }
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print(error)
    }
    
   
    
    // MARK : COREDATA STACK
    
    lazy var applicationDocumentsDirectory: NSURL = {
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        print("path is=====\(urls[urls.count-1])")
        
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = NSBundle.mainBundle().URLForResource("Application", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("Application.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                
                //               window?.rootViewController = UIStoryboard(name: "Secondary", bundle: nil).instantiateViewControllerWithIdentifier("ABORT")
                abort()
            }
        }
    }
    
    //MARK: - GCM and APNS setup and delegate methods
    
    
//    func setupForPushNotifications(){
//        var configureError:NSError?
//        GGLContext.sharedInstance().configureWithError(&configureError)
//        assert(configureError == nil, "Error configuring Google services: \(configureError)")
//        gcmSenderID = GGLContext.sharedInstance().configuration.gcmSenderID
//        
//        UIApplication.sharedApplication().registerUserNotificationSettings(UIUserNotificationSettings(forTypes:[.Alert,.Sound,.Badge], categories: nil))
//        UIApplication.sharedApplication().registerForRemoteNotifications()
//        
//        let gcmConfig = GCMConfig.defaultConfig()
//        gcmConfig.receiverDelegate = self
//        GCMService.sharedInstance().startWithConfig(gcmConfig)
//    }
//    
//    func connectToGCMServer(){
//        GCMService.sharedInstance().connectWithHandler({
//            (NSError error) -> Void in
//            if error != nil {
//                print("Could not connect to GCM: \(error.localizedDescription)")
//            } else {
//                print("Connected to GCM")
//            }
//        })
//    }
//    
//    func setDeviceTokenOnGCM(deviceToken:NSData){
//        let instanceIDConfig = GGLInstanceIDConfig.defaultConfig()
//        instanceIDConfig.delegate = self
//        GGLInstanceID.sharedInstance().startWithConfig(instanceIDConfig)
//        registrationOptions = [kGGLInstanceIDRegisterAPNSOption:deviceToken,
//            kGGLInstanceIDAPNSServerTypeSandboxOption:true]
//        GGLInstanceID.sharedInstance().tokenWithAuthorizedEntity(gcmSenderID,
//            scope: kGGLInstanceIDScopeGCM, options: registrationOptions, handler: registrationHandler)
//    }
//    
//    func registrationHandler(registrationToken: String!, error: NSError!) {
//        if (registrationToken != nil) {
//            setDeviceToken(registrationToken)
//            let userInfo = ["registrationToken": registrationToken]
//            AppCommonFunctions.sharedInstance.updateGCMTokenOnServer()
//            NSNotificationCenter.defaultCenter().postNotificationName(
//                self.registrationKey, object: nil, userInfo: userInfo)
//        } else {
//            print("Registration to GCM failed with error: \(error.localizedDescription)")
//            setDeviceToken("PUSH+NOTIFICATION+DEVICE+TOKEN+PLACEHOLDER")
//            let userInfo = ["error": error.localizedDescription]
//            NSNotificationCenter.defaultCenter().postNotificationName(
//                self.registrationKey, object: nil, userInfo: userInfo)
//        }
//    }
//    func onTokenRefresh() {
//        GGLInstanceID.sharedInstance().tokenWithAuthorizedEntity(gcmSenderID,
//            scope: kGGLInstanceIDScopeGCM, options: registrationOptions, handler: registrationHandler)
//    }
//    func willSendDataMessageWithID(messageID: String!, error: NSError!) {
//        if (error != nil) {
//        } else {
//        }
//    }
//    
//    func didSendDataMessageWithID(messageID: String!) {
//    }
//    
//    func didDeleteMessagesOnServer() {
//    }
//
    func application(application: UIApplication, supportedInterfaceOrientationsForWindow window: UIWindow?) -> UIInterfaceOrientationMask {
        if tforPortraitFforLandscape == false {
                return UIInterfaceOrientationMask.LandscapeRight;
        } else {
            return UIInterfaceOrientationMask.Portrait;
        }
    }

}

