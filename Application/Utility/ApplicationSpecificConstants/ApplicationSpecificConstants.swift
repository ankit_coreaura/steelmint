//
//  ApplicationSpecificConstants.h
//  Application
//
//  Created by Alok Singh on 25/06/15.
//  Copyright (c) 2015 Alok Singh. All rights reserved.
//
//
import UIKit

struct GlobalConstants {

static let SCREEN_WIDTH = UIScreen.mainScreen().bounds.size.width
static let SCREEN_HEIGHT = UIScreen.mainScreen().bounds.size.height


static let MINIMUM_LENGTH_LIMIT_USERNAME = 1
static let MAXIMUM_LENGTH_LIMIT_USERNAME = 32

static let MINIMUM_LENGTH_LIMIT_FIRST_NAME = 0
static let MAXIMUM_LENGTH_LIMIT_FIRST_NAME = 64

static let MINIMUM_LENGTH_LIMIT_LAST_NAME = 0
static let MAXIMUM_LENGTH_LIMIT_LAST_NAME = 64

static let MINIMUM_LENGTH_LIMIT_PASSWORD = 6
static let MAXIMUM_LENGTH_LIMIT_PASSWORD = 20

static let MINIMUM_LENGTH_LIMIT_MOBILE_NUMBER = 7
static let MAXIMUM_LENGTH_LIMIT_MOBILE_NUMBER = 14

static let MINIMUM_LENGTH_LIMIT_EMQIL = 7
static let MAXIMUM_LENGTH_LIMIT_EMAIL = 64

// message titles and descriptions

static let MESSAGE_TITLE___FOR_NETWORK_NOT_REACHABILITY = "Oops!"
static let MESSAGE_TEXT___FOR_NETWORK_NOT_REACHABILITY = "No internet connection detected."

static let MESSAGE_TITLE___FOR_SERVER_NOT_REACHABILITY = "Oops!"
static let MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY = "There is a network error. Please try again!"
static let CREDENTIALS_ERROR = "Either your email or password is incorrect"
static let UNAUTHORISED_ACCESS = "Request failed: forbidden (403)"
static let UNAUTHORISED_ACCESS_2 = "Request failed: unacceptable (406)"
static let UNAUTHORISED_ACCESS_2_RESPONSE = "Either your email or phone number already exists"

static let ALREADY_LOGGEDIN_ERROR = "You are already logged in from some other device. Please logout first ."
static let SERVER_ERROR = "Request failed: internal server error (500)"
static let SERVER_ERROR_SHOW = "There is some internal server error. Please try again"

// font used in application
static let FONT_BOLD = "OpenSans-Bold"
static let FONT_SEMI_BOLD = "OpenSans-Semibold"
static let FONT_REGULAR = "OpenSans-Light"

static let APPDELEGATE = UIApplication.sharedApplication().delegate as! AppDelegate

static let APP_THEME_DARK_BLUE_COLOR = UIColorFromRGB(0x0D4779)
static let APP_THEME_BLUE_COLOR = UIColorFromRGB(0x1e88e5)
static let APP_NAME = "SteelMint"
static let APP_THEME_LIGHTGREY_COLOR = UIColorFromRGB(0xEEEEEE)

static let APP_THEME_DARK_BLUE_COLOR_UINT  : UInt = 0x0D4779
static let APP_THEME_BLUE_COLOR_UINT : UInt = 0x0D4779

// parse

static let PF_USER_USERNAME_CUSTOMER_EXECUTIVE	= "info@steelmint.com"				//	String


/* Installation */
static let PF_INSTALLATION_USER				= "user"					//	Pointer to User Class

/* User */
static let PF_USER_CLASS_NAME					= "_User"                   //	Class name
static let PF_USER_OBJECTID					= "objectId"				//	String
static let PF_USER_USERNAME					= "username"				//	String
static let PF_USER_PASSWORD					= "password"				//	String
static let PF_USER_EMAIL						= "email"                   //	String

/* Chat */
static let PF_CHAT_CLASS_NAME					= "Chat"					//	Class name
static let PF_CHAT_USER						= "user"					//	Pointer to User Class
static let PF_CHAT_GROUPID						= "groupId"                 //	String
static let PF_CHAT_TEXT						= "text"					//	String
static let PF_CHAT_PICTURE						= "picture"                 //	File
static let PF_CHAT_VIDEO						= "video"                   //	File
static let PF_CHAT_CREATEDAT					= "createdAt"               //	Date

/* Groups */
static let PF_GROUPS_CLASS_NAME				= "Groups"                  //	Class name
static let PF_GROUPS_NAME                      = "name"					//	String

/* Messages*/
static let PF_MESSAGES_CLASS_NAME				= "Messages"				//	Class name
static let PF_MESSAGES_USER					= "user"					//	Pointer to User Class
static let PF_MESSAGES_GROUPID					= "groupId"                 //	String
static let PF_MESSAGES_DESCRIPTION				= "description"             //	String
static let PF_MESSAGES_LASTUSER				= "lastUser"				//	Pointer to User Class
static let PF_MESSAGES_LASTMESSAGE				= "lastMessage"             //	String
static let PF_MESSAGES_COUNTER					= "counter"                 //	Number
static let PF_MESSAGES_UPDATEDACTION			= "updatedAction"           //	Date

/* Notification */
static let NOTIFICATION_APP_STARTED			= "NCAppStarted"
static let NOTIFICATION_USER_LOGGED_IN			= "NCUserLoggedIn"
static let NOTIFICATION_USER_LOGGED_OUT		= "NCUserLoggedOut"
}

