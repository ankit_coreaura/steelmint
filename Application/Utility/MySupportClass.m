//
//  MySupportClass.m
//  Application
//
//  Created by Ankit Nandal on 15/03/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//

#import "MySupportClass.h"
#import "Steelmint-Swift.h"

@implementation MySupportClass
+ (void) reconizeCardWithCardImage:(UIImage *) image : (CamCardViewController*)refrence
{
    // test for recognize and process
    ISRecogenizeCard *recognizeCard = [[ISRecogenizeCard alloc] init];
    ISSDKAuthorizeStatus authorizeStatus = [recognizeCard constructResourcesWithAppKey:@"N8BMPCWSHrQ2HgYK30J8FQ2A" subAppkey:nil];
    if (authorizeStatus == ISSDKAuthorizeStatusSuccess)
    {
        
        [recognizeCard recognizeCard:image recognizeLanguage:nil needImageProcess:YES recognizeResultHandler:^(ISContactInfo *recognizeResultInfo, NSError *error)
         {
             NSLog(@"Recognize result vcf content:%@, error:%@", recognizeResultInfo, error);
            // refrence.userDict = [NSString stringWithFormat:@"%@\n error: %@", recognizeResultInfo, error];
             
//             [(UILabel*)[refrence.view viewWithTag:55] setText:[NSString stringWithFormat:@"%@\n error: %@", recognizeResultInfo, error]];
             
             NSMutableDictionary *cd = [NSMutableDictionary new];
             if(recognizeResultInfo.firstName != nil){
                 [cd setObject:recognizeResultInfo.firstName forKey:@"fn"];
   
             }
             if(recognizeResultInfo.lastName != nil){
                 [cd setObject:recognizeResultInfo.lastName forKey:@"ln"];
  
             }
             if(recognizeResultInfo.phones != nil){
                 [cd setObject:recognizeResultInfo.phones forKey:@"ph"];
   
             }
             if(recognizeResultInfo.emails != nil){
                 [cd setObject:recognizeResultInfo.emails forKey:@"em"];
  
             }
            // [cd setObject:recognizeResultInfo.addresses forKey:@"ad"];

            
             [[NSNotificationCenter defaultCenter] postNotificationName:@"kCamCardNotification"
                                                                 object:cd];
             //self.textView.text = [NSString stringWithFormat:@"content:\n%@\n error: %@", recognizeResultInfo, error];
             
         }
           imageProcessResultHandler:^(UIImage *cropResultImage, NSError *error)
         {
             NSLog(@"Process result vcf content:%@ , error:%@", cropResultImage, error);
             [(UIImageView*)[refrence.view viewWithTag:909]setImage:cropResultImage];
             
         }];
        
        
        
        
        
    }
    else
    {
           }
}

@end
