//
//  MySupportClass.h
//  Application
//
//  Created by Ankit Nandal on 15/03/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <ISBusinessCardSDK/ISBusinessCardSDK.h>
@class CamCardViewController;

@interface MySupportClass : NSObject
+ (void) reconizeCardWithCardImage:(UIImage *) image : (CamCardViewController*)refrence;
@end
