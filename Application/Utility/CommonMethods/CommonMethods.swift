//
//  CommonMethods.swift
//  Application
//
//  Created by Alok Singh on 25/06/15.
//  Copyright (c) 2015 Alok Singh. All rights reserved.
//

import Foundation
import UIKit
import SwiftSpinner
import CWStatusBarNotification
import CoreLocation
import SCLAlertView
import Reachability

enum PopupMessageType{
    case Success
    case Information
    case Error
}


public func sessionId()->String{
    if let sessionID = NSUserDefaults.standardUserDefaults().valueForKey("sessionId") as? String{
        return sessionID
    }
    return ""
}
public func isUserLoggedIn()->Bool{
    if let _ = NSUserDefaults.standardUserDefaults().valueForKey("sessionId") as? String{
        return true
    }
    else{
        
        let alertView = SCLAlertView()
        alertView.showTitle("SteelMint", subTitle: "Please login first to access all details", style: SCLAlertViewStyle.Success, closeButtonTitle: "Cancel", duration: 20, colorStyle: 0x1e88e5, colorTextButton: 0xEEEEEE)
        return false
        
        
    }
    //return true
}
//MARK: SIZE BASED ON TEXT
public func getSizeBasedOnText(widthOfContainer:CGFloat,heightOfContainer:CGFloat,string:NSString,fontSize:Int)->CGRect{
    let constraint = CGSize(width: widthOfContainer, height: heightOfContainer)
    let size =  string.boundingRectWithSize(constraint, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont.systemFontOfSize(CGFloat(fontSize))], context: nil)
    return size
}



public func documentsDirectory() -> String {
    let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,.UserDomainMask, true)[0]
    return documentsPath
}

public func opensEmail(emailId : NSString) {
    UIApplication.sharedApplication().openURL(NSURL(string: "mailto:\(emailId)")!)
}

public func isInternetConnectivityAvailable (showUserWarningMessage : Bool) -> Bool {
    var isReachable : Bool = true
    let networkReachability:Reachability = Reachability.reachabilityForInternetConnection()
    let networkStatus = networkReachability.currentReachabilityStatus()
    isReachable = (networkStatus != NetworkStatus.NotReachable)
    if isReachable == false && showUserWarningMessage {
        showNotification( GlobalConstants.MESSAGE_TEXT___FOR_NETWORK_NOT_REACHABILITY, showOnNavigation: false, showAsError: false)
    }
    return isReachable
}

public func copyData(sourceDictionary:NSDictionary?,sourceKey:NSString?,destinationDictionary:NSDictionary?,destinationKey:NSString?,methodName:NSString?) {
    if sourceDictionary != nil && sourceKey != nil && destinationDictionary != nil && destinationKey != nil{
        destinationDictionary?.setValue(sourceDictionary?.objectForKey((sourceKey!as? String)!), forKey: (destinationKey as? String)!)
    }else{
        reportMissingParameter(sourceKey!, methodName: methodName!)
    }
}

public func copyData(data:AnyObject?,destinationDictionary:NSDictionary?,destinationKey:NSString?,methodName:NSString?) {
    if data != nil && destinationDictionary != nil && destinationKey != nil{
        destinationDictionary?.setValue(data, forKey: (destinationKey as? String)!)
    }else{
        reportMissingParameter(destinationKey!, methodName: methodName!)
    }
}


public func printErrorMessage (error : NSError? , methodName : NSString?) -> () {
    print("\nERROR MESSAGE :--- \(error?.localizedDescription) ---IN METHOD : \(methodName)\n")
}

public func reportMissingParameter (missingParameter : NSString , methodName : NSString) -> () {
    print("\nMISSING PARAMETER :--- \(missingParameter) ---IN METHOD : \(methodName)\n")
}

public func showNotification (text : NSString , showOnNavigation : Bool , showAsError : Bool) -> () {
    dispatch_async(dispatch_get_main_queue()) { () -> Void in
        let notificationView = CWStatusBarNotification()
        notificationView.notificationAnimationInStyle = CWNotificationAnimationStyle.Top;
        notificationView.notificationAnimationOutStyle = CWNotificationAnimationStyle.Top;
        notificationView.notificationStyle = showOnNavigation ?
            CWNotificationStyle.NavigationBarNotification : CWNotificationStyle.StatusBarNotification;
        notificationView.notificationLabelTextColor = UIColor.whiteColor()
        if showAsError{
            notificationView.notificationLabelBackgroundColor = UIColor.redColor()
        }else{
            notificationView.notificationLabelBackgroundColor =  GlobalConstants.APP_THEME_BLUE_COLOR
        }
        notificationView.displayNotificationWithMessage((text as String).capitalizedString, forDuration: 2)
    }
}

func showPopupAlertMessage (text : NSString , messageType: PopupMessageType) -> () {
    dispatch_async(dispatch_get_main_queue()) { () -> Void in
        
    }
}

public func userCurrentLocation() ->(NSMutableDictionary){
    let connectionManager = CLLocationManager()
    connectionManager.startUpdatingLocation()
    connectionManager.requestWhenInUseAuthorization()
    connectionManager.requestAlwaysAuthorization()
    let lat = connectionManager.location?.coordinate.latitude
    let long = connectionManager.location?.coordinate.longitude
    let locationDictionary = NSMutableDictionary()
    if let latitude=lat,longitude=long{
        locationDictionary.setObject(latitude, forKey: "latitude")
        locationDictionary.setObject(longitude , forKey: "longitude")
        return locationDictionary
    }else{
        return ["latitude":0.000000,"longitude":0.00000]
    }
}
// MARK: ALERTS

public func showAlertController(title:String?,message:String?, reference:UIViewController?){
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
    alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler:nil))
    reference!.presentViewController(alertController, animated: true, completion: nil)
}

public func showAlertDifferentType(message:String?){
    let alertView = SCLAlertView()
    alertView.showTitle("SteelMint", subTitle: message!, style: SCLAlertViewStyle.Success, closeButtonTitle: "Cancel", duration: 50, colorStyle: 0x1e88e5, colorTextButton: 0xEEEEEE)
}
public func showAlertDifferentTypeFailure(message:String?){
    let alertView = SCLAlertView()
    alertView.showTitle("SteelMint", subTitle: message!, style: SCLAlertViewStyle.Error, closeButtonTitle: "Cancel", duration: 50, colorStyle: 0x1e88e5, colorTextButton: 0xEEEEEE)
}
public func showAlertDifferentTypeForChat(message:String?){
    let alertView = SCLAlertView()
    alertView.showTitle("SteelMint", subTitle: message!, style: SCLAlertViewStyle.Error, closeButtonTitle: "Cancel", duration: 50, colorStyle: 0x1e88e5, colorTextButton: 0xEEEEEE)
}
// MARK: JSON PARSING

public func parsedJsonFrom (data : NSData?) -> (NSDictionary?) {
        let parsedData : NSDictionary?
    //let str =  NSString(data: data!,encoding: NSUTF8StringEncoding)
    //getHTMlRenderedString(str as! String).string
   //print("data recieved and parsing starts...")
    do {
                parsedData =  try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary

    
        } catch {
                parsedData = nil;
        }
       // let dataAsString : NSString? = NSString(data: data!,encoding: NSUTF8StringEncoding);
       // print("\n\nRECEIVED DATA BEFORE PARSING IS \n\n\(dataAsString)\n\n\n")

   print("\n\nRECEIVED DATA AFTER PARSING IS \n\n\(parsedData)\n\n\n")

        return parsedData
}
public func parsedJsonFrom2 (data : NSData?) -> (NSDictionary?) {
    let parsedData : NSDictionary?
    print("data recieved and parsing starts...")
    do {
        parsedData =  try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary
        
        
    } catch {
        parsedData = nil;
    }
    // let dataAsString : NSString? = NSString(data: data!,encoding: NSUTF8StringEncoding);
    //        print("\n\nRECEIVED DATA BEFORE PARSING IS \n\n\(dataAsString)\n\n\n")
   // print("\n\nRECEIVED DATA AFTER PARSING IS \n\n\(parsedData)\n\n\n")
    
    return parsedData
}

public func print(str:String){

  Swift.print(str)
}

func setupNavigationBarTitleType1(title:NSString?,viewController:UIViewController?){
    var fontSize = 21 as CGFloat
    if title?.length > 14 {
        fontSize = 16
    }
    viewController!.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(),NSFontAttributeName:UIFont.systemFontOfSize(fontSize)]
    viewController?.title = title as? String
}

public func showActivityIndicator (text : NSString) -> () {
    dispatch_async(dispatch_get_main_queue()) { () -> Void in
        SwiftSpinner.show(text as String)
    }
}

public func hideActivityIndicator () {
    dispatch_async(dispatch_get_main_queue()) { () -> Void in
        SwiftSpinner.hide()
    }
}

public func disableAutoCorrectionsAndTextSuggestionGlobally () {
    struct Static {
        static var token : dispatch_once_t = 0
    }
    dispatch_once(&Static.token) {
        
    }
}

func UIColorFromRGB(rgbValue: UInt) -> UIColor {
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

func addNavigationBarButton(viewController:UIViewController?,image:UIImage?,title:NSString?,isLeft:Bool,observer:UIViewController?){
    let button: UIButton = UIButton()
    button.setImage(image, forState: UIControlState.Normal)
    button.setTitle(title as? String, forState: UIControlState.Normal)
    button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
    button.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Highlighted)
    button.titleLabel?.font = UIFont(name:  GlobalConstants.FONT_REGULAR, size: 16)
    button.frame = CGRectMake(0, 0, 53, 31)
    if isLeft{
        button.addTarget(observer, action: "onClickOfLeftBarButton:", forControlEvents: UIControlEvents.TouchUpInside)
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        viewController!.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }else{
        button.addTarget(observer, action: "onClickOfRightBarButton:", forControlEvents: UIControlEvents.TouchUpInside)
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Right
        viewController!.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    }
    viewController!.navigationController?.setNavigationBarHidden(false,animated:false)
}


func addNavigationBarButton(viewController:UIViewController?,image:UIImage?,title:NSString?,isLeft:Bool){
    let button: UIButton = UIButton()
    button.setImage(image, forState: UIControlState.Normal)
    button.setTitle(title as? String, forState: UIControlState.Normal)
    button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
    button.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Highlighted)
    button.titleLabel?.font = UIFont(name:  GlobalConstants.FONT_REGULAR, size: 16)
    button.frame = CGRectMake(0, 0, 53, 31)
    if isLeft{
        button.addTarget(viewController, action: "onClickOfLeftBarButton:", forControlEvents: UIControlEvents.TouchUpInside)
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        viewController!.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }else{
        button.addTarget(viewController, action: "onClickOfRightBarButton:", forControlEvents: UIControlEvents.TouchUpInside)
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Right
        viewController!.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    }
    viewController!.navigationController?.setNavigationBarHidden(false,animated:false)
}

func addNavigationBarButton(viewController:UIViewController?,title:NSString?,titleColor:UIColor,isLeft:Bool){
    let button: UIButton = UIButton()
    button.setTitle(title as? String, forState: UIControlState.Normal)
    button.setTitleColor(titleColor, forState: UIControlState.Normal)
    button.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Highlighted)
    button.titleLabel?.font = UIFont(name:  GlobalConstants.FONT_REGULAR, size: 16)
    button.frame = CGRectMake(0, 0, 53, 31)
    if isLeft{
        button.addTarget(viewController, action: "onClickOfLeftBarButton:", forControlEvents: UIControlEvents.TouchUpInside)
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        viewController!.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }else{
        button.addTarget(viewController, action: "onClickOfRightBarButton:", forControlEvents: UIControlEvents.TouchUpInside)
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Right
        viewController!.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    }
    viewController!.navigationController?.setNavigationBarHidden(false,animated:false)
}


func isNull(object:AnyObject?)->(Bool){
    if object != nil{
        if object!.isKindOfClass(NSString){
            if object as! String == "null" || object as! String == "<null>" || object as! String == "(null)" || object as! String == "" {
                return true
            }
        }
        else if object!.isKindOfClass(NSNull){
            return true
        }
        return false
    }
    return true
}

func isNotNull(object:AnyObject?)->(Bool){
    return !isNull(object)
}

public func getDateFromTimestamp(timeStamp:String?,dateFormat:String) -> String{
    if let timeStampDowncasted = timeStamp{
        let double = NSNumberFormatter().numberFromString(timeStampDowncasted)?.doubleValue
        if let doubleVAlue = double{
            let date:NSDate = NSDate(timeIntervalSince1970:doubleVAlue)
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = dateFormat
            let stringDate = dateFormatter.stringFromDate(date)
            return stringDate
        }
    }
    
    return ""
}
public func getDateFromTimestamp2(timeStamp:NSNumber?,dateFormat:String) -> String{
    if let timeStampDowncasted = timeStamp{
        let double = timeStampDowncasted.doubleValue
            let date:NSDate = NSDate(timeIntervalSince1970:double)
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = dateFormat
            let stringDate = dateFormatter.stringFromDate(date)
            return stringDate
        
    }
    
    return ""
}

func setDeviceToken(token:String?){
    NSUserDefaults.standardUserDefaults().setObject(token, forKey: "deviceToken")
}

public func getHTMlRenderedString(str:String)->(NSAttributedString){
    do {
        let stringRendered = try NSAttributedString(data: str.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute: NSUTF8StringEncoding], documentAttributes: nil)
        return stringRendered
    } catch {
        print(error)
        return NSMutableAttributedString(string:"")
        
    }
}

public func dateDifference(date1:String,date2:String,format:String){
    let start = "2010-09-01"
    let end = "2010-09-05"
    
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    
    let startDate:NSDate = dateFormatter.dateFromString(start)!
    let endDate:NSDate = dateFormatter.dateFromString(end)!
    
    let cal = NSCalendar.currentCalendar()
    
    
    let unit:NSCalendarUnit = .Month
    
    let components = cal.components(unit, fromDate: startDate, toDate: endDate, options: .MatchStrictly)
    
    
    print(components)
}
public func dateComparisonMethod(endDate:NSDate){
    // Date comparision to compare current date and end date.
    let dateComparisionResult:NSComparisonResult = NSDate().compare(endDate)
    
    if dateComparisionResult == NSComparisonResult.OrderedAscending
    {
        // Current date is smaller than end date.
    }
    else if dateComparisionResult == NSComparisonResult.OrderedDescending
    {
        // Current date is greater than end date.
    }
    else if dateComparisionResult == NSComparisonResult.OrderedSame
    {
        // Current date and end date are same.
    }
    
}
