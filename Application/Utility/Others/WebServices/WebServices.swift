  //
//  WebServices.swift
//  Application
//
//  Created by Alok Kumar Singh on 13/10/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

//MARK: - WebServices : This class handles communication of application with its Server.

import Foundation
import AFNetworking
//MARK: - Url's
struct ServerSupportURLS {
    static let BASE_URL = "http://steelmint.apprick.com/"
    //"http://www.steelmint.temp.coreaura.net/"
    //"http://www.steelmint.com/api/public/"
    
    
    static let BASE_URL_LIVE = "http://www.steelmint.com/api/public/"
    
    static let EXAMPLE_URL = "exampleApi"
    static let REGISTER_USER = "user"

    static let PASSWORD_CHANGE = "user"
    static let FORGET_PASSWORD = "user/forget_password"
    static let LOGIN_URL = "user/refresh_token"
    static let SESSION_ID_URL="user/token"
    static let GET_NEWS = "v2/action/news"
    
    static let GET_NEWS_FILTERS = "v2/action/news/filters"
    static let GET_PLANTBEE_FILTERS = "v1/plantbee/searchfilers"
    static let GET_TENDERS_FILTERS = "v2/action/tenders/filters"
    static let GET_PARTICIPANTS_FILTERS = "v3/action/events/filters/"
    static let GET_PARTICIPANTS_FILTERED_DATA = "v3/action/events/participants/"
    static let INAPP_BEFORE = "v2/action/subscription/beforeplan"
    static let INAPP_AFTER = "v2/action/subscription/afterplan"
    static let PAYPAL_AFTER = "v3/action/events/payment"
    static let PLANTBEE_LEADIMPRESSION = "v1/plantbee/leadimpression/"
    
    static let GET_TENDERS_BID = "v2/action/tenders/enquiry"
    static let GET_TENDERS = "v2/action/tenders"
    static let GET_NEWS_CATEGORIES = "v2/action/news/category"
    static let GET_TENDER_CATEGORIES = "/v2/action/tenders/category"
    static let GET_PRICES_CATEGORIES = "/v3/action/price/category"
    static let SUBSCRIPTION_PLANS = "v1/action/getsubscriptionplan"
    static let GET_EVENTS = "v3/action/events"
    static let GET_PRICES = "v3/action/price"
    static let GET_LOGISTICS = "v2/action/logistics"
    static let GET_FAQs = "v2/action/faqs"
    static let GET_TERMS = "v2/action/termsandconditions"
    static let USER_INFO = "user"
    static let SET_PUSH_NOTIFICATION = "v2/notification/register"
    static let SET_PUSH_NOTIFICATION_STATUS = "v2/notification/status"
    static let RSA_KEY = "http://www.steelmint.com/api/public/v2/payment/ccavenue/getrsa"
    static let EXPRESS_INTEREST = "v1/plantbee/enquiry"
    
    static let USER_LISTINGS = "user/listing"
    static let  COUNTRY_LIST = "user/countrystate"
    static let  CONTACT_US = "v1/action/getcms"
    static let  LOGOUT_URL = "user/logout"
    static let  PLANTBEE_POST_OFFER = "v1/plantbee/createpost"
    static let  PLANTBEE_BUY = "v1/plantbee/plantype/buy"
    static let  PLANTBEE_SELL = "v1/plantbee/plantype/sell"
    static let  PLANTBEE_LEADS_DETAIL = "v1/plantbee/plantype/getleaddetail"
    static let  PLANTBEE_POST_DETAIL = "v1/plantbee/getallposts"
    static let  PLANTBEE_IMAGES = "v1/plantbee/homepageimage"
    
    static let  PLANTBEE_TAGS = "v1/plantbee/getalltags"
    
    static let  PUSH_NOTIFICATION_REGISTRATION = "v1/plantbee/homepageimage"
    static let  EventPriceList =  "v3/action/events/delegate_price/"
    
    
    static let DELEGATE_FORM_FILL = "v3/action/events/delegate_registration/"
    
    //http://www.steelmint.com/api/public/v3/action/events/delegate_price/5
}

//MARK: - Constants
struct Constants {
    static let RESPONSE_CODE_KEY = "status#"
    static let RESPONSE_CODE_SUCCESS_VALUE = "1"
    static let RESPONSE_MESSAGE_KEY = "message#"
    static let RESPONSE_MESSAGE = "message#"
    
}

//MARK: - Response Error Handling Options
enum ResponseErrorOption {
    case DontShowErrorResponseMessage
    case ShowErrorResponseWithUsingNotification
    case ShowErrorResponseWithUsingPopUp
}

//MARK: - Completion block
typealias WSCompletionBlock = (responseData :AnyObject?) ->()
// Generate session id
extension WebServices{
    func generateSessionID(){
        let tokenDict = NSMutableDictionary()
        print("generating new token")
        tokenDict.setObject(NSUserDefaults.standardUserDefaults().valueForKey("token")!, forKey: "refreshToken")
        let webservicesInstance = WebServices()
        webservicesInstance.getSessionId(tokenDict, completionBlock: {
            (responseData) -> () in
            if let _ = responseData{
                NSUserDefaults.standardUserDefaults().setValue(responseData?.valueForKey("data#")?.valueForKey("token"), forKey: "sessionId")
            }
            }
        )
    }
}

//MARK: - Custom methods
extension WebServices {
    func registerUser(information: NSDictionary ,completionBlock: WSCompletionBlock)  {
        
        performPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE+ServerSupportURLS.REGISTER_USER, completionBlock: completionBlock)
    }
    func subscribeUser(information: NSDictionary ,completionBlock: WSCompletionBlock)  {
        
        performPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE+ServerSupportURLS.INAPP_BEFORE, completionBlock: completionBlock)
    }
    func registerForPushNotificationAtServer(information: NSDictionary ,completionBlock: WSCompletionBlock) -> () {
        
        // performJsonPostRequest(bodyData, urlString: ServerSupportURLS.BASE_URL+ServerSupportURLS.PUSH_NOTIFICATION_REGISTRATION, completionBlock: completionBlock)
    }
    
    func forgetPassword(information:NSDictionary,completionBlock:WSCompletionBlock){
        let bodyData = NSMutableDictionary()
        
        copyData(information, sourceKey: "email", destinationDictionary: bodyData, destinationKey: "email", methodName:__FUNCTION__)
        performPostRequest(bodyData, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.FORGET_PASSWORD, completionBlock: completionBlock)
    }
    
    func changePassword(information:NSDictionary,completionBlock:WSCompletionBlock){
        
        performPatchRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.PASSWORD_CHANGE, completionBlock: completionBlock)
    }
    
    func login(information:NSDictionary,completionBlock:WSCompletionBlock){
        let bodyData = NSMutableDictionary()
        copyData(information, sourceKey: "username", destinationDictionary: bodyData, destinationKey: "username", methodName:__FUNCTION__)
        copyData(information, sourceKey: "password", destinationDictionary: bodyData, destinationKey: "password", methodName:__FUNCTION__)
        performPostRequest(bodyData, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.LOGIN_URL, completionBlock: completionBlock)
    }
    
    func getSessionId(information:NSDictionary,completionBlock:WSCompletionBlock){
        let bodyData = NSMutableDictionary()
        copyData(information, sourceKey: "refreshToken", destinationDictionary: bodyData, destinationKey: "refreshToken", methodName:__FUNCTION__)
        performPostRequest(bodyData, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.SESSION_ID_URL, completionBlock: completionBlock)
    }
    
    func getNews(information:NSDictionary,completionBlock:WSCompletionBlock){
        let bodyData = NSMutableDictionary()
        copyData(information, sourceKey: "token", destinationDictionary: bodyData, destinationKey: "token", methodName:__FUNCTION__)
        addCommonInformation(bodyData)
        performPostRequest(bodyData, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.GET_NEWS, completionBlock: completionBlock)
    }
    
    func getParticipantsFilters(information:NSDictionary,eventId: String,completionBlock:WSCompletionBlock){
        let bodyData = NSMutableDictionary()
        performGetRequest(bodyData, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.GET_PARTICIPANTS_FILTERS + eventId, completionBlock: completionBlock)
    }
    
    func getParticipantsFilteredData(information:NSDictionary,eventId: String,completionBlock:WSCompletionBlock){
        performPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.GET_PARTICIPANTS_FILTERED_DATA + eventId, completionBlock: completionBlock)
    }
    
    
    
    func getNewsFilters(information:NSDictionary,completionBlock:WSCompletionBlock){
        let bodyData = NSMutableDictionary()
        performGetRequest(bodyData, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.GET_NEWS_FILTERS, completionBlock: completionBlock)
    }
    func getPlantbeeFilters(information:NSDictionary,completionBlock:WSCompletionBlock){
        performGetRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.GET_PLANTBEE_FILTERS, completionBlock: completionBlock)
    }
    func getTenderFilters(information:NSDictionary,completionBlock:WSCompletionBlock){
        let bodyData = NSMutableDictionary()
        performGetRequest(bodyData, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.GET_TENDERS_FILTERS, completionBlock: completionBlock)
    }
    
    func getNewsCategoryWise(information:NSDictionary,completionBlock:WSCompletionBlock){
        let bodyData = NSMutableDictionary()
        copyData(information, sourceKey: "category", destinationDictionary: bodyData, destinationKey: "category", methodName:__FUNCTION__)
        copyData(information, sourceKey: "postfrom", destinationDictionary: bodyData, destinationKey: "postfrom", methodName:__FUNCTION__)
        copyData(information, sourceKey: "filter", destinationDictionary: bodyData, destinationKey: "filter", methodName:__FUNCTION__)
        performPostRequest(bodyData, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.GET_NEWS, completionBlock: completionBlock)
    }
    
    func getNewsCategories(information:NSDictionary,completionBlock:WSCompletionBlock){
        
        performGetRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.GET_NEWS_CATEGORIES, completionBlock: completionBlock)
        
    }
    
    func getEventPrice(information:NSDictionary?, eventId:String?, completionBlock:WSCompletionBlock){
        
        performGetRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.EventPriceList + eventId!, completionBlock: completionBlock)
        
    }
    
    func getTenderCategories(information:NSDictionary,completionBlock:WSCompletionBlock){
        
        performGetRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.GET_TENDER_CATEGORIES, completionBlock: completionBlock)
        
    }
    
    func getTenderCategoryWise(information:NSDictionary?,completionBlock:WSCompletionBlock){
        
        performPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.GET_TENDERS, completionBlock: completionBlock)
    }
    
    //PAYMENTS
    func inAppBeforePayment(information:NSDictionary?,completionBlock:WSCompletionBlock){
        
        performPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.INAPP_BEFORE, completionBlock: completionBlock)
    }
    func inAppAfterPayment(information:NSDictionary?,completionBlock:WSCompletionBlock){
        
        performPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.INAPP_AFTER, completionBlock: completionBlock)
    }
    
    func paypalAfterPayment(information:NSDictionary?,completionBlock:WSCompletionBlock){
        
        performPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.PAYPAL_AFTER, completionBlock: completionBlock)
    }
    
    //DONE PAYMENTS
    func getEvents(information:NSDictionary,completionBlock:WSCompletionBlock){
        
        performPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.GET_EVENTS, completionBlock: completionBlock)
        
    }
    func getEventsIdWise(information:NSDictionary,id: String,completionBlock:WSCompletionBlock){
        
        performPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.GET_EVENTS + "/\(id)", completionBlock: completionBlock)
        
    }
    func fillDelegateForm(information:NSDictionary?,id: String,formData:NSData,completionBlock:WSCompletionBlock){
        
//        performMultipartPostRequest(nil, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.DELEGATE_FORM_FILL + "\(id)", constructBody: { (AFMultipartFormData) -> Void in
//            
//            AFMultipartFormData.appendPartWithHeaders(nil,body: formData)
//            //AFMultipartFormData.appendPartWithFormData(formData, name: "delegates")
//            }) { (responseData) -> () in
//                
//        }
            performJsonPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.DELEGATE_FORM_FILL + "\(id)", completionBlock: completionBlock)
//        
    }
    //PENDING==
    func getSubscriptionPlans(information:NSDictionary,completionBlock:WSCompletionBlock){
        performPostRequest(information, urlString: ServerSupportURLS.BASE_URL + ServerSupportURLS.SUBSCRIPTION_PLANS, completionBlock: completionBlock)
    }
    
    func getUserInfo(information:NSDictionary,completionBlock:WSCompletionBlock){
        let bodyData = NSMutableDictionary()
        copyData(information, sourceKey: "token", destinationDictionary: bodyData, destinationKey: "token", methodName:__FUNCTION__)
        performGetRequest(bodyData, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.USER_INFO, completionBlock: completionBlock)
    }
    
    func getRsaKey(orderId:String,completionBlock:WSCompletionBlock){
        
        performGetRequest(nil, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.RSA_KEY + orderId, completionBlock: completionBlock)
    }
    
    //new
    func contactUs(information: NSDictionary ,completionBlock: WSCompletionBlock)  {
        let bodyData = NSMutableDictionary()
        copyData(information, sourceKey: "type", destinationDictionary: bodyData, destinationKey: "type", methodName:__FUNCTION__)
        
        copyData(information, sourceKey: "name", destinationDictionary: bodyData, destinationKey: "name", methodName:__FUNCTION__)
        copyData(information, sourceKey: "lastname", destinationDictionary: bodyData, destinationKey: "lastname", methodName:__FUNCTION__)
        copyData(information, sourceKey: "email", destinationDictionary: bodyData, destinationKey: "email", methodName:__FUNCTION__)
        copyData(information, sourceKey: "phoneno", destinationDictionary: bodyData, destinationKey: "phoneno", methodName:__FUNCTION__)
        copyData(information, sourceKey: "interest", destinationDictionary: bodyData, destinationKey: "interest", methodName:__FUNCTION__)
        copyData(information, sourceKey: "address", destinationDictionary: bodyData, destinationKey: "address", methodName:__FUNCTION__)
        performPostRequest(bodyData, urlString: ServerSupportURLS.BASE_URL_LIVE+ServerSupportURLS.CONTACT_US, completionBlock: completionBlock)
    }
    //new
    func feedback(information: NSDictionary ,completionBlock: WSCompletionBlock)  {
        let bodyData = NSMutableDictionary()
        copyData(information, sourceKey: "type", destinationDictionary: bodyData, destinationKey: "type", methodName:__FUNCTION__)
        copyData(information, sourceKey: "name", destinationDictionary: bodyData, destinationKey: "name", methodName:__FUNCTION__)
        copyData(information, sourceKey: "message", destinationDictionary: bodyData, destinationKey: "message", methodName:__FUNCTION__)
        performPostRequest(bodyData, urlString: ServerSupportURLS.BASE_URL_LIVE+ServerSupportURLS.CONTACT_US, completionBlock: completionBlock)
    }
    //new
    func aboutUs(information: NSDictionary ,completionBlock: WSCompletionBlock)  {
        let bodyData = NSMutableDictionary()
        copyData(information, sourceKey: "type", destinationDictionary: bodyData, destinationKey: "type", methodName:__FUNCTION__)
        performPostRequest(bodyData, urlString: ServerSupportURLS.BASE_URL_LIVE+ServerSupportURLS.CONTACT_US, completionBlock: completionBlock)
    }
    func expressInterset(information: NSDictionary ,completionBlock: WSCompletionBlock)  {
        
        performPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE+ServerSupportURLS.EXPRESS_INTEREST, completionBlock: completionBlock)
    }
    
    func logout(information:NSDictionary,completionBlock:WSCompletionBlock){
        
        performPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.LOGOUT_URL, completionBlock: completionBlock)
    }
    
func bidOnTender(information:NSDictionary,completionBlock:WSCompletionBlock){
        
        performPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.GET_TENDERS_BID, completionBlock: completionBlock)
    }
    
    func countryList(information:NSDictionary,completionBlock:WSCompletionBlock){
        let bodyData = NSMutableDictionary()
        performGetRequest(bodyData, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.COUNTRY_LIST , completionBlock: completionBlock)
    }
    func getPriceCategories(information:NSDictionary,completionBlock:WSCompletionBlock){
        performGetRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.GET_PRICES_CATEGORIES, completionBlock: completionBlock)
    }
    func getPrices(information:NSDictionary,completionBlock:WSCompletionBlock){
        performPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.GET_PRICES, completionBlock: completionBlock)
        
    }
    //new
    func getLogistics(information:NSDictionary,completionBlock:WSCompletionBlock){
        performPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.GET_LOGISTICS, completionBlock: completionBlock)
        
    }
    func getPriceDetailById(information:NSDictionary,id:String,completionBlock:WSCompletionBlock){
        
        performPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.GET_PRICES + "/\(id)", completionBlock: completionBlock)
        
    }
    //new
    func FAQs(information:NSDictionary,completionBlock:WSCompletionBlock){
        let bodyData = NSMutableDictionary()
        performGetRequest(bodyData, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.GET_FAQs , completionBlock: completionBlock)
    }
    //new
    func getTermsAndConditions(information:NSDictionary,completionBlock:WSCompletionBlock){
        let bodyData = NSMutableDictionary()
        performGetRequest(bodyData, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.GET_TERMS , completionBlock: completionBlock)
    }
    func getBuyTypePlants(information:NSDictionary,completionBlock:WSCompletionBlock){
        let bodyData = NSMutableDictionary()
        performGetRequest(bodyData, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.PLANTBEE_BUY , completionBlock: completionBlock)
    }
    func getSellTypePlants(information:NSDictionary,completionBlock:WSCompletionBlock){
        let bodyData = NSMutableDictionary()
        performGetRequest(bodyData, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.PLANTBEE_SELL , completionBlock: completionBlock)
    }
    func getPostDetails(information:NSDictionary,completionBlock:WSCompletionBlock){
        
        performPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.PLANTBEE_POST_DETAIL , completionBlock: completionBlock)
    }

    func PostYourOffer(information:NSDictionary,imagesArray:NSMutableArray?,docsArray:NSMutableArray?,completionBlock:WSCompletionBlock){
        
// performPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.PLANTBEE_POST_OFFER , completionBlock: completionBlock)
        
        performMultipartPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE+ServerSupportURLS.PLANTBEE_POST_OFFER, constructBody:
            { (AFMultipartFormData) -> Void in
                var i = 0
                if imagesArray?.count>0{
                 
                    for images in imagesArray!{
                       
                       AFMultipartFormData.appendPartWithFileData(UIImagePNGRepresentation(images as! UIImage)!, name: "images[]", fileName: "imageName\(i).png", mimeType: "image/png")
                        i++
                    }
                    
                }
                if docsArray?.count>0{
                    for images in docsArray!{
                        AFMultipartFormData.appendPartWithFileData(UIImagePNGRepresentation(images as! UIImage)!, name: "docs[]", fileName: "imageName\(i).png", mimeType: "image/png")
                        i++
                    }
                }
               
                
            }, completionBlock: completionBlock)

    }
    func getPlantBeeImages(information:NSDictionary,completionBlock:WSCompletionBlock){
        performGetRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.PLANTBEE_IMAGES , completionBlock: completionBlock)
    }
    func getPlantBeeTags(information:NSDictionary?,completionBlock:WSCompletionBlock){
        performPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.PLANTBEE_TAGS , completionBlock: completionBlock)
    }
    func setLeadImpression(information:NSDictionary?,id:String,completionBlock:WSCompletionBlock){
        performGetRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.PLANTBEE_LEADIMPRESSION + id , completionBlock: completionBlock)
    }
    func getUserListings(information:NSDictionary,completionBlock:WSCompletionBlock){
        performGetRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.USER_LISTINGS , completionBlock: completionBlock)
    }
    //pending
    func setPushNotification(information:NSDictionary,completionBlock:WSCompletionBlock){
        performPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.SET_PUSH_NOTIFICATION, completionBlock: completionBlock)
        
    }
    func setPushNotificationStatus(information:NSDictionary,completionBlock:WSCompletionBlock){
        performPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.SET_PUSH_NOTIFICATION_STATUS, completionBlock: completionBlock)
        
    }
    func getNewsSearchWise(information:NSDictionary,completionBlock:WSCompletionBlock){
        performPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.GET_NEWS, completionBlock: completionBlock)
    }
    func getTendersSearchWise(information:NSDictionary,completionBlock:WSCompletionBlock){
        performPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.GET_TENDERS, completionBlock: completionBlock)
    }
    func getPlantsSearchWise(information:NSDictionary,completionBlock:WSCompletionBlock){
        performPostRequest(information, urlString: ServerSupportURLS.BASE_URL_LIVE + ServerSupportURLS.PLANTBEE_POST_DETAIL, completionBlock: completionBlock)
    }
}

//MARK: - Example usage of class
extension WebServices {
    /// An Example function demonstrating usage of JSON Post Request.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: parsed server response via completionBlock
    func simpleJsonPostRequestExample(information: NSDictionary ,completionBlock: WSCompletionBlock) -> () {
        let bodyData = NSMutableDictionary();
        copyData(information, sourceKey: "email", destinationDictionary: bodyData, destinationKey: "email", methodName:__FUNCTION__)
        copyData(information, sourceKey: "password", destinationDictionary: bodyData, destinationKey: "password", methodName:__FUNCTION__)
        addCommonInformation(bodyData)
        performJsonPostRequest(bodyData, urlString: ServerSupportURLS.BASE_URL+ServerSupportURLS.EXAMPLE_URL, completionBlock: completionBlock)
    }
    /// An Example function demonstrating usage of Post Request.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: parsed server response via completionBlock
    func simplePostRequestExample(information: NSDictionary ,completionBlock: WSCompletionBlock) -> () {
        let bodyData = NSMutableDictionary();
        copyData(information, sourceKey: "email", destinationDictionary: bodyData, destinationKey: "email", methodName:__FUNCTION__)
        copyData(information, sourceKey: "password", destinationDictionary: bodyData, destinationKey: "password", methodName:__FUNCTION__)
        addCommonInformation(bodyData)
        performPostRequest(bodyData, urlString: ServerSupportURLS.BASE_URL+ServerSupportURLS.EXAMPLE_URL, completionBlock: completionBlock)
    }
    /// An Example function demonstrating usage of Multipart Post Request.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: parsed server response via completionBlock
    func simpleMultiPartPostRequestExample(information: NSDictionary ,completionBlock: WSCompletionBlock) -> () {
        let bodyData = NSMutableDictionary();
        copyData(information, sourceKey: "email", destinationDictionary: bodyData, destinationKey: "email", methodName:__FUNCTION__)
        copyData(information, sourceKey: "password", destinationDictionary: bodyData, destinationKey: "password", methodName:__FUNCTION__)
        addCommonInformation(bodyData)
        performMultipartPostRequest(bodyData, urlString: ServerSupportURLS.BASE_URL+ServerSupportURLS.EXAMPLE_URL, constructBody:
            { (AFMultipartFormData) -> Void in
                AFMultipartFormData.appendPartWithFileData(NSData(), name: "image", fileName: "test.png", mimeType: "image/png")
            }, completionBlock: completionBlock)
    }
}

//MARK: - Private
 class WebServices: NSObject {
    
    var completionBlock: WSCompletionBlock?
    var responseErrorOption: ResponseErrorOption = ResponseErrorOption.ShowErrorResponseWithUsingNotification
    var progresssIndicatorText: String?
    var returnFailureResponseAlso: Bool = false
    
    /// Perform Json Encoded Post Request.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: parsed server response via completionBlock
    func performGetRequest(body:NSDictionary? , urlString:NSString? ,completionBlock: WSCompletionBlock)->(){
        if isInternetConnectivityAvailable(true)==false {
            return;
        }
        let url = NSURL(string: urlString as! String)
        print("\n\n\n                  HITTING URL\n\n \(url?.absoluteString)\n\n\n                  WITH GET REQUEST\n\n\(body)\n\n")
        if progresssIndicatorText != nil{
            showActivityIndicator(progresssIndicatorText!);
        }
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFHTTPResponseSerializer()
        // manager.requestSerializer = AFJSONRequestSerializer()
        manager.GET(urlString as! String, parameters: body, success:
            { (requestOperation, responseObject) -> Void in
                self.verifyServerResponse(responseObject, error: nil, completionBlock: completionBlock)
            })
            { (requestOperation, error) -> Void in
                self.verifyServerResponse(nil, error: error, completionBlock: completionBlock)
        }
    }
    
    func performPatchRequest(body:NSDictionary? , urlString:NSString? ,completionBlock: WSCompletionBlock)->(){
        if isInternetConnectivityAvailable(true)==false {
            return;
        }
        let url = NSURL(string: urlString as! String)
        print("\n\n\n                  HITTING URL\n\n \(url?.absoluteString)\n\n\n                  WITH PATCH REQUEST\n\n\(body)\n\n")
        if progresssIndicatorText != nil{
            showActivityIndicator(progresssIndicatorText!);
        }
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFHTTPResponseSerializer()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.PATCH(urlString as! String, parameters: body, success:
            { (requestOperation, responseObject) -> Void in
                self.verifyServerResponse(responseObject, error: nil, completionBlock: completionBlock)
            })
            { (requestOperation, error) -> Void in
                self.verifyServerResponse(nil, error: error, completionBlock: completionBlock)
        }
    }
    
    
    
    func performJsonPostRequest(body:NSDictionary? , urlString:NSString? ,completionBlock: WSCompletionBlock)->(){
        
        if isInternetConnectivityAvailable(true)==false {
            return;
        }
        let url = NSURL(string: urlString as! String)
        print("\n\n\n                  HITTING URL\n\n \(url?.absoluteString)\n\n\n                  WITH POST JSON BODY\n\n\(body)\n\n")
        if progresssIndicatorText != nil{
            showActivityIndicator(progresssIndicatorText!);
        }
        
        
//      //  AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//        AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
//        [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//        [serializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//        manager.requestSerializer = serializer;
        
        
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFHTTPResponseSerializer()
        manager.requestSerializer = AFJSONRequestSerializer()
        //manager.requestSerializer.set
        manager.POST(urlString as! String, parameters: body, success:
            { (requestOperation, responseObject) -> Void in
                self.verifyServerResponse(responseObject, error: nil, completionBlock: completionBlock)
            })
            { (requestOperation, error) -> Void in
                self.verifyServerResponse(nil, error: error, completionBlock: completionBlock)
        }
    }
    /// Perform Post Request.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: parsed server response via completionBlock
    func performPostRequest(body:NSDictionary? , urlString:NSString? ,completionBlock: WSCompletionBlock)->(){
        if isInternetConnectivityAvailable(true)==false {
            return;
        }
        
        let url = NSURL(string: urlString as! String)
        print("\n\n\n                  HITTING URL\n\n \(url?.absoluteString)\n\n\n                  WITH POST BODY\n\n\(body)\n\n")
        if progresssIndicatorText != nil{
            showActivityIndicator(progresssIndicatorText!);
        }
        
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFHTTPResponseSerializer()
        manager.requestSerializer = AFHTTPRequestSerializer()
        
        if let dataExists = body{
            if sessionId().characters.count>0{
            let dictWithToken = NSMutableDictionary(dictionary:dataExists)
            dictWithToken.setObject(sessionId(), forKey: "token")
                
                print("\n\n\nHITTING URL\n\n \(url?.absoluteString)\n\n\n                  WITH POST BODY + TOKEN \n\n\(dictWithToken)\n\n")
                
                manager.POST(urlString as! String, parameters: dictWithToken, success:
                    { (requestOperation, responseObject) -> Void in
                        self.verifyServerResponse(responseObject, error: nil, completionBlock: completionBlock)
                    })
                    { (requestOperation,error) -> Void in
                        
                        
                        
                        // datastring?.valueForKey("message#")
                        
                        self.verifyServerResponse(nil, error: error, completionBlock: completionBlock)
                }
                
                
            }else{
                manager.POST(urlString as! String, parameters: dataExists, success:
                    { (requestOperation, responseObject) -> Void in
                        self.verifyServerResponse(responseObject, error: nil, completionBlock: completionBlock)
                    })
                    { (requestOperation,error) -> Void in
                        
                        
                        
                        // datastring?.valueForKey("message#")
                        
                        self.verifyServerResponse(nil, error: error, completionBlock: completionBlock)
                }

                
            }
        }else{
            manager.POST(urlString as! String, parameters: body, success:
                { (requestOperation, responseObject) -> Void in
                    self.verifyServerResponse(responseObject, error: nil, completionBlock: completionBlock)
                })
                { (requestOperation,error) -> Void in
                    
                    
                    
                    // datastring?.valueForKey("message#")
                    
                    self.verifyServerResponse(nil, error: error, completionBlock: completionBlock)
            }
        }
      
    }
   
    
    
    
    /// Perform Multipart Post Request.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: parsed server response via completionBlock
    func performMultipartPostRequest(body:NSDictionary? , urlString:NSString? , constructBody:((AFMultipartFormData) -> Void)?,completionBlock: WSCompletionBlock)->(){
        if isInternetConnectivityAvailable(true)==false {
            return;
        }
        let url = NSURL(string: urlString as! String)
        print("\n\n\n                  HITTING URL\n\n \(url?.absoluteString)\n\n\n                  WITH POST BODY\n\n\(body)\n\n")
        if progresssIndicatorText != nil{
            showActivityIndicator(progresssIndicatorText!);
        }
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFHTTPResponseSerializer()
        manager.requestSerializer = AFHTTPRequestSerializer()
        manager.POST(urlString as! String , parameters: body, constructingBodyWithBlock: constructBody,success: { (requestOperation, responseObject) -> Void in
            self.verifyServerResponse(responseObject, error: nil, completionBlock: completionBlock)
            }, failure: { (requestOperation, error) -> Void in
                self.verifyServerResponse(nil, error: error, completionBlock: completionBlock)
        })
    }
    
    func addCommonInformation(information:NSMutableDictionary?)->(){
        
        if let sessionId=NSUserDefaults.standardUserDefaults().valueForKey("sessionId")
        {
            information?.setObject(sessionId, forKey: "token")
            
        }
        
    }
    /// To display server not responding message via notification banner.
    func showServerNotRespondingMessage(reason:String){
        
        switch reason{
        case  GlobalConstants.UNAUTHORISED_ACCESS_2:
            showNotification( GlobalConstants.UNAUTHORISED_ACCESS_2_RESPONSE, showOnNavigation: false, showAsError: true)
        case  GlobalConstants.UNAUTHORISED_ACCESS:
            showNotification( GlobalConstants.CREDENTIALS_ERROR, showOnNavigation: false, showAsError: true)
            
        case  GlobalConstants.SERVER_ERROR:
            showNotification( GlobalConstants.SERVER_ERROR_SHOW, showOnNavigation: false, showAsError: true)
            case "Request failed: not found (404)","The request timed out.":
            showNotification( "Network Error", showOnNavigation: false, showAsError: false)
        default:
            showNotification(reason, showOnNavigation: false, showAsError: true)
        }
        
    }
    /// To check wether the server operation succeeded or not.
    /// - returns: bool
    func isSuccess(response:NSDictionary?)->(Bool){
        if let statusKey = response?.valueForKey(Constants.RESPONSE_CODE_KEY) as? String{
            if response != nil{
                if statusKey == "1"{
                    return true
                }
                else if statusKey == "1310"{
                    return true
                }
            
            
            }
        }
        return false
    }
    /// To check wether the server operation failed or not.
    /// - returns: bool
    func isFailure(response:NSDictionary?)->(Bool){
        if response != nil{
            if let _ = response?.valueForKey(Constants.RESPONSE_CODE_KEY){
                if (response?.objectForKey(Constants.RESPONSE_CODE_KEY) as! NSString?)!.isEqualToString(Constants.RESPONSE_CODE_SUCCESS_VALUE){
                    return false
                }}
        }
        return true
    }
    /// To verify the server response received and perform action on basis of that.
    /// - parameter response: data received from server in the form of NSData
    func verifyServerResponse(response:AnyObject?,error:NSError?,completionBlock: WSCompletionBlock?)->(){
        if progresssIndicatorText != nil{
            hideActivityIndicator();
        }
        if error != nil {
            if responseErrorOption != ResponseErrorOption.DontShowErrorResponseMessage {
                if let errorData = error?.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] as? NSData
                {
                    
                    print("---error in json data---\(NSString(data: errorData, encoding: NSUTF8StringEncoding))")
                        let decodedString = parsedJsonFrom(errorData)
                        print(decodedString)
                if let decodedExists = decodedString{
                    if decodedExists["message#"] != nil{
                                
                                if decodedExists["status#"] != nil{
                                    
                                    
                                    if decodedExists["message#"] as! String == "Unauthorized action. Token access denied" && decodedExists["status#"] as! String == "403"{
                                        //generate session id if token is expired
                                        self.generateSessionID()
                                    }else{
                            showServerNotRespondingMessage(decodedExists["message#"] as! String)
                                    }
                                    
                                    
                                }else{
                                    
                        showServerNotRespondingMessage(decodedExists["message#"] as! String)}
                                
                            }
                    else{
                        showServerNotRespondingMessage(error!.localizedDescription)
                    }
                        }
                        else{
                            showServerNotRespondingMessage(error!.localizedDescription)
                        }
                  
                }
                else{
                    showServerNotRespondingMessage(error!.localizedDescription)
                }
            }
            printErrorMessage(error, methodName: __FUNCTION__)
            completionBlock!(responseData:nil);
        }
        else if response != nil {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), { () -> Void in
                
                let responseDictionary = parsedJsonFrom(response as? NSData)
                
                
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    if (responseDictionary?.isKindOfClass(NSDictionary) != nil) {
                        if self.isSuccess(responseDictionary ){
                            completionBlock!(responseData: responseDictionary);
                            
                            
                        }
                        else if self.isFailure(responseDictionary ){
                            var errorMessage : NSString?;
                            if (responseDictionary?.objectForKey(Constants.RESPONSE_MESSAGE_KEY) != nil) {
                                if (responseDictionary?.objectForKey(Constants.RESPONSE_MESSAGE_KEY)?.isKindOfClass(NSDictionary) != nil) {
                                    errorMessage = responseDictionary?.objectForKey(Constants.RESPONSE_MESSAGE_KEY) as? NSString
                                }else{
                                    errorMessage = responseDictionary?.description
                                }
                            }
                            else {
                                errorMessage =  GlobalConstants.MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY;
                            }
                            if self.responseErrorOption == ResponseErrorOption.ShowErrorResponseWithUsingNotification {
                                showNotification(errorMessage!, showOnNavigation: false, showAsError: true)
                            }
                            else if self.responseErrorOption == ResponseErrorOption.ShowErrorResponseWithUsingPopUp {
                                showPopupAlertMessage(errorMessage!, messageType: PopupMessageType.Error)
                            }
                            if self.returnFailureResponseAlso{
                                completionBlock!(responseData:responseDictionary);
                            }else{
                                completionBlock!(responseData:nil);
                            }
                        }
                        else {
                            if self.responseErrorOption == ResponseErrorOption.ShowErrorResponseWithUsingNotification {
                                showNotification( GlobalConstants.MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY, showOnNavigation: false, showAsError: true)
                            }
                            else if self.responseErrorOption == ResponseErrorOption.ShowErrorResponseWithUsingPopUp {
                                showPopupAlertMessage( GlobalConstants.MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY, messageType: PopupMessageType.Error)
                            }
                            completionBlock!(responseData:nil);
                        }
                    }
                    else {
                        if self.responseErrorOption == ResponseErrorOption.ShowErrorResponseWithUsingNotification {
                            showNotification( GlobalConstants.MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY, showOnNavigation: false, showAsError: true)
                        }
                        else if self.responseErrorOption == ResponseErrorOption.ShowErrorResponseWithUsingPopUp {
                            showPopupAlertMessage( GlobalConstants.MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY, messageType: PopupMessageType.Error)
                        }
                        completionBlock!(responseData:nil);
                    }
                    
                })
            })
        }
            ////////////
        else {
            if responseErrorOption == ResponseErrorOption.ShowErrorResponseWithUsingNotification {
                showNotification( GlobalConstants.MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY, showOnNavigation: false, showAsError: true)
            }
            else if responseErrorOption == ResponseErrorOption.ShowErrorResponseWithUsingPopUp {
                showPopupAlertMessage( GlobalConstants.MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY, messageType: PopupMessageType.Error)
            }
            completionBlock!(responseData:nil);
        }
    }
}

