//
//  WebServices.swift
//  Application
//
//  Created by Alok Kumar Singh on 13/10/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

//MARK: - AppCommonFunctions : This singleton class implements some app specific functions which are frequently needed in application.

import Foundation
import UIKit
import IQKeyboardManagerSwift
import SCLAlertView
import Parse
import Bolts
import SKPhotoBrowser
//import LNNotificationsUI

enum NotificationType{
    case NotificationForNews
    case NotificationForTender
    case NotificationForPrice
    case NotificationForPlantBee
    case NotificationForEvent
}

//MARK: - Completion block
typealias ACFCompletionBlock = (returnedData :AnyObject?) ->()

class AppCommonFunctions: NSObject{
    var tempview = UIView()
    var indicator = UIActivityIndicatorView()
    
    var completionBlock: ACFCompletionBlock?
    class var sharedInstance: AppCommonFunctions {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: AppCommonFunctions? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = AppCommonFunctions()
        }
        return Static.instance!
    }
    private override init() {
        
    }
    
//    func handleNotification(notification:NSDictionary?){
//        if isNotNull(notification){
//            let message = notification!.objectForKey("aps")!.objectForKey("alert")!.objectForKey("body") as! String
//            let notificationController = LNNotification(message:message)
//            notificationController.defaultAction = LNNotificationAction(title: "view", handler: { (action) -> Void in
//                self.handleNotificationWhenTappedToView(notification)
//            })
//            LNNotificationCenter.defaultCenter().presentNotification(notificationController, forApplicationIdentifier:  GlobalConstants.APP_NAME)
//        }
//    }
    
    func getTypeOfPushNotification(notification:NSDictionary?)->(NotificationType){
        let dataAsString = (notification?.objectForKey("gcm.notification.data") as! String)
        let dataAsNSData = dataAsString.dataUsingEncoding(NSUTF8StringEncoding)
        let dataAsDictionary = parsedJsonFrom(dataAsNSData)
        if let dataDIct =  dataAsDictionary{
        if (dataDIct.objectForKey("type") as! String) == "News" {
        return NotificationType.NotificationForNews
        }
        else if (dataDIct.objectForKey("type") as! String) == "Tender" {
        return NotificationType.NotificationForTender
        }
        else if (dataDIct.objectForKey("type") as! String) == "Price" {
        return NotificationType.NotificationForPrice
        }
        else if (dataDIct.objectForKey("type") as! String) == "PlantBee" {
        return NotificationType.NotificationForPlantBee
        }
        else if (dataDIct.objectForKey("type") as! String) == "Event" {
        return NotificationType.NotificationForEvent
        }
        }
       
        return NotificationType.NotificationForNews
    }
    
    func handleNotificationWhenTappedToView(notification:NSDictionary?){
        let notificationType = getTypeOfPushNotification(notification)
        let dataAsString = (notification?.objectForKey("gcm.notification.data") as! String)
        let dataAsNSData = dataAsString.dataUsingEncoding(NSUTF8StringEncoding)
        let dataAsDictionary = parsedJsonFrom(dataAsNSData)
        print(dataAsDictionary)
        if notificationType == NotificationType.NotificationForNews {
        }
        else if notificationType == NotificationType.NotificationForTender {
        }
        else if notificationType == NotificationType.NotificationForPrice {
        }
        else if notificationType == NotificationType.NotificationForPlantBee {
        }
        else if notificationType == NotificationType.NotificationForEvent {
        }
    }
    
    // MARK:   SUB CATEGORIES SCROLL SETUP
    func buttonsetup(arr: NSArray,fontSize: Int,scrollView:UIScrollView, refrence:UIViewController)
    {   var lastMaxY :CGFloat = 5
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.bounces = false
        scrollView.layer.borderWidth = 1.0
        scrollView.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        for var i=0 ;i<arr.count ; i++ {
            
            guard let str = arr[i] as? NSString else{return}
            let size = str.sizeWithAttributes([NSFontAttributeName: UIFont.systemFontOfSize(CGFloat(fontSize))])
            
            let frame = CGRectMake(CGFloat(lastMaxY), 1, size.width, 33)
            let button = UIButton(frame: frame)
            button.tag = i+2
            let categories = arr[i] as? String
            button.setTitle(categories?.capitalizedString, forState: .Normal)
            button.addTarget(refrence, action: "butonIndexed:" , forControlEvents:.TouchUpInside )
            
            button.titleLabel?.font = UIFont.systemFontOfSize(13)
            if button.tag==2{
                button.titleLabel?.font = UIFont.boldSystemFontOfSize(14)
					 button.titleLabel?.textAlignment = .Center
                button.setTitleColor(UIColor(red: 30.0/255.0, green: 136.0/255.0, blue: 229.0/255.0, alpha: 1), forState: .Normal)
                
            }
            else{
                button.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
            }
            button.backgroundColor = UIColor.clearColor()
            
            lastMaxY += button.bounds.maxX
            //print(lastMaxY)
            scrollView.contentSize = CGSizeMake(lastMaxY, 30)
            
            scrollView.addSubview(button)
            //print(button.frame)
            
        }

    }
    func buttonsetupForLogistics(arr: NSArray,fontSize: Int,scrollView:UIScrollView, refrence:UIViewController)
    {   var lastMaxY :CGFloat = 5
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.bounces = false
        scrollView.layer.borderWidth = 1.0
        scrollView.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        for var i=0 ;i<arr.count ; i++ {
            
            guard let str = arr[i] as? NSString else{return}
            let size = str.sizeWithAttributes([NSFontAttributeName: UIFont.systemFontOfSize(CGFloat(fontSize))])
            
            let frame = CGRectMake(CGFloat(lastMaxY), 1, size.width, 33)
            let button = UIButton(frame: frame)
            button.tag = i+888
            let categories = arr[i] as? String
            button.setTitle(categories?.capitalizedString, forState: .Normal)
            button.addTarget(refrence, action: "buttonIndexedForLogistics:" , forControlEvents:.TouchUpInside )
            
            button.titleLabel?.font = UIFont.systemFontOfSize(13)
            if button.tag==888{
                button.titleLabel?.font = UIFont.boldSystemFontOfSize(14)
                button.titleLabel?.textAlignment = .Center
                button.setTitleColor(UIColor(red: 30.0/255.0, green: 136.0/255.0, blue: 229.0/255.0, alpha: 1), forState: .Normal)
                
            }
            else{
                button.setTitleColor(UIColor.blackColor(), forState: .Normal)
            }
            button.backgroundColor = UIColor.clearColor()
            
            lastMaxY += button.bounds.maxX
            //print(lastMaxY)
            scrollView.contentSize = CGSizeMake(lastMaxY, 30)
            
            scrollView.addSubview(button)
            //print(button.frame)
            
        }
        
        
    }
    // MARK: PICKER VIEW DELEGATES
    func showPicker(refrence:UIViewController)->(UIPickerView){
        let height = refrence.view.frame.size.height
        let width = refrence.view.frame.size.width
        
        tempview = UIView(frame: CGRectMake(0, 0, width, height))
        let picker = UIPickerView(frame:CGRectMake(0, height-180, width, 200))
        picker.backgroundColor = UIColor.whiteColor()
        
        let containerView = UIView(frame: CGRectMake(0, height-220, width, 40))
        containerView.backgroundColor = UIColor(red: 238.0/255, green: 238.0/255, blue: 238.0/255.0, alpha: 1.0)
        let doneButton = UIButton(frame: CGRectMake(width-width/3, height-220, width/3, 40))
        
        doneButton.setTitle("Done", forState: .Normal)
        doneButton.setTitleColor(UIColor.blueColor(), forState: .Normal)
        doneButton.addTarget(refrence, action: "donePickerButton:" , forControlEvents:.TouchUpInside )
        let cancelButton = UIButton(frame: CGRectMake(0, height-220, width/3, 40))
        cancelButton.setTitle("Cancel", forState: .Normal)
        cancelButton.setTitleColor(UIColor.redColor(), forState: .Normal)
        cancelButton.addTarget(refrence, action: "cancelPickerButton:" , forControlEvents:.TouchUpInside )
        
        
        tempview.addSubview(containerView)
        tempview.addSubview(doneButton)
        tempview.addSubview(cancelButton)
        tempview.addSubview(picker)
        refrence.view.addSubview(tempview)
        return picker
    }
    func showNativeActivityIndicator(refrence: UIViewController){
        indicator = UIActivityIndicatorView  (activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
        indicator.frame = CGRectMake(0.0, 0.0, 10.0, 10.0)
        indicator.backgroundColor = UIColor.blueColor()
        tempview = UIView(frame: refrence.view.bounds)
        //        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.ExtraLight)
        //        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        //        blurEffectView.frame = tempview.bounds
        //        blurEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight] // for supporting device rotation
        //        tempview.addSubview(blurEffectView)
        tempview.addSubview(indicator)
        indicator.center = tempview.center
        refrence.view.addSubview(tempview)
        indicator.bringSubviewToFront(refrence.view)
        indicator.startAnimating()
    }
    
    
    func stopActivityIndicator() -> (UIView){
        indicator.stopAnimating()
        tempview.removeFromSuperview()
        return tempview
        
    }
    
    
    // MARK: PICKER END
    func prepareUserForParseChat(){
        let userInfo = CacheManager.sharedInstance .loadObject("userinfo") as! NSDictionary?
        
        if isNull(PFUser.currentUser()){
            //#Mohit  -- Currentr user email-
            
            var username  = "steelMintGuestUser1"
            var password = "password"
            var email = "steelMintGuestUser1@guestUser.com"
            if isNotNull(userInfo){
                username = ((userInfo!.objectForKey("data#") as? NSDictionary)?.objectForKey("username") as? String)!
                password = ((userInfo!.objectForKey("data#") as? NSDictionary)?.objectForKey("username") as? String)!
                //email = ((userInfo!.objectForKey("data#") as? NSDictionary)?.objectForKey("email") as? String)!
                email = ((userInfo!.objectForKey("data#") as? NSDictionary)?.objectForKey("username") as? String)!
}else
            {
                //Alert for user : if user is not loggedin
                let alertView = SCLAlertView()
                alertView.showTitle("SteelMint", subTitle: "Please login first before start chat", style: SCLAlertViewStyle.Warning, closeButtonTitle: "ok", duration: 20, colorStyle: 0xE4E4E4, colorTextButton: 0xFFFFFF)
                return
                
            }
            PFUser.registerSubclass()
            PFUser.logInWithUsernameInBackground(username, password: password, block: { (user, error) -> Void in
                if error == nil {
                    print("user logged in successfully")
                } else {
                    let user = PFUser()
                    user.username = username
                    user.password = password
                    user.email = email
                    user.signUpInBackgroundWithBlock({ (status, error) -> Void in
                        if error == nil {
                            print("me  registered successfully")
                            self.performSelector("prepareUserForParseChat", withObject: nil, afterDelay: 0)
                        } else {
                         print("me  not registered successfully")
                        }
                    })
                }
            })
        }
    }
    
    func signupInParseWith(username:String,email:String){
        let user = PFUser()
        user.username = username
        user.password = email
        user.email = email
        user.signUpInBackgroundWithBlock({ (status, error) -> Void in
            if error == nil {
                print("\(username) registered successfully for parse chat")
                
                print("setting notification for parse chat")
            
                NSNotificationCenter.defaultCenter().postNotificationName("kParseUserSignUp", object: self)
            }else{
                showAlertDifferentTypeForChat("Unable to chat with this user . please try after some time");
            }
            
        })
    }
    
    func prepareForStartUp(){
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)
        prepareForScreensToShow()
        setupIQKeyboardManager()
       // prepareUserForParseChat()
        setupRateMyApp()
       // setupLNNotifications()
    }
    
    func prepareForScreensToShow(){
        
        if NSUserDefaults.standardUserDefaults().boolForKey("isTuitorialShown") != true {
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isTuitorialShown")
            showTuitorialScreen()
        }else {
            showHomeScreen()
        }
    }
    
    func showTuitorialScreen(){
        let storyboard = UIStoryboard(name: "Secondary", bundle: nil)
        let tutorialViewController = storyboard.instantiateViewControllerWithIdentifier("TutorialsViewController") as! TutorialsViewController
        UIApplication.sharedApplication().delegate?.window?!.rootViewController = tutorialViewController
    }
    
    func showHomeScreen(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let leftViewController = storyboard.instantiateViewControllerWithIdentifier("LeftViewController") as! LeftViewController
        let TabController = storyboard.instantiateViewControllerWithIdentifier("TabController")
        UINavigationBar.appearance().barTintColor =  GlobalConstants.APP_THEME_BLUE_COLOR
        let slideMenuController = SlideMenuController(mainViewController:TabController, leftMenuViewController: leftViewController)
        let nav = UINavigationController(rootViewController: slideMenuController)
        nav.navigationBarHidden = true
        UIApplication.sharedApplication().delegate?.window?!.rootViewController = nav
    }
    
    func setupIQKeyboardManager(){
        IQKeyboardManager.sharedManager().enable = true
        
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        IQKeyboardManager.sharedManager().shouldPlayInputClicks = true
        IQKeyboardManager.sharedManager().shouldAdoptDefaultKeyboardAnimation = true
    }
//    func setupLNNotifications(){
//        LNNotificationCenter.defaultCenter().registerApplicationWithIdentifier(GlobalConstants.APP_NAME, name: GlobalConstants.APP_NAME, icon: UIImage(named: "Icon-76"), defaultSettings: LNNotificationDefaultAppSettings)
//    }
    func setupOtherSettings(){
        disableAutoCorrectionsAndTextSuggestionGlobally()
    }
    func setupRateMyApp(){
        RateMyApp.sharedInstance.appID = "974748812"
        RateMyApp.sharedInstance.trackAppUsage()
    }
    func isUserLoggedIn()->(Bool){
        if let _ = CacheManager.sharedInstance.loadObject("userinfo"){
            return true
        }
        return false
    }
    func share(title:NSString?,description:NSString?,url:NSString?, fromViewController:UIViewController){
        var textToShare : String?
        var urlToShare : NSURL?
        var itemsToshare = [AnyObject]()
        var titl = ""
        var desc = ""
        if let titleData = title{
            titl = titleData as String + "  "
        }
        if let descdata = description{
         desc   = "\n\n" + (descdata as String)
        }
        textToShare = titl + desc
        itemsToshare.append(getHTMlRenderedString(textToShare!))

        if let urlData = url{
            if let urlData = NSURL(string: urlData as String){
                urlToShare = urlData
                itemsToshare .append(urlToShare!)
            }
          
        }
       
        let activityVC = UIActivityViewController(activityItems: itemsToshare, applicationActivities: nil)
        fromViewController.presentViewController(activityVC, animated: true, completion: nil)
        
    }
    
    func logOutUser(refrence:UIViewController){
        let alertView = SCLAlertView()
        alertView.showTitle("SteelMint", subTitle: "Are you sure!\n you want to logout?", style: SCLAlertViewStyle.Warning, closeButtonTitle: "Cancel", duration: 20, colorStyle: 0xE4E4E4, colorTextButton: 0xFFFFFF)
        let button =  alertView.addButton("Yes") { () -> Void in
            let webservicesInstance = WebServices()
            webservicesInstance.progresssIndicatorText = "Signing out"
            
            let userInfo = NSMutableDictionary()
//            let username = NSUserDefaults.standardUserDefaults().valueForKey("username") as?String
//            let password = NSUserDefaults.standardUserDefaults().valueForKey("password") as?String
//            if username != nil || password != nil{
//                userInfo.setObject(username!, forKey: "username")
//                userInfo.setObject(password!, forKey: "password")
//            }
			//p498YYzG0bIHWnlRRQqs8n3ZCohJZTa7
            userInfo.setObject(sessionId(), forKey: "token")
            webservicesInstance.logout(userInfo, completionBlock:{
                (responseData) -> ()
                in
                print(responseData)
                if let _ = responseData{
                    NSUserDefaults.standardUserDefaults().setValue(nil, forKey: "token")

                    CacheManager.sharedInstance.saveObject(nil, identifier: "userinfo")
                    NSUserDefaults.standardUserDefaults().setValue(nil, forKey: "sessionId")
                    CacheManager.sharedInstance.saveObject(nil, identifier: "token")
                    
                    DatabaseManager.sharedInstance.resetCoreDataEntities()
                    PFUser.logOutInBackground()
                    refrence.slideMenuController()?.changeMainViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("TabController"), close: true)


                }
            })
        }
        button.backgroundColor = UIColor.redColor()
    }
    
    func showImages(fromVC:UIViewController?,images:NSArray?,initialIndex:Int,zoomFromView:UIView?){
        var photos = [SKPhoto]()
        for urlObject in images! {
            var photo : SKPhoto?
            let imageFromCache = SDWebImageManager.sharedManager().imageCache.imageFromDiskCacheForKey(SDWebImageManager.sharedManager().cacheKeyForURL(urlObject as! NSURL))
            if imageFromCache != nil {
                photo = SKPhoto.photoWithImage(imageFromCache)
            }else{
                photo = SKPhoto.photoWithImageURL(((urlObject as! NSURL).absoluteString))
            }
            photo!.shouldCachePhotoURLImage = true
            photos.append(photo!)
        }
        let urlObject = images?.objectAtIndex(initialIndex)
        let initialImageFromCache = SDWebImageManager.sharedManager().imageCache.imageFromDiskCacheForKey(SDWebImageManager.sharedManager().cacheKeyForURL(urlObject as! NSURL))
        var browser : SKPhotoBrowser?
        if initialImageFromCache != nil && zoomFromView != nil {
            browser = SKPhotoBrowser(originImage: initialImageFromCache, photos: photos, animatedFromView: zoomFromView!)
        }else{
            browser = SKPhotoBrowser(photos: photos)
        }
        browser!.initializePageIndex(initialIndex)
        browser!.disableVerticalSwipe = true
        fromVC!.presentViewController(browser!, animated: true, completion: {})
    }
    
    func generateUserNameFromName(name : NSString?)-> NSString {
        let userName = NSMutableString()
        let words = (name as! String).condense(".").componentsSeparatedByString(" ")
        for word in words {
            userName.appendFormat("%@.", word.lowercaseString)
        }
        if userName.length > 0 {
            return userName.substringToIndex(userName.length-1)
        }
        return  "\(userName)"
    }
    
    func generateEmailIdFromName(name : NSString?)-> NSString {
        var userName = NSMutableString()
        let words = (name as! String).condense(".").componentsSeparatedByString(" ")
        for word in words {
            userName.appendFormat("%@.", word.lowercaseString)
        }
        if userName.length > 0 {
            userName = NSMutableString(string: userName.substringToIndex(userName.length-1))
        }
        return "\(userName)@fakeemailId.com"
    }

    func getUserNameParticipant(participant:Participant)->(String){
        return self.generateUserNameFromName(participant.name) as (String)
    }
    
    func getEmailParticipant(participant:Participant)->(String){
        //using email id of  participant
        return participant.email!
    }
    
    func updateGCMTokenOnServer(){
        if AppCommonFunctions.sharedInstance.isUserLoggedIn(){
            if isNotNull(NSUserDefaults.standardUserDefaults().objectForKey("deviceToken")){
                let information = NSMutableDictionary()
                copyData(NSUserDefaults.standardUserDefaults().objectForKey("deviceToken"), destinationDictionary: information, destinationKey: "gcmRegId", methodName: __FUNCTION__)
                let webService = WebServices()
                webService.responseErrorOption = ResponseErrorOption.DontShowErrorResponseMessage
                webService.registerForPushNotificationAtServer(information, completionBlock: { (responseData) -> () in
                    if let _ = responseData {
                    }
                })
            }
        }
    }
}






