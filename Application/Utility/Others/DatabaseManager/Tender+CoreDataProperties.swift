//
//  Tender+CoreDataProperties.swift
//  Application
//
//  Created by Ankit Nandal on 10/03/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Tender {

    @NSManaged var buy_sell: String?
    @NSManaged var category_all: String?
    @NSManaged var closing_date: String?
    @NSManaged var company_name: String?
    @NSManaged var corrigendum: String?
    @NSManaged var country_name: String?
    @NSManaged var data_target: String?
    @NSManaged var data_target_href: String?
    @NSManaged var day: String?
    @NSManaged var dd: String?
    @NSManaged var description_tender: String?
    @NSManaged var document_class: String?
    @NSManaged var due_date: NSNumber?
    @NSManaged var f_date: String?
    @NSManaged var grade: String?
    @NSManaged var green_text: String?
    @NSManaged var imgpath: String?
    @NSManaged var isBookmarked: NSNumber?
    @NSManaged var live_expired: String?
    @NSManaged var month_year: String?
    @NSManaged var od: String?
    @NSManaged var od_display: String?
    @NSManaged var offer_type: String?
    @NSManaged var opening_date: String?
    @NSManaged var pdf_path: String?
    @NSManaged var phone_no: String?
    @NSManaged var publish_date: String?
    @NSManaged var qty: String?
    @NSManaged var region: String?
    @NSManaged var state_name: String?
    @NSManaged var sub_category: String?
    @NSManaged var sub_category_all: String?
    @NSManaged var tender_id: String?
    @NSManaged var title: String?
    @NSManaged var viaSearchOrFilter: NSNumber?

}
