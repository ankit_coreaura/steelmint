//
//  DatabaseManager.swift
//  Application
//
//  Created by Alok Kumar Singh on 13/10/15.
//  Copyright © 2015 Alok Singh. All rights reserved.
//

//MARK: - DatabaseManager : This class handles communication of application with its database (Core Data).

import Foundation
import UIKit
import CoreData
//MARK: - Completion block
typealias DMCompletionBlock = (returnedData :AnyObject?) ->()

class DatabaseManager: NSObject{
    var managedObjectContext:NSManagedObjectContext?
    var managedObjectModel: NSManagedObjectModel?
    var completionBlock: DatabaseManager?
    class var sharedInstance: DatabaseManager {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: DatabaseManager? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = DatabaseManager()
            Static.instance?.managedObjectContext = GlobalConstants.APPDELEGATE.managedObjectContext
            Static.instance?.managedObjectModel = GlobalConstants.APPDELEGATE.managedObjectModel
        }
        return Static.instance!
    }
    private override init() {
    }
     // MARK: EVENTS DB MANAGEMENT
    
    func addEvents(object:NSDictionary)
    {
        let entity =  NSEntityDescription.entityForName("Event",
            inManagedObjectContext:managedObjectContext!)
        var managedObject = getEvents(object.objectForKey("id") as? String)
        if managedObject == nil
        {
            managedObject = NSManagedObject(entity: entity!,
                insertIntoManagedObjectContext: managedObjectContext) as? Event
        }
        managedObject!.id = object.objectForKey("id") as? String
        if managedObject!.img == nil{
            managedObject!.img  = object.objectForKey("img") as? String
        }
        if managedObject!.agenda == nil{
            managedObject!.agenda  = object.objectForKey("agenda") as? String
        }
        if managedObject!.video == nil{
            managedObject!.video  = object.objectForKey("video") as? String
        }
        if managedObject!.enable == nil{
            managedObject!.enable  = object.objectForKey("enable") as? Bool
        }
        if managedObject!.timestamp == nil{
            managedObject!.timestamp  = object.objectForKey("timestamp") as? String
        }
        if managedObject!.thumb_img == nil{
            managedObject!.thumb_img  = object.objectForKey("thumb_img") as? String
        }
        if managedObject!.location == nil{
            managedObject!.location  = object.objectForKey("location") as? String
        }
        if managedObject!.name == nil{
            managedObject!.name  = object.objectForKey("name") as? String
        }
        if managedObject!.img == nil{
            managedObject!.img  = object.objectForKey("img") as? String
        }
        
        if let speakers = object.objectForKey("speakers") as? NSArray{
            managedObject?.speakrs = nil
            addSpeaker(speakers, event:managedObject!)
        }
        
        if let schedule = object.objectForKey("schedule") as? NSArray{
            managedObject?.schedules = nil

            addSchedule(schedule , event:managedObject!)
        }
        
        if let participant = object.objectForKey("participant") as? NSArray{
            managedObject?.participants = nil

            addParticipant(participant , event:managedObject!)
        }
        
        if let sponsors = object.objectForKey("sponsors") as? NSArray{
            managedObject?.sponsors = nil

            addSponsors(sponsors , event:managedObject!)
        }
        
        if let exhibitors = object.objectForKey("exhibitors") as? NSArray{
            managedObject?.exhibitors = nil

            addExhibitors(exhibitors , event:managedObject!)
        }

        do {
            try managedObjectContext!.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    
    func addSpeaker(array:NSArray ,event:Event)
    {
        for  object  in array
        {
            let entity =  NSEntityDescription.entityForName("Speaker",
                inManagedObjectContext:managedObjectContext!)
            let managedObject = NSManagedObject(entity: entity!,
                insertIntoManagedObjectContext: managedObjectContext) as? Speaker
         
            managedObject!.name = object.objectForKey("name")as? String
            managedObject!.subject = object.objectForKey("subject")as? String
            managedObject!.position = object.objectForKey("position")as? String
            managedObject!.desc = object.objectForKey("desc")as? String
            managedObject!.thumb_img = object.objectForKey("thumb_img")as? String
            managedObject!.id = object.objectForKey("id")as? String
            managedObject!.event_id = object.objectForKey("event_id")as? String
            managedObject!.company = object.objectForKey("company")as? String
            managedObject!.image = object.objectForKey("image")as? String
             managedObject!.event = event
            do {
                try managedObjectContext!.save()
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }
        }
    }
    
    
    func addSchedule(array:NSArray ,event:Event)
    {
        for  object  in array
        {
            let entity =  NSEntityDescription.entityForName("Schedule",
                inManagedObjectContext:managedObjectContext!)
            let managedObject = NSManagedObject(entity: entity!,
                insertIntoManagedObjectContext: managedObjectContext) as? Schedule
            managedObject!.timestamp = object.objectForKey("time")as? NSNumber
            managedObject!.title = object.objectForKey("title")as? String
            managedObject!.desc = object.objectForKey("desc")as? String
            managedObject!.event = event
            do {
                try managedObjectContext!.save()
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }
        }
    }
    
    
    
    
    
    
    func addParticipant(array:NSArray ,event:Event)
    {
        for  object  in array
        {
            let entity =  NSEntityDescription.entityForName("Participant",
                inManagedObjectContext:managedObjectContext!)
            let managedObject = NSManagedObject(entity: entity!,
                insertIntoManagedObjectContext: managedObjectContext) as? Participant
            managedObject!.name = object.objectForKey("name")as? String
            managedObject!.id = object.objectForKey("id")as? String
            managedObject!.email = object.objectForKey("email")as? String
            managedObject!.region = object.objectForKey("region")as? String
            managedObject!.designation = object.objectForKey("designation")as? String
            managedObject!.company = object.objectForKey("company")as? String
            managedObject!.thumb_img = object.objectForKey("thumb_img")as? String
            
            managedObject!.phoneno = object.objectForKey("phoneno")as? String
            managedObject!.img = object.objectForKey("img")as? String

            managedObject!.event = event
            
           
            do {
                try managedObjectContext!.save()
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }
        }
    }
    func getParticipants(id:String?)->(Participant?){
        if let idOfEvent = id{
            
            let predicate = NSPredicate(format: "id = %@", argumentArray:[idOfEvent])
            let fetchRequest = NSFetchRequest(entityName:"Participant")
            fetchRequest.predicate = predicate
            do {
                let results =
                try managedObjectContext!.executeFetchRequest(fetchRequest)
                results as! [NSManagedObject]
                
                if results.count > 0 {
                    return results[0] as? Participant
                }else{
                    return nil
                }
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
                return nil
            }
        }
        return nil
    }
    
    
    func addSponsors(array:NSArray ,event:Event)
    {
        for  typeobject  in array
        {
            let typeSponser = typeobject.objectForKey("name")as? String
            if let typeSponserArray = typeobject.objectForKey("value")as? NSArray
            {
            for  object  in typeSponserArray
            {
              
                let entity =  NSEntityDescription.entityForName("Sponsor",
                    inManagedObjectContext:managedObjectContext!)
                let managedObject = NSManagedObject(entity: entity!,
                    insertIntoManagedObjectContext: managedObjectContext) as? Sponsor
                managedObject!.name = object.objectForKey("name")as? String
                managedObject!.id = object.objectForKey("id")as? String
                managedObject!.region = object.objectForKey("region")as? String
                managedObject!.event_id = object.objectForKey("event_id")as? String
                managedObject!.website = object.objectForKey("website")as? String
               
                managedObject!.img = object.objectForKey("img")as? String
                managedObject!.sponser_logo = object.objectForKey("thumb_img")as? String
                managedObject!.event = event

                managedObject!.sponsors_type=typeSponser
                
                do {
                    try managedObjectContext!.save()
                } catch let error as NSError  {
                    print("Could not save \(error), \(error.userInfo)")
                }
            }
            }
        }
    }
    
    
    
    func addExhibitors(array:NSArray ,event:Event)
    {
        for  object  in array
        {
            let entity =  NSEntityDescription.entityForName("Exhibitor",
                inManagedObjectContext:managedObjectContext!)
            let managedObject = NSManagedObject(entity: entity!,
                insertIntoManagedObjectContext: managedObjectContext) as? Exhibitor
            managedObject!.name = object.objectForKey("name")as? String
            managedObject!.id = object.objectForKey("id")as? String
            managedObject!.region = object.objectForKey("region")as? String
            managedObject!.event_id = object.objectForKey("event_id")as? String
            managedObject!.img = object.objectForKey("img")as? String
            managedObject!.website = object.objectForKey("website")as? String
            managedObject!.thumb_img = object.objectForKey("thumb_img")as? String
            managedObject!.event = event
            

            do {
                try managedObjectContext!.save()
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }
        }
    }
    
    
    func getEvents(id:String?)->(Event?){
        if let idOfEvent = id{
        
            let predicate = NSPredicate(format: "id = %@", argumentArray:[idOfEvent])
            let fetchRequest = NSFetchRequest(entityName:"Event")
            fetchRequest.predicate = predicate
            do {
                let results =
                try managedObjectContext!.executeFetchRequest(fetchRequest)
                results as! [NSManagedObject]
                
                if results.count > 0 {
                    return results[0] as? Event
                }else{
                    return nil
                }
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
                return nil
            }
        }
        return nil
    }
    func getEventsPropertyWise(id:String?,propertyWise:String)->([AnyObject]?){
        if let idOfEvent = id{
            let predicate = NSPredicate(format: "id = %@", argumentArray:[idOfEvent])
            let fetchRequest = NSFetchRequest(entityName:"Event")
            
            
            
//            NSDictionary *attributes = [NSEntityDescription entityForName"@ContrSector" inManagedObjectContext:self.managedObjectContext] relationshipsByName];
//            NSAttributeDescription *industriesRelationship = [attributes objectForKey:@"industries"];
//            
//            request.propertiesToFetch = @[industriesRelationship];
            
            
//            let attribute = NSEntityDescription.entityForName("Event", inManagedObjectContext: managedObjectContext!)
//            let entity =  NSEntityDescription.entityForName("Speakrs",
//                inManagedObjectContext:managedObjectContext!)
//            let relationShip = attribute?.attributesByName
            
            
            fetchRequest.propertiesToFetch = [propertyWise]
            fetchRequest.predicate = predicate
            do {
                let results =
                try managedObjectContext!.executeFetchRequest(fetchRequest)
                results as! [NSManagedObject]
                
                if results.count > 0 {
                    return results
                }else{
                    return nil
                }
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
                return nil
            }
        }
        return nil
    }
    func getAllEvents()->([AnyObject]?){
        let fetchRequest = NSFetchRequest(entityName: "Event")
        //fetchRequest.resultType = NSFetchRequestResultType.DictionaryResultType
        
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as? [NSManagedObject]
            if results.count > 0
            {
                return results
            }else
            {
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    func deleteAllEvents(){
        let fetchRequest = NSFetchRequest(entityName: "Event")
        //fetchRequest.resultType = NSFetchRequestResultType.DictionaryResultType
        
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as? [NSManagedObject]
            if results.count > 0
            {
                for item in results
                {
                    
                    managedObjectContext?.deleteObject(item as! NSManagedObject)
                    
                }
                do {
                    try self.managedObjectContext!.save()
                } catch {
                    let saveError = error as NSError
                    print(saveError)
                }
            }else
            {
                
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            
        }
    }

    func getAllEventsId()->([AnyObject]?){
        let fetchRequest = NSFetchRequest(entityName: "Event")
        fetchRequest.resultType = NSFetchRequestResultType.DictionaryResultType
        fetchRequest.propertiesToFetch = ["id"]
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as? [NSManagedObject]
            if results.count > 0
            {
                return results
            }else
            {
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    
 // MARK: NEWS DB MANAGEMENT
    func deleteNewsforCategory(category:String)
    {
   
        let fetchRequest = NSFetchRequest(entityName: "News")
        
        if  category.characters.count > 0{
            let predicate = NSPredicate(format: "main_category  == [C] %@  || category = [C] %@ && viaSearchOrFilter = %@", argumentArray:[category,category,false])
            fetchRequest.predicate = predicate
            
        }else{
            let predicate = NSPredicate(format: " viaSearchOrFilter = %@", argumentArray:[false])
            fetchRequest.predicate = predicate
        }
        fetchRequest.includesPropertyValues = false
        fetchRequest.resultType = NSFetchRequestResultType.ManagedObjectResultType
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as? [NSManagedObject]
            if results.count > 0
            {
                for item in results
                {

                    managedObjectContext?.deleteObject(item as! NSManagedObject)
                    
                }
            }else
            {
                
            }
            do {
                try self.managedObjectContext!.save()
            } catch {
                let saveError = error as NSError
                print(saveError)
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
    }
    func addNews(object:NSDictionary)
    {
        
        let entity =  NSEntityDescription.entityForName("News",
            inManagedObjectContext:managedObjectContext!)
        var managedObject = getNews(object)
        if managedObject == nil
        {
            managedObject = NSManagedObject(entity: entity!,
                insertIntoManagedObjectContext: managedObjectContext) as? News
        }
    
       
       
        managedObject!.news_id = object.objectForKey("ID") as? String
        managedObject!.post_title = object.objectForKey("post_title") as? String
        managedObject!.post_content = object.objectForKey("post_content")as? String
        managedObject!.author_name = object.objectForKey("author_name")as? String
        managedObject!.href = object.objectForKey("href")as? String
        managedObject!.image = object.objectForKey("image")as? String
        managedObject!.medium_image = object.objectForKey("medium_image")as? String
        
        if let date = object.objectForKey("post_date")as? String{
            let nsformatter = NSDateFormatter();
            nsformatter.dateFormat = "MMM dd, yyyy HH:mm"
            let timestamp = (nsformatter.dateFromString(date))?.timeIntervalSince1970
            managedObject!.post_date = timestamp
            
        }
        
        managedObject!.category = object.objectForKey("sub_category")as? String
        managedObject!.main_category = object.objectForKey("category")as? String
        managedObject!.post_modified = object.objectForKey("post_modified")as? String
        managedObject!.viaSearchOrFilter = false

        do {
            try managedObjectContext!.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
    }
    
  
    func addNewsINBackgroundThread(objectArray:NSArray){
        
        
        // Adding values in background thread
       
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) { () -> Void in
                
                
                let mainManagedObjectContext: NSManagedObjectContext
                var privateManagedObjectContext: NSManagedObjectContext!
                
                mainManagedObjectContext = self.managedObjectContext!
                
                privateManagedObjectContext = NSManagedObjectContext(concurrencyType: .PrivateQueueConcurrencyType)
                
                // Configure Managed Object Context
                privateManagedObjectContext.parentContext = mainManagedObjectContext
                
                
                for object in objectArray{
                    let entity =  NSEntityDescription.entityForName("News",
                        inManagedObjectContext: privateManagedObjectContext)
                    var managedObject = self.getNewsInBackground(object as! NSDictionary, managedContext: privateManagedObjectContext)
                    if managedObject == nil
                    {
                        managedObject = NSManagedObject(entity: entity!,
                            insertIntoManagedObjectContext: privateManagedObjectContext) as? News
                    }
                    
                    let objectID =  (NSManagedObject(entity: entity!,
                        insertIntoManagedObjectContext: privateManagedObjectContext) as? News)!.objectID
                    let managedObjectPrivate = privateManagedObjectContext.objectWithID(objectID) as? News

                    
                    // Do Some Work
                    
                    managedObjectPrivate!.news_id = object.objectForKey("ID") as? String
                    managedObjectPrivate!.post_title = object.objectForKey("post_title") as? String
                    managedObjectPrivate!.post_content = object.objectForKey("post_content")as? String
                    managedObjectPrivate!.author_name = object.objectForKey("author_name")as? String
                    managedObjectPrivate!.href = object.objectForKey("href")as? String
                    managedObjectPrivate!.image = object.objectForKey("image")as? String
                    managedObjectPrivate!.medium_image = object.objectForKey("medium_image")as? String
                    
                    if let date = object.objectForKey("post_date")as? String{
                        let nsformatter = NSDateFormatter();
                        nsformatter.dateFormat = "MMM dd, yyyy HH:mm"
                        let timestamp = (nsformatter.dateFromString(date))?.timeIntervalSince1970
                        managedObjectPrivate!.post_date = timestamp
                        
                    }
                    
                    managedObjectPrivate!.category = object.objectForKey("sub_category")as? String
                    managedObjectPrivate!.main_category = object.objectForKey("category")as? String
                    managedObjectPrivate!.post_modified = object.objectForKey("post_modified")as? String
                    managedObjectPrivate!.viaSearchOrFilter = false
                    
                    if privateManagedObjectContext.hasChanges {
                        do {
                            try privateManagedObjectContext.save()
                        } catch {
                            // Error Handling
                            // ...
                        }
                    }
   
                }
                
            }
        
    }
    
    func getNewsInBackground(object:NSDictionary,managedContext:NSManagedObjectContext)->(News?){
        let predicate = NSPredicate(format: "news_id = %@", argumentArray:[object.objectForKey("ID")!])
        let fetchRequest = NSFetchRequest(entityName: "News")
        fetchRequest.predicate = predicate
        do {
            let results =
            try managedContext.executeFetchRequest(fetchRequest)
            results as! [NSManagedObject]
            
            if results.count > 0 {
                return results[0] as? News
            }else{
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    
    func getNews(object:NSDictionary)->(News?){
        let predicate = NSPredicate(format: "news_id = %@", argumentArray:[object.objectForKey("ID")!])
        let fetchRequest = NSFetchRequest(entityName: "News")
        fetchRequest.predicate = predicate
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as! [NSManagedObject]
            
            if results.count > 0 {
                return results[0] as? News
            }else{
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    
    
    
    func getAllNews()->([AnyObject]?){
        let fetchRequest = NSFetchRequest(entityName: "News")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key:"post_date", ascending:false)]
        fetchRequest.resultType = NSFetchRequestResultType.DictionaryResultType
        
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            
            
            if results.count > 0
            {
                return results
            }else
            {
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    func getAllNewsId(category:String)->([AnyObject]?){
        let fetchRequest = NSFetchRequest(entityName: "News")

        if  category.characters.count > 0{
         let predicate = NSPredicate(format: "main_category  == [C] %@  || category = [C] %@ && viaSearchOrFilter = %@", argumentArray:[category,category,false])
            fetchRequest.predicate = predicate

        }else{
         let predicate = NSPredicate(format: " viaSearchOrFilter = %@", argumentArray:[false])
            fetchRequest.predicate = predicate
        }
        
        fetchRequest.resultType = NSFetchRequestResultType.DictionaryResultType
        fetchRequest.propertiesToFetch = ["news_id"]
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as? [NSDictionary]
            if results.count > 0
            {
                return results
            }else
            {
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }

    // MARK: PLANTBEE DB MANAGEMENT
    func addPlantbee(object:NSDictionary,type:String)
    {
        let entity =  NSEntityDescription.entityForName("Plantbee",
            inManagedObjectContext:managedObjectContext!)
        var managedObject = getPlantbee(object)
        if managedObject == nil
        {
            managedObject = NSManagedObject(entity: entity!,
                insertIntoManagedObjectContext: managedObjectContext) as? Plantbee
        }
        if let name = object.objectForKey("name") as? String{
            managedObject!.name =  getHTMlRenderedString(name).string
        }
        managedObject!.desc = object.objectForKey("name") as? String
        managedObject!.parent_cat_code = object.objectForKey("parent_cat_code") as? String
        managedObject!.image = object.objectForKey("image") as? String
        managedObject!.plant_type_code = object.objectForKey("plant_type_code") as? String
        managedObject!.total_leads = object.objectForKey("total_leads") as? NSNumber
        managedObject!.children = object.objectForKey("children") as? NSArray
        
        do {
            try managedObjectContext!.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
 
    func getPlantbee(object:NSDictionary)->(Plantbee?){
        guard let name = object.objectForKey("name") as? String else{return nil}
        let predicate = NSPredicate(format: "name = %@", argumentArray:[              getHTMlRenderedString(name).string])
        let fetchRequest = NSFetchRequest(entityName: "Plantbee")
        fetchRequest.predicate = predicate
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as! [NSManagedObject]
            
            if results.count > 0 {
                return results[0] as? Plantbee
            }else{
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    
    func getAllPlantbee()->([AnyObject]?){
        let fetchRequest = NSFetchRequest(entityName: "Plantbee")
        fetchRequest.resultType = NSFetchRequestResultType.DictionaryResultType
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as? [NSDictionary]
            if results.count > 0
            {
                return results
            }else
            {
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    
    func addPlantbeeSell(object:NSDictionary,type:String)
    {
        let entity =  NSEntityDescription.entityForName("PlantbeeSell",
            inManagedObjectContext:managedObjectContext!)
        var managedObject = getPlantbeeSell(object)
        if managedObject == nil
        {
            managedObject = NSManagedObject(entity: entity!,
                insertIntoManagedObjectContext: managedObjectContext) as? PlantbeeSell
        }
       
        
        if let name = object.objectForKey("name") as? String{
            managedObject!.name =  getHTMlRenderedString(name).string
        }
        managedObject!.desc = object.objectForKey("name") as? String
        managedObject!.parent_cat_code = object.objectForKey("parent_cat_code") as? String
        managedObject!.image = object.objectForKey("image") as? String
        managedObject!.plant_type_code = object.objectForKey("plant_type_code") as? String
        managedObject!.total_leads = object.objectForKey("total_leads") as? NSNumber
        managedObject!.children = object.objectForKey("children") as? NSArray
        do {
            try managedObjectContext!.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    func getPlantbeeSell(object:NSDictionary)->(PlantbeeSell?){
        guard let name = object.objectForKey("name") as? String else{return nil}
        let predicate = NSPredicate(format: "name = %@", argumentArray:[              getHTMlRenderedString(name).string])
        let fetchRequest = NSFetchRequest(entityName: "PlantbeeSell")
        fetchRequest.predicate = predicate
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as! [NSManagedObject]
            
            if results.count > 0 {
                return results[0] as? PlantbeeSell
            }else{
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    
    func getAllPlantbeeSell()->([AnyObject]?){
        let fetchRequest = NSFetchRequest(entityName: "PlantbeeSell")
        fetchRequest.resultType = NSFetchRequestResultType.DictionaryResultType
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as? [NSDictionary]
            if results.count > 0
            {
                return results
            }else
            {
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    
    
    func addPlantbeePost(object:NSDictionary)
    {
        let entity =  NSEntityDescription.entityForName("PlantbeePost",
            inManagedObjectContext:managedObjectContext!)
        var managedObject = getPlantbeePost(object)
        if managedObject == nil
        {
            managedObject = NSManagedObject(entity: entity!,
                insertIntoManagedObjectContext: managedObjectContext) as? PlantbeePost
        }
        managedObject!.verified = object.objectForKey("verified") as? String
        managedObject!.user_status = object.objectForKey("user_status") as? String
        managedObject!.user_personal_detail_id = object.objectForKey("user_personal_detail_id") as? String
        managedObject!.type = object.objectForKey("type") as? String
        managedObject!.title = object.objectForKey("title") as? String
        managedObject!.tags_id = object.objectForKey("tags_id") as? String
        managedObject!.prices_detail = object.objectForKey("prices_detail") as? String
        managedObject!.plant_type_code = object.objectForKey("plant_type_code") as? String
        managedObject!.parent_code = object.objectForKey("parent_code") as? String

        managedObject!.offer_detail = object.objectForKey("offer_detail") as? String
        managedObject!.mobile = object.objectForKey("mobile") as? String
        managedObject!.locality = object.objectForKey("locality") as? String
        managedObject!.id = object.objectForKey("id") as? String
        
        
        if let date = object.objectForKey("datetime")as? String{
            let nsformatter = NSDateFormatter();
            nsformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let timestamp = (nsformatter.dateFromString(date))?.timeIntervalSince1970
            managedObject!.datetime = timestamp
            
        }
        
        
        managedObject!.dataLead = object.objectForKey("dataLead") as? NSArray
        managedObject!.country_name = object.objectForKey("country_name") as? String
        managedObject!.contact_person = object.objectForKey("contact_person") as? String
        managedObject!.category = object.objectForKey("category") as? String
        managedObject!.company_name = object.objectForKey("company_name") as? String
        managedObject!.business_type = object.objectForKey("business_type") as? String
        managedObject!.address = object.objectForKey("address") as? String
        managedObject!.about_company = object.objectForKey("about_company") as? String
        managedObject!.designation = object.objectForKey("designation") as? String
        managedObject!.email_id = object.objectForKey("email_id") as? String
        managedObject!.status = object.objectForKey("status") as? String
        managedObject!.state_id = object.objectForKey("state_id") as? String
        managedObject!.city_id = object.objectForKey("city_id") as? String
        managedObject!.have_images = object.objectForKey("have_images") as? String
        managedObject!.have_doc = object.objectForKey("have_doc") as? String
        managedObject!.country_id = object.objectForKey("country_id") as? String
        managedObject!.city_name = object.objectForKey("city_name") as? String
        managedObject!.state_name = object.objectForKey("state_name") as? String
        managedObject!.latitude = object.objectForKey("latitude") as? String
        managedObject!.longitude = object.objectForKey("longitude") as? String
        saveContext()

    }
    
    func getPlantbeePost(object:NSDictionary)->(PlantbeePost?){
        let predicate = NSPredicate(format: "id = %@", argumentArray:[object.objectForKey("id")!])
        let fetchRequest = NSFetchRequest(entityName: "PlantbeePost")
        fetchRequest.predicate = predicate
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as! [NSManagedObject]
            
            if results.count > 0 {
                return results[0] as? PlantbeePost
            }else{
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    func getPlantbeePostID(id:String)->(PlantbeePost?){
        let predicate = NSPredicate(format: "id = %@", argumentArray:[id])
        let fetchRequest = NSFetchRequest(entityName: "PlantbeePost")
        fetchRequest.predicate = predicate
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as! [NSManagedObject]
            
            if results.count > 0 {
                return results[0] as? PlantbeePost
            }else{
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    func getAllPlantbeePost()->([AnyObject]?){
        //isBookmarked
        
        let predicate = NSPredicate(format: "isBookmarked = %@", argumentArray:[true])
        let fetchRequest = NSFetchRequest(entityName: "PlantbeePost")
        fetchRequest.resultType = NSFetchRequestResultType.DictionaryResultType
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "datetime", ascending: false)]
        fetchRequest.predicate = predicate
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as! [NSManagedObject]
            
            if results.count > 0 {
                return results 
            }else{
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    func getAllPlantbeePostCodeWise(plantCode:String,parentCode:String,type:String)->([AnyObject]?){
        let fetchRequest = NSFetchRequest(entityName: "PlantbeePost")
        let predicate = NSPredicate(format: "(plant_type_code = [C] %@ && category = [C]%@) || (parent_code = [C] %@ && category = [C]%@)", argumentArray: [plantCode,type,parentCode,type])
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "datetime", ascending: false)]
        fetchRequest.resultType = NSFetchRequestResultType.DictionaryResultType
        fetchRequest.predicate = predicate
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as? [NSDictionary]
            if results.count > 0
            {
                return results
            }else
            {
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    // MARK: TENDER DB MANAGEMENT
    
    func addTender(object:NSDictionary){
        let entity =  NSEntityDescription.entityForName("Tender",
            inManagedObjectContext:managedObjectContext!)
        var managedObject = getTender(object)
        if managedObject == nil
        {
            managedObject = NSManagedObject(entity: entity!,
                insertIntoManagedObjectContext: managedObjectContext) as? Tender
        }
        
        managedObject!.tender_id = object.objectForKey("ID") as? String
        managedObject!.f_date = object.objectForKey("f_date") as? String
        managedObject!.company_name = object.objectForKey("company_name") as? String
        managedObject!.qty = object.objectForKey("qty") as? String
        managedObject!.grade = object.objectForKey("grade") as? String
        managedObject!.opening_date = object.objectForKey("opening_date") as? String
        managedObject!.od = object.objectForKey("od") as? String
        managedObject!.closing_date = object.objectForKey("closing_date") as? String
        
        if let date = object.objectForKey("due_date")as? String{
            let nsformatter = NSDateFormatter();
            nsformatter.dateFormat = "dd MMM yy"
            let timestamp = (nsformatter.dateFromString(date))?.timeIntervalSince1970
            managedObject!.due_date = timestamp
            
        }
        
        managedObject!.dd = object.objectForKey("dd") as? String
        managedObject!.pdf_path = object.objectForKey("pdf_path") as? String
        managedObject!.sub_category = object.objectForKey("sub_category") as? String
        managedObject!.sub_category_all = object.objectForKey("sub_category_all") as? String
        managedObject!.category_all = object.objectForKey("category_all") as? String
        managedObject!.title = object.objectForKey("title") as? String
        managedObject!.buy_sell = object.objectForKey("buy_sell") as? String
        managedObject!.imgpath = object.objectForKey("imgpath") as? String
        managedObject!.offer_type = object.objectForKey("offer_type") as? String
        managedObject!.state_name = object.objectForKey("state_name") as? String
        managedObject!.country_name = object.objectForKey("country_name") as? String
        managedObject!.region = object.objectForKey("region") as? String
        managedObject!.live_expired = object.objectForKey("live_expired") as? String
        managedObject!.corrigendum = object.objectForKey("corrigendum") as? String
        managedObject!.publish_date = object.objectForKey("publish_date") as? String
        managedObject!.data_target = object.objectForKey("data_target") as? String
        managedObject!.data_target_href = object.objectForKey("data_target_href") as? String
        managedObject!.od_display = object.objectForKey("od_display") as? String
        managedObject!.green_text = object.objectForKey("green_text") as? String
        managedObject!.day = object.objectForKey("day") as? String
        managedObject!.month_year = object.objectForKey("month_year") as? String
        managedObject!.document_class = object.objectForKey("document_class") as? String
        managedObject!.phone_no = object.objectForKey("phone_no") as? String
        managedObject!.description_tender = object.objectForKey("description") as? String
        managedObject!.viaSearchOrFilter = false
        do {
            try managedObjectContext!.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    func getTender(object:NSDictionary)->(Tender?){
        let predicate = NSPredicate(format: "tender_id = %@", argumentArray:[object.objectForKey("ID")!])
        let fetchRequest = NSFetchRequest(entityName: "Tender")
        fetchRequest.predicate = predicate
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as! [NSManagedObject]
            
            if results.count > 0 {
                return results[0] as? Tender
            }else{
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    
    
    func getAllTenders()->([AnyObject]?){
        let fetchRequest = NSFetchRequest(entityName: "Tender")
        fetchRequest.resultType = NSFetchRequestResultType.DictionaryResultType
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "due_date", ascending: true)]

        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as? [NSManagedObject]
            if results.count > 0
            {
                return results
            }else
            {
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    func getAllTendersId(category:String)->([AnyObject]?){
        let fetchRequest = NSFetchRequest(entityName: "Tender")
        
        if  category.characters.count > 0{
            let predicate = NSPredicate(format: "category_all  == [C] %@ && viaSearchOrFilter = %@", argumentArray:[category,false])
            fetchRequest.predicate = predicate
            
        }else{
            let predicate = NSPredicate(format: " viaSearchOrFilter = %@", argumentArray:[false])
            fetchRequest.predicate = predicate
        }
        fetchRequest.resultType = NSFetchRequestResultType.DictionaryResultType
        fetchRequest.propertiesToFetch = ["tender_id"]
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as? [NSManagedObject]
            if results.count > 0
            {
                return results
            }else
            {
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    func deleteTenderforCategory(category:String)
    {
        
        let fetchRequest = NSFetchRequest(entityName: "Tender")
        
        if  category.characters.count > 0{
            let predicate = NSPredicate(format: "category_all  == [C] %@ && viaSearchOrFilter = %@", argumentArray:[category,false])
            fetchRequest.predicate = predicate
            
        }else{
            let predicate = NSPredicate(format: " viaSearchOrFilter = %@", argumentArray:[false])
            fetchRequest.predicate = predicate
        }
        fetchRequest.includesPropertyValues = false
        fetchRequest.resultType = NSFetchRequestResultType.ManagedObjectResultType
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as? [NSManagedObject]
            if results.count > 0
            {
                for item in results
                {
                    
                    managedObjectContext?.deleteObject(item as! NSManagedObject)
                    
                }
            }else
            {
                
            }
            do {
                try self.managedObjectContext!.save()
            } catch {
                let saveError = error as NSError
                print(saveError)
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
    }
    // MARK: MY LISTINGS DB MANAGEMENT
    
    
    func addMyListings(object:NSDictionary,time:Double){
        let entity = NSEntityDescription.entityForName("MyListings", inManagedObjectContext: managedObjectContext!)
        
        let managedObject = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedObjectContext) as? MyListings
        
        
        managedObject?.information = object
        managedObject?.time = time
        saveContext()
        
        
    }
    func getAllMyListings()-> [AnyObject]?{
        let fetchRequest = NSFetchRequest(entityName: "MyListings")
        fetchRequest.resultType = NSFetchRequestResultType.DictionaryResultType
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as? [NSManagedObject]
            if results.count > 0
            {
                return results
            }else
            {
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }

    }
    // MARK: PRICE DB MANAGEMENT
    func addPrice(object:NSDictionary){
        let entity =  NSEntityDescription.entityForName("Price",
            inManagedObjectContext:managedObjectContext!)
        var managedObject = getPrice(object)
        if managedObject == nil
        {
            managedObject = NSManagedObject(entity: entity!,
                insertIntoManagedObjectContext: managedObjectContext) as? Price
        }
        
        managedObject!.id = object.objectForKey("id") as? String
        managedObject!.category = object.objectForKey("category") as? String
        managedObject!.sub_category = (object.objectForKey("sub_category") as? String)?.stringByReplacingOccurrencesOfString("_", withString: " ")
        managedObject!.country = object.objectForKey("country") as? String
        managedObject!.grade = object.objectForKey("Grade") as? String
        
        if let size = object.objectForKey("size") as? String{
         managedObject!.size =   getHTMlRenderedString(size).string

        }
        managedObject!.price = object.objectForKey("price") as? String
        
        managedObject!.date = object.objectForKey("date") as? String
        managedObject!.delivery = object.objectForKey("delivery") as? String
        managedObject!.change = object.objectForKey("change") as? String
        managedObject!.assesment = object.objectForKey("assesment") as? String
        managedObject!.graphData = object.objectForKey("graph_data") as? NSArray
        managedObject!.sub_id = object.objectForKey("sub_id") as? String
        if let tabsArray = object.objectForKey("tabs") as? NSArray{
            managedObject!.tabs = tabsArray
 
        }
        if let stringTab = object.objectForKey("tabs") as? String{
            let tabsArray = NSArray(object: stringTab)
            managedObject!.tabs = tabsArray

        }
        managedObject!.details = object.objectForKey("details") as? NSDictionary
        managedObject!.graph_id = object.objectForKey("graph_id") as? String
        managedObject!.graph_view = object.objectForKey("graph_view") as? Bool
        managedObject!.top_flag = object.objectForKey("top_flag") as? String

        managedObject!.f_price_column = object.objectForKey("f_price_column") as? String
     
        saveContext()
        
    }
    
    func getPrice(object:NSDictionary)->(Price?){
        let predicate = NSPredicate(format: "id = %@ AND sub_id = %@", argumentArray:[object.objectForKey("id")!,object.objectForKey("sub_id")!])
        let fetchRequest = NSFetchRequest(entityName: "Price")
        fetchRequest.predicate = predicate
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as! [NSManagedObject]
            
            if results.count > 0 {
                return results[0] as? Price
            }else{
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    
    
    func getAllPrice()->([AnyObject]?){
        let fetchRequest = NSFetchRequest(entityName: "Price")
        fetchRequest.resultType = NSFetchRequestResultType.DictionaryResultType
        
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as? [NSManagedObject]
            if results.count > 0
            {
                return results
            }else
            {
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    func getAllPriceId()->([AnyObject]?){
        let fetchRequest = NSFetchRequest(entityName: "Price")
        fetchRequest.resultType = NSFetchRequestResultType.DictionaryResultType
        fetchRequest.propertiesToFetch = ["id"]
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as? [NSManagedObject]
            if results.count > 0
            {
                return results
            }else
            {
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    
    
    
    func addPriceTabWise(object:NSDictionary){
        let entity =  NSEntityDescription.entityForName("PriceTabWise",
            inManagedObjectContext:managedObjectContext!)
        var managedObject = getPriceTabWise(object)
        if managedObject == nil
        {
            managedObject = NSManagedObject(entity: entity!,
                insertIntoManagedObjectContext: managedObjectContext) as? PriceTabWise
        }
        
        managedObject!.category = object.objectForKey("category") as? String
        managedObject!.sub_category = (object.objectForKey("sub_category") as? String)?.stringByReplacingOccurrencesOfString("_", withString: " ")
        managedObject!.country = object.objectForKey("country") as? String
        managedObject!.grade = object.objectForKey("Grade") as? String
        managedObject!.size = object.objectForKey("size") as? String
        managedObject!.price = object.objectForKey("price") as? String
        managedObject!.date = object.objectForKey("date") as? String
        managedObject!.delivery = object.objectForKey("delivery") as? String
        managedObject!.change = object.objectForKey("change") as? String
        managedObject!.assesment = object.objectForKey("assesment") as? String
        managedObject!.graphData = object.objectForKey("graph_data") as? NSArray
        managedObject!.sub_id = object.objectForKey("sub_id") as? String
        managedObject!.tabs = object.objectForKey("tabs") as? NSArray
        managedObject!.details = object.objectForKey("details") as? NSDictionary
        managedObject!.graph_id = object.objectForKey("graph_id") as? String
        managedObject!.graph_view = object.objectForKey("graph_view") as? Bool

//top_flag
        managedObject!.f_price_column = object.objectForKey("f_price_column") as? String
        managedObject!.tab = object.objectForKey("tab") as? String
        managedObject!.id = object.objectForKey("id") as? String
        managedObject!.s_country = object.objectForKey("s_country") as? String
        saveContext()
        
    }
    
    func getPriceTabWise(object:NSDictionary)->(PriceTabWise?){
        let predicate = NSPredicate(format: "sub_id = %@ && tab =[C] %@ && f_price_column = [C]%@ && assesment = [C]%@", argumentArray:[object.objectForKey("sub_id")!,object.objectForKey("tab")!,object.objectForKey("f_price_column")!,object.objectForKey("assesment")!])
        let fetchRequest = NSFetchRequest(entityName: "PriceTabWise")
        fetchRequest.predicate = predicate
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as! [NSManagedObject]
            
            if results.count > 0 {
                return results[0] as? PriceTabWise
            }else{
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    
    
    func getAllPriceTabWise(tab:String)->([AnyObject]?){
        let predicate = NSPredicate(format: "tab =[C] %@", argumentArray: [tab])
        let fetchRequest = NSFetchRequest(entityName: "PriceTabWise")
        fetchRequest.resultType = NSFetchRequestResultType.DictionaryResultType
        fetchRequest.predicate = predicate
        do {
            let results =
            try managedObjectContext!.executeFetchRequest(fetchRequest)
            results as? [NSManagedObject]
            if results.count > 0
            {
                return results
            }else
            {
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
  
    
    // MARK: SAVE CONTEXT
    
    
    
    func saveContext(){
        saveContextPrivate()
//        NSObject.cancelPreviousPerformRequestsWithTarget(self)
//        performSelector("saveContextPrivate", withObject: nil, afterDelay: 0.2)
        
    }
    func saveContextPrivate(){
        
//        print("context saved")
        do {
            try managedObjectContext!.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
  
    //MARK: RESET CORE DATA
    func resetCoreDataEntities(){
        let myManagedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext

        let dict:NSDictionary? = managedObjectModel?.entitiesByName
        if dict?.count>0{
            for entities in dict!.allKeys {
                //clearCoreData(entities as! String)
                let fetchRequest = NSFetchRequest(entityName: entities as! String)
                if #available(iOS 9.0, *) {
                    let delete = NSBatchDeleteRequest(fetchRequest: fetchRequest)
                    do {
                        try GlobalConstants.APPDELEGATE.persistentStoreCoordinator.executeRequest(delete, withContext: myManagedObjectContext)
                    } catch let error as NSError {
                        print("Error occured while deleting: \(error)")
                    }
                } else {
                    // Fallback on earlier versions
                    let entityToDelete = NSFetchRequest()
                    entityToDelete.entity = NSEntityDescription.entityForName(entities as! String, inManagedObjectContext: myManagedObjectContext)
                    entityToDelete.includesPropertyValues = false
                    
                    do {
                        let cars: NSArray = try myManagedObjectContext.executeFetchRequest(entityToDelete)
                        
                        for car in cars {
                            myManagedObjectContext.delete(car)
                        }
                        
                        try myManagedObjectContext.save()
                        
                    } catch let error as NSError {
                        print("Error occured while fetching or saving: \(error)")
                    }
                }
                
            }
        }
    }
        
    
}

