//
//  PriceTabWise+CoreDataProperties.swift
//  Application
//
//  Created by Saurabh Singh on 15/06/16.
//  Copyright © 2016 Saurabh Singh. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension PriceTabWise {

    @NSManaged var assesment: String?
    @NSManaged var category: String?
    @NSManaged var change: String?
    @NSManaged var country: String?
    @NSManaged var date: String?
    @NSManaged var delivery: String?
    @NSManaged var details: NSObject?
    @NSManaged var f_price_column: String?
    @NSManaged var grade: String?
    @NSManaged var graph_id: String?
    @NSManaged var graph_view: NSNumber?
    @NSManaged var graphData: NSObject?
    @NSManaged var id: String?
    @NSManaged var price: String?
    @NSManaged var s_country: String?
    @NSManaged var size: String?
    @NSManaged var sub_category: String?
    @NSManaged var sub_id: String?
    @NSManaged var tab: String?
    @NSManaged var tabs: NSObject?

}
