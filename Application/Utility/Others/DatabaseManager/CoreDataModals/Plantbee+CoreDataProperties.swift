//
//  Plantbee+CoreDataProperties.swift
//  Application
//
//  Created by Ankit Nandal on 12/02/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Plantbee {

    @NSManaged var children: NSObject?
    @NSManaged var desc: String?
    @NSManaged var image: String?
    @NSManaged var name: String?
    @NSManaged var parent_cat_code: String?
    @NSManaged var plant_type_code: String?
    @NSManaged var total_leads: NSNumber?

}
