//
//  MyListings+CoreDataProperties.swift
//  Application
//
//  Created by Ankit Nandal on 20/02/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension MyListings {

    @NSManaged var information: NSObject?
    @NSManaged var time: NSNumber?

}
