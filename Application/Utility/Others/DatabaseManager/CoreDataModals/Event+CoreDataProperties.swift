//
//  Event+CoreDataProperties.swift
//  Application
//
//  Created by Ankit Nandal on 26/02/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Event {

    @NSManaged var agenda: String?
    @NSManaged var enable: NSNumber?
    @NSManaged var id: String?
    @NSManaged var img: String?
    @NSManaged var isBookmarked: NSNumber?
    @NSManaged var location: String?
    @NSManaged var name: String?
    @NSManaged var thumb_img: String?
    @NSManaged var timestamp: String?
    @NSManaged var venue: String?
    @NSManaged var video: String?
    @NSManaged var exhibitors: NSOrderedSet?
    @NSManaged var participants: NSOrderedSet?
    @NSManaged var schedules: NSOrderedSet?
    @NSManaged var speakrs: NSOrderedSet?
    @NSManaged var sponsors: NSOrderedSet?

}
