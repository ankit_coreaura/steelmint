//
//  Participant+CoreDataProperties.swift
//  Application
//
//  Created by Ankit Nandal on 12/02/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Participant {

    @NSManaged var company: String?
    @NSManaged var designation: String?
    @NSManaged var email: String?
    @NSManaged var id: String?
    @NSManaged var img: String?
    @NSManaged var name: String?
    @NSManaged var phoneno: String?
    @NSManaged var region: String?
    @NSManaged var thumb_img: String?
    @NSManaged var event: Event?

}
