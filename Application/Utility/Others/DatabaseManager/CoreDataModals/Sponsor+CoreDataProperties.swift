//
//  Sponsor+CoreDataProperties.swift
//  Application
//
//  Created by Ankit Nandal on 12/02/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Sponsor {

    @NSManaged var event_id: String?
    @NSManaged var id: String?
    @NSManaged var img: String?
    @NSManaged var name: String?
    @NSManaged var region: String?
    @NSManaged var sponser_logo: String?
    @NSManaged var sponsors_type: String?
    @NSManaged var website: String?
    @NSManaged var event: Event?

}
