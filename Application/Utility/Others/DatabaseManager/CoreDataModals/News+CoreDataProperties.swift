//
//  News+CoreDataProperties.swift
//  Application
//
//  Created by Ankit Nandal on 03/03/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension News {

    @NSManaged var author_name: String?
    @NSManaged var category: String?
    @NSManaged var href: String?
    @NSManaged var image: String?
    @NSManaged var isBookmarked: NSNumber?
    @NSManaged var main_category: String?
    @NSManaged var medium_image: String?
    @NSManaged var news_id: String?
    @NSManaged var post_content: String?
    @NSManaged var post_date: NSNumber?
    @NSManaged var post_modified: String?
    @NSManaged var post_title: String?
    @NSManaged var viaSearchOrFilter: NSNumber?

}
