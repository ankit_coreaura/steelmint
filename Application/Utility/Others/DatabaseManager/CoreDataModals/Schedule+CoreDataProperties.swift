//
//  Schedule+CoreDataProperties.swift
//  Application
//
//  Created by Ankit Nandal on 16/02/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Schedule {

    @NSManaged var desc: String?
    @NSManaged var timestamp: NSNumber?
    @NSManaged var title: String?
    @NSManaged var event: Event?

}
