//
//  PlantbeePost+CoreDataProperties.swift
//  Application
//
//  Created by Ankit Nandal on 02/05/16.
//  Copyright © 2016 Alok Singh. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension PlantbeePost {

    @NSManaged var about_company: String?
    @NSManaged var address: String?
    @NSManaged var business_type: String?
    @NSManaged var category: String?
    @NSManaged var city_id: String?
    @NSManaged var city_name: String?
    @NSManaged var company_name: String?
    @NSManaged var contact_person: String?
    @NSManaged var country_id: String?
    @NSManaged var country_name: String?
    @NSManaged var dataLead: NSObject?
    @NSManaged var datetime: NSNumber?
    @NSManaged var designation: String?
    @NSManaged var email_id: String?
    @NSManaged var have_doc: String?
    @NSManaged var have_images: String?
    @NSManaged var id: String?
    @NSManaged var isBookmarked: NSNumber?
    @NSManaged var locality: String?
    @NSManaged var mobile: String?
    @NSManaged var offer_detail: String?
    @NSManaged var parent_code: String?
    @NSManaged var plant_type_code: String?
    @NSManaged var prices_detail: String?
    @NSManaged var state_id: String?
    @NSManaged var state_name: String?
    @NSManaged var status: String?
    @NSManaged var tags_id: String?
    @NSManaged var title: String?
    @NSManaged var type: String?
    @NSManaged var user_personal_detail_id: String?
    @NSManaged var user_status: String?
    @NSManaged var verified: String?
    @NSManaged var latitude: String?
    @NSManaged var longitude: String?

}
