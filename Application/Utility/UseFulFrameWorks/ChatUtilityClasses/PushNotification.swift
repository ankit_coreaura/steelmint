//
//  PushNotification.swift
//  SwiftParseChat
//
//  Created by Jesse Hu on 2/22/15.
//  Copyright (c) 2015 Jesse Hu. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import Parse
import Bolts

class PushNotication {
    
    class func parsePushUserAssign() {
        let installation = PFInstallation.currentInstallation()
        installation[GlobalConstants.PF_INSTALLATION_USER] = PFUser.currentUser()
        installation.saveInBackgroundWithBlock { (succeeded, error) -> Void in
            if error != nil {
                print("parsePushUserAssign save error.")
            }
        }
    }
    
    class func parsePushUserResign() {
        let installation = PFInstallation.currentInstallation()
        installation.removeObjectForKey(GlobalConstants.PF_INSTALLATION_USER)
        installation.saveInBackgroundWithBlock { (succeeded, error) -> Void in
            if error != nil {
                print("parsePushUserResign save error")
            }
        }
    }
    
    class func sendPushNotification(groupId: String, text: String , userObjectID:PFUser,email:String?) {
        
        let installation = PFInstallation.currentInstallation()
        installation["user"] = PFUser.currentUser()
        installation.saveInBackground()
        
        
        let query = PFQuery(className: GlobalConstants.PF_MESSAGES_CLASS_NAME)
        query.whereKey(GlobalConstants.PF_MESSAGES_GROUPID, equalTo: groupId)
        query.whereKey(GlobalConstants.PF_MESSAGES_USER, equalTo: PFUser.currentUser()!)
        query.whereKey(GlobalConstants.PF_MESSAGES_USER, equalTo: userObjectID)
        //query.whereKey("deviceType", equalTo: "ios")
        query.includeKey(GlobalConstants.PF_MESSAGES_USER)
        query.limit = 1000
        
        let installationQuery = PFInstallation.query()
        installationQuery!.whereKey(GlobalConstants.PF_INSTALLATION_USER, matchesKey: GlobalConstants.PF_MESSAGES_USER, inQuery: query)
        
        let push = PFPush()
        push.setQuery(installationQuery)
        push.setMessage(text)
        let data = ["alert": text,"badge":"0","sounds":"default","chat" : "true","emailId" : email ?? ""]
    push.setData(data)
        push.sendPushInBackgroundWithBlock { (succeeded, error) -> Void in
            if error != nil {
                print("sendPushNotification error")
            }
        }
    }
    
}