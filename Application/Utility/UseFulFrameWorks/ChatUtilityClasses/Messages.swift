//
//  Messages.swift
//  SwiftParseChat
//
//  Created by Jesse Hu on 2/21/15.
//  Copyright (c) 2015 Jesse Hu. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import Parse
import Bolts

class Messages {
    
    class func startPrivateChat(user1: PFUser, user2: PFUser) -> String {
        let id1 = user1.objectId
        let id2 = user2.objectId
        let groupId = (id1 < id2) ? "\(id1!)\(id2!)" : "\(id2!)\(id1!)"
        createMessageItem(user1, groupId: groupId, description: user2[GlobalConstants.PF_USER_USERNAME] as! String)
        createMessageItem(user2, groupId: groupId, description: user1[GlobalConstants.PF_USER_USERNAME] as! String)
        return groupId
    }
    
    class func startMultipleChat(users: [PFUser]!) -> String {
        var groupId = ""
        var description = ""
        
        var userIds = [String]()
        
        for user in users {
            userIds.append(user.objectId!)
        }
        
        let sorted = userIds.sort { $0.localizedCaseInsensitiveCompare($1) == NSComparisonResult.OrderedAscending }
        
        for userId in sorted {
            groupId = groupId + userId
        }
        
        for user in users {
            if description.characters.count > 0 {
                description = description + " & "
            }
            description = description + (user[GlobalConstants.PF_USER_USERNAME] as! String)
        }
        
        for user in users {
            Messages.createMessageItem(user, groupId: groupId, description: description)
        }
        
        return groupId
    }
    
    class func createMessageItem(user: PFUser, groupId: String, description: String) {
        let query = PFQuery(className: GlobalConstants.PF_MESSAGES_CLASS_NAME)
        query.whereKey(GlobalConstants.PF_MESSAGES_USER, equalTo: user)
        query.whereKey(GlobalConstants.PF_MESSAGES_GROUPID, equalTo: groupId)
        query.findObjectsInBackgroundWithBlock { (objects, error) -> Void in
            if error == nil {
                if objects!.count == 0 {
                    let message = PFObject(className: GlobalConstants.PF_MESSAGES_CLASS_NAME)
                    message[GlobalConstants.PF_MESSAGES_USER] = user;
                    message[GlobalConstants.PF_MESSAGES_GROUPID] = groupId;
                    message[GlobalConstants.PF_MESSAGES_DESCRIPTION] = description;
                    message[GlobalConstants.PF_MESSAGES_LASTUSER] = PFUser.currentUser()
                    message[GlobalConstants.PF_MESSAGES_LASTMESSAGE] = "";
                    message[GlobalConstants.PF_MESSAGES_COUNTER] = 0
                    message[GlobalConstants.PF_MESSAGES_UPDATEDACTION] = NSDate()
                    message.saveInBackgroundWithBlock({ (succeeded, error) -> Void in
                        if (error != nil) {
                            print("Messages.createMessageItem save error.")
                            print(error)
                        }
                    })
                }
            } else {
                print("Messages.createMessageItem save error.")
                print(error)
            }
        }
    }
}

func deleteMessageItem(message: PFObject) {
    message.deleteInBackgroundWithBlock { (succeeded, error) -> Void in
        if error != nil {
            print("UpdateMessageCounter save error.")
            print(error)
        }
    }
}

func updateMessageCounter(groupId: String, lastMessage: String) {
    let query = PFQuery(className: GlobalConstants.PF_MESSAGES_CLASS_NAME)
    query.whereKey(GlobalConstants.PF_MESSAGES_GROUPID, equalTo: groupId)
    query.limit = 1000
    query.findObjectsInBackgroundWithBlock { (objects, error) -> Void in
        if error == nil {
            for message in objects as [PFObject]! {
                let user = message[GlobalConstants.PF_MESSAGES_USER] as! PFUser
                if user.objectId != PFUser.currentUser()!.objectId {
                    message.incrementKey(GlobalConstants.PF_MESSAGES_COUNTER) // Increment by 1
                    message[GlobalConstants.PF_MESSAGES_LASTUSER] = PFUser.currentUser()
                    message[GlobalConstants.PF_MESSAGES_LASTMESSAGE] = lastMessage
                    message[GlobalConstants.PF_MESSAGES_UPDATEDACTION] = NSDate()
                    message.saveInBackgroundWithBlock({ (succeeded, error) -> Void in
                        if error != nil {
                            print("UpdateMessageCounter save error.")
                            print(error)
                        }
                    })
                }
            }
        } else {
            print("UpdateMessageCounter save error.")
            print(error)
        }
    }
}
 func clearMessageCounter(groupId: String) {
    let query = PFQuery(className: GlobalConstants.PF_MESSAGES_CLASS_NAME)
    query.whereKey(GlobalConstants.PF_MESSAGES_GROUPID, equalTo: groupId)
    query.whereKey(GlobalConstants.PF_MESSAGES_USER, equalTo: PFUser.currentUser()!)
    query.findObjectsInBackgroundWithBlock { (objects, error) -> Void in
        if error == nil {
            for message in objects as [PFObject]! {
                message[GlobalConstants.PF_MESSAGES_COUNTER] = 0
                message.saveInBackgroundWithBlock({ (succeeded, error) -> Void in
                    if error != nil {
                        print("ClearMessageCounter save error.")
                        print(error)
                    }
                })
            }
        } else {
            print("ClearMessageCounter save error.")
            print(error)
        }
    }
}


