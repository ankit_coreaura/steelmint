//
//  StringExtension.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/22/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import Foundation
import UIKit
extension UIImage{
   class func image(image: UIImage, withColor color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(image.size.width, image.size.height), false, image.scale)
        let context = UIGraphicsGetCurrentContext()
        color.set()
        CGContextTranslateCTM(context, 0, image.size.height)
        CGContextScaleCTM(context, 1, -1)
        let rect = CGRectMake(0, 0, image.size.width, image.size.height)
        CGContextClipToMask(context, rect, image.CGImage)
        CGContextFillRect(context, rect)
        let coloredImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return coloredImage
    }
}
extension NSLayoutConstraint {
    class func reportAmbiguity (var v:UIView?) {
    if v == nil {
    v = UIApplication.sharedApplication().keyWindow
    }
    for vv in v!.subviews {
    print("\(vv) \(vv.hasAmbiguousLayout())")
    if vv.subviews.count > 0 {
    self.reportAmbiguity(vv)
    }
    } }
    class func listConstraints (var v:UIView?) {
    if v == nil {
    v = UIApplication.sharedApplication().keyWindow
    }
    for vv in v!.subviews {
    let arr1 = vv.constraintsAffectingLayoutForAxis(.Horizontal)
    let arr2 = vv.constraintsAffectingLayoutForAxis(.Vertical)
    NSLog("\n\n%@\nH: %@\nV:%@", vv, arr1, arr2)
    if vv.subviews.count > 0 {
    self.listConstraints(vv)
    }
    } }
}

extension String {
    var isValidPassword: Bool {
        if characters.count < 6 { return false }
        if rangeOfCharacterFromSet(.letterCharacterSet(), options: .LiteralSearch, range: nil) == nil { return false }
        if rangeOfCharacterFromSet(.decimalDigitCharacterSet(), options: .LiteralSearch, range: nil) == nil { return false }

        return true
    }
    
    var isEmail: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .CaseInsensitive)
            return regex.firstMatchInString(self, options: NSMatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) != nil
        } catch {
            return false
        }
    }
    
    //validate PhoneNumber
    var isPhoneNumber: Bool {
        if  characters.count >= 7 && characters.count<=14 {let charcter  = NSCharacterSet(charactersInString: "+0123456789").invertedSet
            var filtered:NSString!
            let inputString:NSArray = self.componentsSeparatedByCharactersInSet(charcter)
            filtered = inputString.componentsJoinedByString("")
            return  self == filtered }
        else{return false}
        
        
        
    }
    
    static func className(aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).componentsSeparatedByString(".").last!
    }
}

extension String {
    func condense(string:String)-> String {
        let components = self.componentsSeparatedByCharactersInSet(NSCharacterSet(charactersInString: string)).filter({_  in
            return true
        })
        return components.joinWithSeparator("")
    }
    
    func chopPrefix(count: Int = 1) -> String {
        return self.substringFromIndex(self.startIndex.advancedBy(count))
    }
    
    func chopSuffix(count: Int = 1) -> String {
        return self.substringToIndex(self.endIndex.advancedBy(-count))
    }
    
    func enhancedString()->String {
        var string = self
        let pattern = "^\\s+|\\s+$|\\s+(?=\\s)"
        string = string.stringByReplacingOccurrencesOfString(pattern, withString: "", options: .RegularExpressionSearch)
        string = string.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        return string.capitalizedString
    }
    
    func asNSURL()->NSURL {
        return NSURL(string: self.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)!
    }
    
}


extension NSDate {
    func yearsFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Year, fromDate: date, toDate: self, options: []).year
    }
    func monthsFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Month, fromDate: date, toDate: self, options: []).month
    }
    func weeksFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.WeekOfYear, fromDate: date, toDate: self, options: []).weekOfYear
    }
    func daysFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Day, fromDate: date, toDate: self, options: []).day
    }
    func hoursFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Hour, fromDate: date, toDate: self, options: []).hour
    }
    func minutesFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Minute, fromDate: date, toDate: self, options: []).minute
    }
    func secondsFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Second, fromDate: date, toDate: self, options: []).second
    }
    func offsetFrom(date:NSDate) -> String {
        if yearsFrom(date)   > 0 { return "\(yearsFrom(date))y"   }
        if monthsFrom(date)  > 0 { return "\(monthsFrom(date))M"  }
        if weeksFrom(date)   > 0 { return "\(weeksFrom(date))w"   }
        if daysFrom(date)    > 0 { return "\(daysFrom(date))d"    }
        if hoursFrom(date)   > 0 { return "\(hoursFrom(date))h"   }
        if minutesFrom(date) > 0 { return "\(minutesFrom(date))m" }
        if secondsFrom(date) > 0 { return "\(secondsFrom(date))s" }
        return ""
    }
}
